#include <stdlib.h>
#include <stdio.h>
#include <android/bitmap.h>
#include <android/log.h>
#include <webp/decode.h>

#define TAG "image.c"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, TAG, __VA_ARGS__)

jclass class_Bitmap = NULL;
jmethodID class_Bitmap_createBitmap = NULL;
jclass class_Bitmap_Config = NULL;
jfieldID class_Bitmap_Config_ARGB_8888 = NULL;

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved)
{
    JNIEnv *env = NULL;
    if ((*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_6)) {
        LOGE("Failed to get the environment");
        return JNI_ERR;
    }

    class_Bitmap = (*env)->NewGlobalRef(env, (*env)->FindClass(env, "android/graphics/Bitmap"));
    if (!class_Bitmap) {
        LOGE("Failed to load class /android/graphics/Bitmap");
        return JNI_ERR;
    }

    class_Bitmap_createBitmap = (*env)->GetStaticMethodID(env, class_Bitmap, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");
    if (!class_Bitmap_createBitmap) {
        LOGE("Failed to load method /android/graphics/Bitmap.createBitmap");
        return JNI_ERR;
    }

    class_Bitmap_Config = (*env)->NewGlobalRef(env, (*env)->FindClass(env, "android/graphics/Bitmap$Config"));
    if (!class_Bitmap_Config) {
        LOGE("Failed to load class /android/graphics/Bitmap&Config");
        return JNI_ERR;
    }

    class_Bitmap_Config_ARGB_8888 = (*env)->GetStaticFieldID(env, class_Bitmap_Config, "ARGB_8888", "Landroid/graphics/Bitmap$Config;");
    if (!class_Bitmap_Config_ARGB_8888) {
        LOGE("Failed to load field /android/graphics/Bitmap&Config.ARGB_8888");
        return JNI_ERR;
    }

    LOGI("JNI_OnLoad completed");

    return JNI_VERSION_1_6;
}

JNIEXPORT jobject JNICALL Java_org_tdlib_ippogram_Utils_doBlur(JNIEnv *env, jclass class, jobject sentBitmap, jint radius, jboolean canReuseInBitmap)
{
    /*if (!sentBitmap) {
        return NULL;
    }

    AndroidBitmapInfo info;
    if (AndroidBitmap_getInfo(env, sentBitmap, &info) < 0) {
        return NULL;
    }
    void *pixels = NULL;
    if (AndroidBitmap_lockPixels(env, sentBitmap, pixels) < 0) {
        return NULL;
    }*/
}

JNIEXPORT jobject JNICALL Java_org_tdlib_ippogram_Utils_decodeWebP(JNIEnv *env, jclass type, jbyteArray jencoded) {
    int width = 0;
    int height = 0;
    void *pixels = NULL;
    int ret;
    AndroidBitmapInfo info;

    jbyte *encoded = (*env)->GetByteArrayElements(env, jencoded, NULL);
    jsize encodedLength = (*env)->GetArrayLength(env, jencoded);

    ret = WebPGetInfo((const uint8_t *)encoded, (size_t)encodedLength, &width, &height);
    if (!ret) {
        LOGE("Invalid WebP format");
        return NULL;
    }

    jobject Config_ARGB_8888 = (*env)->GetStaticObjectField(env, class_Bitmap_Config, class_Bitmap_Config_ARGB_8888);
    jobject outBitmap = (*env)->CallStaticObjectMethod(env, class_Bitmap, class_Bitmap_createBitmap, (jint)width, (jint)height, Config_ARGB_8888);

    if (!outBitmap) {
        (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);
        LOGE("Failed to allocate Bitmap");
        return NULL;
    }

    outBitmap = (*env)->NewLocalRef(env, outBitmap);

    ret = AndroidBitmap_getInfo(env, outBitmap, &info);
    if (ret != ANDROID_BITMAP_RESULT_SUCCESS) {
        (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);
        (*env)->DeleteLocalRef(env, outBitmap);
        LOGE("Failed to get bitmap info: %d", ret);
        return NULL;
    }

    if (info.format != ANDROID_BITMAP_FORMAT_RGBA_8888) {
        (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);
        (*env)->DeleteLocalRef(env, outBitmap);
        LOGE("Bitmap format is not RGBA_8888!");
        return NULL;
    }

    ret = AndroidBitmap_lockPixels(env, outBitmap, &pixels);
    if (ret != ANDROID_BITMAP_RESULT_SUCCESS) {
        (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);
        (*env)->DeleteLocalRef(env, outBitmap);
        LOGE("Failed to lock Bitmap pixels: %d", ret);
        return NULL;
    }


    if (!WebPDecodeRGBAInto((uint8_t *)encoded, (size_t)encodedLength, (uint8_t *)pixels, info.height * info.stride, info.stride)) {
        AndroidBitmap_unlockPixels(env, outBitmap);
        (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);
        (*env)->DeleteLocalRef(env, outBitmap);
        LOGE("Failed to lock decode webp");
        return NULL;
    }

    AndroidBitmap_unlockPixels(env, outBitmap);

    (*env)->ReleaseByteArrayElements(env, jencoded, encoded, JNI_ABORT);

    return outBitmap;
}