#include <jni.h>
#include <opusfile.h>
#include <android/log.h>
#include <stdlib.h>

char logMsg[255];
OggOpusFile *opusFile;

JNIEXPORT jlong JNICALL Java_org_tdlib_ippogram_Utils_openOggFile(JNIEnv *env, jclass class, jstring path)
{
    int opus_error = 0;
    const jbyte *cPath = (*env)->GetStringUTFChars(env, path, NULL);
    opusFile = op_open_file(cPath, &opus_error);
    if (opusFile == NULL) {
        sprintf(logMsg, "Failed to open file '%s': %i\n", cPath, opus_error);
        __android_log_write(ANDROID_LOG_ERROR, "Voice", logMsg);
        return -1;
    }
    if (cPath != NULL) {
        (*env)->ReleaseStringUTFChars(env, path, cPath);
    }
    return op_pcm_total(opusFile, -1);
    /*duration = 0;
    if (op_seekable(opusFile)) {
        sprintf(logMsg, "Total number of links: %i", op_link_count(opusFile));
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        sprintf(logMsg, "(%li samples @ 48 kHz)", (long)op_pcm_total(opusFile, -1));
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        sprintf(logMsg, "Total size: %lli", op_raw_total(opusFile, -1));
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        sprintf(logMsg, "Channel count: %i", op_channel_count(opusFile, -1));
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        sprintf(logMsg, "Bitrate: %i", op_bitrate(opusFile, -1));
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
    }

    ogg_int64_t pcm_offset;
    ogg_int64_t pcm_print_offset;
    ogg_int64_t nsamples;
    opus_int32  bitrate;
    int         prev_li;
    prev_li = -1;
    nsamples = 0;
    pcm_offset = op_pcm_tell(opusFile);
    if (pcm_offset != NULL) {
        sprintf(logMsg,"Non-zero starting PCM offset: %li",(long)pcm_offset);
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
    }
    pcm_print_offset = pcm_offset - 48000;
    bitrate = 0;
    for (;;) {
        ogg_int64_t   next_pcm_offset;
        opus_int16    pcm[120 * 48 * 2];
        unsigned char out[120 * 48 * 2 * 2];
        int           li;
        int           si;
        opus_error = op_read_stereo(opusFile, pcm, sizeof(pcm) / sizeof(*pcm));
        if (opus_error < NULL) {
            sprintf(logMsg, "Error decoding '%s': %i", cPath, opus_error);
            __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
            break;
        }
        li = op_current_link(opusFile);
        if (li != prev_li) {
            const OpusHead *head;
            int             ci;
            //We found a new link.
              //Print out some information.
            sprintf(logMsg, "Decoding link %i:                          \n", li);
            __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
            head = op_head(opusFile, li);
            sprintf(logMsg, "  Channels: %i", head->channel_count);
            __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
            if (op_seekable(opusFile)) {
                sprintf(logMsg, "(%li samples @ 48 kHz)", (long)op_pcm_total(opusFile, -1));
                __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
                sprintf(logMsg, "Size: %lli", op_raw_total(opusFile, -1));
                __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
                if (head->input_sample_rate){
                    sprintf(logMsg, "  Original sampling rate: %lu Hz", (unsigned long)head->input_sample_rate);
                    __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
                }
            }
            if (!op_seekable(opusFile)){
                pcm_offset = op_pcm_tell(opusFile) - opus_error;
                if (pcm_offset != 0){
                    sprintf(logMsg,"Non-zero starting PCM offset in link %i: %li", li,(long)pcm_offset);
                    __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
                }
            }
        }
        if (li != prev_li || pcm_offset >= pcm_print_offset + 48000){
            opus_int32 next_bitrate;
            opus_int64 raw_offset;
            next_bitrate = op_bitrate_instant(opusFile);
            if (next_bitrate >= 0) {
                bitrate = next_bitrate;
            }
            raw_offset = op_raw_tell(opusFile);
            pcm_print_offset = pcm_offset;
        }
        next_pcm_offset = op_pcm_tell(opusFile);
        if (pcm_offset + opus_error != next_pcm_offset) {
            sprintf(logMsg,"PCM offset gap! %li+%i!=%li", (long)pcm_offset, opus_error, (long)next_pcm_offset);
            __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        }
        pcm_offset = next_pcm_offset;
        if (opus_error <= 0) {
            opus_error = EXIT_SUCCESS;
            break;
        }
        //Ensure the data is little-endian before writing it out.
        for (si = 0; si < 2 * opus_error; si++) {
            out[2 * si + 0] = (unsigned char)(pcm[si] & 0xFF);
            out[2 * si + 1] = (unsigned char)(pcm[si] >> 8 & 0xFF);
        }
        nsamples += opus_error;
        prev_li = li;
    }
    if (opus_error == EXIT_SUCCESS) {
        sprintf(logMsg, "Done: played ");
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
        //print_duration(stderr,nsamples,3);
        sprintf(logMsg, " (%li samples @ 48 kHz).\n",(long)nsamples);
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
    }
    if (op_seekable(opusFile) && nsamples != duration) {
        sprintf(logMsg,"WARNING: ""Number of output samples does not match declared file duration.");
        __android_log_write(ANDROID_LOG_DEBUG, "Voice", logMsg);
    }
    op_free(opusFile);
    if (cPath != NULL) {
        (*env)->ReleaseStringUTFChars(env, path, cPath);
    }*/
}

JNIEXPORT jlong JNICALL Java_org_tdlib_ippogram_Utils_readOggFile(JNIEnv *env, jclass class, jshortArray out)
{
    if (opusFile && op_seekable(opusFile)) {
        ogg_int64_t readSamplesCount = 0;
        opus_int16 *outArray = (*env)->GetShortArrayElements(env, out, NULL);
        ogg_int64_t samplesCount = op_pcm_total(opusFile, -1);
        while (readSamplesCount < samplesCount) {
            readSamplesCount += op_read(opusFile, outArray + readSamplesCount, (*env)->GetArrayLength(env, out), NULL);
        }
        if (outArray != NULL) {
            (*env)->ReleaseShortArrayElements(env, out, outArray, 0);
        }
        return readSamplesCount;
    }
    return -1;
}