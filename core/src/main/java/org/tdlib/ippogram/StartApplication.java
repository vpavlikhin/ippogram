package org.tdlib.ippogram;

import android.app.Application;
import android.os.Environment;
import android.util.Log;

import org.drinkless.td.libcore.telegram.TG;
import org.tdlib.ippogram.ui.R;

import java.io.File;

public class StartApplication extends Application {

    private ChatsCache mChatsCache;
    private String mCacheDir = "/tdlib/";

    static {
        try {
            System.loadLibrary("ippogram");
        } catch (Exception e) {
            Log.e("Ippogram", "Couldn't load library");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Utils.sContext = getApplicationContext();
        Utils.TAG = Utils.sContext.getString(R.string.app_name);
        Utils.sDensity = Utils.sContext.getResources().getDisplayMetrics().density;
        //Utils.sLocalBroadcastManager = LocalBroadcastManager.getInstance(this);

        mChatsCache = new ChatsCache();

        Utils.sUpdatesHandler = new UpdatesHandler();

        String rootPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        File folder = new File(rootPath + mCacheDir);
        if (!folder.exists()) {
            folder.mkdir();
        }
        TG.setDir(folder.getAbsolutePath());
        TG.setUpdatesHandler(Utils.sUpdatesHandler);
        TG.setLogVerbosity(2);
        Utils.sClient = TG.getClientInstance();
    }
}
