package org.tdlib.ippogram;

import android.content.Context;
import android.os.Vibrator;
import android.util.Log;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;

import java.util.ArrayList;
import java.util.HashMap;

public class UpdatesHandler implements Client.ResultHandler {

    //for all
    public interface UpdatesDelegate {
        void updateInterface();
    }

    //for chats, a chat, profiles
    public interface NotifyUpdatesDelegate extends UpdatesDelegate {
        void updateNotifySettings();
    }

    //for contacts, user profile, group create
    public interface UpdatesForContactsDelegate extends UpdatesDelegate {
        void updateContactsList();
    }

    //for chats, a chat
    public interface MessageUpdatesDelegate extends NotifyUpdatesDelegate {
        void updateMessageId(long chatId, int oldId, int newId);
        void updateMessageDate(long chatId, int messageId, int newDate);
        void updateMessageViews(long chatId, int messageId, int views);
        void updateListsWithEmoji();
        //void updateDownloadedFile(int fileId);
    }

    //for a chat, group profile
    public interface GroupUpdatesDelegate extends NotifyUpdatesDelegate {
        void updateChatParts(int groupId, TdApi.GroupFull groupFull);
    }

    //for a chat, user profile
    public interface UserUpdatesDelegate extends NotifyUpdatesDelegate {
        void updateUserInfo(int userId, boolean blocked, TdApi.BotInfo botInfo);
    }

    //for a chat, channel profile
    public interface ChannelUpdatesDelegate extends NotifyUpdatesDelegate {
        void updateChannelInfo(int channelId, TdApi.ChannelFull channelFull);
    }

    //for chats
    public interface ListUpdatesDelegate extends MessageUpdatesDelegate {
        void updateChatsList();
    }

    //for a chat
    public interface UpdatesForChatDelegate extends MessageUpdatesDelegate {
        void updateReadMessages();
        void removeDeletedMessages(ArrayList<Integer> deletedMessages);
        void addNewMessages(long chatId, ArrayList<TdApi.Message> messages);
        void updateMessageContent(long chatId, int messageId, TdApi.MessageContent newContent);
        //void updateDownloadProgress(int fileId, int ready, int size);
    }

    public interface DownloadUpdatesDelegate extends UpdatesDelegate {
        void updateDownloadProgress(int fileId, int ready, int size);
        void updateDownloadedFile(int fileId);
    }

    private long mLastUpdateTime = 0;

    @Override
    public void onResult(TdApi.TLObject object) {
        Log.e(Utils.TAG + " updates", "" + object);

        final HashMap<Long, HashMap<Integer, Integer>> updatedMessages = new HashMap<>();
        final HashMap<Long, HashMap<Integer, Integer>> updatedMessagesDate = new HashMap<>();
        final HashMap<Long, HashMap<Integer, TdApi.MessageContent>> updatedMessagesContent = new HashMap<>();
        final HashMap<Long, HashMap<Integer, Integer>> updatedMessagesViews = new HashMap<>();
        final ArrayList<TdApi.Update> updatesUser = new ArrayList<>();
        final ArrayList<TdApi.Update> updatesGroup = new ArrayList<>();
        final ArrayList<TdApi.Update> updatesChannel = new ArrayList<>();
        final ArrayList<TdApi.Update> updatesChat = new ArrayList<>();
        final HashMap<Long, ArrayList<Integer>> deletedMessages = new HashMap<>();
        final HashMap<Long, Integer> lastReadOutboxMessages = new HashMap<>();
        final ArrayList<TdApi.Update> lastReadInboxMessages = new ArrayList<>();
        final HashMap<Long, String> updatedChatTitle = new HashMap<>();
        final HashMap<Long, TdApi.ChatPhoto> updatedChatPhoto = new HashMap<>();
        final HashMap<Long, Long> updatedChatOrder = new HashMap<>();
        final HashMap<Long, ArrayList<TdApi.Message>> assignMessages = new HashMap<>();
        final ArrayList<TdApi.UpdateFile> updatedFiles = new ArrayList<>();
        TdApi.Message lastMessage = null;

        boolean downloadingFile = false;

        //ConcurrentHashMap<Integer, TdApi.User> users = ChatsCache.sUsers;
        //ConcurrentHashMap<Integer, TdApi.GroupChat> groupChats = ChatsCache.sGroups;

        switch (object.getConstructor()) {
            case TdApi.UpdateOption.CONSTRUCTOR:
                final TdApi.UpdateOption updateOption = (TdApi.UpdateOption)object;
                Utils.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        if (updateOption.name.contains("connection_state")) {
                            TdApi.OptionString state = (TdApi.OptionString) updateOption.value;
                            Utils.sOptionReceived = true;
                            switch (state.value) {
                                case "Connecting":
//                                    if (!Utils.isNetworkExist()) {
//                                        Utils.sCurrentConnectionState = 1;
//                                    } else {
//                                        Utils.sCurrentConnectionState = 2;
//                                    }
                                    Utils.sCurrentConnectionState = 2;
                                    break;
                                case "Updating":
                                    Utils.sCurrentConnectionState = 3;
                                    break;
                                case "Ready":
                                    Utils.sCurrentConnectionState = 0;
                                    break;
                            }
                        } else if (updateOption.name.contains("my_id")) {
                            if (updateOption.value.getConstructor() != TdApi.OptionEmpty.CONSTRUCTOR) {
                                Utils.sCurrentUserId = ((TdApi.OptionInteger) updateOption.value).value;
                            }
                        }
                    }
                });
                break;
            case TdApi.UpdateUser.CONSTRUCTOR:
                updatesUser.add((TdApi.Update)object);
                break;
            case TdApi.UpdateUserStatus.CONSTRUCTOR:
                updatesUser.add((TdApi.Update)object);
                break;
            case TdApi.UpdateUserAction.CONSTRUCTOR:
                updatesUser.add((TdApi.Update)object);
                break;
            case TdApi.UpdateUserBlocked.CONSTRUCTOR:
                updatesUser.add((TdApi.Update)object);
                break;
            case TdApi.UpdateNotificationSettings.CONSTRUCTOR:
                updatesUser.add((TdApi.Update)object);
                break;
            case TdApi.UpdateStickers.CONSTRUCTOR:
                break;
            case TdApi.UpdateNewMessage.CONSTRUCTOR:
                TdApi.UpdateNewMessage unm = (TdApi.UpdateNewMessage)object;
                long chatId = unm.message.chatId;
                ArrayList<TdApi.Message> messagesArr = assignMessages.get(chatId);
                if (messagesArr == null) {
                    messagesArr = new ArrayList<>();
                    assignMessages.put(chatId, messagesArr);
                }
                messagesArr.add(unm.message);
                if (unm.message.fromId != Utils.sCurrentUserId && unm.message.chatId != 0) {
                    if (chatId != ChatsCache.sOpenChatId) {
                        lastMessage = unm.message;
                    }
                }
                break;
            case TdApi.UpdateNewAuthorization.CONSTRUCTOR:
                break;
            case TdApi.UpdateMessageId.CONSTRUCTOR:
                TdApi.UpdateMessageId updateMessageId = (TdApi.UpdateMessageId)object;
                HashMap<Integer, Integer> idPair = updatedMessages.get(updateMessageId.chatId);
                if (idPair == null) {
                    idPair = new HashMap<>();
                    updatedMessages.put(updateMessageId.chatId,  idPair);
                }
                idPair.put(updateMessageId.oldId, updateMessageId.newId);
                break;
            case TdApi.UpdateMessageDate.CONSTRUCTOR:
                TdApi.UpdateMessageDate updateMessageDate = (TdApi.UpdateMessageDate)object;
                HashMap<Integer, Integer> datePair = updatedMessagesDate.get(updateMessageDate.chatId);
                if (datePair == null) {
                    datePair = new HashMap<>();
                    updatedMessagesDate.put(updateMessageDate.chatId, datePair);
                }
                datePair.put(updateMessageDate.messageId, updateMessageDate.newDate);
                break;
            case TdApi.UpdateMessageContent.CONSTRUCTOR:
                TdApi.UpdateMessageContent updateMessageContent = (TdApi.UpdateMessageContent)object;
                HashMap<Integer, TdApi.MessageContent> content = updatedMessagesContent.get(updateMessageContent.chatId);
                if (content == null) {
                    content = new HashMap<>();
                    updatedMessagesContent.put(updateMessageContent.chatId, content);
                }
                content.put(updateMessageContent.messageId, updateMessageContent.newContent);
                break;
            case TdApi.UpdateMessageViews.CONSTRUCTOR:
                TdApi.UpdateMessageViews updateMessageViews = (TdApi.UpdateMessageViews)object;
                HashMap<Integer, Integer> views = updatedMessagesViews.get(updateMessageViews.chatId);
                if (views == null) {
                    views = new HashMap<>();
                    updatedMessagesViews.put(updateMessageViews.chatId, views);
                }
                views.put(updateMessageViews.messageId, updateMessageViews.views);
                break;
            case TdApi.UpdateFileProgress.CONSTRUCTOR:
                final TdApi.UpdateFileProgress updateFileProgress = (TdApi.UpdateFileProgress)object;
                downloadingFile = true;
                Utils.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        /*Intent intent = new Intent();
                        intent.setAction(Utils.ACTION_PROGRESS_UPDATE);
                        intent.putExtra("file_id", updateFileProgress.fileId);
                        intent.putExtra("ready", updateFileProgress.ready);
                        intent.putExtra("size", updateFileProgress.size);
                        Utils.sLocalBroadcastManager.sendBroadcast(intent);*/
                        ChatsCache.updateDownloadProgress(updateFileProgress.fileId, updateFileProgress.ready, updateFileProgress.size);
                        Log.e(Utils.TAG, "UpdateFileProgress: " + updateFileProgress);
                    }
                });
                break;
            case TdApi.UpdateFile.CONSTRUCTOR:
                TdApi.UpdateFile updateFile = (TdApi.UpdateFile)object;
                updatedFiles.add(updateFile);
                Log.e(Utils.TAG, "UpdateFile: " + updateFile);
                break;
            case TdApi.UpdateChatReadInbox.CONSTRUCTOR:
                lastReadInboxMessages.add((TdApi.UpdateChatReadInbox) object);
                break;
            case TdApi.UpdateChatReadOutbox.CONSTRUCTOR:
                TdApi.UpdateChatReadOutbox readOutbox = (TdApi.UpdateChatReadOutbox)object;
                lastReadOutboxMessages.put(readOutbox.chatId, readOutbox.lastReadOutboxMessageId);
                break;
            case TdApi.UpdateChatTitle.CONSTRUCTOR:
                TdApi.UpdateChatTitle updateChatTitle = (TdApi.UpdateChatTitle)object;
                updatedChatTitle.put(updateChatTitle.chatId, updateChatTitle.title);
                break;
            case TdApi.UpdateChatPhoto.CONSTRUCTOR:
                TdApi.UpdateChatPhoto updateChatPhoto = (TdApi.UpdateChatPhoto)object;
                updatedChatPhoto.put(updateChatPhoto.chatId, updateChatPhoto.photo);
                break;
            case TdApi.UpdateChatOrder.CONSTRUCTOR:
                TdApi.UpdateChatOrder updateChatOrder = (TdApi.UpdateChatOrder)object;
                updatedChatOrder.put(updateChatOrder.chatId, updateChatOrder.order);
                break;
            case TdApi.UpdateGroup.CONSTRUCTOR:
                updatesGroup.add((TdApi.Update) object);
                break;
            case TdApi.UpdateChannel.CONSTRUCTOR:
                updatesChannel.add((TdApi.Update)object);
                break;
            case TdApi.UpdateChat.CONSTRUCTOR:
                updatesChat.add((TdApi.Update)object);
                break;
            case TdApi.UpdateDeleteMessages.CONSTRUCTOR:
                TdApi.UpdateDeleteMessages updateDeleteMessages = (TdApi.UpdateDeleteMessages)object;
                ArrayList<Integer> messageIds = deletedMessages.get(updateDeleteMessages.chatId);
                if (messageIds == null) {
                    messageIds = new ArrayList<>();
                    deletedMessages.put(updateDeleteMessages.chatId, messageIds);
                }
                for (int ids : updateDeleteMessages.messageIds) {
                    messageIds.add(ids);
                }
                break;
            default:
                Log.e(Utils.TAG + " updates", "Unknown update: " + object);
        }

        final TdApi.Message lastMessageArg = lastMessage;
        final boolean downloadingFileArg = downloadingFile;
        if (!updatedMessages.isEmpty() || !updatedMessagesDate.isEmpty() || !updatedMessagesContent.isEmpty() || !updatedMessagesViews.isEmpty()) {
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    if (!updatedMessagesDate.isEmpty()) {
                        for (HashMap.Entry<Long, HashMap<Integer, Integer>> entry : updatedMessagesDate.entrySet()) {
                            for (HashMap.Entry<Integer, Integer> entryIn : entry.getValue().entrySet()) {
                                Integer id = entryIn.getKey();
                                Integer newDate = entryIn.getValue();
                                ChatsCache.updateMessageDate(entry.getKey(), id, newDate);
                            }
                        }
                    }

                    if (!updatedMessages.isEmpty()) {
                        for (HashMap.Entry<Long, HashMap<Integer, Integer>> entry : updatedMessages.entrySet()) {
                            for (HashMap.Entry<Integer, Integer> entryIn : entry.getValue().entrySet()) {
                                Integer oldId = entryIn.getKey();
                                Integer newId = entryIn.getValue();
                                ChatsCache.updateMessageId(entry.getKey(), oldId, newId);
                            }
                        }
                    }

                    if (!updatedMessagesContent.isEmpty()) {
                        for (HashMap.Entry<Long, HashMap<Integer, TdApi.MessageContent>> entry : updatedMessagesContent.entrySet()) {
                            for (HashMap.Entry<Integer, TdApi.MessageContent> entryIn : entry.getValue().entrySet()) {
                                Integer id = entryIn.getKey();
                                TdApi.MessageContent newContent = entryIn.getValue();
                                ChatsCache.updateMessageContent(entry.getKey(), id, newContent);
                            }
                        }
                    }

                    if (!updatedMessagesViews.isEmpty()) {
                        for (HashMap.Entry<Long, HashMap<Integer, Integer>> entry : updatedMessagesViews.entrySet()) {
                            for (HashMap.Entry<Integer, Integer> entryIn : entry.getValue().entrySet()) {
                                Integer id = entryIn.getKey();
                                Integer views = entryIn.getValue();
                                ChatsCache.updateMessageViews(entry.getKey(), id, views);
                            }
                        }
                    }
                }
            });
        }

        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                if (!updatesUser.isEmpty()) {
                    for (TdApi.Update update : updatesUser) {
                        if (update.getConstructor() == TdApi.UpdateUserStatus.CONSTRUCTOR) {
                            TdApi.User currentUser = ChatsCache.getUser(((TdApi.UpdateUserStatus) update).userId);
                            if (currentUser != null) {
                                currentUser.status = ((TdApi.UpdateUserStatus) update).status;
                            }
                        } else if (update.getConstructor() == TdApi.UpdateUser.CONSTRUCTOR) {
                            TdApi.UpdateUser updateUser = (TdApi.UpdateUser) update;
                            //Log.e(Utils.TAG, "updateUser " + updateUser.user);
                            TdApi.User currentUser = ChatsCache.sUsers.get(updateUser.user.id);
                            if (currentUser == null) {
                                currentUser = updateUser.user;
                                ChatsCache.sUsers.put(currentUser.id, currentUser);
                            } else {
                                currentUser.firstName = updateUser.user.firstName;
                                currentUser.lastName = updateUser.user.lastName;
                                currentUser.username = updateUser.user.username;
                                currentUser.phoneNumber = updateUser.user.phoneNumber;
                                currentUser.profilePhoto = updateUser.user.profilePhoto;
                                if (currentUser.myLink != updateUser.user.myLink) {
                                    ContactsCache.getInstance().loadContacts();
                                    currentUser.myLink = updateUser.user.myLink;
                                }
                                currentUser.foreignLink = updateUser.user.foreignLink;
                                currentUser.type = updateUser.user.type;
                            }
                        } else if (update.getConstructor() == TdApi.UpdateUserAction.CONSTRUCTOR) {
                            TdApi.UpdateUserAction userAction = (TdApi.UpdateUserAction)update;
                            ChatsCache.updateUserAction(userAction.chatId, userAction.userId, userAction.action);
                        } else if (update.getConstructor() == TdApi.UpdateUserBlocked.CONSTRUCTOR) {
                            TdApi.UpdateUserBlocked userBlocked = (TdApi.UpdateUserBlocked) update;
                            ChatsCache.updateUserInfo(userBlocked.userId, userBlocked.isBlocked, null);
                        } else if (update.getConstructor() == TdApi.UpdateNotificationSettings.CONSTRUCTOR) {
                            TdApi.UpdateNotificationSettings notifySettings = (TdApi.UpdateNotificationSettings) update;
                            if (notifySettings.scope.getConstructor() == TdApi.NotificationSettingsForChat.CONSTRUCTOR) {
                                TdApi.NotificationSettingsForChat notifyForChat = (TdApi.NotificationSettingsForChat) notifySettings.scope;
                                final TdApi.Chat chat = ChatsCache.sChats.get(notifyForChat.chatId);
                                if (chat != null) {
                                    chat.notificationSettings = notifySettings.notificationSettings;
                                    if (notifySettings.notificationSettings.muteFor > 0) {
                                        if (notifySettings.notificationSettings.muteFor > 60 * 60 * 24 * 365) {
                                            chat.notificationSettings.muteFor = Integer.MAX_VALUE;
                                        } else {
                                            chat.notificationSettings.muteFor = notifySettings.notificationSettings.muteFor;
                                        }
                                        Runnable muteRunnable = new Runnable() {
                                            @Override
                                            public void run() {
                                                chat.notificationSettings.muteFor = 0;
                                                ChatsCache.sChatsMuteRunnable.remove(chat.id);
                                                ChatsCache.updateNotifySettings();
                                            }
                                        };
                                        ChatsCache.sChatsMuteRunnable.put(chat.id, muteRunnable);
                                        Utils.runOnUIThread(muteRunnable, chat.notificationSettings.muteFor * 1000L);
                                    } else {
                                        chat.notificationSettings.muteFor = 0;
                                        Utils.cancelRunOnUIThread(ChatsCache.sChatsMuteRunnable.get(chat.id));
                                        ChatsCache.sChatsMuteRunnable.remove(chat.id);
                                    }
                                }
                            }
                        }
                    }
                    ChatsCache.updateNotifySettings();
                }

                if (!updatesChat.isEmpty()) {
                    for (TdApi.Update update : updatesChat) {
                        if (update.getConstructor() == TdApi.UpdateChat.CONSTRUCTOR) {
                            TdApi.UpdateChat updateChat = (TdApi.UpdateChat) update;
                            //Log.e(Utils.TAG, "updateChat " + updateChat.chat);
                            TdApi.Chat currentChat = ChatsCache.sChats.get(updateChat.chat.id);
                            if (currentChat == null) {
                                currentChat = updateChat.chat;
                                ChatsCache.sChats.put(currentChat.id, currentChat);
                            } else {
                                currentChat.lastReadInboxMessageId = updateChat.chat.lastReadInboxMessageId;
                                currentChat.lastReadOutboxMessageId = updateChat.chat.lastReadOutboxMessageId;
                                currentChat.notificationSettings = updateChat.chat.notificationSettings;
                                currentChat.order = updateChat.chat.order;
                                currentChat.photo = updateChat.chat.photo;
                                currentChat.replyMarkupMessageId = updateChat.chat.replyMarkupMessageId;
                                currentChat.title = updateChat.chat.title;
                                currentChat.topMessage = updateChat.chat.topMessage;
                                currentChat.type = updateChat.chat.type;
                                currentChat.unreadCount = updateChat.chat.unreadCount;
                            }
                        }
                    }
                }

                if (!updatesGroup.isEmpty()) {
                    for (TdApi.Update update : updatesGroup) {
                        if (update.getConstructor() == TdApi.UpdateGroup.CONSTRUCTOR) {
                            TdApi.UpdateGroup updateGroup = (TdApi.UpdateGroup) update;
                            //Log.e(Utils.TAG, "updateGroup " + updateGroup.group);
                            TdApi.Group currentGroup = ChatsCache.sGroups.get(updateGroup.group.id);
                            if (currentGroup == null) {
                                currentGroup = updateGroup.group;
                                ChatsCache.sGroups.put(currentGroup.id, currentGroup);
                            } else {
                                currentGroup.anyoneCanEdit = updateGroup.group.anyoneCanEdit;
                                currentGroup.role = updateGroup.group.role;
                                currentGroup.isActive = updateGroup.group.isActive;
                                currentGroup.migratedToChannelId = updateGroup.group.migratedToChannelId;
                                currentGroup.participantsCount = updateGroup.group.participantsCount;
                            }
                        }
                    }
                }

                if (!updatesChannel.isEmpty()) {
                    for (TdApi.Update update : updatesChannel) {
                        if (update.getConstructor() == TdApi.UpdateChannel.CONSTRUCTOR) {
                            TdApi.UpdateChannel updateChannel = (TdApi.UpdateChannel) update;
                            //Log.e(Utils.TAG, "updateChannel " + updateChannel.channel);
                            TdApi.Channel currentChannel = ChatsCache.sChannels.get(updateChannel.channel.id);
                            if (currentChannel == null) {
                                currentChannel = updateChannel.channel;
                                ChatsCache.sChannels.put(currentChannel.id, currentChannel);
                            } else {
                                currentChannel.date = updateChannel.channel.date;
                                currentChannel.role = updateChannel.channel.role;
                                currentChannel.isBroadcast = updateChannel.channel.isBroadcast;
                                currentChannel.isSupergroup = updateChannel.channel.isSupergroup;
                                currentChannel.isVerified = updateChannel.channel.isVerified;
                                currentChannel.restrictionReason = updateChannel.channel.restrictionReason;
                                currentChannel.username = updateChannel.channel.username;
                            }
                        }
                    }
                }

                if (!lastReadInboxMessages.isEmpty()) {
                    for (TdApi.Update update : lastReadInboxMessages) {
                        TdApi.UpdateChatReadInbox readInbox = (TdApi.UpdateChatReadInbox) update;
                        TdApi.Chat chat = ChatsCache.sChats.get(readInbox.chatId);
                        if (chat != null) {
                            chat.lastReadInboxMessageId = readInbox.lastReadInboxMessageId;
                            chat.unreadCount = readInbox.unreadCount;
                        }
                    }
                }

                if (!lastReadOutboxMessages.isEmpty()) {
                    for (HashMap.Entry<Long, Integer> entry : lastReadOutboxMessages.entrySet()) {
                        Long chatId = entry.getKey();
                        TdApi.Chat chat = ChatsCache.sChats.get(chatId);
                        if (chat != null) {
                            chat.lastReadOutboxMessageId = entry.getValue();
                        }
                        ChatsCache.updateReadMessages();
                    }
                }

                if (!assignMessages.isEmpty()) {
                    for (HashMap.Entry<Long, ArrayList<TdApi.Message>> entry : assignMessages.entrySet()) {
                        Long key = entry.getKey();
                        ArrayList<TdApi.Message> value = entry.getValue();
                        ChatsCache.updateListsWithMessages(key, value);
                    }
                }

                if (!updatedChatTitle.isEmpty()) {
                    for (HashMap.Entry<Long, String> entry : updatedChatTitle.entrySet()) {
                        Long chatId = entry.getKey();
                        String title = entry.getValue();
                        ChatsCache.updateChatTitle(chatId, title);
                    }
                }

                if (!updatedChatPhoto.isEmpty()) {
                    for (HashMap.Entry<Long, TdApi.ChatPhoto> entry : updatedChatPhoto.entrySet()) {
                        Long chatId = entry.getKey();
                        TdApi.ChatPhoto photo = entry.getValue();
                        ChatsCache.updateChatPhoto(chatId, photo);
                    }
                }

                if (!updatedChatOrder.isEmpty()) {
                    for (HashMap.Entry<Long, Long> entry : updatedChatOrder.entrySet()) {
                        Long chatId = entry.getKey();
                        Long order = entry.getValue();
                        ChatsCache.updateChatOrder(chatId, order);
                    }
                }

                if (!deletedMessages.isEmpty()) {
                    for (HashMap.Entry<Long, ArrayList<Integer>> entry : deletedMessages.entrySet()) {
                        Long chatId = entry.getKey();
                        ArrayList<Integer> messagesIds = entry.getValue();

                        ChatsCache.deleteMessages(chatId, messagesIds);
                    }
                }

                if (!updatedFiles.isEmpty()) {
                    for (TdApi.UpdateFile updateFile : updatedFiles) {
                        ChatsCache.sFiles.put(updateFile.file.id, updateFile.file);
                        if (ChatsCache.sDownloadFileStates.containsKey(updateFile.file.id)) {
                            ChatsCache.sDownloadFileStates.remove(updateFile.file.id);
                        }
                        ChatsCache.updateDownloadedFile(updateFile.file.id);
                    }
                }

                if (!downloadingFileArg  /*&& mLastUpdateTime < System.currentTimeMillis() - 1000*/) {
                    ChatsCache.updateInterface();
                }

                if (lastMessageArg != null) {
                    TdApi.Chat chat = ChatsCache.sChats.get(lastMessageArg.chatId);
                    if (chat != null && chat.notificationSettings.muteFor == 0 && mLastUpdateTime >= System.currentTimeMillis() - 1000 && chat.unreadCount != 0) {
                        Vibrator v = (Vibrator) Utils.sContext.getSystemService(Context.VIBRATOR_SERVICE);
                        v.vibrate(100);

                        ChatsCache.playNotificationSound();
                    }
                }

                mLastUpdateTime = System.currentTimeMillis();
            }
        });
    }
 }
