package org.tdlib.ippogram;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.SparseArray;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ui.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class ContactsCache {

    public static class Contact {
        public int id;
        public ArrayList<String> phones = new ArrayList<>();
        public ArrayList<String> phoneTypes = new ArrayList<>();
        public String firstName;
        public String lastName;
    }

    public HashMap<Integer, Contact> assignContacts = new HashMap<>();
    public HashMap<String, ArrayList<Contact>> contactsBySections = new HashMap<>();

    //public ArrayList<TdApi.User> users = new ArrayList<>();
    //public SparseArray<TdApi.User> assignUsers = new SparseArray<>();
    //public HashMap<String, ArrayList<TdApi.User>> usersBySections = new HashMap<>();

    public ArrayList<Integer> users = new ArrayList<>();
    public SparseArray<TdApi.User> assignUsers = new SparseArray<>();
    public HashMap<String, ArrayList<Integer>> usersBySections = new HashMap<>();

    private final Object mLoadingContactsSync = new Object();
    private boolean mLoadingContacts = false;

    private static volatile ContactsCache sInstance;

    private ContactsCache() {

    }

    public static ContactsCache getInstance() {
        ContactsCache localInstance = sInstance;
        if (localInstance == null) {
            synchronized (ContactsCache.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new ContactsCache();
                }
            }
        }
        return localInstance;
    }

    public void reset() {
        assignContacts.clear();
        contactsBySections.clear();
        users.clear();
        assignUsers.clear();
        usersBySections.clear();
        mLoadingContacts = false;
    }

    public void readContacts() {
        if (assignContacts.size() != 0) {
            return;
        }
        Utils.sMainThread.post(new Runnable() {
            @Override
            public void run() {
                final HashMap<Integer, Contact> internalAssignContacts = new HashMap<>();
                final HashMap<String, ArrayList<Contact>> internalContactsBySections = new HashMap<>();

                ContentResolver contentResolver = Utils.sContext.getContentResolver();

                StringBuilder ids = new StringBuilder();
                String[] projectionPhones = {
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.TYPE,
                        ContactsContract.CommonDataKinds.Phone.LABEL
                };

                Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projectionPhones, null, null, null);
                if (cursor == null) {
                    return;
                }
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        int id = cursor.getInt(cursor.getColumnIndex(projectionPhones[0]));
                        if (ids.length() != 0) {
                            ids.append(",");
                        }
                        ids.append(id);

                        Contact contact = internalAssignContacts.get(id);
                        if (contact == null) {
                            contact = new Contact();
                            contact.id = id;
                            contact.firstName = "";
                            contact.lastName = "";
                            internalAssignContacts.put(id, contact);
                        }

                        String phoneNumber = cursor.getString(cursor.getColumnIndex(projectionPhones[1]));
                        if (phoneNumber == null || phoneNumber.length() == 0) {
                            continue;
                        }
                        phoneNumber = Utils.stripExceptNumbers(phoneNumber);
                        if (phoneNumber.startsWith("8")) {
                            phoneNumber = "7" + phoneNumber.substring(1);
                        }
                        contact.phones.add(phoneNumber);

                        int type = cursor.getInt(cursor.getColumnIndex(projectionPhones[2]));
                        switch (type) {
                            case ContactsContract.CommonDataKinds.Phone.TYPE_CUSTOM:
                                contact.phoneTypes.add(cursor.getString(cursor.getColumnIndex(projectionPhones[3])));
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE:
                                contact.phoneTypes.add(Utils.sContext.getString(R.string.contact_phone_mobile));
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_HOME:
                                contact.phoneTypes.add(Utils.sContext.getString(R.string.contact_phone_home));
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_WORK:
                                contact.phoneTypes.add(Utils.sContext.getString(R.string.contact_phone_work));
                                break;
                            case ContactsContract.CommonDataKinds.Phone.TYPE_MAIN:
                                contact.phoneTypes.add(Utils.sContext.getString(R.string.contact_phone_main));
                                break;
                            default:
                                contact.phoneTypes.add(Utils.sContext.getString(R.string.contact_phone_other));
                        }
                    }
                }
                cursor.close();

                String[] projectionNames = {
                        ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID,
                        ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                        ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                        ContactsContract.Data.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME
                };
                cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, projectionNames,
                        ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " IN (" + ids + ") AND " +
                                ContactsContract.Data.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'", null, null);
                if (cursor == null) {
                    return;
                }
                if (cursor.getCount() > 0) {
                    while (cursor.moveToNext()) {
                        int id = cursor.getInt(cursor.getColumnIndex(projectionNames[0]));
                        String firstName = cursor.getString(cursor.getColumnIndex(projectionNames[1]));
                        String lastName = cursor.getString(cursor.getColumnIndex(projectionNames[2]));
                        String displayName = cursor.getString(cursor.getColumnIndex(projectionNames[3]));
                        String middleName = cursor.getString(cursor.getColumnIndex(projectionNames[4]));
                        Contact contact = internalAssignContacts.get(id);
                        if (contact != null) {
                            contact.firstName = firstName;
                            contact.lastName = lastName;
                            if (contact.firstName == null) {
                                contact.firstName = "";
                            }
                            if (middleName != null && middleName.length() != 0) {
                                if (contact.firstName.length() != 0) {
                                    contact.firstName += " " + middleName;
                                } else {
                                    contact.firstName = middleName;
                                }
                            }
                            if (contact.lastName == null) {
                                contact.lastName = "";
                            }
                            if (contact.firstName.length() == 0 && contact.lastName.length() == 0 && displayName != null && displayName.length() != 0) {
                                contact.firstName = displayName;
                            }
                        }
                    }
                }
                cursor.close();

                ArrayList<TdApi.InputContact> contactsForImport = new ArrayList<>();
                StringBuilder strForImport = new StringBuilder();
                for (HashMap.Entry<Integer, Contact> pair : internalAssignContacts.entrySet()) {
                    int id = pair.getKey();
                    Contact contact = pair.getValue();
                    for (String phone : contact.phones) {
                        TdApi.InputContact inputContact = new TdApi.InputContact(phone, contact.firstName, contact.lastName);
                        contactsForImport.add(inputContact);
                        strForImport.append(id).append(inputContact.phoneNumber).append(inputContact.firstName).append(inputContact.lastName);
                    }
                    String key = contact.firstName;
                    if (key.length() == 0) {
                        key = contact.lastName;
                    }
                    if (key.length() == 0) {
                        key = "#";
                        if (contact.phones.size() != 0) {
                            contact.firstName = "+" + contact.phones.get(0);
                        }
                    } else {
                        key = key.toUpperCase();
                    }
                    if (key.length() > 1) {
                        key = key.substring(0, 1);
                    }
                    ArrayList<Contact> contacts = internalContactsBySections.get(key);
                    if (contacts == null) {
                        contacts = new ArrayList<>();
                        internalContactsBySections.put(key, contacts);
                    }
                    contacts.add(contact);
                }
                for (HashMap.Entry<String, ArrayList<Contact>> pair : internalContactsBySections.entrySet()) {
                    Collections.sort(pair.getValue(), new Comparator<Contact>() {
                        @Override
                        public int compare(Contact lhs, Contact rhs) {
                            String name = lhs.firstName;
                            if (name.length() == 0) {
                                name = lhs.lastName;
                            }
                            String name2 = rhs.firstName;
                            if (name2.length() == 0) {
                                name2 = rhs.lastName;
                            }
                            return name.compareTo(name2);
                        }
                    });
                }

                String importHash = Utils.MD5(strForImport.toString());
                if (!contactsForImport.isEmpty() && !Utils.sImportHash.equals(importHash)) {
                    Utils.sImportHash = importHash;
                    Utils.sContext.getSharedPreferences("contacts", Context.MODE_PRIVATE).edit().putString("import_hash", importHash).commit();
                    importContacts(contactsForImport);
                } else {
                    loadContacts();
                }

                Utils.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        assignContacts = internalAssignContacts;
                        contactsBySections = internalContactsBySections;
                    }
                });
            }
        });
    }

    public void loadContacts() {
        synchronized (mLoadingContactsSync) {
            if (mLoadingContacts) {
                return;
            }
            mLoadingContacts = true;
        }
        Utils.sClient.send(new TdApi.GetContacts(), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Contacts.CONSTRUCTOR) {
                    TdApi.Contacts contacts = (TdApi.Contacts) object;
                    forwardLoadedContacts(new ArrayList<>(Arrays.asList(contacts.users)), true);
                }
            }
        });
    }

    private void importContacts(ArrayList<TdApi.InputContact> inputContacts) {
        Utils.sClient.send(new TdApi.ImportContacts(inputContacts.toArray(new TdApi.InputContact[inputContacts.size()])), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Contacts.CONSTRUCTOR) {
                    TdApi.Contacts contacts = (TdApi.Contacts) object;
                    forwardLoadedContacts(new ArrayList<>(Arrays.asList(contacts.users)), false);
                }
            }
        });
    }

    private void forwardLoadedContacts(final ArrayList<TdApi.User> internalUsers, final boolean load) {
        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                for (TdApi.User user : internalUsers) {
                    if (user.id == 0) {
                        continue;
                    }
                    ChatsCache.sUsers.put(user.id, user);
                }
                Utils.sMainThread.post(new Runnable() {
                    @Override
                    public void run() {
                        if (!load) {
                            loadContacts();
                        }
                        Collections.sort(internalUsers, new Comparator<TdApi.User>() {
                            @Override
                            public int compare(TdApi.User lhs, TdApi.User rhs) {
                                String name = lhs.firstName;
                                if (name == null || name.length() == 0) {
                                    name = lhs.lastName;
                                }
                                String name2 = rhs.firstName;
                                if (name2 == null || name2.length() == 0) {
                                    name2 = rhs.lastName;
                                }
                                return name.compareTo(name2);
                            }
                        });

                        final HashMap<String, ArrayList<Integer>> internalUsersBySection = new HashMap<>();
                        final SparseArray<TdApi.User> internalAssignUsers = new SparseArray<>();

                        for (TdApi.User user : internalUsers) {
                            internalAssignUsers.put(user.id, user);

                            String key = user.firstName;
                            if (key == null || key.length() == 0) {
                                key = user.lastName;
                            }
                            if (key.length() == 0) {
                                key = "#";
                            } else {
                                key = key.toUpperCase();
                            }
                            if (key.length() > 1) {
                                key = key.substring(0, 1);
                            }
                            ArrayList<Integer> internalUsers = internalUsersBySection.get(key);
                            if (internalUsers == null) {
                                internalUsers = new ArrayList<>();
                                internalUsersBySection.put(key, internalUsers);
                            }
                            internalUsers.add(user.id);
                        }
                        for (HashMap.Entry<String, ArrayList<Integer>> pair : internalUsersBySection.entrySet()) {
                            Collections.sort(pair.getValue(), new Comparator<Integer>() {
                                @Override
                                public int compare(Integer lhs, Integer rhs) {
                                    TdApi.User lUser = internalAssignUsers.get(lhs);
                                    TdApi.User rUser = internalAssignUsers.get(rhs);
                                    String name = lUser.firstName;
                                    if (name == null || name.length() == 0) {
                                        name = lUser.lastName;
                                    }
                                    String name2 = rUser.firstName;
                                    if (name2 == null || name2.length() == 0) {
                                        name2 = rUser.lastName;
                                    }
                                    return name.compareTo(name2);
                                }
                            });
                        }
                        Utils.runOnUIThread(new Runnable() {
                            @Override
                            public void run() {
                                users.clear();
                                for (TdApi.User user : internalUsers) {
                                    if (user.id == 0) {
                                        continue;
                                    }
                                    //if (!load) {
                                    users.add(user.id);
                                    //}
                                }
                                assignUsers = internalAssignUsers;
                                usersBySections = internalUsersBySection;
                                if (load) {
                                    synchronized (mLoadingContactsSync) {
                                        mLoadingContacts = false;
                                    }
                                }
                                ChatsCache.updateContactsList();
                                ChatsCache.updateInterface();
                            }
                        });
                    }
                });
            }
        });
    }

    private void updateSections(boolean sort) {
        if (sort) {
            Collections.sort(users, new Comparator<Integer>() {
                @Override
                public int compare(Integer lhs, Integer rhs) {
                    TdApi.User lUser = ChatsCache.getUser(lhs);
                    TdApi.User rUser = ChatsCache.getUser(rhs);
                    String name = lUser.firstName;
                    if (name == null || name.length() == 0) {
                        name = lUser.lastName;
                    }
                    String name2 = rUser.firstName;
                    if (name2 == null || name2.length() == 0) {
                        name2 = rUser.lastName;
                    }
                    return name.compareTo(name2);
                }
            });
        }

        final HashMap<String, ArrayList<Integer>> internalUsersBySection = new HashMap<>();

        for (Integer userId : users) {
            TdApi.User user = ChatsCache.getUser(userId);
            if (user == null) {
                continue;
            }
            String key = user.firstName;
            if (key == null || key.length() == 0) {
                key = user.lastName;
            }
            if (key.length() == 0) {
                key = "#";
            } else {
                key = key.toUpperCase();
            }
            if (key.length() > 1) {
                key = key.substring(0, 1);
            }
            ArrayList<Integer> internalUsers = internalUsersBySection.get(key);
            if (internalUsers == null) {
                internalUsers = new ArrayList<>();
                internalUsersBySection.put(key, internalUsers);
            }
            internalUsers.add(user.id);
        }
        for (HashMap.Entry<String, ArrayList<Integer>> pair : internalUsersBySection.entrySet()) {
            Collections.sort(pair.getValue(), new Comparator<Integer>() {
                @Override
                public int compare(Integer lhs, Integer rhs) {
                    TdApi.User lUser = ChatsCache.getUser(lhs);
                    TdApi.User rUser = ChatsCache.getUser(rhs);
                    String name = lUser.firstName;
                    if (name == null || name.length() == 0) {
                        name = lUser.lastName;
                    }
                    String name2 = rUser.firstName;
                    if (name2 == null || name2.length() == 0) {
                        name2 = rUser.lastName;
                    }
                    return name.compareTo(name2);
                }
            });
        }

        usersBySections = internalUsersBySection;
    }

    public void addContact(TdApi.User user) {
        if (user == null) {
            return;
        }

        TdApi.InputContact addingContact = new TdApi.InputContact(user.phoneNumber, user.firstName, user.lastName);
        TdApi.ImportContacts importContacts = new TdApi.ImportContacts();
        importContacts.inputContacts = new TdApi.InputContact[]{addingContact};
        Utils.sClient.send(importContacts, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Contacts.CONSTRUCTOR) {
                    final TdApi.Contacts contacts = (TdApi.Contacts)object;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            for (TdApi.User user : contacts.users) {
                                ChatsCache.sUsers.put(user.id, user);
                                if (assignUsers.get(user.id) == null) {
                                    users.add(user.id);
                                    assignUsers.put(user.id, user);
                                }
                            }
                            updateSections(true);
                            ChatsCache.updateContactsList();
                        }
                    });
                }
            }
        });
    }

    public void deleteContacts(final ArrayList<TdApi.User> deleteUsers) {
        if (deleteUsers == null || deleteUsers.isEmpty()) {
            return;
        }
        int[] usersIds = new int[deleteUsers.size()];
        for (int i = 0; i < deleteUsers.size(); i++) {
            usersIds[i] = deleteUsers.get(i).id;
        }
        Utils.sClient.send(new TdApi.DeleteContacts(usersIds), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Ok.CONSTRUCTOR) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            for (TdApi.User user : deleteUsers) {
                                users.remove(Integer.valueOf(user.id));
                                assignUsers.remove(user.id);
                            }
                            updateSections(false);
                            ChatsCache.updateInterface();
                            ChatsCache.updateContactsList();
                        }
                    });
                }
            }
        });
    }
}
