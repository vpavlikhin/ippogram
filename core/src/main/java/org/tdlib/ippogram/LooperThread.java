package org.tdlib.ippogram;

import android.os.Looper;

import java.util.concurrent.CountDownLatch;
import android.os.Handler;
import android.util.Log;

public class LooperThread extends Thread {

    private CountDownLatch mSyncLatch = new CountDownLatch(1);
    private Handler mHandler;

    public LooperThread(String name) {
        super(name);
        start();
    }

    @Override
    public void run() {
        try {
            Looper.prepare();
            mHandler = new Handler();
            mSyncLatch.countDown();
            Looper.loop();
        } catch (Exception e) {
            Log.e("LoopingThread", "run()" + e.getMessage());
        }
    }

    public void post(Runnable task) {
        post(task, 0);
    }

    public void post(Runnable task, long delay) {
        try {
            mSyncLatch.await();
            if (delay == 0) {
                mHandler.post(task);
            } else {
                mHandler.postDelayed(task, delay);
            }
        } catch (InterruptedException ie) {
            Log.d("LoopingThread", "post()" + ie.getMessage());
        }
    }
}
