package org.tdlib.ippogram.bottomsheet;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

public class PaddingItemDecoration extends RecyclerView.ItemDecoration {

    private int mPaddingTopPx;
    private int mPaddingRightPx;

    public PaddingItemDecoration() {
        Resources resources = Resources.getSystem();
        DisplayMetrics dm = resources.getDisplayMetrics();
        mPaddingTopPx = (int)(9 * dm.density);
        mPaddingRightPx = (int)(7 * dm.density);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);

        final int itemPosition = parent.getChildAdapterPosition(view);
        if (itemPosition == RecyclerView.NO_POSITION) {
            return;
        }
        int orientation = getOrientation(parent);
        final int itemCount = state.getItemCount();

        int left = 0;
        int top = 0;
        int right = 0;
        int bottom = 0;

        /** HORIZONTAL */
        if (orientation == LinearLayoutManager.HORIZONTAL) {
            /** all positions */
            top = mPaddingTopPx;
            right = mPaddingRightPx;

            /** first position */
            if (itemPosition == 0) {
                left = (mPaddingTopPx + mPaddingRightPx) / 2;
            }
            /** last position */
            else if (itemCount > 0 && itemPosition == itemCount - 1) {
                right += (mPaddingTopPx - mPaddingRightPx) / 2;
            }
        }

        if (!isReverseLayout(parent)) {
            outRect.set(left, top, right, bottom);
        } else {
            outRect.set(right, bottom, left, top);
        }
    }

    private boolean isReverseLayout(RecyclerView parent) {
        if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
            return layoutManager.getReverseLayout();
        }
        throw new IllegalStateException("PaddingItemDecoration can only be used with a LinearLayoutManager.");
    }

    private int getOrientation(RecyclerView parent) {
        if (parent.getLayoutManager() instanceof LinearLayoutManager) {
            LinearLayoutManager layoutManager = (LinearLayoutManager) parent.getLayoutManager();
            return layoutManager.getOrientation();
        }
        throw new IllegalStateException("PaddingItemDecoration can only be used with a LinearLayoutManager.");
    }
}
