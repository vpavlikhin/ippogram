package org.tdlib.ippogram;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.time.FastDateFormat;
import org.tdlib.ippogram.ui.R;
import org.tdlib.ippogram.ui.components.LockPatternView;
import org.tdlib.ippogram.ui.components.MaterialProgressDrawable;
import org.tdlib.ippogram.ui.components.TextDrawable;
import org.tdlib.ippogram.ui.components.TypefaceSpan;
import org.tdlib.ippogram.ui.components.URLSpanNoUnderline;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import static com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import static org.tdlib.ippogram.SmsListener.OnSmsListener;

public class Utils {

    public static final String ACTION_START_DOWNLOAD = "org.tdlib.ippogram.action.START_DOWNLOAD";
    public static final String ACTION_PROGRESS_UPDATE = "org.tdlib.ippogram.action.PROGRESS_UPDATE";
    public static final String ACTION_CANCEL_DOWNLOAD = "org.tdlib.ippogram.action.CANCEL_DOWNLOAD";

    private static final Object sLock = new Object();
    private static final Hashtable<String, Typeface> sCache = new Hashtable<>();
    public static String TAG;
    public static Handler sHandler;
    public static Context sContext;
    public static AlertDialog sProgressDialog;
    public static int sCurrentConnectionState = 0;

    //public static LocalBroadcastManager sLocalBroadcastManager;

    public static float sDensity = 1;

    public static LooperThread sMainThread = new LooperThread("mainThread");
    public static LooperThread sLoadImageThread = new LooperThread("loadImageThread");
    public static LooperThread sLoadFileThread = new LooperThread("loadFileThread");

    public static int sCurrentUserId = 0;

    public static String sImportHash;

    public static String sPasscodeHash = "";
    public static int sAutoLock = 60 * 60;
    public static boolean sLocked = false;
    public static int sPasscodeType = 1;

    public static UpdatesHandler sUpdatesHandler;
    public static Client sClient;
    public static OnSmsListener sSmsListener;

    public static int sStatusBarHeight = 0;
    public static Point sDisplaySize = new Point();
    public static boolean sOptionReceived = false;
    public static FastDateFormat sDay;
    public static FastDateFormat sDayOfWeek;
    public static FastDateFormat sMonth;
    public static FastDateFormat sYear;
    public static FastDateFormat sChatDate;
    public static FastDateFormat sChatFullDate;
    private static int[] sArrColors = {0xffe56555, 0xfff28c48, 0xffeec764, 0xff76c84d, 0xff5fbed5, 0xff549cdd, 0xff8e85ee, 0xfff2749a};
    private static int[] sArrColorsProfiles = {0xffd86f65, 0xfff69d61, 0xfffabb3c, 0xff67b35d, 0xff56a2bb, 0xff5c98cd, 0xff8c79d2, 0xfff37fa6};

    public static native Bitmap doBlur(Bitmap sentBitmap, int radius, boolean canReuseInBitmap);

    public static native long openOggFile(String path);
    public static native long readOggFile(short[] out);

    public static native Bitmap decodeWebP(byte[] decoded);

    //public static native String jni();

    /*public static BroadcastReceiver sDownloadingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_PROGRESS_UPDATE)) {

            }
        }
    };*/

    static {
        //Locale locale = Locale.getDefault();
        sDay = FastDateFormat.getInstance("h:mm a", Locale.US);
        sDayOfWeek = FastDateFormat.getInstance("EEE", Locale.US);
        sMonth = FastDateFormat.getInstance("MMM dd ", Locale.US);
        sYear = FastDateFormat.getInstance("dd.MM.yy", Locale.US);
        sChatDate = FastDateFormat.getInstance("MMMM d", Locale.US);
        sChatFullDate = FastDateFormat.getInstance("MMMM d, yyyy", Locale.US);
    }

    public static void savePasscode(Activity activity) {
        SharedPreferences.Editor passcodeEditor = activity.getSharedPreferences("passcode", Context.MODE_PRIVATE).edit();
        passcodeEditor.putString("passcode_hash", Utils.sPasscodeHash);
        passcodeEditor.putBoolean("locked", Utils.sLocked);
        passcodeEditor.putInt("auto_lock", Utils.sAutoLock);
        passcodeEditor.putInt("type", Utils.sPasscodeType);
        passcodeEditor.commit();
    }

    public static String patternToString(List<LockPatternView.Cell> pattern) {
        int patternSize = pattern.size();
        byte[] bytes = new byte[patternSize];
        for (int i = 0; i < patternSize; ++i) {
            LockPatternView.Cell cell = pattern.get(i);
            bytes[i] = (byte)(cell.getRow() * 3 + cell.getColumn());
        }
        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static List<LockPatternView.Cell> stringToPattern(String string) {
        byte[] bytes = Base64.decode(string, Base64.DEFAULT);
        List<LockPatternView.Cell> pattern = new ArrayList<>();
        for (byte b : bytes) {
            pattern.add(LockPatternView.Cell.of(b / 3, b % 3));
        }
        return pattern;
    }

    public static void setCursorDrawable(EditText editText, @DrawableRes int resId) {
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(editText, resId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String MD5(String source) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] array = md.digest(source.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte el : array) {
                sb.append(Integer.toHexString((el & 0xff) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void runOnUIThread(Runnable runnable) {
        runOnUIThread(runnable, 0);
    }

    public static void runOnUIThread(Runnable runnable, long delay) {
        synchronized (sLock) {
            if (sHandler == null) {
                sHandler = new Handler(sContext.getMainLooper());
            }
            if (delay == 0) {
                sHandler.post(runnable);
            } else {
                sHandler.postDelayed(runnable, delay);
            }
        }
    }

    public static void cancelRunOnUIThread(Runnable runnable) {
        synchronized (sLock) {
            if (sHandler == null) {
                return;
            }
            sHandler.removeCallbacks(runnable);
        }
    }

    public static Typeface getTypeface(String name) {
        synchronized (sCache) {
            if (!sCache.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(sContext.getAssets(), String.format("fonts/%s.ttf", name));
                sCache.put(name, t);
            }
            return sCache.get(name);
        }
    }

    public static void showKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager)view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(View view) {
        if (view == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager)view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isNetworkExist() {
        ConnectivityManager cm = (ConnectivityManager)sContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && (activeNetwork.isConnectedOrConnecting() || activeNetwork.isAvailable());
    }

    public static void showProgressDialog(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FrameLayout progressContainer = new FrameLayout(activity);

                LinearLayout progressLayout = new LinearLayout(activity);
                progressLayout.setOrientation(LinearLayout.HORIZONTAL);
                progressLayout.setBaselineAligned(false);
                progressLayout.setPadding(convertDpToPx(8), convertDpToPx(10), convertDpToPx(8), convertDpToPx(10));
                progressContainer.addView(progressLayout);
                FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)progressLayout.getLayoutParams();
                flParams.width = FrameLayout.LayoutParams.MATCH_PARENT;
                flParams.height = FrameLayout.LayoutParams.MATCH_PARENT;
                progressLayout.setLayoutParams(flParams);

                ProgressBar progressBar = new ProgressBar(activity);
                progressBar.setMax(10000);
                progressBar.setIndeterminate(true);
                progressLayout.addView(progressBar);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)progressBar.getLayoutParams();
                params.width = convertDpToPx(36);
                params.height = convertDpToPx(36);
                params.setMargins(convertDpToPx(12), convertDpToPx(12), convertDpToPx(24), convertDpToPx(12));
                //params.leftMargin = convertDpToPx(12);
                //params.rightMargin = convertDpToPx(24);
                progressBar.setLayoutParams(params);

                MaterialProgressDrawable progressDrawable = new MaterialProgressDrawable(activity, progressBar);
                progressDrawable.setBackgroundColor(ContextCompat.getColor(sContext, R.color.white));
                progressDrawable.setColorSchemeColors(ContextCompat.getColor(sContext, R.color.blue));
                progressDrawable.setAlpha(255);
                progressBar.setIndeterminateDrawable(progressDrawable);

                TextView messageTextView = new TextView(activity);
                messageTextView.setTypeface(Utils.getTypeface("regular"));
                if (message != null) {
                    messageTextView.setText(message);
                }
                progressLayout.addView(messageTextView);
                LinearLayout.LayoutParams paramsText = (LinearLayout.LayoutParams)messageTextView.getLayoutParams();
                paramsText.width = LinearLayout.LayoutParams.MATCH_PARENT;
                paramsText.height = LinearLayout.LayoutParams.WRAP_CONTENT;
                paramsText.gravity = Gravity.CENTER_VERTICAL;
                messageTextView.setLayoutParams(paramsText);

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setView(progressContainer);
                sProgressDialog = builder.create();
                sProgressDialog.setCanceledOnTouchOutside(false);
                sProgressDialog.setCancelable(false);
                sProgressDialog.show();
            }
        });
    }

    public static void hideProgressDialog(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (sProgressDialog != null) {
                    sProgressDialog.dismiss();
                }
            }
        });
    }

    public static String getFormatDate(int date) {
        Calendar rightNow = Calendar.getInstance();
        int todayDay = rightNow.get(Calendar.DAY_OF_YEAR);
        int todayYear = rightNow.get(Calendar.YEAR);
        rightNow.setTimeInMillis(date * 1000L);
        int day = rightNow.get(Calendar.DAY_OF_YEAR);
        int year = rightNow.get(Calendar.YEAR);

        if (todayYear != year) {
            return sYear.format(new Date(date * 1000L));
        } else {
            int diff = todayDay - day;
            if (diff == 0 || diff == 1 && getCurrentTime() - date < 60 * 60 * 8) {
                return sDay.format(new Date(date * 1000L));
            } else if (diff >= 1 && diff < 7) {
                return sDayOfWeek.format(new Date(date * 1000L));
            } else {
                return sMonth.format(new Date(date * 1000L));
            }
        }
    }

    public static String getFormatDateChat(int date) {
        Calendar rightNow = Calendar.getInstance();
        int todayYear = rightNow.get(Calendar.YEAR);
        rightNow.setTimeInMillis(date * 1000L);
        int year = rightNow.get(Calendar.YEAR);

        if (todayYear == year) {
            return sChatDate.format(new Date(date * 1000L));
        }
        return sChatFullDate.format(new Date(date * 1000L));
    }

    public static String formatMessageDate(int date) {
        Calendar rightNow = new GregorianCalendar();
        rightNow.setTimeInMillis(date * 1000L);
        int dateDay = rightNow.get(Calendar.DAY_OF_YEAR);
        int dateYear = rightNow.get(Calendar.YEAR);
        int dateMonth = rightNow.get(Calendar.MONTH);
        return String.format("%d_%02d_%02d", dateYear, dateMonth, dateDay);
    }

    public static String getFormatDateStatus(int date) {
        long diff = Math.abs(System.currentTimeMillis() - date * 1000L);
        String format = "h:mm a";

        if (diff > 518400000) {
            format = "MMM d, y";
        } else if (diff > 43200000) {
            format = "EEEE";
        }

        return FastDateFormat.getInstance(format, Locale.US).format(new Date(date * 1000L));
    }

    public static String formatUserStatus(TdApi.User user) {
        if (user != null && user.status != null && user.type.getConstructor() == TdApi.UserTypeGeneral.CONSTRUCTOR) {
            if (user.id != Utils.sCurrentUserId) {
                switch (user.status.getConstructor()) {
                    case TdApi.UserStatusOnline.CONSTRUCTOR:
                        TdApi.UserStatusOnline online = (TdApi.UserStatusOnline) user.status;
                        if (getCurrentTime() < online.expires) {
                            return Utils.sContext.getString(R.string.online_status);
                        }
                        return String.format("%s %s", Utils.sContext.getString(R.string.last_seen_status), getRelativeTime(online.expires));
                    case TdApi.UserStatusOffline.CONSTRUCTOR:
                        return String.format("%s %s", Utils.sContext.getString(R.string.last_seen_status), getRelativeTime(((TdApi.UserStatusOffline) user.status).wasOnline));
                    case TdApi.UserStatusRecently.CONSTRUCTOR:
                        return Utils.sContext.getString(R.string.recently_status);
                    case TdApi.UserStatusLastWeek.CONSTRUCTOR:
                        return Utils.sContext.getString(R.string.last_week_status);
                    case TdApi.UserStatusLastMonth.CONSTRUCTOR:
                        return Utils.sContext.getString(R.string.last_month_status);
                }
            } else {
                return Utils.sContext.getString(R.string.online_status);
            }
        } else if (user != null && user.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR) {
            return Utils.sContext.getString(R.string.bot_status);
        }
        return Utils.sContext.getString(R.string.long_ago_status);
    }

    public static String getRelativeTime(int date) {
        long diff = Math.abs(System.currentTimeMillis() - date * 1000L);
        if (diff < 60000) {
            return Utils.sContext.getString(R.string.relative_time_just_now);
        }
        if (diff < 3600000) {
            int minutes = (int) Math.floor(diff / 60000);
            if (minutes == 1) {
                return Utils.sContext.getString(R.string.relative_time_minute_ago);
            }
            return String.format("%s %s", minutes, Utils.sContext.getString(R.string.relative_time_minutes_ago));
        }
        if (diff < 86400000) {
            int hours = (int) Math.floor(diff / 3600000);
            if (hours == 1) {
                return Utils.sContext.getString(R.string.relative_time_hour_ago);
            }
            return String.format("%s %s", hours, Utils.sContext.getString(R.string.relative_time_hours_ago));
        }
        return getFormatDateStatus(date);
    }

    public static String formatPhone(String phone) {
        String countryCode = "RU";
        try {
            TelephonyManager telephonyManager = (TelephonyManager)Utils.sContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                countryCode = telephonyManager.getSimCountryIso().toUpperCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (countryCode.length() == 0) {
            try {
                Locale current = Utils.sContext.getResources().getConfiguration().locale;
                countryCode = current.getCountry().toUpperCase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (countryCode.length() == 0) {
            countryCode = "RU";
        }

        PhoneNumberUtil util = PhoneNumberUtil.getInstance();
        String formattedPhone = null;
        try {
            PhoneNumber phoneNumber = util.parseAndKeepRawInput(phone, countryCode);
            if (util.isPossibleNumber(phoneNumber)) {
                formattedPhone = util.formatInOriginalFormat(phoneNumber, countryCode);
            } else {
                formattedPhone = phone;
            }
        } catch (NumberParseException e) {
            e.printStackTrace();
            formattedPhone = phone;
        }
        return formattedPhone;
    }

    public static int getCurrentTime() {
        return (int)(System.currentTimeMillis() / 1000);
    }

    public static int getColorIndex(int id) {
        if (id >= 0 && id < 8) {
            return id;
        }
        try {
            String str;
            if (id >= 0) {
                str = String.format(Locale.US, "%d%d", id, sCurrentUserId);
            } else {
                str = String.format(Locale.US, "%d", id);
            }
            if (str.length() > 15) {
                str = str.substring(0, 15);
            }
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(str.getBytes());
            int b = digest[Math.abs(id % 16)];
            if (b < 0) {
                b += 256;
            }
            return Math.abs(b) % sArrColors.length;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return id % sArrColors.length;
    }

    public static TextDrawable getUserAvatar(TdApi.User user, int height) {
        if (user != null) {
            return getAvatar(user.id, user.firstName, user.lastName, height);
        }
        return null;
    }

    public static TextDrawable getChatAvatar(TdApi.Chat chat, int height) {
        if (chat != null) {
            return getAvatar(chat.id, chat.title, null, height);
        }
        return null;
    }

    public static TextDrawable getAvatar(long id, String firstName, String lastName, int height) {
        if (ChatsCache.sUserAvatars.containsKey(id)) {
            return (TextDrawable)ChatsCache.sUserAvatars.get(id);
        }
        int color;
        if ((int)id == sCurrentUserId) {
            color = sArrColorsProfiles[getColorIndex((int)id)];
        } else {
            color = sArrColors[getColorIndex((int)id)];
        }

        /*if (height == 64) {
            color = 0xff5c98cd;
        }*/

        if (firstName == null || firstName.length() == 0) {
            firstName = lastName;
            lastName = null;
        }

        String text = "";
        if (firstName != null && firstName.length() > 0) {
            text += firstName.substring(0, 1);
        }
        if (lastName != null && lastName.length() > 0) {
            String lastch = null;
            for (int a = lastName.length() - 1; a >= 0; a--) {
                if (lastch != null && lastName.charAt(a) == ' ') {
                    break;
                }
                lastch = lastName.substring(a, a + 1);
            }
            if (Build.VERSION.SDK_INT >= 16) {
                text += "\u200C" + lastch;
            } else {
                text += lastch;
            }
        } else if (firstName != null && firstName.length() > 0) {
            for (int a = firstName.length() - 1; a >= 0; a--) {
                if (firstName.charAt(a) == ' ') {
                    if (a != firstName.length() - 1 && firstName.charAt(a + 1) != ' ') {
                        if (Build.VERSION.SDK_INT >= 16) {
                            text += "\u200C" + firstName.substring(a + 1, a + 2);
                        } else {
                            text += firstName.substring(a + 1, a + 2);
                        }
                        break;
                    }
                }
            }
        }

        TextDrawable avatar =  TextDrawable.builder().beginConfig()
                .textColor(0xffffffff)
                .useFont(getTypeface("medium"))
                .fontSize(convertDpToPx(/*height == 40 ? 14 : */22))
                .width(convertDpToPx(64))
                .height(convertDpToPx(64))
                //.fontSize((int)convertDpToPx(height ? 14 : 20, sContext))
                //.width((int) convertDpToPx(height ? 40 : 56, sContext))
                //.height((int) convertDpToPx(height ? 40 : 56, sContext))
                .toUpperCase()
                .endConfig()
                .buildRound(text, color);
        ChatsCache.sUserAvatars.put(id, avatar);
        return avatar;
    }

    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static int convertDpToPx(float dp){
        Resources resources = sContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160.0f));
    }

    public static int convertPxToDp(float px){
        Resources resources = sContext.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int)(px / (metrics.densityDpi / 160f));
    }

    private static final int[] RES_IDS_ACTION_BAR_SIZE = { R.attr.actionBarSize };
    public static int calculateActionBarSize(Context context) {
        if (context == null) {
            return 0;
        }

        Resources.Theme curTheme = context.getTheme();
        if (curTheme == null) {
            return 0;
        }

        TypedArray att = curTheme.obtainStyledAttributes(RES_IDS_ACTION_BAR_SIZE);
        if (att == null) {
            return 0;
        }

        float size = att.getDimension(0, 0);
        att.recycle();
        return (int) size;
    }

    public static String formatFileSize(int size) {
        DecimalFormatSymbols dotSymbol = new DecimalFormatSymbols();
        dotSymbol.setDecimalSeparator('.');
        int[] dividers = new int[] { 1024 * 1024 * 1024, 1024 * 1024, 1024, 1};
        String[] units = new String[] {"Gb", "Mb", "Kb", "b"};
        if (size < 1) {
            return "0 b";
        }
        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            if (size >= dividers[i]) {
                result = new DecimalFormat("###0.#", dotSymbol).format(dividers[i] > 1 ? (double) size / (double) dividers[i] : (double) size) + " " + units[i];
                break;
            }
        }
        return result;
    }

    public static String formatMembersCount(int members) {
        DecimalFormatSymbols dotSymbol = new DecimalFormatSymbols();
        dotSymbol.setDecimalSeparator('.');
        int[] dividers = new int[] {1000 * 1000, 1000, 1};
        String[] units = new String[] {"m", "k", ""};
        if (members == 0) {
            return Utils.sContext.getString(R.string.group_no_members);
        }
        if (members == 1) {
            return String.format("%d %s", members, Utils.sContext.getString(R.string.group_member));
        }

        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            if (members >= dividers[i]) {
                result = new DecimalFormat("###0.#", dotSymbol).format(dividers[i] > 1 ? (double) members / (double) dividers[i] : (double) members) + units[i];
                break;
            }
        }
        return String.format("%s %s", result, Utils.sContext.getString(R.string.group_members));
    }

    public static String formatViewsCount(int views) {
        DecimalFormatSymbols dotSymbol = new DecimalFormatSymbols();
        dotSymbol.setDecimalSeparator('.');
        int[] dividers = new int[] {1000 * 1000, 1000, 1};
        String[] units = new String[] {"m", "k", ""};
        if (views == 0) {
            return "0";
        }

        String result = null;
        for (int i = 0; i < dividers.length; i++) {
            if (views >= dividers[i]) {
                result = new DecimalFormat("###0.#", dotSymbol).format(dividers[i] > 1 ? (double) views / (double) dividers[i] : (double) views) + units[i];
                break;
            }
        }
        return result;
    }

    public static String formatName(String firstName, String lastName) {
        if (firstName == null || firstName.length() == 0) {
            return lastName;
        } else if (firstName.length() != 0 && lastName.length() != 0) {
            return String.format("%s %s", firstName, lastName);
        }
        return firstName;
    }

    public static SpannableStringBuilder formatWebPageText(String siteName, String title, String description) {
        SpannableStringBuilder res;
        if (siteName == null || siteName.length() == 0) {
            res = new SpannableStringBuilder(title);
            res.setSpan(new ForegroundColorSpan(0xff333333) {
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setTypeface(Utils.getTypeface("medium"));
                }
            }, 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (siteName.length() != 0 && title.length() != 0) {
            res = new SpannableStringBuilder(String.format("%s\n%s", siteName, title));
            res.setSpan(new ForegroundColorSpan(0xff569ace) {
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setTypeface(Utils.getTypeface("medium"));
                }
            }, 0, siteName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            res.setSpan(new ForegroundColorSpan(0xff333333) {
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setTypeface(Utils.getTypeface("medium"));
                }
            }, res.length() - title.length(), res.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else {
            res = new SpannableStringBuilder(siteName);
            res.setSpan(new ForegroundColorSpan(0xff569ace) {
                @Override
                public void updateDrawState(TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setTypeface(Utils.getTypeface("medium"));
                }
            }, 0, siteName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        if (res.length() == 0) {
            res = new SpannableStringBuilder(description);
            res.setSpan(new ForegroundColorSpan(0xff333333), 0, description.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else if (res.length() != 0 && description.length() != 0) {
            res = res.append("\n").append(description);
            res.setSpan(new ForegroundColorSpan(0xff333333), res.length() - description.length(), res.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return res;
    }

    public static void removeUrlUnderline(TextView textView) {
        Spannable s = (Spannable)textView.getText();
        URLSpan[] spans = s.getSpans(0, s.length(), URLSpan.class);
        for (URLSpan span : spans) {
            int start = s.getSpanStart(span);
            int end = s.getSpanEnd(span);
            s.removeSpan(span);
            span = new URLSpanNoUnderline(span.getURL());
            s.setSpan(span, start, end, 0);
        }
    }

    public static Spannable formatText(CharSequence text, TdApi.MessageEntity[] entities) {
        Spannable spannable;
        if (text instanceof Spannable) {
            spannable = (Spannable) text;
        } else {
            spannable = Spannable.Factory.getInstance().newSpannable(text);
        }
        URLSpan[] spans = spannable.getSpans(0, spannable.length(), URLSpan.class);
        for (TdApi.MessageEntity entity : entities) {
            if (entity.getConstructor() == TdApi.MessageEntityBold.CONSTRUCTOR) {
                TdApi.MessageEntityBold entityBold = (TdApi.MessageEntityBold) entity;
                spannable.setSpan(new TypefaceSpan(getTypeface("bold")), entityBold.offset, entityBold.offset + entityBold.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (entity.getConstructor() == TdApi.MessageEntityItalic.CONSTRUCTOR) {
                TdApi.MessageEntityItalic entityItalic = (TdApi.MessageEntityItalic) entity;
                spannable.setSpan(new TypefaceSpan(getTypeface("italic")), entityItalic.offset, entityItalic.offset + entityItalic.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (entity.getConstructor() == TdApi.MessageEntityCode.CONSTRUCTOR) {
                TdApi.MessageEntityCode entityCode = (TdApi.MessageEntityCode) entity;
                spannable.setSpan(new TypefaceSpan(Typeface.MONOSPACE, convertDpToPx(14)), entityCode.offset, entityCode.offset + entityCode.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (entity.getConstructor() == TdApi.MessageEntityUrl.CONSTRUCTOR) {
                TdApi.MessageEntityUrl entityUrl = (TdApi.MessageEntityUrl) entity;
                if (spans != null && spans.length > 0) {
                    for (int i = 0; i < spans.length; i++) {
                        if (spans[i] == null) {
                            continue;
                        }
                        int start = spannable.getSpanStart(spans[i]);
                        int end = spannable.getSpanEnd(spans[i]);
                        if (entityUrl.offset <= start && entityUrl.offset + entityUrl.length >= start || entityUrl.offset <= end && entityUrl.offset + entityUrl.length >= end) {
                            spannable.removeSpan(spans[i]);
                            spans[i] = null;
                        }
                    }
                }
                String url = text.subSequence(entityUrl.offset, entityUrl.offset + entityUrl.length).toString();
                spannable.setSpan(new URLSpanNoUnderline(url), entityUrl.offset, entityUrl.offset + entityUrl.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (entity.getConstructor() == TdApi.MessageEntityHashtag.CONSTRUCTOR) {
                TdApi.MessageEntityHashtag entityHashtag = (TdApi.MessageEntityHashtag) entity;
                if (spans != null && spans.length > 0) {
                    for (int i = 0; i < spans.length; i++) {
                        if (spans[i] == null) {
                            continue;
                        }
                        int start = spannable.getSpanStart(spans[i]);
                        int end = spannable.getSpanEnd(spans[i]);
                        if (entityHashtag.offset <= start && entityHashtag.offset + entityHashtag.length >= start || entityHashtag.offset <= end && entityHashtag.offset + entityHashtag.length >= end) {
                            spannable.removeSpan(spans[i]);
                            spans[i] = null;
                        }
                    }
                }
                String hashtag = text.subSequence(entityHashtag.offset, entityHashtag.offset + entityHashtag.length).toString();
                spannable.setSpan(new URLSpanNoUnderline(hashtag), entityHashtag.offset, entityHashtag.offset + entityHashtag.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } else if (entity.getConstructor() == TdApi.MessageEntityMention.CONSTRUCTOR) {
                TdApi.MessageEntityMention entityMention = (TdApi.MessageEntityMention) entity;
                if (spans != null && spans.length > 0) {
                    for (int i = 0; i < spans.length; i++) {
                        if (spans[i] == null) {
                            continue;
                        }
                        int start = spannable.getSpanStart(spans[i]);
                        int end = spannable.getSpanEnd(spans[i]);
                        if (entityMention.offset <= start && entityMention.offset + entityMention.length >= start || entityMention.offset <= end && entityMention.offset + entityMention.length >= end) {
                            spannable.removeSpan(spans[i]);
                            spans[i] = null;
                        }
                    }
                }
                String mention = text.subSequence(entityMention.offset, entityMention.offset + entityMention.length).toString();
                spannable.setSpan(new URLSpanNoUnderline(mention), entityMention.offset, entityMention.offset + entityMention.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        return spannable;
    }

    public static String convertMessageToText(TdApi.Message message) {
        TdApi.User fromUser = ChatsCache.getUser(message.fromId);
        if (message.isPost || fromUser != null) {
            TdApi.MessageContent messageContent = message.content;
            switch (messageContent.getConstructor()) {
                case TdApi.MessageAudio.CONSTRUCTOR:
                    return sContext.getString(R.string.message_audio);
                case TdApi.MessageChannelChatCreate.CONSTRUCTOR:
                    return sContext.getString(R.string.message_channel_chat_create);
                case TdApi.MessageChatAddParticipants.CONSTRUCTOR:
                    TdApi.MessageChatAddParticipants addedParticipants = (TdApi.MessageChatAddParticipants)messageContent;
                    TdApi.User[] addedUsers = addedParticipants.participants;
                    if (addedUsers.length == 1) {
                        TdApi.User addedUser = addedUsers[0];
                        ChatsCache.sUsers.put(addedUser.id, addedUser);
                        if (fromUser.id == sCurrentUserId && fromUser.id == addedUser.id) {
                            return String.format(sContext.getString(R.string.message_chat_return_part), sContext.getString(R.string.message_you));
                        } else if (fromUser.id == sCurrentUserId) {
                            return String.format(sContext.getString(R.string.message_chat_add_part), sContext.getString(R.string.message_you), formatName(addedUser.firstName, addedUser.lastName));
                        } else if (fromUser.id == addedUser.id) {
                            return String.format(sContext.getString(R.string.message_chat_return_part), formatName(addedUser.firstName, addedUser.lastName));
                        } else if (addedUser.id == sCurrentUserId) {
                            return String.format(sContext.getString(R.string.message_chat_add_part), formatName(fromUser.firstName, fromUser.lastName), sContext.getString(R.string.message_you).toLowerCase());
                        } else {
                            return String.format(sContext.getString(R.string.message_chat_add_part), formatName(fromUser.firstName, fromUser.lastName), formatName(addedUser.firstName, addedUser.lastName));
                        }
                    }
                case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                    if (message.isPost) {
                        return sContext.getString(R.string.message_channel_change_photo);
                    }
                    if (fromUser.id == sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_change_photo), sContext.getString(R.string.message_you));
                    }
                    return String.format(sContext.getString(R.string.message_chat_change_photo), formatName(fromUser.firstName, fromUser.lastName));
                case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                    if (message.isPost) {
                        return sContext.getString(R.string.message_channel_change_name_short);
                    }
                    //TdApi.MessageChatChangeTitle changedTitle = (TdApi.MessageChatChangeTitle)messageContent;
                    if (fromUser.id == sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_change_title_short), sContext.getString(R.string.message_you));
                    }
                    return String.format(sContext.getString(R.string.message_chat_change_title_short), formatName(fromUser.firstName, fromUser.lastName));
                case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                    TdApi.MessageChatDeleteParticipant deletedParticipant = (TdApi.MessageChatDeleteParticipant)messageContent;
                    TdApi.User deletedUser = deletedParticipant.user;
                    if (fromUser.id == sCurrentUserId && fromUser.id == deletedUser.id) {
                        return String.format(sContext.getString(R.string.message_chat_left_part), sContext.getString(R.string.message_you));
                    } else if (fromUser.id == Utils.sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_del_part), sContext.getString(R.string.message_you), formatName(deletedUser.firstName, deletedUser.lastName));
                    } else if (fromUser.id == deletedUser.id) {
                        return String.format(sContext.getString(R.string.message_chat_left_part), formatName(deletedUser.firstName, deletedUser.lastName));
                    } else if (deletedUser.id == Utils.sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_del_part), formatName(fromUser.firstName, fromUser.lastName), sContext.getString(R.string.message_you).toLowerCase());
                    } else {
                        return String.format(sContext.getString(R.string.message_chat_del_part), formatName(fromUser.firstName, fromUser.lastName), formatName(deletedUser.firstName, deletedUser.lastName));
                    }
                case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                    if (message.isPost) {
                        return sContext.getString(R.string.message_channel_delete_photo);
                    }
                    if (fromUser.id == sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_delete_photo), sContext.getString(R.string.message_you));
                    }
                    return String.format(sContext.getString(R.string.message_chat_delete_photo), formatName(fromUser.firstName, fromUser.lastName));
                case TdApi.MessageChatJoinByLink.CONSTRUCTOR:
                    if (fromUser.id == sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_chat_join_by_link_short), sContext.getString(R.string.message_you));
                    }
                    return String.format(sContext.getString(R.string.message_chat_join_by_link_short), formatName(fromUser.firstName, fromUser.lastName));
                case TdApi.MessageContact.CONSTRUCTOR:
                    return sContext.getString(R.string.message_contact);
                case TdApi.MessageDeleted.CONSTRUCTOR:
                    return sContext.getString(R.string.deleted_message);
                case TdApi.MessageDocument.CONSTRUCTOR:
                    return sContext.getString(R.string.message_file);
                case TdApi.MessageLocation.CONSTRUCTOR:
                    return sContext.getString(R.string.message_location);
                case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
                    if (fromUser.id == sCurrentUserId) {
                        return String.format(sContext.getString(R.string.message_group_chat_create_short), sContext.getString(R.string.message_you));
                    }
                    return String.format(sContext.getString(R.string.message_group_chat_create_short), formatName(fromUser.firstName, fromUser.lastName));
                case TdApi.MessagePhoto.CONSTRUCTOR:
                    return sContext.getString(R.string.message_photo);
                case TdApi.MessageSticker.CONSTRUCTOR:
                    TdApi.Sticker sticker = ((TdApi.MessageSticker)messageContent).sticker;
                    return String.format("%s %s", sticker.emoji, sContext.getString(R.string.message_sticker));
                case TdApi.MessageVenue.CONSTRUCTOR:
                    return sContext.getString(R.string.message_location);
                case TdApi.MessageVideo.CONSTRUCTOR:
                    return sContext.getString(R.string.message_video);
                case TdApi.MessageVoice.CONSTRUCTOR:
                    return sContext.getString(R.string.message_voice);
            }
        }
        return "";
    }

    public static String stripExceptNumbers(String str) {
        StringBuilder res = new StringBuilder(str);
        String phoneChars = "0123456789";
        for (int i = res.length() - 1; i >= 0; i--) {
            if (!phoneChars.contains(res.substring(i, i + 1))) {
                res.deleteCharAt(i);
            }
        }
        return res.toString();
    }

    public static int getPhotoSize() {
        if (Build.VERSION.SDK_INT >= 16) {
            return 1280;
        }
        return 800;
    }

    public static ArrayList<TdApi.PhotoSize> generateThumbs(TdApi.Photo photo) {
        ArrayList<TdApi.PhotoSize> photoSizes = new ArrayList<>();
        Collections.addAll(photoSizes, photo.photos);
        Collections.sort(photoSizes, new Comparator<TdApi.PhotoSize>() {
            @Override
            public int compare(TdApi.PhotoSize lhs, TdApi.PhotoSize rhs) {
                int lMaxSize = Math.max(lhs.width, lhs.height);
                int rMaxSize = Math.max(rhs.width, rhs.height);
                if (lMaxSize > rMaxSize) {
                    return 1;
                } else if (lMaxSize < rMaxSize) {
                    return -1;
                }
                return 0;
            }
        });
        return photoSizes;
    }

    public static TdApi.PhotoSize getRightPhotoSize(ArrayList<TdApi.PhotoSize> photoSizes, int side) {
        if (photoSizes == null || photoSizes.isEmpty()) {
            return null;
        }
        int lastSide = 0;
        TdApi.PhotoSize photoSize = null;
        for (TdApi.PhotoSize size : photoSizes) {
            if (size == null) {
                continue;
            }
            int currentSide = Math.max(size.width, size.height);
            if (photoSize == null || currentSide <= side && lastSide < currentSide) {
                photoSize = size;
                lastSide = currentSide;
            }
        }
        return photoSize;
    }

    public static byte[] streamToByteArray(InputStream is) {
        ByteArrayOutputStream ous = new ByteArrayOutputStream();
        try {
            int read;
            byte[] buffer = new byte[16 * 1024];
            while ((read = is.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
            ous.flush();
        } catch (IOException e) {
            Log.w(TAG, "Error reading data from file", e);
            return null;
        }
        return ous.toByteArray();
    }

    public static byte[] fileToByteArray(File file) {
        try {
            InputStream is = new FileInputStream(file);
            return streamToByteArray(is);
        } catch (FileNotFoundException e) {
            Log.w(TAG, "File not found", e);
            return null;
        }
    }

    public static Bitmap decodeSticker(byte[] encoded) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            return BitmapFactory.decodeByteArray(encoded, 0, encoded.length);
        }
        return decodeWebP(encoded);
    }

    public static File createImageFile() {
        String timeStamp = FastDateFormat.getInstance("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), Utils.sContext.getString(R.string.app_name));
        if (!storageDir.exists()) {
            storageDir.mkdirs();
        }
        return new File(storageDir.getPath() + "/" + timeStamp + ".jpg");
    }

    public static void addImageToGallery(String path) {
        if (path == null) {
            return;
        }
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(path);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sContext.sendBroadcast(mediaScanIntent);
    }
}
