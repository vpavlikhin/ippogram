package org.tdlib.ippogram;

import android.graphics.drawable.Drawable;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ui.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

public class ChatsCache {

    public static final ArrayList<UpdatesHandler.UpdatesDelegate> sUpdatesDelegates = new ArrayList<>();

    public static ConcurrentHashMap<Long, TdApi.Chat> sChats = new ConcurrentHashMap<>(100, 1.0f, 2);
    public static ConcurrentHashMap<Integer, TdApi.User> sUsers = new ConcurrentHashMap<>(100, 1.0f, 2);
    public static ConcurrentHashMap<Integer, TdApi.Group> sGroups = new ConcurrentHashMap<>(100, 1.0f, 2);
    public static ConcurrentHashMap<Integer, TdApi.Channel> sChannels = new ConcurrentHashMap<>(100, 1.0f, 2);

    public static ConcurrentHashMap<Integer, TdApi.File> sFiles = new ConcurrentHashMap<>(100, 1.0f, 2);

    public static ConcurrentHashMap<Long, ArrayList<TdApi.User>> sUsersActions = new ConcurrentHashMap<>(100, 1.0f, 2);
    public static HashMap<Long, String> sActionStrings = new HashMap<>();

    //public static ConcurrentHashMap<Integer, ParcelableFile> sParcelableFiles = new ConcurrentHashMap<>(100, 1.0f, 2);

    public static ConcurrentHashMap<Integer, HashMap<Integer, Integer>> sDownloadFileStates = new ConcurrentHashMap<>(100, 1.0f, 1);

    public static ArrayList<TdApi.Chat> sChatsList = new ArrayList<>();
    public static ConcurrentHashMap<Long, Runnable> sChatsMuteRunnable = new ConcurrentHashMap<>(100, 1.0f, 1);

    public static HashMap<Long, Drawable> sUserAvatars = new HashMap<>();
    public static HashMap<Integer, TdApi.UserFull> sUsersFull = new HashMap<>();

    public static boolean sLoadingChats = false;
    public static boolean sChatsEndReached = false;
    public static long sOpenChatId;
    private static SoundPool sSoundPool;
    private static int sSound;
    private static long sLastSoundPlay = 0;

    public static MediaPlayer sMediaPlayer = null;
    public static int sPlayingFileId = 0;
    private static Runnable sActionRunnable = null;

    public ChatsCache() {
        addSupportUsers();

        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                sSoundPool = new SoundPool(1, AudioManager.STREAM_NOTIFICATION, 0);
            } else {
                AudioAttributes attrs = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION_COMMUNICATION_INSTANT)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build();
                sSoundPool = new SoundPool.Builder().setMaxStreams(1).setAudioAttributes(attrs).build();
            }
            sSound = sSoundPool.load(Utils.sContext, R.raw.sound_a, 1);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void reset() {
        sUsers.clear();
        sGroups.clear();
        sChatsList.clear();
        sLoadingChats = false;
        sChatsEndReached = false;
        addSupportUsers();
    }

    public static void addSupportUsers() {
        TdApi.User user = new TdApi.User();
        user.id = 333000;
        user.firstName = "Telegram";
        user.lastName = "";
        user.username = "";
        user.phoneNumber = "333";
        user.status = null;
        user.myLink = new TdApi.LinkStateKnowsPhoneNumber();
        user.foreignLink = new TdApi.LinkStateNone();
        user.type = new TdApi.UserTypeGeneral();
        sUsers.put(user.id, user);

        user = new TdApi.User();
        user.id = 777000;
        user.firstName = "Telegram";
        user.lastName = "";
        user.username = "";
        user.phoneNumber = "42777";
        user.status = null;
        user.myLink = new TdApi.LinkStateKnowsPhoneNumber();
        user.foreignLink = new TdApi.LinkStateNone();
        user.type = new TdApi.UserTypeGeneral();
        sUsers.put(user.id, user);
    }

    public static TdApi.User getUser(final int userId) {
        TdApi.User user = sUsers.get(userId);
        if (user == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetUser(userId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.User.CONSTRUCTOR) {
                            sUsers.put(userId, (TdApi.User) object);
                        }
                        latch.countDown();
                    }
                });
                latch.await();
                user = sUsers.get(userId);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return user;
    }

    public static TdApi.File getFile(final int fileId) {
        TdApi.File file = sFiles.get(fileId);
        if (file == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetFile(fileId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.File.CONSTRUCTOR) {
                            sFiles.put(fileId, (TdApi.File) object);
                        }
                        latch.countDown();
                    }
                });
                latch.await();
                file = sFiles.get(fileId);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return file;
    }

    @Deprecated
    public static void putUser(TdApi.User user) {
        if (user == null) {
            return;
        }
        TdApi.User currentUser = sUsers.get(user.id);
        if (currentUser == null) {
            currentUser = user;
            sUsers.put(currentUser.id, currentUser);
        } else {
            currentUser.firstName = user.firstName;
            currentUser.lastName = user.lastName;
            currentUser.username = user.username;
            currentUser.phoneNumber = user.phoneNumber;
            //currentUser.status = user.status;
            currentUser.profilePhoto = user.profilePhoto;
            currentUser.myLink = user.myLink;
            currentUser.foreignLink = user.foreignLink;
            currentUser.type = user.type;
        }
    }

    public static TdApi.Group getGroup(final int groupId) {
        TdApi.Group group = sGroups.get(groupId);
        if (group == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetGroup(groupId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.Group.CONSTRUCTOR) {
                            sGroups.put(groupId, (TdApi.Group) object);
                        }
                        latch.countDown();
                    }
                });
                latch.await();
                group = sGroups.get(groupId);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return group;
    }

    public static TdApi.Channel getChannel(final int channelId) {
        TdApi.Channel channel = sChannels.get(channelId);
        if (channel == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetChannel(channelId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.Channel.CONSTRUCTOR) {
                            sChannels.put(channelId, (TdApi.Channel) object);
                        }
                        latch.countDown();
                    }
                });
                latch.await();
                channel = sChannels.get(channelId);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return channel;
    }

    public static TdApi.Chat getChat(final long chatId) {
        TdApi.Chat chat = sChats.get(chatId);
        if (chat == null) {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetChat(chatId), new Client.ResultHandler() {
                    @Override
                    public void onResult(final TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                            sChats.put(chatId, (TdApi.Chat) object);
                            sChatsList.add((TdApi.Chat) object);

                            Collections.sort(sChatsList, new Comparator<TdApi.Chat>() {
                                @Override
                                public int compare(TdApi.Chat lhs, TdApi.Chat rhs) {
                                    if (lhs.order < rhs.order) {
                                        return 1;
                                    } else if (lhs.order > rhs.order) {
                                        return -1;
                                    } else {
                                        if (lhs.id < rhs.id) {
                                            return 1;
                                        } else if (lhs.id > rhs.id) {
                                            return -1;
                                        } else {
                                            return 0;
                                        }
                                    }
                                }
                            });
                        }
                        latch.countDown();
                    }
                });
                latch.await();
                chat = sChats.get(chatId);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return chat;
    }

    public static void addDelegate(UpdatesHandler.UpdatesDelegate delegate) {
        synchronized (sUpdatesDelegates) {
            if (!sUpdatesDelegates.contains(delegate)) {
                sUpdatesDelegates.add(delegate);
            }
        }
    }

    public static void removeDelegate(UpdatesHandler.UpdatesDelegate delegate) {
        synchronized (sUpdatesDelegates) {
            if (sUpdatesDelegates.contains(delegate)) {
                sUpdatesDelegates.remove(delegate);
            }
        }
    }

    /**********************************************************************************************/
    /*
     * Process updates
     */
    public static void updateInterface() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                delegate.updateInterface();
            }
        }
    }

    public static void updateNotifySettings() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.NotifyUpdatesDelegate) {
                    ((UpdatesHandler.NotifyUpdatesDelegate)delegate).updateNotifySettings();
                }
            }
        }
    }

    public static void updateMessageDate(long chatId, int messageId, int newDate) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.MessageUpdatesDelegate) {
                    ((UpdatesHandler.MessageUpdatesDelegate)delegate).updateMessageDate(chatId, messageId, newDate);
                }
            }
        }
    }

    public static void updateMessageId(long chatId, int oldId, int newId) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.MessageUpdatesDelegate) {
                    ((UpdatesHandler.MessageUpdatesDelegate)delegate).updateMessageId(chatId, oldId, newId);
                }
            }
        }
    }

    public static void updateMessageViews(long chatId, int messageId, int views) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.MessageUpdatesDelegate) {
                    ((UpdatesHandler.MessageUpdatesDelegate)delegate).updateMessageViews(chatId, messageId, views);
                }
            }
        }
    }

    public static void updateListsWithEmoji() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.MessageUpdatesDelegate) {
                    ((UpdatesHandler.MessageUpdatesDelegate)delegate).updateListsWithEmoji();
                }
            }
        }
    }

    public static void updateChatParts(int groupId, TdApi.GroupFull groupFull) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.GroupUpdatesDelegate) {
                    ((UpdatesHandler.GroupUpdatesDelegate)delegate).updateChatParts(groupId, groupFull);
                }
            }
        }
    }

    public static void updateUserInfo(int userId, boolean blocked, TdApi.BotInfo botInfo) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UserUpdatesDelegate) {
                    ((UpdatesHandler.UserUpdatesDelegate)delegate).updateUserInfo(userId, blocked, botInfo);
                }
            }
        }
    }

    public static void updateChatsList() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.ListUpdatesDelegate) {
                    ((UpdatesHandler.ListUpdatesDelegate)delegate).updateChatsList();
                }
            }
        }
    }

    public static void updateChannelInfo(int channelId, TdApi.ChannelFull channelFull) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.ChannelUpdatesDelegate) {
                    ((UpdatesHandler.ChannelUpdatesDelegate)delegate).updateChannelInfo(channelId, channelFull);
                }
            }
        }
    }

    public static void updateReadMessages() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForChatDelegate) {
                    ((UpdatesHandler.UpdatesForChatDelegate)delegate).updateReadMessages();
                }
            }
        }
    }

    public static void removeDeletedMessages(ArrayList<Integer> deletedMessages) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForChatDelegate) {
                    ((UpdatesHandler.UpdatesForChatDelegate)delegate).removeDeletedMessages(deletedMessages);
                }
            }
        }
    }

    public static void addNewMessages(long chatId, ArrayList<TdApi.Message> messages) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForChatDelegate) {
                    ((UpdatesHandler.UpdatesForChatDelegate)delegate).addNewMessages(chatId, messages);
                }
            }
        }
    }

    public static void updateMessageContent(long chatId, int messageId, TdApi.MessageContent newContent) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForChatDelegate) {
                    ((UpdatesHandler.UpdatesForChatDelegate)delegate).updateMessageContent(chatId, messageId, newContent);
                }
            }
        }
    }

    public static void updateContactsList() {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForContactsDelegate) {
                    ((UpdatesHandler.UpdatesForContactsDelegate)delegate).updateContactsList();
                }
            }
        }
    }

    public static void updateDownloadProgress(int fileId, int ready, int size) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.DownloadUpdatesDelegate) {
                    ((UpdatesHandler.DownloadUpdatesDelegate)delegate).updateDownloadProgress(fileId, ready, size);
                }
            }
        }
    }

    public static void updateDownloadedFile(int fileId) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.DownloadUpdatesDelegate) {
                    ((UpdatesHandler.DownloadUpdatesDelegate)delegate).updateDownloadedFile(fileId);
                }
            }
        }
    }

    /*public static void updateDownloadProgress(int fileId, int ready, int size) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.UpdatesForChatDelegate) {
                    ((UpdatesHandler.UpdatesForChatDelegate)delegate).updateDownloadProgress(fileId, ready, size);
                }
            }
        }
    }

    public static void updateDownloadedFile(int fileId) {
        synchronized (sUpdatesDelegates) {
            for (UpdatesHandler.UpdatesDelegate delegate : sUpdatesDelegates) {
                if (delegate instanceof UpdatesHandler.MessageUpdatesDelegate) {
                    ((UpdatesHandler.MessageUpdatesDelegate)delegate).updateDownloadedFile(fileId);
                }
            }
        }
    }*/

    /**********************************************************************************************/

    public static void playNotificationSound() {
        if (sLastSoundPlay > System.currentTimeMillis() - 1800) {
            return;
        }
        try {
            sLastSoundPlay = System.currentTimeMillis();
            sSoundPool.play(sSound, 1, 1, 1, 0, 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadGroupFull(final int groupId) {
        Utils.sClient.send(new TdApi.GetGroupFull(groupId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.GroupFull.CONSTRUCTOR) {
                    final TdApi.GroupFull groupFull = (TdApi.GroupFull) object;
                    for (TdApi.ChatParticipant participant : groupFull.participants) {
                        sUsers.putIfAbsent(participant.user.id, participant.user);
                    }
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            updateChatParts(groupId, groupFull);
                        }
                    });
                }
            }
        });
    }

    public static void loadChannelFull(final int channelId) {
        Utils.sClient.send(new TdApi.GetChannelFull(channelId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.ChannelFull.CONSTRUCTOR) {
                    final TdApi.ChannelFull channelFull = (TdApi.ChannelFull) object;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            updateChannelInfo(channelId, channelFull);
                        }
                    });
                }
            }
        });
    }

    public static void loadUserFull(final int userId) {
        Utils.sClient.send(new TdApi.GetUserFull(userId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.UserFull.CONSTRUCTOR) {
                    final TdApi.UserFull userFull = (TdApi.UserFull)object;
                    sUsersFull.put(userId, userFull);
                    sUsers.putIfAbsent(userId, userFull.user);
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUserInfo(userId, userFull.isBlocked, userFull.botInfo);
                        }
                    });
                }
            }
        });
    }

    public static void blockUser(final int userId) {
        Utils.sClient.send(new TdApi.BlockUser(userId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Ok.CONSTRUCTOR) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUserInfo(userId, true, null);
                        }
                    });
                }
            }
        });
    }

    public static void unblockUser(final int userId) {
        Utils.sClient.send(new TdApi.UnblockUser(userId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Ok.CONSTRUCTOR) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUserInfo(userId, false, null);
                        }
                    });
                }
            }
        });
    }

    public static void sendMessage(long chatId, ArrayList<String> photosPath) {
        if (photosPath.size() == 0) {
            return;
        }
        for (String path : photosPath) {
            sendMessage(chatId, null, 0, path);
        }
    }

    public static void sendMessage(long chatId, String message) {
        sendMessage(chatId, message, 0, null);
    }

    public static void sendMessage(long chatId, int userId) {
        sendMessage(chatId, null, userId, null);
    }

    public static void sendMessage(final long chatId, String message, int userId, String photoPath) {
        TdApi.InputMessageContent inMessage = null;
        if (message != null) {
            inMessage = new TdApi.InputMessageText(message, false, null);
        } else if (userId != 0) {
            TdApi.User user = getUser(userId);
            if (user != null) {
                inMessage = new TdApi.InputMessageContact(user.phoneNumber, user.firstName, user.lastName, userId);
            }
        } else if (photoPath != null) {
            TdApi.InputFile inputPhoto = new TdApi.InputFileLocal(photoPath);
            inMessage = new TdApi.InputMessagePhoto(inputPhoto, "");
        }

        if (inMessage == null) {
            return;
        }

        Utils.sClient.send(new TdApi.SendMessage(chatId, 0, false, false, false, new TdApi.ReplyMarkupNone(), inMessage), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Message.CONSTRUCTOR) {
                    final TdApi.Message newMessage = (TdApi.Message) object;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            ArrayList<TdApi.Message> messages = new ArrayList<>();
                            messages.add(newMessage);

                            updateListsWithMessages(chatId, messages);
                            updateChatsList();
                        }
                    });
                }
            }
        });
    }

    public static void updateListsWithMessages(final long chatId, ArrayList<TdApi.Message> messages) {
        int unreadCount = 0;
        int lastDate = 0;
        TdApi.Message lastMessage = null;
        TdApi.Chat chat = sChats.get(chatId);

        addNewMessages(chatId, messages);

        for (TdApi.Message message : messages) {
            if (sOpenChatId != chatId && message.fromId != Utils.sCurrentUserId) {
                ++unreadCount;
            }
            if (lastMessage == null || lastDate < message.date) {
                lastMessage = message;
                lastDate = message.date;
            }
        }

        if (chat != null && sChatsList.contains(chat)) {
            chat.topMessage = lastMessage;
            chat.unreadCount += unreadCount;
        } else {
            final CountDownLatch latch = new CountDownLatch(1);
            try {
                Utils.sClient.send(new TdApi.GetChat(chatId), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                            final TdApi.Chat newChat = (TdApi.Chat) object;
                            sChats.put(chatId, newChat);

                            sChatsList.clear();
                            sChatsList.addAll(sChats.values());
                        }
                        latch.countDown();
                    }
                });
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(sChatsList, new Comparator<TdApi.Chat>() {
            @Override
            public int compare(TdApi.Chat lhs, TdApi.Chat rhs) {
                if (lhs.order < rhs.order) {
                    return 1;
                } else if (lhs.order > rhs.order) {
                    return -1;
                } else {
                    if (lhs.id < rhs.id) {
                        return 1;
                    } else if (lhs.id > rhs.id) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        });
    }

    public static void updateChatTitle(long chatId, String title) {
        TdApi.Chat chat = sChats.get(chatId);
        if (chat != null) {
            chat.title = title;
        }
    }

    public static void updateChatPhoto(long chatId, TdApi.ChatPhoto photo) {
        TdApi.Chat chat = sChats.get(chatId);
        if (chat != null) {
            chat.photo = photo;
        }
    }

    public static void updateChatOrder(long chatId, long order) {
        TdApi.Chat chat = sChats.get(chatId);
        if (chat != null) {
            chat.order = order;
        }
    }

    public static void updateUserAction(final long chatId, int userId, TdApi.SendMessageAction action) {
        if (sActionRunnable != null) {
            Utils.cancelRunOnUIThread(sActionRunnable);
        }
        ArrayList<TdApi.User> users = sUsersActions.get(chatId);
        if (users == null) {
            users = new ArrayList<>();
            sUsersActions.put(chatId, users);
        }

        final TdApi.User user = getUser(userId);
        if (user != null) {
            if (action.getConstructor() == TdApi.SendMessageTypingAction.CONSTRUCTOR) {
                if (!users.contains(user)) {
                    users.add(user);
                }
            }
        }

        StringBuilder actionString = new StringBuilder();

        TdApi.Chat chat = sChats.get(chatId);
        if (chat.type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            if (action.getConstructor() == TdApi.SendMessageTypingAction.CONSTRUCTOR) {
                actionString.append(Utils.sContext.getString(R.string.typing_action));
            }
        } else if (chat.type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR ||
                chat.type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
            if (action.getConstructor() == TdApi.SendMessageTypingAction.CONSTRUCTOR) {
                int count  = 0;
                for (TdApi.User actionUser : users) {
                    if (count == 1) {
                        actionString.append(", ");
                    } else if (count == 2) {
                        break;
                    }
                    actionString.append(formatNameForAction(actionUser));
                    ++count;
                }
                if (users.size() == 1) {
                    actionString.append(" ").append(Utils.sContext.getString(R.string.typing_action_one));
                } else if (users.size() == 2) {
                    actionString.append(" ").append(Utils.sContext.getString(R.string.typing_action_two));
                } else {
                    actionString.append(" ").append(String.format(Utils.sContext.getString(R.string.typing_action_many), users.size() - 2));
                }
            }
        }

        sActionStrings.put(chatId, actionString.toString());

        sActionRunnable = new Runnable() {
            @Override
            public void run() {
                ArrayList<TdApi.User> internalUsers = sUsersActions.get(chatId);
                if (internalUsers != null && internalUsers.contains(user)) {
                    internalUsers.remove(user);
                    if (internalUsers.isEmpty()) {
                        sUsersActions.remove(chatId);
                        sActionStrings.remove(chatId);
                    }
                }
                updateInterface();
                Utils.cancelRunOnUIThread(sActionRunnable);
            }
        };
        Utils.runOnUIThread(sActionRunnable, 5900L);
    }

    public static String formatNameForAction(TdApi.User user) {
        if (user.firstName == null || user.firstName.length() == 0) {
            return user.lastName;
        } else if (user.firstName.length() != 0 && user.lastName.length() != 0) {
            return user.firstName;
        }
        return user.firstName;
    }

    @Deprecated
    public static void markChatAsRead(long chatId, int fromId, int offset, int limit) {
        if (fromId == Integer.MAX_VALUE && offset == 0) {
            return;
        }
        Utils.sClient.send(new TdApi.GetChatHistory(chatId, fromId, offset, limit), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });

        if (offset == 0) {
            TdApi.Chat chat = ChatsCache.sChats.get(chatId);
            if (chat != null) {
                chat.unreadCount = 0;
                updateChatsList();
            }
        }
    }

    public static void viewMessages(long chatId, ArrayList<TdApi.Message> messages) {
        int[] messagesIdsArray = new int[messages.size()];
        for (int i = 0; i < messages.size(); i++) {
            messagesIdsArray[i] = messages.get(i).id;
        }
        Utils.sClient.send(new TdApi.ViewMessages(chatId, messagesIdsArray), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });

        TdApi.Chat chat = ChatsCache.sChats.get(chatId);
        if (chat != null) {
            chat.unreadCount = 0;
            updateChatsList();
        }
    }

    public static void deleteMessages(long chatId, ArrayList<Integer> messagesIds) {
        removeDeletedMessages(messagesIds);
        TdApi.Chat chat = sChats.get(chatId);
        if (chat != null) {
            TdApi.Message message = chat.topMessage;
            if (message != null) {
                for (Integer messageId : messagesIds) {
                    if (message.id == messageId) {
                        message.content = new TdApi.MessageDeleted();
                    }
                }
            }
        }
    }

    public static void deleteChat(long chatId, boolean clearHistory) {
        TdApi.Chat chat = sChats.get(chatId);
        if (chat != null) {
            if (!clearHistory) {
                sChatsList.remove(chat);
                sChats.remove(chatId);
            } else {
                chat.unreadCount = 0;
            }
            chat.topMessage.id = 0;
        }
        updateChatsList();
        Utils.sClient.send(new TdApi.DeleteChatHistory(chatId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
    }

    public static void deleteUserFromChat(final long chatId, final int userId, final boolean deleteChat) {
        Utils.sClient.send(new TdApi.ChangeChatParticipantRole(chatId, userId, new TdApi.ChatParticipantRoleKicked()), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Ok.CONSTRUCTOR && userId == Utils.sCurrentUserId) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            if (deleteChat) {
                                deleteChat(chatId, false);
                            }
                        }
                    });
                }
            }
        });
    }

    public static void addUserToChat(long chatId, int userId, int forwardLimit) {
        Utils.sClient.send(new TdApi.AddChatParticipant(chatId, userId, forwardLimit), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
    }

    public static boolean isChatMuted(long chatId) {
        TdApi.Chat chat = sChats.get(chatId);
        TdApi.NotificationSettings notifySettings = chat.notificationSettings;
        return notifySettings.muteFor != 0;
    }

    public static void updateNotifySettingsForChat(long chatId) {
        updateNotifySettings();

        TdApi.Chat chat = sChats.get(chatId);
        Runnable muteRunnable = sChatsMuteRunnable.get(chatId);
        if (muteRunnable != null) {
            Utils.runOnUIThread(muteRunnable, chat.notificationSettings.muteFor * 1000L);
        }
        TdApi.SetNotificationSettings setNotifySettings = new TdApi.SetNotificationSettings();
        setNotifySettings.notificationSettings = new TdApi.NotificationSettings();
        setNotifySettings.notificationSettings.muteFor = chat.notificationSettings.muteFor;
        setNotifySettings.notificationSettings.sound = "default";
        setNotifySettings.notificationSettings.showPreviews = true;
        setNotifySettings.scope = new TdApi.NotificationSettingsForChat(chatId);
        Utils.sClient.send(setNotifySettings, new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
    }
}
