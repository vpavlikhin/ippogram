package org.tdlib.ippogram.ui;

import android.content.Intent;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;

import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Utils;

public class MainActivity extends AppCompatActivity {

    private FragmentManager mFragmentManager;
    private boolean mReceiversRegistered;

    private Runnable mAutoLockRunnable = new Runnable() {
        @Override
        public void run() {
            Utils.sLocked = true;
            Utils.savePasscode(MainActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_layout);
        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);

        if (mFragmentManager == null) {
            mFragmentManager = getFragmentManager();
        }

        if (mFragmentManager.getBackStackEntryCount() == 0) {
            ChatsFragment chatsFragment = new ChatsFragment();
            chatsFragment.onPreCreate();
            /*Bundle args = new Bundle();
            args.putBoolean("select_chat", false);
            chatsFragment.setArguments(args);*/
            chatsFragment.setAddedToBackStack(true);
            mFragmentManager.beginTransaction().addToBackStack(null).add(R.id.flContainer, chatsFragment, "chats").commitAllowingStateLoss();
        }

        //registerReceiver();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Utils.cancelRunOnUIThread(mAutoLockRunnable);
        if (Utils.sPasscodeHash.length() > 0 && Utils.sLocked) {
            createFragment(EnterPasscodeFragment.newInstance(Utils.sPasscodeType, true), "unlock_passcode", true, true);
            Utils.sLocked = false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.runOnUIThread(mAutoLockRunnable, Utils.sAutoLock * 1000L);
    }

    /*private void registerReceiver() {
        unregisterReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Utils.ACTION_PROGRESS_UPDATE);
        intentFilter.addAction(Utils.ACTION_CANCEL_DOWNLOAD);
        Utils.sLocalBroadcastManager.registerReceiver(mDownloadingReceiver, intentFilter);
        mReceiversRegistered = true;
    }

    private void unregisterReceiver() {
        if (mReceiversRegistered) {
            Utils.sLocalBroadcastManager.unregisterReceiver(mDownloadingReceiver);
            mReceiversRegistered = false;
        }
    }

    private BroadcastReceiver mDownloadingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Utils.ACTION_PROGRESS_UPDATE)) {
                int fileId = intent.getIntExtra("file_id", 0);
                int ready = intent.getIntExtra("ready", 0);
                int size = intent.getIntExtra("size", 0);
                ChatsCache.updateDownloadProgress(fileId, ready, size);
            }
        }
    };*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void logout() {
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (mFragmentManager == null) {
            return;
        }
        if (mFragmentManager.getBackStackEntryCount() != 0) {
            MainFragment mainFragment = (MainFragment)mFragmentManager.findFragmentById(R.id.flContainer);
            if (mainFragment != null) {
                if (mainFragment.isAddedToBackStack() && (mainFragment.getTag().equals("unlock_passcode") || mainFragment.getTag().equals("chats"))) {
                    if (mainFragment.onBackPressed()) {
                        finish();
                        return;
                    }
                }
                if (mainFragment.onBackPressed()) {
                    destroyFragment(mainFragment);
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (ChatsCache.sMediaPlayer != null) {
            ChatsCache.sMediaPlayer.release();
            ChatsCache.sMediaPlayer = null;
        }
        //unregisterReceiver();
    }

    public void createFragment(MainFragment fragment, String tag, boolean notRemoveLast) {
        createFragment(fragment, tag, notRemoveLast, true, true, true);
    }

    public void createFragment(MainFragment fragment, String tag, boolean notRemoveLast, boolean addToBackStack) {
        createFragment(fragment, tag, notRemoveLast, addToBackStack, true, true);
    }

    public void createFragment(MainFragment fragment, String tag, boolean notRemoveLast, boolean addToBackStack, boolean enterAnimated, boolean exitAnimated) {
        if (getCurrentFocus() != null) {
            Utils.hideKeyboard(getCurrentFocus());
        }
        fragment.onPreCreate();
        fragment.withAnimation(enterAnimated, exitAnimated);
        fragment.setAddedToBackStack(addToBackStack);
        if (mFragmentManager == null) {
            return;
        }
        if (!notRemoveLast) {
            mFragmentManager.popBackStack();
        }
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        if (addToBackStack) {
            ft.addToBackStack(null);
        }
        ft.add(R.id.flContainer, fragment, tag).commitAllowingStateLoss();
    }

    public void destroyFragment(MainFragment fragment) {
        if (getCurrentFocus() != null) {
            Utils.hideKeyboard(getCurrentFocus());
        }
        if (mFragmentManager == null) {
            return;
        }
        if (fragment.isAddedToBackStack()) {
            mFragmentManager.popBackStack();
        }
        getFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        fragment.onDestroy();
    }

    public void destroyFragment(String tag) {
        if (getCurrentFocus() != null) {
            Utils.hideKeyboard(getCurrentFocus());
        }
        if (mFragmentManager == null) {
            return;
        }
        MainFragment fragment = (MainFragment)mFragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            if (fragment.isAddedToBackStack()) {
                mFragmentManager.popBackStack();
            }
            getFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
            fragment.onDestroy();
        }
    }
}
