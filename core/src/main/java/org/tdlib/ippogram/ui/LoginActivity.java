package org.tdlib.ippogram.ui;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.app.FragmentManager;
import android.app.Fragment;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.SmsListener;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.NonSwipeableViewPager;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements Client.ResultHandler, SmsListener.OnSmsListener {

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private SlideFragment mCurrentFragment;
    private HashMap<String, Object> mParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Utils.sSmsListener = this;

        Utils.sContext = getApplicationContext();

        mViewPager = (NonSwipeableViewPager)findViewById(R.id.viewPager);
        mPagerAdapter = new SlideFragmentPagerAdapter(getFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
    }

    private class SlideFragmentPagerAdapter extends FragmentStatePagerAdapter {

        public SlideFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Nullable
        @Override
        public Fragment getItem(int position) {
            SlideFragment slideFragment = null;
            switch (position) {
                case 0:
                    slideFragment = new PhoneFragment();
                    break;
                case 1:
                    slideFragment = new NameFragment();
                    break;
                case 2:
                    slideFragment = new SmsFragment();
                    break;
                case 3:
                    slideFragment = PasswordFragment.newInstance(false);
                    break;
                case 4:
                    slideFragment = PasswordFragment.newInstance(true);
                    break;
            }
            if (slideFragment != null) {
                slideFragment.setParentActivity(LoginActivity.this);
            }
            return slideFragment;
        }

        @Override
        public int getCount() {
            return 5;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            SlideFragment slideFragment = (SlideFragment)object;
            if (mCurrentFragment != slideFragment) {
                mCurrentFragment = slideFragment;
                if (mParams != null) {
                    mCurrentFragment.setParams(mParams);
                    mParams = null;
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        if (mCurrentFragment != null) {
            mCurrentFragment.onResult(object);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utils.sSmsListener = null;
    }

    @Override
    public void onBackPressed() {
        if (mViewPager.getCurrentItem() == 0) {
            if (mCurrentFragment != null) {
                if (!getFragmentManager().popBackStackImmediate()) {
                    super.onBackPressed();
                } else {
                    mCurrentFragment.onBackPressed();
                }
            }
        } else {
            if (mCurrentFragment != null) {
                mCurrentFragment.onBackPressed();
            }
            if (mViewPager.getCurrentItem() == 2 || mViewPager.getCurrentItem() == 3) {
                mViewPager.setCurrentItem(0);
            } else {
                mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
            }
        }
    }

    public void sendParams(HashMap<String, Object> params) {
        mParams = params;
    }

    public void onNextAction() {
        mCurrentFragment.setParentActivity(this);
        mCurrentFragment.onDonePressed();
    }

    public void slidePager(int item, boolean smoothScroll) {
        mViewPager.setCurrentItem(item, smoothScroll);
    }

    public void showAlert(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setMessage(message);
                builder.setPositiveButton(Utils.sContext.getString(R.string.ok_button), null);
                AlertDialog alertDialog = builder.show();
                alertDialog.setCanceledOnTouchOutside(true);
                Button okButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                okButton.setTypeface(Utils.getTypeface("medium"));
                okButton.setTextColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
            }
        });
    }

    public void showProgress() {
        Utils.showProgressDialog(this, getResources().getString(R.string.loading));
    }

    public void hideProgress() {
        Utils.hideProgressDialog(this);
    }

    public void send(TdApi.TLFunction function) {
        if (Utils.sClient != null) {
            Utils.sClient.send(function, this);
        }
    }

    public void finishActivity() {
        Intent intent = new Intent(this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSmsReceived(String code) {
        if (mCurrentFragment != null) {
            mCurrentFragment.onSmsReceived(code);
        }
    }
}
