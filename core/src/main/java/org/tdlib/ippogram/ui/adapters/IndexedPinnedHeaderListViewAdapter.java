package org.tdlib.ippogram.ui.adapters;

import android.view.View;
import android.widget.TextView;

import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;

public abstract class IndexedPinnedHeaderListViewAdapter extends BasePinnedHeaderListViewAdapter {

    private int mPinnedHeaderBackgroundColor;
    private int mPinnedHeaderTextColor;

    public void setPinnedHeaderBackgroundColor(int pinnedHeaderBackgroundColor) {
        mPinnedHeaderBackgroundColor = pinnedHeaderBackgroundColor;
    }

    public void setPinnedHeaderTextColor(int pinnedHeaderTextColor) {
        mPinnedHeaderTextColor = pinnedHeaderTextColor;
    }

    @Override
    public CharSequence getSectionTitle(int sectionIndex) {
        return getSections()[sectionIndex].toString();
    }

    @Override
    public void configurePinnedHeader(View header, int position, int alpha) {
        TextView headerView = (TextView)header;
        int sectionIndex = getSectionForPosition(position);
        Object[] sections = getSections();
        if (sections != null && sections.length != 0) {
            CharSequence title = getSectionTitle(sectionIndex);
            headerView.setText(title);
        }
        headerView.setTypeface(Utils.getTypeface("medium"));
        headerView.setBackgroundColor(mPinnedHeaderBackgroundColor);
        headerView.setTextColor(mPinnedHeaderTextColor);
        headerView.setAlpha(alpha / 255.0f);
    }

    public class ViewHolder {
        public TextView letterSectionTextView;

        public ViewHolder(View itemView) {
            letterSectionTextView = (TextView) itemView.findViewById(R.id.letterSectionTextView);
            letterSectionTextView.setTypeface(Utils.getTypeface("medium"));
        }
    }
}
