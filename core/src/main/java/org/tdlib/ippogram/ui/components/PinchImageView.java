package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;

public class PinchImageView extends ImageView {

    //wrapped motion event code.
    protected static final int ACTION_INIT = 100;
    protected static final int ACTION_SCALE = 1001;
    protected static final int ACTION_TRANSLATE = 1002;
    protected static final int ACTION_SCALE_TO_TRANSLATE = 1003;
    protected static final int ACTION_TRANSLATE_TO_SCALE = 1004;
    protected static final int ACTION_FIT_CENTER = 1005;
    protected static final int ACTION_CANCEL = -1;

    private final static float MAX_SCALE_TO_SCREEN = 2.f;
    private final static float MIN_SCALE_TO_SCREEN = 1.f;

    private static final long DOUBLE_TAP_MARGIN_TIME = 200;
    private static final float MIN_SCALE_SPAN = 10.f;

    //calculated min / max scale ratio based on image & screen size.
    private float mMinScaleFactor = 1.f;
    private float mMaxScaleFactor = 2.f;

    private boolean mIsFirstDraw = true;	//check flag to calculate necessary init values.
    private int mImageWidth;	//current set image width
    private int mImageHeight;	//current set image height

    public PinchImageView(Context context) {
        super(context);
        init();
    }

    public PinchImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PinchImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setScaleType(ScaleType.MATRIX);
        Matrix matrix = getImageMatrix();
        matrix.reset();
        setImageMatrix(matrix);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (bm != null) {
            mImageWidth = bm.getWidth();
            mImageHeight = bm.getHeight();
        }
        fitCenter();
        calculateScaleFactorLimit();
    }

    @Override
    public void setImageURI(Uri uri) {
        try {
            InputStream inputStream = getContext().getContentResolver().openInputStream(uri);
            setImageBitmap(BitmapFactory.decodeStream(inputStream));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mIsFirstDraw) {
            mIsFirstDraw = false;
            fitCenter();
            calculateScaleFactorLimit();
        }
        setImageMatrix(mCurrentMatrix);
        canvas.drawRGB(0, 0, 0);

        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mIsFirstDraw = true;
    }

    private void calculateScaleFactorLimit() {
        //set max / min scale factor.
        mMaxScaleFactor = Math.max(getHeight() * MAX_SCALE_TO_SCREEN / mImageHeight, getWidth() * MAX_SCALE_TO_SCREEN / mImageWidth);
        mMinScaleFactor = Math.min(getHeight() * MIN_SCALE_TO_SCREEN / mImageHeight, getWidth() * MIN_SCALE_TO_SCREEN / mImageWidth);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = parseMotionEvent(event);
        switch (action) {
            case ACTION_INIT:
                initGestureAction(event.getX(), event.getY());
                break;
            case ACTION_SCALE:
                handleScale(event);
                break;
            case ACTION_TRANSLATE:
                handleTranslate(event);
                break;
            case ACTION_TRANSLATE_TO_SCALE:
                initGestureAction(event.getX(), event.getY());
                break;
            case ACTION_SCALE_TO_TRANSLATE:
                int activeIndex = (event.getActionIndex() == 0 ? 1 : 0);
                initGestureAction(event.getX(activeIndex), event.getY(activeIndex));
                break;
            case ACTION_FIT_CENTER:
                fitCenter();
                initGestureAction(event.getX(), event.getY());
                break;
            case ACTION_CANCEL:
                break;
        }

        //check current position of bitmap.
        validateMatrix();
        updateMatrix();
        return true; // indicate event was handled
    }

    private int parseMotionEvent(MotionEvent ev) {
        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                if (isDoubleTap(ev)) {
                    return ACTION_FIT_CENTER;
                }
                return ACTION_INIT;
            case MotionEvent.ACTION_POINTER_DOWN:
                //more than one pointer is pressed...
                return ACTION_TRANSLATE_TO_SCALE;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                if (ev.getPointerCount() == 2) {
                    return ACTION_SCALE_TO_TRANSLATE;
                }
                return ACTION_INIT;
            case MotionEvent.ACTION_MOVE:
                if (ev.getPointerCount() == 1) {
                    return ACTION_TRANSLATE;
                } else if (ev.getPointerCount() == 2) {
                    return ACTION_SCALE;
                }
                return 0;
        }
        return 0;
    }

    /////////////////////////////////////////////////
    // Related matrix calculation stuffs.
    /////////////////////////////////////////////////

    private Matrix mCurrentMatrix = new Matrix();
    private Matrix mSavedMatrix = new Matrix();

    // Remember some things for zooming
    private PointF mStartPoint = new PointF();
    private PointF mMidPoint = new PointF();
    private float mInitScaleSpan = 1f;

    protected void initGestureAction(float x, float y) {
        mSavedMatrix.set(mCurrentMatrix);
        mStartPoint.set(x, y);
        mInitScaleSpan = 0.f;
    }

    /**
     * check user double tapped this view.. or not.
     */
    private long mLastTouchDownTime = 0;
    protected boolean isDoubleTap(MotionEvent ev) {
        //if old pointer is tapped?
        if (ev.getPointerCount() > 1) {
            //if there are more than one pointer... reset
            mLastTouchDownTime = 0;
            return false;
        }

        long downTime = ev.getDownTime();
        long diff = downTime - mLastTouchDownTime;
        mLastTouchDownTime = downTime;

        return diff < DOUBLE_TAP_MARGIN_TIME;
    }

    protected void handleScale(MotionEvent event) {
        float newSpan = spacing(event);

        //if two finger is too close, pointer index is bumped.. so just ignore it.
        if (newSpan < MIN_SCALE_SPAN) {
            return;
        }

        if (mInitScaleSpan == 0.f) {
            //init values. scale gesture action is just started.
            mInitScaleSpan = newSpan;
            midPoint(mMidPoint, event);
        } else {
            float scale = normalizeScaleFactor(mSavedMatrix, newSpan, mInitScaleSpan);
            mCurrentMatrix.set(mSavedMatrix);
            mCurrentMatrix.postScale(scale, scale, mMidPoint.x, mMidPoint.y);
        }
    }

    private float normalizeScaleFactor(Matrix curMat, float newSpan, float stdSpan) {

        float values[] = new float[9];
        curMat.getValues(values);
        float scale = values[Matrix.MSCALE_X];

        if (stdSpan == newSpan) {
            return scale;
        } else {
            float newScaleFactor = newSpan / stdSpan;
            float candinateScale = scale * newScaleFactor;

            if (candinateScale > mMaxScaleFactor) {
                return mMaxScaleFactor / scale;
            } else if (candinateScale < mMinScaleFactor) {
                return mMinScaleFactor / scale;
            } else {
                return newScaleFactor;
            }
        }
    }

    protected void handleTranslate(MotionEvent event) {
        mCurrentMatrix.set(mSavedMatrix);
        mCurrentMatrix.postTranslate(event.getX() - mStartPoint.x, event.getY() - mStartPoint.y);
    }

    private RectF mTranslateLimitRect = new RectF(); //reuse instance.
    private void validateMatrix() {
        float values[] = new float[9];
        mCurrentMatrix.getValues(values);

        //get current matrix values.
        float scale = values[Matrix.MSCALE_X];
        float tranX = values[Matrix.MTRANS_X];
        float tranY = values[Matrix.MTRANS_Y];

        int imageHeight = (int) (scale * mImageHeight);
        int imageWidth = (int) (scale * mImageWidth);

        mTranslateLimitRect.setEmpty();
        //don't think about optimize code. first, just write code case by case.

        //check TOP & BOTTOM
        if (imageHeight > getHeight()) {
            //image height is taller than view
            mTranslateLimitRect.top = getHeight() - imageHeight - getPaddingTop() - getPaddingBottom();
            mTranslateLimitRect.bottom = 0.f;
        } else {
            mTranslateLimitRect.top = mTranslateLimitRect.bottom = (getHeight() - imageHeight - getPaddingTop() - getPaddingBottom() ) / 2.f;
        }

        //check LEFT & RIGHT
        if (imageWidth > getWidth()) {
            //image width is longer than view
            mTranslateLimitRect.left = getWidth() - imageWidth - getPaddingRight() - getPaddingLeft();
            mTranslateLimitRect.right = 0.f;
        } else {
            mTranslateLimitRect.left = mTranslateLimitRect.right = (getWidth() - imageWidth - getPaddingLeft() - getPaddingRight()) / 2.f;
        }

        float newTranX = tranX;
        newTranX = Math.max(newTranX, mTranslateLimitRect.left);
        newTranX = Math.min(newTranX, mTranslateLimitRect.right);

        float newTranY = tranY;
        newTranY = Math.max(newTranY, mTranslateLimitRect.top);
        newTranY = Math.min(newTranY, mTranslateLimitRect.bottom);

        values[Matrix.MTRANS_X] = newTranX;
        values[Matrix.MTRANS_Y] = newTranY;
        mCurrentMatrix.setValues(values);

        if (!mTranslateLimitRect.contains(tranX, tranY)) {
            //set new start point.
            mStartPoint.offset(tranX - newTranX, tranY - newTranY);
        }
    }

    protected void updateMatrix() {
        setImageMatrix(mCurrentMatrix);
    }

    public void fitCenter() {
        //move image to center....
        mCurrentMatrix.reset();

        float scaleX = (getWidth() - getPaddingLeft() - getPaddingRight()) / (float)mImageWidth;
        float scaleY = (getHeight() - getPaddingTop() - getPaddingBottom()) / (float)mImageHeight;
        float scale = Math.min(scaleX, scaleY);

        float dx = (getWidth() - getPaddingLeft() - getPaddingRight() - mImageWidth * scale) / 2.f;
        float dy = (getHeight() - getPaddingTop() - getPaddingBottom() - mImageHeight * scale) / 2.f;

        mCurrentMatrix.postScale(scale, scale);
        mCurrentMatrix.postTranslate(dx, dy);
        setImageMatrix(mCurrentMatrix);
    }

    /** Determine the space between the first two fingers */
    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

    /** Calculate the mid point of the first two fingers */
    private void midPoint(PointF point, MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        point.set(x / 2, y / 2);
    }
}
