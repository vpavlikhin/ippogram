package org.tdlib.ippogram.ui.components;

import android.widget.SectionIndexer;

public class SectionedSectionIndexer implements SectionIndexer {

    private SimpleSection[] mSectionArray;

    public SectionedSectionIndexer(SimpleSection[] sections) {
        mSectionArray = sections;
        int previousIndex = -1;
        for (SimpleSection simpleSection : mSectionArray) {
            simpleSection.startIndex = ++previousIndex;
            previousIndex += simpleSection.getItemsCount();
            simpleSection.endIndex = previousIndex;
        }
        mSectionArray[mSectionArray.length - 1].endIndex -= 1;
    }

    @Override
    public SimpleSection[] getSections() {
        return mSectionArray;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return sectionIndex < 0 || sectionIndex >= mSectionArray.length ? -1 : mSectionArray[sectionIndex].startIndex;
    }

    /** given a flat position, returns the position within the section */
    public int getPositionInSection(int position) {
        int sectionForPosition = getSectionForPosition(position);
        SimpleSection simpleSection = mSectionArray[sectionForPosition];
        return position - simpleSection.startIndex;
    }

    @Override
    public int getSectionForPosition(int position) {
        if (position < 0) {
            return -1;
        }
        int start = 0;
        int end = mSectionArray.length - 1;
        int piv = (start + end) / 2;
        while (true) {
            SimpleSection section = mSectionArray[piv];
            if (position >= section.startIndex && position <= section.endIndex) {
                return piv;
            }
            if (piv == start && start == end) {
                return -1;
            }
            if (position < section.startIndex) {
                end = piv - 1;
            } else {
                start = piv + 1;
            }
            piv = (start + end) / 2;
        }
    }

    public Object getItem(int position) {
        int sectionIndex = getSectionForPosition(position);
        SimpleSection section = mSectionArray[sectionIndex];
        return section.getItem(position - section.startIndex);
    }

    public Object getItem(int sectionIndex, int positionInSection) {
        SimpleSection section = mSectionArray[sectionIndex];
        return section.getItem(positionInSection);
    }

    public int getRawPosition(int sectionIndex, int positionInSection) {
        SimpleSection section = mSectionArray[sectionIndex];
        return section.startIndex + positionInSection;
    }

    public int getItemsCount() {
        if (mSectionArray.length == 0) {
            return 0;
        }
        return mSectionArray[mSectionArray.length - 1].endIndex + 1;
    }

    public static abstract class SimpleSection {

        private String name;
        private int startIndex, endIndex;

        public SimpleSection() {

        }

        public SimpleSection(String sectionName) {
            this.name = sectionName;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public abstract int getItemsCount();

        public abstract Object getItem(int posInSection);

        @Override
        public String toString() {
            return name;
        }
    }
}
