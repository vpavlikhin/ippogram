package org.tdlib.ippogram.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class AbsDrawerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter {

    public static final int TYPE_HEADER = Integer.MAX_VALUE;
    private int mSizeDiff = 1;

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return getMainItemType(position - mSizeDiff);
    }

    protected abstract int getMainItemCount();

    protected abstract int getMainItemType(int position);

    public void notifyHeaderChanged() {
        notifyItemChanged(0);
    }

    public void notifyMainItemChanged(int position) {
        notifyItemChanged(position + mSizeDiff);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            return onCreateHeaderViewHolder(layoutInflater, parent);
        }
        return onCreateMainViewHolder(layoutInflater, parent, viewType);
    }

    protected abstract HeaderHolder onCreateHeaderViewHolder(LayoutInflater inflater, ViewGroup parent);

    protected abstract VH onCreateMainViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType);

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == TYPE_HEADER) {
            onBindHeaderViewHolder((HeaderHolder)holder);
            return;
        }
        onBindMainViewHolder((VH)holder, position - mSizeDiff);
    }

    @Override
    public int getItemCount() {
        return getMainItemCount() + mSizeDiff;
    }

    protected abstract <HH extends HeaderHolder> void onBindHeaderViewHolder(HH holder);

    protected abstract void onBindMainViewHolder(VH holder, int position);

    protected static class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
}
