package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class SizeChangingRelativeLayout extends RelativeLayout {

    public SizeChangingRelativeLayoutDelegate delegate;

    public SizeChangingRelativeLayout(Context context) {
        super(context);
    }

    public SizeChangingRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SizeChangingRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (delegate != null) {
            delegate.onSizeChanged(w, h, oldw, oldh);
        }
    }

    public interface SizeChangingRelativeLayoutDelegate {
        void onSizeChanged(int w, int h, int oldw, int oldh);
    }
}
