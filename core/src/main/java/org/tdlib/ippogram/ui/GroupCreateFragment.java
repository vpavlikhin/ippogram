package org.tdlib.ippogram.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GroupCreateFragment extends MainFragment implements UpdatesHandler.UpdatesForContactsDelegate {

    private ImageButton mDoneImageButton;

    private EditText mEnterTitleField;

    private ListView mMembersListView;
    private MembersAdapter mMembersAdapter;

    private ArrayList<Integer> mCheckedUsers;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            updateVisibleItems();
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        mCheckedUsers = getArguments().getIntegerArrayList("checked_users");
        Utils.runOnUIThread(runnable, 1000L);
        ChatsCache.addDelegate(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_group_create, container, false);

        mToolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(GroupCreateFragment.this);
            }
        });

        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(Utils.sContext.getString(R.string.new_group));

        mDoneImageButton = (ImageButton)mRootView.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEnterTitleField.getText().length() == 0) {
                    return;
                }
                TdApi.CreateNewGroupChat createNewGroupChat = new TdApi.CreateNewGroupChat();
                createNewGroupChat.title = mEnterTitleField.getText().toString();
                createNewGroupChat.participantIds = new int[mCheckedUsers.size()];
                for (int i = 0; i < mCheckedUsers.size(); i++) {
                    createNewGroupChat.participantIds[i] = mCheckedUsers.get(i);
                }
                Utils.showProgressDialog(getParentActivity(), Utils.sContext.getString(R.string.loading));
                Utils.sClient.send(createNewGroupChat, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                            final TdApi.Chat chat = (TdApi.Chat)object;
                            Utils.runOnUIThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!ChatsCache.sChats.containsKey(chat.id)) {
                                        ChatsCache.sChats.put(chat.id, chat);

                                        ChatsCache.sChatsList.clear();
                                        ChatsCache.sChatsList.addAll(ChatsCache.sChats.values());

                                        Collections.sort(ChatsCache.sChatsList, new Comparator<TdApi.Chat>() {
                                            @Override
                                            public int compare(TdApi.Chat lhs, TdApi.Chat rhs) {
                                                if (lhs.topMessage.date == rhs.topMessage.date) {
                                                    return 0;
                                                } else if (lhs.topMessage.date < rhs.topMessage.date) {
                                                    return 1;
                                                } else {
                                                    return -1;
                                                }
                                            }
                                        });
                                    }
                                    ChatFragment chatFragment = new ChatFragment();
                                    Bundle args = new Bundle();
                                    args.putLong("chat_id", chat.id);
                                    TdApi.GroupChatInfo groupChatInfo = (TdApi.GroupChatInfo) chat.type;
                                    args.putInt("group_chat_id", groupChatInfo.group.id);
                                    chatFragment.setArguments(args);
                                    ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + chat.id, true);
                                    Utils.hideProgressDialog(getParentActivity());
                                }
                            });
                        }
                    }
                });
            }
        });

        mEnterTitleField = (EditText)mRootView.findViewById(R.id.enterTitleField);
        mEnterTitleField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });

        mMembersListView = (ListView)mRootView.findViewById(R.id.membersListView);
        mMembersListView.setVerticalScrollBarEnabled(false);

        mMembersListView.setAdapter(mMembersAdapter = new MembersAdapter());
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }

        if (mMembersAdapter != null) {
            mMembersAdapter.notifyDataSetChanged();
        }

        if (mEnterTitleField != null) {
            mEnterTitleField.requestFocus();
            Utils.showKeyboard(mEnterTitleField);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(runnable);
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }

    @Override
    public void updateInterface() {
        if (mMembersListView != null) {
            mMembersListView.invalidateViews();
        }
    }

    @Override
    public void updateContactsList() {
        if (mMembersAdapter != null) {
            mMembersAdapter.notifyDataSetChanged();
        }
    }

    private void updateVisibleItems() {
        if (mMembersListView != null) {
            int count = mMembersListView.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = mMembersListView.getChildAt(i);
                Object tag = child.getTag();
                if (tag instanceof MembersAdapter.ViewHolder) {
                    ((MembersAdapter.ViewHolder)tag).updateItem();
                }
            }
        }
    }

    private class MembersAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return mCheckedUsers.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.subheader_listview, parent, false);
                }
                TextView subHeaderTextView = (TextView)convertView.findViewById(R.id.subHeaderTextView);
                subHeaderTextView.setTypeface(Utils.getTypeface("medium"));
                if (mCheckedUsers.size() > 1) {
                    subHeaderTextView.setText(String.format("%d %s", mCheckedUsers.size(), Utils.sContext.getString(R.string.group_members)));
                } else {
                    subHeaderTextView.setText(String.format("%d %s", mCheckedUsers.size(), Utils.sContext.getString(R.string.group_member)));
                }
            } else if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.member_list_item, parent, false);
                }
                ViewHolder viewHolder = (ViewHolder)convertView.getTag();
                if (viewHolder == null) {
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }

                viewHolder.user = ChatsCache.getUser(mCheckedUsers.get(position - 1));
                viewHolder.updateItem();
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return 0;
            }
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 2;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        private class ViewHolder {
            public final BlockingImageView avatarImageView;
            public final TextView primaryTextView;
            public final TextView secondaryTextView;
            public TdApi.User user;

            public ViewHolder(View itemView) {
                avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
                primaryTextView = (TextView)itemView.findViewById(R.id.primaryTextView);
                secondaryTextView = (TextView)itemView.findViewById(R.id.secondaryTextView);
            }

            public void updateItem() {
                avatarImageView.setProfilePhoto(user.profilePhoto, Utils.getUserAvatar(user, 48));
                primaryTextView.setText(Utils.formatName(user.firstName, user.lastName));
                String userStatus = Utils.formatUserStatus(user);
                switch (userStatus) {
                    case "online":
                        secondaryTextView.setTextColor(0xff6b9cc2);
                        break;
                    default:
                        secondaryTextView.setTextColor(0xff8a8a8a);
                }
                secondaryTextView.setText(userStatus);
            }
        }
    }
}
