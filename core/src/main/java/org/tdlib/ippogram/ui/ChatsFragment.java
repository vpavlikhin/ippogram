package org.tdlib.ippogram.ui;

import android.animation.Animator;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.adapters.ChatsAdapter;
import org.tdlib.ippogram.ui.adapters.DrawerAdapter;
import org.tdlib.ippogram.ui.components.DividerItemDecoration;
import org.tdlib.ippogram.ui.components.EmptyRecyclerView;
import org.tdlib.ippogram.ui.components.FloatingActionButton;
import org.tdlib.ippogram.ui.components.FloatingActionMenu;

import java.util.Collections;
import java.util.Comparator;

public class ChatsFragment extends MainFragment implements DrawerAdapter.OnItemClickListener,
        UpdatesHandler.ListUpdatesDelegate, UpdatesHandler.UpdatesForContactsDelegate, UpdatesHandler.DownloadUpdatesDelegate {

    private DrawerLayout mDrawerLayout;
    private RecyclerView mDrawerListView;
    private DrawerAdapter mDrawerAdapter;

    private ImageButton mLockImageButton;

    private EmptyRecyclerView mChatsListView;
    private LinearLayoutManager mLayoutManager;
    private ChatsAdapter mChatsAdapter;
    private FloatingActionMenu mAddFloatingMenu;

    private View mLoadingChatsProgressBar;
    private LinearLayout mEmptyLayout;

    private String[] mDrawerMenuItems;

    private boolean mChatsLoaded = false;
    private BroadcastReceiver mNetworkReceiver;

    private SettingsFragment mSettingsFragment;
    private AlertDialog.Builder mBuilder;

    private boolean mHideFab = true;
    private boolean mSelectChat = false;
    private int mSelectedUserId;

    private long mLastOrder = Long.MAX_VALUE;
    private long mLastChatId = 0;

    private int[] mDrawerIcons = {R.drawable.ic_settings, R.drawable.ic_logout};

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);

        if (getArguments() != null) {
            mSelectChat = getArguments().getBoolean("select_chat", false);
            mSelectedUserId = getArguments().getInt("selected_user_id", 0);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
        if (!mSelectChat) {
            if (mNetworkReceiver == null) {
                mNetworkReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (Utils.isNetworkExist() && Utils.sOptionReceived) {
                            Utils.sCurrentConnectionState = 2;
                            updateToolbar();
                        }
                    }
                };
            }
            getParentActivity().registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }*/
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        if (mSelectChat) {
            return super.onCreateAnimator(transit, enter, nextAnim);
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_chats, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(mSelectChat ? R.drawable.ic_ab_back : R.drawable.ic_ab_menu);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectChat) {
                    mExitAnimated = true;
                    ((MainActivity) getParentActivity()).destroyFragment(ChatsFragment.this);
                } else {
                    if (!mDrawerLayout.isDrawerOpen(mDrawerListView)) {
                        mDrawerLayout.openDrawer(mDrawerListView);
                    }
                }
            }
        });
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));

        mLockImageButton = (ImageButton)mToolbar.findViewById(R.id.lockImageButton);
        mLockImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.sLocked) {
                    Utils.sLocked = true;
                    mLockImageButton.setImageResource(R.drawable.ic_lock_close);
                } else {
                    Utils.sLocked = false;
                    mLockImageButton.setImageResource(R.drawable.ic_lock_open);
                }
                Utils.savePasscode(getParentActivity());
            }
        });

        mDrawerMenuItems = getResources().getStringArray(R.array.drawer_menu);
        mDrawerLayout = (DrawerLayout)mRootView.findViewById(R.id.drawerLayout);
        mDrawerLayout.setStatusBarBackgroundColor(ContextCompat.getColor(Utils.sContext, R.color.status_bar_color));
        mDrawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {
                if (newState == DrawerLayout.STATE_IDLE) {
                    if (mSettingsFragment != null) {
                        ((MainActivity) getParentActivity()).createFragment(mSettingsFragment, "settings", true);
                        mSettingsFragment = null;
                    } else if (mBuilder != null) {
                        showAlertDialog(mBuilder);
                        mBuilder = null;
                    }
                }
            }
        });

        mDrawerListView = (RecyclerView)mRootView.findViewById(R.id.drawerListView);
        mDrawerListView.setHasFixedSize(true);
        mDrawerListView.setLayoutManager(new LinearLayoutManager(getParentActivity()));
        mDrawerListView.setAdapter(mDrawerAdapter = new DrawerAdapter(mDrawerMenuItems, mDrawerIcons, this));
        mDrawerListView.post(new Runnable() {
            @Override
            public void run() {
                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                int marginRight = Utils.convertDpToPx(56);
                int maxWidth = Utils.convertDpToPx(320);
                DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) mDrawerListView.getLayoutParams();
                params.width = Math.min(displayMetrics.widthPixels - marginRight, maxWidth);
                mDrawerListView.setLayoutParams(params);
            }
        });

        mChatsListView = (EmptyRecyclerView)mRootView.findViewById(R.id.chatsListView);
        mChatsListView.setHasFixedSize(false);
        mChatsListView.setLayoutManager(mLayoutManager = new LinearLayoutManager(getParentActivity()));
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        ((DefaultItemAnimator)itemAnimator).setSupportsChangeAnimations(false);
        mChatsListView.setItemAnimator(itemAnimator);
        Drawable listDivider = ContextCompat.getDrawable(Utils.sContext, R.drawable.list_divider);
        mChatsListView.addItemDecoration(new DividerItemDecoration(listDivider));
        /*if (!mSelectChat) {
            mChatsListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    AnimatorSet animatorSet;
                    if (dy > 0) {
                        if (mHideFab) {
                            animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_down_fab);
                            animatorSet.setTarget(mNewMessageButton);
                            animatorSet.addListener(new Animator.AnimatorListener() {
                                @Override
                                public void onAnimationStart(Animator animation) {

                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    if (mNewMessageButton.getVisibility() == View.VISIBLE) {
                                        mNewMessageButton.setVisibility(View.GONE);
                                    }
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {

                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {

                                }
                            });
                            animatorSet.start();
                            mHideFab = false;
                        }
                    } else {
                        if (!mHideFab) {
                            animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_up_fab);
                            animatorSet.setTarget(mNewMessageButton);
                            if (mNewMessageButton.getVisibility() == View.GONE) {
                                mNewMessageButton.setVisibility(View.VISIBLE);
                            }
                            animatorSet.start();
                            mHideFab = true;
                        }
                    }
                }
            });
        }*/
        mChatsListView.setOnItemClickListener(new EmptyRecyclerView.OnItemClickListener() {
            @Override
            public boolean onItemClick(View v, int position) {
                TdApi.Chat chat = ChatsCache.sChatsList.get(position);
                TdApi.ChatInfo type = chat.type;
                ChatFragment chatFragment = new ChatFragment();
                Bundle args = new Bundle();
                args.putLong("chat_id", chat.id);
                if (type.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                    TdApi.PrivateChatInfo privateChatInfo = (TdApi.PrivateChatInfo) type;
                    args.putInt("user_id", privateChatInfo.user.id);
                } else if (type.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
                    TdApi.GroupChatInfo groupChatInfo = (TdApi.GroupChatInfo) type;
                    args.putInt("group_id", groupChatInfo.group.id);
                } else if (type.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
                    TdApi.ChannelChatInfo channelChatInfo = (TdApi.ChannelChatInfo) type;
                    args.putInt("channel_id", channelChatInfo.channel.id);
                }
                if (mSelectChat) {
                    args.putInt("selected_user_id", mSelectedUserId);
                    ((MainActivity) getParentActivity()).destroyFragment("chat_" + ChatsFragment.this.getArguments().getLong("chat_id", 0));
                    ((MainActivity) getParentActivity()).destroyFragment("user_profile_" + mSelectedUserId);
                    ((MainActivity) getParentActivity()).destroyFragment("group_profile_" + ChatsFragment.this.getArguments().getInt("group_chat_id", 0));
                }
                chatFragment.setArguments(args);
                ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + chat.id, true, false, true, false);
                return true;
            }
        });
        if (mChatsAdapter == null) {
            mChatsAdapter = new ChatsAdapter();
        }

        mChatsListView.setAdapter(mChatsAdapter);
        mChatsAdapter.notifyDataSetChanged();

        mLoadingChatsProgressBar = mRootView.findViewById(R.id.loadingChatsProgressBar);
        mEmptyLayout = (LinearLayout)mRootView.findViewById(R.id.emptyLayout);

        if (ChatsCache.sLoadingChats && ChatsCache.sChatsList.isEmpty()) {
            mChatsListView.setEmptyView(null);
            mEmptyLayout.setVisibility(View.GONE);
            mLoadingChatsProgressBar.setVisibility(View.VISIBLE);
        } else {
            mChatsListView.setEmptyView(mEmptyLayout);
            mLoadingChatsProgressBar.setVisibility(View.GONE);
        }

        mChatsListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                int visibleItemCount = mLayoutManager.getChildCount();
                if (visibleItemCount > 0) {
                    if (lastVisibleItem == ChatsCache.sChatsList.size()) {
                        loadChats(mLastOrder, mLastChatId, 100);
                    }
                }

                /*if (!mSelectChat) {
                    if (dy > 0 && !mAddFloatingMenu.isMenuButtonHidden()) {
                        mAddFloatingMenu.hideMenuButton(true);
                    } else if (mAddFloatingMenu.isMenuButtonHidden()) {
                        mAddFloatingMenu.showMenuButton(true);
                    }
                }*/
            }
        });

        if (!mChatsLoaded) {
            mLoadingChatsProgressBar.setVisibility(View.VISIBLE);
            loadCurrentUser();
            loadChats(mLastOrder, mLastChatId, 100);
            mChatsLoaded = true;
        }

        //mNewMessageButton = (ImageButton)mRootView.findViewById(R.id.newMessageButton);
        mAddFloatingMenu = (FloatingActionMenu)mRootView.findViewById(R.id.addFloatingMenu);
        mAddFloatingMenu.setMenuButtonColorNormal(0xff5795cc);
        mAddFloatingMenu.setMenuButtonColorPressed(0xff4c8cc4);
        mAddFloatingMenu.setClosedOnTouchOutside(true);

        FloatingActionButton newChatFloatingButton = new FloatingActionButton(Utils.sContext);
        newChatFloatingButton.setImageResource(R.drawable.ic_person_white);
        newChatFloatingButton.setButtonSize(FloatingActionButton.SIZE_MINI);
        newChatFloatingButton.setLabelText(Utils.sContext.getString(R.string.new_chat));
        newChatFloatingButton.setElevationCompat(6f);
        newChatFloatingButton.setColorNormal(0xff7bcb81);
        newChatFloatingButton.setColorPressed(0xff62c169);
        newChatFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactsFragment contactsFragment = new ContactsFragment();
                Bundle args = new Bundle();
                args.putBoolean("create_chat", true);
                contactsFragment.setArguments(args);
                ((MainActivity) getParentActivity()).createFragment(contactsFragment, "contacts", true, false, true, false);
                mAddFloatingMenu.close(true);
            }
        });
        mAddFloatingMenu.addMenuButton(newChatFloatingButton);

        FloatingActionButton newGroupFloatingButton = new FloatingActionButton(Utils.sContext);
        newGroupFloatingButton.setImageResource(R.drawable.ic_supervisor_account_white);
        newGroupFloatingButton.setButtonSize(FloatingActionButton.SIZE_MINI);
        newGroupFloatingButton.setLabelText(Utils.sContext.getString(R.string.new_group));
        newGroupFloatingButton.setElevationCompat(6f);
        newGroupFloatingButton.setColorNormal(0xffe6c26c);
        newGroupFloatingButton.setColorPressed(0xffe3bb5b);
        newGroupFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactsFragment contactsFragment = new ContactsFragment();
                Bundle args = new Bundle();
                args.putBoolean("create_group", true);
                contactsFragment.setArguments(args);
                ((MainActivity) getParentActivity()).createFragment(contactsFragment, "select_contacts", true, false, true, false);
                mAddFloatingMenu.close(true);
            }
        });
        mAddFloatingMenu.addMenuButton(newGroupFloatingButton);

//        FloatingActionButton newChannelFloatingButton = new FloatingActionButton(Utils.sContext);
//        newChannelFloatingButton.setImageResource(R.drawable.ic_newchannel_white);
//        newChannelFloatingButton.setButtonSize(FloatingActionButton.SIZE_MINI);
//        newChannelFloatingButton.setLabelText(Utils.sContext.getString(R.string.new_channel));
//        newChannelFloatingButton.setElevationCompat(6f);
//        newChannelFloatingButton.setColorNormal(0xffe48283);
//        newChannelFloatingButton.setColorPressed(0xffe06d6d);
//        mAddFloatingMenu.addMenuButton(newChannelFloatingButton);

        mAddFloatingMenu.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAddFloatingMenu.toggle(true);
            }
        });

        if (mSelectChat) {
            mAddFloatingMenu.setVisibility(View.GONE);
        }

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                updateToolbar();
            }
        });

        return mRootView;
    }

    private void loadChats(long offsetOrder, long offsetChatId, final int limit) {
        if (ChatsCache.sLoadingChats) {
            return;
        }

        ChatsCache.sLoadingChats = true;

        Utils.sClient.send(new TdApi.GetChats(offsetOrder, offsetChatId, limit), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Chats.CONSTRUCTOR) {
                    final TdApi.Chats chats = (TdApi.Chats) object;
                    //Log.e(Utils.TAG, "" + chats);
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            ChatsCache.sLoadingChats = false;
                            for (final TdApi.Chat chat : chats.chats) {
                                TdApi.ChatInfo chatInfo = chat.type;
                                if (chatInfo.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
                                    TdApi.Group group = ((TdApi.GroupChatInfo) chatInfo).group;
                                    ChatsCache.sGroups.put(group.id, group);
                                } else if (chatInfo.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
                                    TdApi.User user = ((TdApi.PrivateChatInfo) chatInfo).user;
                                    ChatsCache.sUsers.put(user.id, user);
                                } else if (chatInfo.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
                                    TdApi.Channel channel = ((TdApi.ChannelChatInfo) chatInfo).channel;
                                    ChatsCache.sChannels.put(channel.id, channel);
                                }
                                TdApi.Chat currentChat = ChatsCache.sChats.get(chat.id);
                                if (currentChat == null) {
                                    ChatsCache.sChats.put(chat.id, chat);
                                } else {
                                    currentChat.topMessage = chat.topMessage;
                                }
                                if (chat.notificationSettings.muteFor > 0) {
                                    Runnable muteRunnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            chat.notificationSettings.muteFor = 0;
                                            ChatsCache.sChatsMuteRunnable.remove(chat.id);
                                            ChatsCache.updateNotifySettings();
                                        }
                                    };
                                    ChatsCache.sChatsMuteRunnable.put(chat.id, muteRunnable);
                                    Utils.runOnUIThread(muteRunnable, chat.notificationSettings.muteFor * 1000L);
                                }
                            }
                            ChatsCache.sChatsList.clear();
                            ChatsCache.sChatsList.addAll(ChatsCache.sChats.values());
                            Collections.sort(ChatsCache.sChatsList, new Comparator<TdApi.Chat>() {
                                @Override
                                public int compare(TdApi.Chat lhs, TdApi.Chat rhs) {
                                    if (lhs.order < rhs.order) {
                                        return 1;
                                    } else if (lhs.order > rhs.order){
                                        return -1;
                                    } else {
                                        if (lhs.id < rhs.id) {
                                            return 1;
                                        } else if (lhs.id > rhs.id) {
                                            return -1;
                                        } else {
                                            return 0;
                                        }
                                    }
                                }
                            });
                            TdApi.Chat lastChat = ChatsCache.sChatsList.get(ChatsCache.sChatsList.size() - 1);
                            mLastOrder = lastChat.order;
                            mLastChatId = lastChat.id;
                            ChatsCache.sChatsEndReached = chats.chats.length == 0;
                            ChatsCache.updateChatsList();
                        }
                    });
                }
            }
        });
    }

    private void loadCurrentUser() {
        Utils.sClient.send(new TdApi.GetMe(), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                final TdApi.User me = (TdApi.User) object;
                Utils.runOnUIThread(new Runnable() {
                    @Override
                    public void run() {
                        ChatsCache.sUsers.put(me.id, me);
                        Utils.sCurrentUserId = me.id;

                        if (mDrawerAdapter != null) {
                            mDrawerAdapter.notifyHeaderChanged();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void updateInterface() {
        updateToolbar();
        if (mChatsAdapter != null) {
            mChatsAdapter.notifyDataSetChanged();
        }
        if (mDrawerAdapter != null) {
            mDrawerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateMessageId(long chatId, int oldId, int newId) {
        TdApi.Chat chat = ChatsCache.sChats.get(chatId);
        if (chat != null) {
            TdApi.Message message = chat.topMessage;
            if (message != null && message.id == oldId) {
                message.id = newId;
                ChatsCache.updateChatsList();
            }
        }
    }

    @Override
    public void updateMessageDate(long chatId, int messageId, int newDate) {
        TdApi.Chat chat = ChatsCache.sChats.get(chatId);
        if (chat != null){
            TdApi.Message message = chat.topMessage;
            if (message != null && message.id == messageId) {
                message.date = newDate;
                ChatsCache.updateChatsList();
            }
        }
    }

    @Override
    public void updateMessageViews(long chatId, int messageId, int views) {
        TdApi.Chat chat = ChatsCache.sChats.get(chatId);
        if (chat != null){
            TdApi.Message message = chat.topMessage;
            if (message != null && message.id == messageId) {
                message.views = views;
                ChatsCache.updateChatsList();
            }
        }
    }

    @Override
    public void updateNotifySettings() {
        updateVisibleItems();
    }

    @Override
    public void updateListsWithEmoji() {
        if (mChatsListView != null) {
            updateVisibleItems();
        }
    }

    @Override
    public void updateChatsList() {
        updateInterface();
        if (mChatsListView != null) {
            if (ChatsCache.sLoadingChats) {
                if (mChatsListView.getEmptyView() != null) {
                    mChatsListView.setEmptyView(null);
                }
                mEmptyLayout.setVisibility(View.GONE);
                mLoadingChatsProgressBar.setVisibility(View.VISIBLE);
            } else {
                if (mChatsListView.getEmptyView() == null) {
                    mChatsListView.setEmptyView(mEmptyLayout);
                }
                mLoadingChatsProgressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void updateContactsList() {
        updateVisibleItems();
    }

    @Override
    public void updateDownloadProgress(int fileId, int ready, int size) {

    }

    @Override
    public void updateDownloadedFile(int fileId) {
        updateVisibleItems();
    }

    public void updateVisibleItems() {
        if (mChatsListView == null || mChatsAdapter == null) {
            return;
        }
        int count = mChatsListView.getChildCount();
        int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
        for (int i = 0; i < count; i++) {
            mChatsAdapter.notifyItemChanged(firstVisibleItem + i);
            /*RecyclerView.ViewHolder viewHolder = mChatsListView.findViewHolderForAdapterPosition(i);
            if (viewHolder != null) {
                mChatsAdapter.onBindViewHolder((ChatsAdapter.ViewHolder)viewHolder, i);
            }*/
        }
    }

    public void updateToolbar() {
        if (mHeaderTextView == null) {
            return;
        }
        if (!mSelectChat) {
            switch (Utils.sCurrentConnectionState) {
                case 0:
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.chats_header));
                    break;
                case 1:
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.waiting_for_network_state));
                    break;
                case 2:
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.connecting_state));
                    break;
                case 3:
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.updating_state));
                    break;
            }
            if (Utils.sPasscodeHash.length() > 0) {
                mLockImageButton.setVisibility(View.VISIBLE);
                if (!Utils.sLocked) {
                    mLockImageButton.setImageResource(R.drawable.ic_lock_open);
                } else {
                    mLockImageButton.setImageResource(R.drawable.ic_lock_close);
                }
            } else {
                mLockImageButton.setVisibility(View.GONE);
            }
        } else {
            mHeaderTextView.setText(Utils.sContext.getString(R.string.select_chat_title));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }

        updateInterface();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if (!mSelectChat) {
            getParentActivity().unregisterReceiver(mNetworkReceiver);
        }*/

        ChatsCache.removeDelegate(this);
        Utils.sOptionReceived = false;
    }

    @Override
    public void onClick(View view, int position) {
        if (position == 1) {
            mSettingsFragment = new SettingsFragment();
        } else if (position == 2) {
            mBuilder = new AlertDialog.Builder(getParentActivity());
            mBuilder.setMessage(R.string.logout_message);
            mBuilder.setPositiveButton(Utils.sContext.getString(R.string.logout_button), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Utils.sClient.send(new TdApi.ResetAuth(false), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {

                        }
                    });
                    ((MainActivity) getParentActivity()).logout();
                }
            });
            mBuilder.setNegativeButton(Utils.sContext.getString(R.string.cancel_button), null);
        }
        mDrawerLayout.closeDrawer(mDrawerListView);
    }

    @Override
    public boolean onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerListView)) {
            mDrawerLayout.closeDrawer(mDrawerListView);
            return false;
        }
        if (mAddFloatingMenu.isOpened()) {
            mAddFloatingMenu.close(true);
            return false;
        }
        if (mSelectChat) {
            mExitAnimated = true;
        }
        return true;
    }
}
