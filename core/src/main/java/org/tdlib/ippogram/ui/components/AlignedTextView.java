package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.graphics.Paint;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import org.tdlib.ippogram.Utils;

public class AlignedTextView extends TextView {

    private boolean mNegativeLineSpacing = false;

    public AlignedTextView(Context context) {
        super(context);
    }

    public AlignedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AlignedTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /*@Override
    public void setLineSpacing(float add, float mult) {
        super.setLineSpacing(add, mult);
    }*/

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        /*Layout layout = getLayout();
        int truncatedHeight = getLineHeight() - getPaint().getFontMetricsInt(null);
        if (truncatedHeight > 0) {
            setMeasuredDimension(widthMeasureSpec, MeasureSpec.makeMeasureSpec((getLineHeight() * layout.getLineCount() - truncatedHeight), MeasureSpec.AT_MOST));
        }*/
    }
}
