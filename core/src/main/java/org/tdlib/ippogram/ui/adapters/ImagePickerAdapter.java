package org.tdlib.ippogram.ui.adapters;

import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.signature.StringSignature;

import org.tdlib.ippogram.ui.R;
import org.tdlib.ippogram.ui.components.BlockingImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImagePickerAdapter extends RecyclerView.Adapter<ImagePickerAdapter.ViewHolder>
        implements ListPreloader.PreloadSizeProvider<String>, ListPreloader.PreloadModelProvider<String> {

    private static final String[] IMAGE_PROJECTION = new String[]{
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA,
            MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
            MediaStore.Images.ImageColumns.DATE_TAKEN,
            MediaStore.Images.ImageColumns.MIME_TYPE
    };

    public interface OnPickListener {
        void onPick(View view, int position);
    }

    private OnPickListener mListener;

    private List<String> mImages;
    private final LayoutInflater mInflater;
    private ArrayList<String> mSelectedImages;
    private final RequestManager mRequestManager;

    private int[] mActualDimensions;

    public ImagePickerAdapter(Context context) {
        this(context, Glide.with(context));
    }

    public ImagePickerAdapter(Context context, RequestManager requestManager) {
        mInflater = LayoutInflater.from(context);
        mImages = new ArrayList<>();
        mRequestManager = requestManager;
        setHasStableIds(true);

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_PROJECTION, null,
                null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String imageLocation = cursor.getString(1);
                mImages.add(imageLocation);
            }
            cursor.close();
        }
    }

    public void setOnPickListener(OnPickListener listener) {
        mListener = listener;
    }

    @Override
    public ImagePickerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = mInflater.inflate(R.layout.image_picker, parent, false);
        if (mActualDimensions == null) {
            v.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    if (mActualDimensions == null) {
                        mActualDimensions = new int[]{v.getWidth(), v.getHeight()};
                    }
                    v.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ImagePickerAdapter.ViewHolder holder, int position) {
        String path = mImages.get(position);
        Key signature = new StringSignature(path);
        //mRequestManager.load(path).clone().signature(signature).centerCrop().into(holder.pickImageView);
        holder.pickImageView.setImage(path, true, false, false, false);
        if (mSelectedImages != null && mSelectedImages.contains(path)) {
            holder.dimView.setVisibility(View.VISIBLE);
            holder.checkImageView.setVisibility(View.VISIBLE);
        } else {
            holder.dimView.setVisibility(View.GONE);
            holder.checkImageView.setVisibility(View.GONE);
        }
        //Glide.with(Utils.sContext).load(mImages.get(position)).thumbnail(0.7f).centerCrop().into(holder.pickImageView);
    }

    @Override
    public List<String> getPreloadItems(int position) {
        return Collections.singletonList(mImages.get(position));
    }

    @Override
    public int[] getPreloadSize(String item, int adapterPosition, int perItemPosition) {
        return mActualDimensions;
    }

    @Override
    public GenericRequestBuilder getPreloadRequestBuilder(String item) {
        return mRequestManager.load(item).clone().signature(new StringSignature(item)).centerCrop();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    public String getPath(int position) {
        return mImages.get(position);
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    public void setSelectedImages(ArrayList<String> selectedImages) {
        mSelectedImages = selectedImages;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final BlockingImageView pickImageView;
        public final View dimView;
        public final ImageView checkImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onPick(v, getLayoutPosition());
                }
            });
            pickImageView = (BlockingImageView)itemView.findViewById(R.id.pickImageView);
            dimView = itemView.findViewById(R.id.dimView);
            checkImageView = (ImageView)itemView.findViewById(R.id.checkImageView);
        }
    }

    /*private static class ImageLoader extends AsyncTaskLoader<List<String>> {

        private static final String[] IMAGE_PROJECTION = new String[]{
                MediaStore.Images.ImageColumns._ID,
                MediaStore.Images.ImageColumns.DATA,
                MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                MediaStore.Images.ImageColumns.DATE_TAKEN,
                MediaStore.Images.ImageColumns.MIME_TYPE
        };

        private List<String> mImagesPath;
        private boolean mObserverRegistered = false;
        private final ForceLoadContentObserver mForceLoadContentObserver = new ForceLoadContentObserver();

        public ImageLoader(Context context) {
            super(context);
        }

        @Override
        public void deliverResult(List<String> data) {
            if (!isReset() && isStarted()) {
                super.deliverResult(data);
            }
        }

        @Override
        protected void onStartLoading() {
            if (mImagesPath != null) {
                deliverResult(mImagesPath);
            }
            if (takeContentChanged() || mImagesPath == null) {
                forceLoad();
            }
            registerContentObserver();
        }

        @Override
        protected void onStopLoading() {
            cancelLoad();
        }

        @Override
        protected void onReset() {
            super.onReset();

            onStopLoading();
            mImagesPath = null;
            unregisterContentObserver();
        }

        @Override
        protected void onAbandon() {
            super.onAbandon();
            unregisterContentObserver();
        }

        @Override
        public List<String> loadInBackground() {
            mImagesPath = new ArrayList<>();
            Cursor cursor = getContext().getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_PROJECTION, null,
                    null, MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String imageLocation = cursor.getString(1);
                    mImagesPath.add(imageLocation);
                }
                cursor.close();
            }
            return mImagesPath;
        }

        private void registerContentObserver() {
            if (!mObserverRegistered) {
                ContentResolver cr = getContext().getContentResolver();
                cr.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false, mForceLoadContentObserver);
                //cr.registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, false,
                        //forceLoadContentObserver);

                mObserverRegistered = true;
            }
        }

        private void unregisterContentObserver() {
            if (mObserverRegistered) {
                mObserverRegistered = false;

                getContext().getContentResolver().unregisterContentObserver(mForceLoadContentObserver);
            }
        }
    }*/
}
