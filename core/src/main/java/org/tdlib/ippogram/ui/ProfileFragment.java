package org.tdlib.ippogram.ui;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.TextDrawable;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ProfileFragment extends MainFragment implements UpdatesHandler.UserUpdatesDelegate,
        UpdatesHandler.UpdatesForContactsDelegate, UpdatesHandler.GroupUpdatesDelegate, UpdatesHandler.ChannelUpdatesDelegate {

    public static final int MUTE_FOR_HOUR_MENU_ITEM = 0;
    public static final int MUTE_FOR_HOURS_MENU_ITEM = 1;
    public static final int MUTE_FOR_DAYS_MENU_ITEM = 2;
    public static final int MUTE_DISABLE_MENU_ITEM = 3;

    private static final int ADD_MENU_ITEM = 4;
    private static final int ADD_TO_GROUP_MENU_ITEM = 5;
    private static final int SHARE_MENU_ITEM = 6;
    private static final int BLOCK_MENU_ITEM = 7;
    private static final int EDIT_MENU_ITEM = 8;
    private static final int DELETE_MENU_ITEM = 9;

    private ListView mProfileListView;
    private ProfileAdapter mProfileAdapter;

    private View mEmptyHeaderView;
    private LinearLayout mEmptyHeader;
    private LinearLayout mProfileHeaderLayout;
    private RelativeLayout mProfileHeaderContainer;
    private LinearLayout mInfoLayout;

    private ToolbarMenuLayout mMoreMenuButton;
    private ToolbarMenuLayout mNotifyMenuButton;

    private long mChatId;
    private int mUserId;
    private int mGroupId;
    private int mChannelId;

    private TextView mBlockItemTextView;

    private TdApi.UserFull mUserFull = null;
    private TdApi.GroupFull mGroupFull = null;
    private TdApi.ChannelFull mChannelFull = null;

    private TdApi.Channel mCurrentChannel = null;
    private TdApi.Group mCurrentGroup = null;

    private TdApi.ChatParticipant[] mChatParticipants = null;
    private ArrayList<TdApi.ChatParticipant> mSortedUsers = new ArrayList<>();
    private ArrayList<Integer> mIgnoreUsers = new ArrayList<>();
    private int mOnlineCount = -1;

    private TextView mSubHeaderTextView;
    private BlockingImageView mAvatarImageView;
    private ImageButton mFloatingActionButton;

    private int mProfileHeaderHeight;
    private int mProfileMinHeaderTranslation;
    private int mProfileAvatarTranslation;
    private int mProfileInfoLayoutTranslation;

    private Configuration mConfig;

    private boolean mHideFab = true;
    private String mProfilePhotoPath;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            if (mUserId != 0) {
                updateUser();
            } else if (mGroupId != 0) {
                updateVisibleItems();
            } else if (mChannelId != 0) {
                updateChannel();
            }
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);
        mChatId = getArguments().getLong("chat_id", 0);
        mUserId = getArguments().getInt("user_id", 0);
        mGroupId = getArguments().getInt("group_chat_id", 0);
        mChannelId = getArguments().getInt("channel_id", 0);
        mConfig = Utils.sContext.getResources().getConfiguration();
        if (mUserId != 0) {
            if (!ChatsCache.sUsersFull.containsKey(mUserId)) {
                ChatsCache.loadUserFull(mUserId);
            } else {
                mUserFull = ChatsCache.sUsersFull.get(mUserId);
            }
        } else if (mGroupId != 0 || mChannelId != 0) {
            updateOnlineCount();
            if (mChannelId != 0) {
                mCurrentChannel = ChatsCache.getChannel(mChannelId);
            } else if (mGroupId != 0) {
                mCurrentGroup = ChatsCache.getGroup(mGroupId);
            }
        }
        Utils.runOnUIThread(runnable, 1000L);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mUserId != 0) {
            mRootView = inflater.inflate(R.layout.fragment_user_profile, container, false);
        } else if (mGroupId != 0) {
            mRootView = inflater.inflate(R.layout.fragment_group_profile, container, false);
        } else if (mChannelId != 0) {
            mRootView = inflater.inflate(R.layout.fragment_channel_profile, container, false);
        }

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(ProfileFragment.this);
            }
        });

        mNotifyMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.notifyMenuButton);
        mNotifyMenuButton.setParentToolbar(mToolbar);
        mNotifyMenuButton.setVisibility(View.VISIBLE);
        mNotifyMenuButton.addItem(MUTE_FOR_HOUR_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hour));
        mNotifyMenuButton.addItem(MUTE_FOR_HOURS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hours));
        mNotifyMenuButton.addItem(MUTE_FOR_DAYS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_days));
        mNotifyMenuButton.addItem(MUTE_DISABLE_MENU_ITEM, Utils.sContext.getString(R.string.notifications_disable));
        mNotifyMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean muted = ChatsCache.isChatMuted(mChatId);
                if (muted) {
                    TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                    if (chat != null) {
                        chat.notificationSettings.muteFor = 0;
                    }
                    Utils.cancelRunOnUIThread(ChatsCache.sChatsMuteRunnable.get(mChatId));
                    ChatsCache.sChatsMuteRunnable.remove(mChatId);
                    ChatsCache.updateNotifySettingsForChat(mChatId);
                    mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
                } else {
                    mNotifyMenuButton.createMenu();
                }
            }
        });
        mNotifyMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                int untilTime = 0;
                switch ((Integer) view.getTag()) {
                    case MUTE_FOR_HOUR_MENU_ITEM:
                        untilTime = 60 * 60;
                        break;
                    case MUTE_FOR_HOURS_MENU_ITEM:
                        untilTime = 60 * 60 * 8;
                        break;
                    case MUTE_FOR_DAYS_MENU_ITEM:
                        untilTime = 60 * 60 * 48;
                        break;
                    case MUTE_DISABLE_MENU_ITEM:
                        untilTime = Integer.MAX_VALUE;
                        break;
                }
                final TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                if (chat != null) {
                    chat.notificationSettings.muteFor = untilTime;
                }
                ChatsCache.sChatsMuteRunnable.put(mChatId, new Runnable() {
                    @Override
                    public void run() {
                        TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                        if (chat != null) {
                            chat.notificationSettings.muteFor = 0;
                        }
                        ChatsCache.sChatsMuteRunnable.remove(mChatId);
                        ChatsCache.updateNotifySettings();
                    }
                });
                ChatsCache.updateNotifySettingsForChat(mChatId);
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
            }
        });
        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more);
        createToolbarMenu();
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });
        mMoreMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                switch ((Integer) view.getTag()) {
                    case ADD_MENU_ITEM:
                        if (mUserId != 0) {
                            ContactAddFragment contactAddFragment = new ContactAddFragment();
                            Bundle addArgs = new Bundle();
                            addArgs.putInt("user_id", mUserId);
                            addArgs.putBoolean("add_contact", true);
                            contactAddFragment.setArguments(addArgs);
                            ((MainActivity) getParentActivity()).createFragment(contactAddFragment, "add_contact", true);
                        } else if (mGroupId != 0) {
                            selectContacts();
                        }
                        break;
                    case ADD_TO_GROUP_MENU_ITEM:
                        break;
                    case SHARE_MENU_ITEM:
                        ChatsFragment chatsFragment = new ChatsFragment();
                        Bundle args = new Bundle();
                        args.putInt("selected_user_id", mUserId);
                        args.putLong("chat_id", mChatId);
                        args.putInt("group_chat_id", mGroupId);
                        args.putBoolean("select_chat", true);
                        chatsFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(chatsFragment, "select_chat", true, false, true, false);
                        break;
                    case BLOCK_MENU_ITEM:
                        if (getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder blockBuilder = new AlertDialog.Builder(getParentActivity());
                        blockBuilder.setMessage((mUserFull != null && mUserFull.isBlocked) ? R.string.unblock_message : R.string.block_message);
                        blockBuilder.setPositiveButton((mUserFull != null && mUserFull.isBlocked) ? R.string.action_unblock : R.string.action_block, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mUserFull != null && mUserFull.isBlocked) {
                                    ChatsCache.unblockUser(mUserId);
                                } else {
                                    ChatsCache.blockUser(mUserId);
                                }
                            }
                        });
                        blockBuilder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(blockBuilder);
                        break;
                    case EDIT_MENU_ITEM:
                        if (mUserId != 0) {
                            ContactAddFragment contactEditFragment = new ContactAddFragment();
                            Bundle editArgs = new Bundle();
                            editArgs.putInt("user_id", mUserId);
                            editArgs.putBoolean("add_contact", false);
                            contactEditFragment.setArguments(editArgs);
                            ((MainActivity) getParentActivity()).createFragment(contactEditFragment, "edit_contact", true);
                        } else if (mGroupId != 0) {
                            GroupProfileChangeNameFragment fragment = new GroupProfileChangeNameFragment();
                            Bundle editNameArgs = new Bundle();
                            editNameArgs.putInt("group_chat_id", mGroupId);
                            editNameArgs.putLong("chat_id", mChatId);
                            fragment.setArguments(editNameArgs);
                            ((MainActivity)getParentActivity()).createFragment(fragment, "change_group_title", true);
                        }
                        break;
                    case DELETE_MENU_ITEM:
                        if (getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(getParentActivity());
                        if (mUserId != 0) {
                            deleteBuilder.setMessage(R.string.delete_contact_message);
                        } else if (mGroupId != 0) {
                            deleteBuilder.setMessage(R.string.delete_and_leave_message);
                        }
                        deleteBuilder.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mUserId != 0) {
                                    ArrayList<TdApi.User> deleteUsers = new ArrayList<>();
                                    TdApi.User user = ChatsCache.getUser(mUserId);
                                    if (user != null) {
                                        deleteUsers.add(user);
                                        ContactsCache.getInstance().deleteContacts(deleteUsers);
                                    }
                                } else if (mGroupId != 0) {
                                    ((MainActivity) getParentActivity()).destroyFragment("chat_" + mChatId);
                                    ChatsCache.deleteUserFromChat(mChatId, Utils.sCurrentUserId, true);
                                    mExitAnimated = true;
                                    ((MainActivity) getParentActivity()).destroyFragment(ProfileFragment.this);
                                }
                            }
                        });
                        deleteBuilder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(deleteBuilder);
                        break;
                }
            }
        });

        mProfileListView = (ListView)mRootView.findViewById(R.id.profileListView);
        mProfileListView.setVerticalScrollBarEnabled(false);

        mProfileHeaderLayout = (LinearLayout) mRootView.findViewById(R.id.profileHeaderLayout);
        mProfileHeaderContainer = (RelativeLayout)mProfileHeaderLayout.findViewById(R.id.profileHeaderContainer);
        mInfoLayout = (LinearLayout)mProfileHeaderLayout.findViewById(R.id.infoLayout);
        mAvatarImageView = (BlockingImageView) mProfileHeaderLayout.findViewById(R.id.avatarImageView);
        mHeaderTextView = (TextView) mProfileHeaderLayout.findViewById(R.id.headerTextView);
        mSubHeaderTextView = (TextView) mProfileHeaderLayout.findViewById(R.id.subHeaderTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }
        if (mSubHeaderTextView != null) {
            mSubHeaderTextView.setTypeface(Utils.getTypeface("regular"));
        }
        mFloatingActionButton = (ImageButton)mProfileHeaderLayout.findViewById(R.id.floatingActionButton);
        if (mUserId != 0) {
            mFloatingActionButton.setImageResource(R.drawable.ic_message);
        } else if (mGroupId != 0 || (mCurrentChannel != null && mCurrentChannel.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR)) {
            mFloatingActionButton.setImageResource(R.drawable.ic_mode_edit_gray);
        } else {
            mFloatingActionButton.setVisibility(View.GONE);
        }
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUserId != 0) {
                    ((MainActivity) getParentActivity()).destroyFragment("chat_" + mChatId);
                    ((MainActivity) getParentActivity()).destroyFragment("group_profile_" + getArguments().getInt("group_chat_id", 0));

                    if (mGroupId != 0) {
                        Utils.showProgressDialog(getParentActivity(), Utils.sContext.getString(R.string.loading));
                        Utils.sClient.send(new TdApi.CreatePrivateChat(mUserId), new Client.ResultHandler() {
                            @Override
                            public void onResult(final TdApi.TLObject object) {
                                if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                                    final TdApi.Chat chat = (TdApi.Chat) object;
                                    if (!ChatsCache.sChats.containsKey(chat.id)) {
                                        ChatsCache.sChats.put(chat.id, chat);
                                    }
                                    Utils.runOnUIThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ChatFragment chatFragment = new ChatFragment();
                                            Bundle args = new Bundle();
                                            args.putLong("chat_id", chat.id);
                                            args.putInt("user_id", mUserId);
                                            chatFragment.setArguments(args);
                                            ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + chat.id, true, false, true, false);
                                            Utils.hideProgressDialog(getParentActivity());
                                        }
                                    });
                                }
                            }
                        });
                    } else {
                        ChatFragment chatFragment = new ChatFragment();
                        Bundle args = new Bundle();
                        args.putLong("chat_id", mChatId);
                        args.putInt("user_id", mUserId);
                        chatFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + mChatId, true, false, true, false);
                    }
                } else if (mGroupId != 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                    final TdApi.Chat chat = ChatsCache.getChat(mChatId);
                    CharSequence[] items;
                    if (chat != null && chat.photo != null && chat.photo.small.id != 0) {
                        items = new CharSequence[]{
                                Utils.sContext.getString(R.string.take_photo),
                                Utils.sContext.getString(R.string.choose_from_gallery),
                                Utils.sContext.getString(R.string.delete_photo)
                        };
                    } else {
                        items = new CharSequence[]{
                                Utils.sContext.getString(R.string.take_photo),
                                Utils.sContext.getString(R.string.choose_from_gallery)
                        };
                    }
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File profilePhoto = Utils.createImageFile();
                                if (profilePhoto != null) {
                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profilePhoto));
                                    mProfilePhotoPath = profilePhoto.getAbsolutePath();
                                }
                                startActivityForResult(cameraIntent, 0);
                            } else if (which == 1) {
                                Intent pickIntent = new Intent(Intent.ACTION_PICK);
                                pickIntent.setType("image/*");
                                startActivityForResult(pickIntent, 1);
                            } else {
                                Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileId(0), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {

                                    }
                                });
                            }
                        }
                    });
                    builder.show().setCanceledOnTouchOutside(true);
                }
            }
        });

        mEmptyHeaderView = inflater.inflate(R.layout.empty_header, mProfileListView, false);
        mProfileListView.addHeaderView(mEmptyHeaderView, null, false);
        mEmptyHeader = (LinearLayout)mEmptyHeaderView.findViewById(R.id.emptyHeader);

        if (mConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (mConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }

        mProfileListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Integer scrollY = getScrollY(view);
                mProfileHeaderLayout.setTranslationY(Math.max(-scrollY, mProfileMinHeaderTranslation));

                final float offset = 1.0f - Math.max((float) (-mProfileMinHeaderTranslation - scrollY) / -mProfileMinHeaderTranslation, 0f);

                mToolbar.setTranslationY(-mProfileMinHeaderTranslation * offset);

                AnimatorSet animatorSet;
                if (offset >= 0.7f) {
                    if (mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_down_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = false;
                    }
                } else {
                    if (!mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_up_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = true;
                    }
                }

                //LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mAvatarImageView.getLayoutParams();
                //params.width = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //params.height = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //mAvatarImageView.setLayoutParams(params););
                mAvatarImageView.setScaleX(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setScaleY(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setTranslationX(Utils.convertDpToPx(37.5f) * offset);
                mAvatarImageView.setTranslationY(mProfileAvatarTranslation * offset);

                mInfoLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mInfoLayout.getLayoutParams();
                        params.height = (int) (Utils.convertDpToPx(61.0f) - Utils.convertDpToPx(13.0f) * offset);
                        params.rightMargin = (int) (Utils.convertDpToPx(16.0f) + Utils.convertDpToPx(96.0f) * offset);
                        mInfoLayout.setLayoutParams(params);

                        params = (LinearLayout.LayoutParams) mHeaderTextView.getLayoutParams();
                        params.bottomMargin = (int) (Utils.convertDpToPx(4) - Utils.convertDpToPx(4) * offset);
                        mHeaderTextView.setLayoutParams(params);
                    }
                });

                mInfoLayout.setTranslationX(Utils.convertDpToPx(14.0f) * offset);
                mInfoLayout.setTranslationY(mProfileInfoLayoutTranslation * offset);

                mHeaderTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20.0f - 2.0f * offset);
            }
        });
        if (mUserId != 0) {
            mProfileAdapter = new UserProfileAdapter();
        } else if (mCurrentGroup != null) {
            mProfileAdapter = new GroupProfileAdapter();
        } else if (mCurrentChannel != null) {
            mProfileAdapter = new ChannelProfileAdapter();
        }
        mProfileListView.setAdapter(mProfileAdapter);
        if (mUserId == 0 && mGroupId != 0) {
            mProfileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position == 5) {
                        selectContacts();
                        return;
                    }
                    int offset = 7;
                    if (mCurrentGroup != null && mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                        offset += 2;
                    }
                    int count = 0;
                    if (mChatParticipants != null) {
                        count += mChatParticipants.length;
                    }

                    if (position > offset && position < count + offset + 1) {
                        int userId = mSortedUsers.get(position - offset - 1).user.id;
                        if (userId == Utils.sCurrentUserId) {
                            return;
                        }
                        ProfileFragment userProfileFragment = new ProfileFragment();
                        Bundle args = new Bundle();
                        args.putLong("chat_id", mChatId);
                        args.putInt("user_id", userId);
                        args.putInt("group_chat_id", mGroupId);
                        userProfileFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + userId, true, false, true, false);
                    }
                }
            });
            mProfileListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    int offset = 7;
                    if (mCurrentGroup != null && mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                        offset += 2;
                    }
                    int count = 0;
                    if (mChatParticipants != null) {
                        count += mChatParticipants.length;
                    }
                    if (position > offset && position < count + offset + 1) {
                        final TdApi.ChatParticipant part = mSortedUsers.get(position - offset - 1);
                        if (part.user.id == Utils.sCurrentUserId) {
                            return false;
                        }
                        if (mGroupFull.adminId != Utils.sCurrentUserId && part.inviterId != Utils.sCurrentUserId) {
                            return false;
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                        CharSequence[] item = new CharSequence[]{Utils.sContext.getString(R.string.remove_from_group)};
                        builder.setItems(item, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    ChatsCache.deleteUserFromChat(mChatId, part.user.id, false);
                                }
                            }
                        });
                        showAlertDialog(builder);
                        return true;
                    }
                    return false;
                }
            });
        }
        return mRootView;
    }

    private int getScrollY(AbsListView view) {
        View c = view.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mProfileHeaderHeight;
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == 0) {
                cropPhoto(mProfilePhotoPath);
                mProfilePhotoPath = null;
            } else if (requestCode == 1) {
                Uri uri = data.getData();
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getParentActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                if (cursor == null) {
                    return;
                }
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mProfilePhotoPath = cursor.getString(columnIndex);
                cursor.close();
                cropPhoto(mProfilePhotoPath);
            } else if (requestCode == 2) {
                Log.d(Utils.TAG, "groupProfile");
                Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileLocal(data.getData().getPath()), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        Log.d(Utils.TAG, "" + object);
                    }
                });
            }
        }
    }

    private void cropPhoto(String path) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            cropIntent.putExtra("scale", "true");
            cropIntent.putExtra("return-data", "false");
            File cropProfilePhoto = Utils.createImageFile();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropProfilePhoto));
            startActivityForResult(cropIntent, 2);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(Utils.sContext, "Your device doesn't support the crop action", Toast.LENGTH_LONG).show();
            Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileLocal(path), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        }
    }

    private void selectContacts() {
        if (mIgnoreUsers.size() != 0) {
            mIgnoreUsers.clear();
        }
        for (TdApi.ChatParticipant part : mChatParticipants) {
            if (part.user.myLink.getConstructor() == TdApi.LinkStateContact.CONSTRUCTOR) {
                if (ContactsCache.getInstance().assignUsers.get(part.user.id) != null) {
                    mIgnoreUsers.add(part.user.id);
                }
            }
        }
        ContactsFragment contactsFragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putLong("chat_id", mChatId);
        args.putBoolean("show_only_users", true);
        args.putBoolean("create_group", false);
        args.putIntegerArrayList("ignore_users", mIgnoreUsers);
        contactsFragment.setArguments(args);
        ((MainActivity) getParentActivity()).createFragment(contactsFragment, "contacts", true);
    }

    private void updateOnlineCount() {
        if (mChatParticipants == null) {
            return;
        }
        mOnlineCount = 0;
        mSortedUsers.clear();
        for (TdApi.ChatParticipant chatParticipant : mChatParticipants) {
            TdApi.User user = ChatsCache.getUser(chatParticipant.user.id);
            if (user == null) {
                return;
            }
            if (Utils.formatUserStatus(user).equals("online")) {
                ++mOnlineCount;
            }
            mSortedUsers.add(chatParticipant);
        }

        Collections.sort(mSortedUsers, new Comparator<TdApi.ChatParticipant>() {
            @Override
            public int compare(TdApi.ChatParticipant lhs, TdApi.ChatParticipant rhs) {
                TdApi.UserStatus status1 = lhs.user.status;
                TdApi.UserStatus status2 = rhs.user.status;
                int statusInt1 = 0;
                int statusInt2 = 0;
                if (status1 != null) {
                    if (lhs.user.id != Utils.sCurrentUserId) {
                        if (status1.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
                            statusInt1 = ((TdApi.UserStatusOnline) status1).expires;
                        } else if (status1.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR) {
                            statusInt1 = ((TdApi.UserStatusOffline) status1).wasOnline;
                        }
                    } else {
                        statusInt1 = Utils.getCurrentTime() + 50000;
                    }
                }

                if (status2 != null) {
                    if (rhs.user.id != Utils.sCurrentUserId) {
                        if (status2.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
                            statusInt2 = ((TdApi.UserStatusOnline) status2).expires;
                        } else if (status2.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR) {
                            statusInt2 = ((TdApi.UserStatusOffline) status2).wasOnline;
                        }
                    } else {
                        statusInt2 = Utils.getCurrentTime() + 50000;
                    }
                }
                if ((statusInt1 > 0 && statusInt2 > 0) || (statusInt1 < 0 && statusInt2 < 0)) {
                    if (statusInt1 < statusInt2) {
                        return 1;
                    } else if (statusInt1 == statusInt2) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if ((statusInt1 < 0 && statusInt2 > 0) || (statusInt1 == 0 && statusInt2 != 0)) {
                    return 1;
                } else if ((statusInt1 > 0 && statusInt2 < 0) || (statusInt1 != 0 && statusInt2 == 0)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (mProfileListView != null) {
            mProfileListView.invalidateViews();
        }
    }

    public void setChatParticipants(TdApi.ChatParticipant[] participants) {
        mChatParticipants = participants;
    }

    public void setGroupFull(TdApi.GroupFull groupFull) {
        mGroupFull = groupFull;
    }

    public void setChannelFull(TdApi.ChannelFull channelFull) {
        mChannelFull = channelFull;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mProfileAdapter != null) {
            mProfileAdapter.notifyDataSetChanged();
        }
        if (mUserId != 0) {
            updateUser();
        } else if (mGroupId != 0) {
            updateGroup();
        } else if (mChannelId != 0) {
            updateChannel();
        }
        updateNotifySettings();
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(runnable);
    }

    @Override
    public void updateInterface() {
        if (mUserId != 0) {
            updateUser();
            if (mProfileListView != null) {
                mProfileListView.invalidateViews();
            }
        } else if (mGroupId != 0) {
            updateOnlineCount();
            updateGroup();
            updateVisibleItems();
        } else if (mChannelId != 0) {
            updateOnlineCount();
            updateChannel();
            if (mProfileListView != null) {
                mProfileListView.invalidateViews();
            }
        }
    }

    @Override
    public void updateNotifySettings() {
        if (mProfileListView != null) {
            if (ChatsCache.isChatMuted(mChatId)) {
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
            } else {
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
            }
        }
    }

    @Override
    public void updateUserInfo(int userId, boolean blocked, TdApi.BotInfo botInfo) {
        if (mUserFull == null) {
            mUserFull = ChatsCache.sUsersFull.get(userId);
            createToolbarMenu();
            if (mProfileListView != null) {
                mProfileListView.invalidateViews();
            }
            return;
        }
        if (userId == mUserId) {
            mUserFull.isBlocked = blocked;
            createToolbarMenu();
            if (botInfo != null) {
                mUserFull.botInfo = botInfo;
            }
            if (mProfileListView != null) {
                mProfileListView.invalidateViews();
            }
        }
    }

    @Override
    public void updateChatParts(int groupId, TdApi.GroupFull groupFull) {
        if (groupId == mGroupId) {
            mGroupFull = groupFull;
            mChatParticipants = groupFull.participants;
            updateOnlineCount();
            if (mProfileAdapter != null) {
                mProfileAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void updateChannelInfo(int channelId, TdApi.ChannelFull channelFull) {
        if (channelId == mChannelId) {
            mChannelFull = channelFull;
            updateOnlineCount();
            if (mProfileListView != null) {
                mProfileListView.invalidateViews();
            }
        }
    }

    @Override
    public void updateContactsList() {
        createToolbarMenu();
    }

    public void updateUser() {
        final TdApi.User currentUser = ChatsCache.getUser(mUserId);
        TextDrawable textDrawable;
        if (currentUser != null) {
            textDrawable = Utils.getUserAvatar(currentUser, 61);
            if (mSubHeaderTextView != null) {
                mAvatarImageView.setProfilePhoto(currentUser.profilePhoto, textDrawable);
                mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentUser.profilePhoto.id != 0) {
                            PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                            Bundle args = new Bundle();
                            TdApi.File smallPhoto = ChatsCache.getFile(currentUser.profilePhoto.small.id);
                            TdApi.File bigPhoto = ChatsCache.getFile(currentUser.profilePhoto.big.id);
                            if (bigPhoto.path.length() != 0) {
                                args.putString("path", bigPhoto.path);
                            } else {
                                args.putString("path", smallPhoto.path);
                                args.putInt("big_photo_id", bigPhoto.id);
                            }
                            photoViewFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                        }
                    }
                });
                if (currentUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                } else {
                    mHeaderTextView.setText(Utils.formatName(currentUser.firstName, currentUser.lastName));
                }
                mSubHeaderTextView.setText(Utils.formatUserStatus(currentUser));
            }
        }
    }

    public void updateGroup() {
        final TdApi.Group currentGroup = ChatsCache.getGroup(mGroupId);
        final TdApi.Chat currentChat = ChatsCache.getChat(mChatId);
        TextDrawable textDrawable;
        String countStatus;
        if (currentGroup != null && mSubHeaderTextView != null) {
            mHeaderTextView.setText(currentChat.title);

            textDrawable = Utils.getChatAvatar(currentChat, 61);
            mAvatarImageView.setChatPhoto(currentChat.photo, textDrawable);
            mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentChat.photo.small.id != 0) {
                        PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                        Bundle args = new Bundle();
                        TdApi.File smallPhoto = ChatsCache.getFile(currentChat.photo.small.id);
                        TdApi.File bigPhoto = ChatsCache.getFile(currentChat.photo.big.id);
                        if (bigPhoto.path.length() != 0) {
                            args.putString("path", bigPhoto.path);
                        } else {
                            args.putString("path", smallPhoto.path);
                            args.putInt("big_photo_id", bigPhoto.id);
                        }
                        photoViewFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                    }
                }
            });

            countStatus = Utils.formatMembersCount(currentGroup.participantsCount);

            if (mOnlineCount > 0) {
                mSubHeaderTextView.setText(String.format("%s, %d %s", countStatus, mOnlineCount, Utils.sContext.getString(R.string.online_status)));
            } else {
                mSubHeaderTextView.setText(countStatus);
            }
        }
    }

    private void updateChannel() {
        final TdApi.Chat currentChat = ChatsCache.getChat(mChatId);
        TextDrawable textDrawable;
        String countStatus;
        if (currentChat != null && mSubHeaderTextView != null) {
            mHeaderTextView.setText(currentChat.title);

            textDrawable = Utils.getChatAvatar(currentChat, 61);
            mAvatarImageView.setChatPhoto(currentChat.photo, textDrawable);
            mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentChat.photo.small.id != 0) {
                        PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                        Bundle args = new Bundle();
                        TdApi.File smallPhoto = ChatsCache.getFile(currentChat.photo.small.id);
                        TdApi.File bigPhoto = ChatsCache.getFile(currentChat.photo.big.id);
                        if (bigPhoto.path.length() != 0) {
                            args.putString("path", bigPhoto.path);
                        } else {
                            args.putString("path", smallPhoto.path);
                            args.putInt("big_photo_id", bigPhoto.id);
                        }
                        photoViewFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                    }
                }
            });

            countStatus = Utils.formatMembersCount(mChannelFull != null ? mChannelFull.participantsCount : 0);

            if (mOnlineCount > 0) {
                mSubHeaderTextView.setText(String.format("%s, %d %s", countStatus, mOnlineCount, Utils.sContext.getString(R.string.online_status)));
            } else {
                mSubHeaderTextView.setText(countStatus);
            }
        }
    }


    private void updateVisibleItems() {
        if (mProfileListView != null) {
            int count = mProfileListView.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = mProfileListView.getChildAt(i);
                Object tag = child.getTag();
                if (tag instanceof GroupProfileAdapter.ViewHolder) {
                    ((GroupProfileAdapter.ViewHolder)tag).updateItem();
                }
            }
        }
    }

    private void createToolbarMenu() {
        if (mMoreMenuButton == null) {
            return;
        }

        if (mUserId != 0) {
            mMoreMenuButton.clearItems();
            TdApi.User currentUser = ContactsCache.getInstance().assignUsers.get(mUserId);
            if (currentUser == null) {
                currentUser = ChatsCache.getUser(mUserId);
                if (currentUser == null) {
                    return;
                }
                if (currentUser.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR) {
                    TdApi.UserTypeBot userBot = (TdApi.UserTypeBot) currentUser.type;
                    if (userBot.canJoinGroupChats) {
                        mMoreMenuButton.addItem(ADD_TO_GROUP_MENU_ITEM, Utils.sContext.getString(R.string.action_add_to_group));
                    }
                    mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
                } else {
                    if (currentUser.phoneNumber != null && currentUser.phoneNumber.length() != 0) {
                        mMoreMenuButton.addItem(ADD_MENU_ITEM, Utils.sContext.getString(R.string.action_add));
                        mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
                    }
                }
                mBlockItemTextView = mMoreMenuButton.addItem(BLOCK_MENU_ITEM, (mUserFull != null && mUserFull.isBlocked) ? Utils.sContext.getString(R.string.action_unblock) : Utils.sContext.getString(R.string.action_block));
            } else {
                mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
                mBlockItemTextView = mMoreMenuButton.addItem(BLOCK_MENU_ITEM, (mUserFull != null && mUserFull.isBlocked) ? Utils.sContext.getString(R.string.action_unblock) : Utils.sContext.getString(R.string.action_block));
                mMoreMenuButton.addItem(EDIT_MENU_ITEM, Utils.sContext.getString(R.string.action_edit));
                mMoreMenuButton.addItem(DELETE_MENU_ITEM, Utils.sContext.getString(R.string.action_delete));
            }
        } else if (mGroupId != 0) {
            mMoreMenuButton.addItem(ADD_MENU_ITEM, Utils.sContext.getString(R.string.action_add_member));
            mMoreMenuButton.addItem(EDIT_MENU_ITEM, Utils.sContext.getString(R.string.action_edit_name));
            mMoreMenuButton.addItem(DELETE_MENU_ITEM, Utils.sContext.getString(R.string.action_delete_and_leave));
        }
    }

    private abstract class ProfileAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.empty_list_item, parent, false);
                }
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0 || position == 1 || position == 2 || position == 3) {
                return 0;
            }
            return super.getItemViewType(position);
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    private class UserProfileAdapter extends ProfileAdapter {

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return 7;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_big_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView secondaryTextView = (TextView)convertView.findViewById(R.id.secondaryTextView);
                TdApi.User user = ChatsCache.getUser(mUserId);
                if (user != null) {
                    if (position == 4) {
                        iconImageView.setImageResource(R.drawable.ic_user);
                        if (user.username == null || user.username.length() == 0) {
                            primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                        } else {
                            primaryTextView.setText("@" + user.username);
                        }
                        secondaryTextView.setText(Utils.sContext.getString(R.string.profile_username));
                    } else if (position == 6 && mUserFull != null) {
                        if (mUserFull.botInfo.getConstructor() == TdApi.BotInfoEmpty.CONSTRUCTOR) {
                            iconImageView.setImageResource(R.drawable.ic_phone);
                            if (user.phoneNumber == null || user.phoneNumber.length() == 0 || user.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                                primaryTextView.setText(Utils.sContext.getString(R.string.unknown_phone));
                            } else {
                                primaryTextView.setText(Utils.formatPhone("+" + user.phoneNumber));
                            }
                            secondaryTextView.setText(Utils.sContext.getString(R.string.profile_phone));
                        } else if (mUserFull.botInfo.getConstructor() == TdApi.BotInfoGeneral.CONSTRUCTOR) {
                            iconImageView.setImageResource(R.drawable.ic_about);
                            TdApi.BotInfoGeneral botInfoGeneral = (TdApi.BotInfoGeneral) mUserFull.botInfo;
                            if (botInfoGeneral.shareText == null || botInfoGeneral.shareText.length() == 0) {
                                primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                            } else {
                                primaryTextView.setText(botInfoGeneral.shareText);
                            }
                            secondaryTextView.setText(Utils.sContext.getString(R.string.profile_about));
                        }
                    }
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_divider, parent, false);
                }
            }
            return super.getView(position, convertView, parent);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 4 || position == 6) {
                return 1;
            } else if (position == 5) {
                return 2;
            }
            return super.getItemViewType(position);
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }
    }

    private class GroupProfileAdapter extends ProfileAdapter {

        @Override
        public boolean isEnabled(int position) {
            int offset = 6;
            if (mCurrentGroup != null && mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                offset += 2;
            }
            return position == 4 || (offset != 6 && position == 6) || (position > offset && position < getCount() - 1);
        }

        @Override
        public int getCount() {
            int count = 7;
            if (mCurrentGroup != null && mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                count += 2;
            }
            if (mChatParticipants != null) {
                count += mChatParticipants.length;
            }
            return ++count;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView valueTextView = (TextView)convertView.findViewById(R.id.valueTextView);
                iconImageView.setVisibility(View.VISIBLE);
                iconImageView.setImageResource(R.drawable.ic_add);
                primaryTextView.setText(Utils.sContext.getString(R.string.action_add_member));
                valueTextView.setVisibility(View.GONE);
                if (mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR && position == 6) {
                    iconImageView.setImageResource(R.drawable.ic_star_rate_gray);
                    primaryTextView.setText(Utils.sContext.getString(R.string.show_admins));
                    valueTextView.setVisibility(View.VISIBLE);
                    if (mCurrentGroup.anyoneCanEdit) {
                        valueTextView.setText(Utils.sContext.getString(R.string.all_admins));
                    } else {
//                        if (mChannelFull != null) {
//                            valueTextView.setText(mChannelFull.adminsCount);
//                        }
                    }
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.space_list_item, parent, false);
                }
            } else if (type == 3) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.group_list_item, parent, false);
                }
                ViewHolder viewHolder = (ViewHolder)convertView.getTag();
                if (viewHolder == null) {
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }
                int offset = 7;
                if (mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                    offset += 2;
                }

                viewHolder.user = ChatsCache.getUser(mSortedUsers.get(position - offset).user.id);
                viewHolder.updateItem();
            }
            return super.getView(position, convertView, parent);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0 || position == 1 || position == 2 || position == 3 || position == getCount() - 1) {
                return 0;
            } else if (position == 4) {
                return 1;
            } else if (position == 5) {
                return 2;
            } else if (mCurrentGroup != null && mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                if (position == 6) {
                    return 1;
                } else if (position == 7) {
                    return 2;
                } else if (position == 8) {
                    return 0;
                }
            } else if (position == 6) {
                return 0;
            }
            return 3;
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        private class ViewHolder {
            public final BlockingImageView avatarImageView;
            public final TextView primaryTextView;
            public final TextView secondaryTextView;
            public TdApi.User user;

            public ViewHolder(View itemView) {
                avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
                primaryTextView = (TextView)itemView.findViewById(R.id.primaryTextView);
                secondaryTextView = (TextView)itemView.findViewById(R.id.secondaryTextView);
            }

            public void updateItem() {
                avatarImageView.setProfilePhoto(user.profilePhoto, Utils.getUserAvatar(user, 48));
                if (user.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    primaryTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                } else {
                    primaryTextView.setText(Utils.formatName(user.firstName, user.lastName));
                }
                String userStatus = Utils.formatUserStatus(user);
                switch (userStatus) {
                    case "online":
                        secondaryTextView.setTextColor(0xff6b9cc2);
                        break;
                    case "bot":
                        secondaryTextView.setTextColor(0xff8a8a8a);
                        TdApi.UserTypeBot userBot = (TdApi.UserTypeBot)user.type;
                        if (userBot.canReadAllGroupChatMessages) {
                            userStatus = Utils.sContext.getString(R.string.access_status);
                        } else {
                            userStatus = Utils.sContext.getString(R.string.no_access_status);
                        }
                        break;
                    default:
                        secondaryTextView.setTextColor(0xff8a8a8a);
                }
                secondaryTextView.setText(userStatus);
            }
        }
    }

    private class ChannelProfileAdapter extends ProfileAdapter {

        @Override
        public boolean isEnabled(int position) {
            if (mCurrentChannel != null) {
                return mCurrentChannel.isSupergroup && (position == 5 || position == 7 || position == 9);
            }
            return super.isEnabled(position);
        }

        @Override
        public int getCount() {
            int count = 7;
            if (!mCurrentChannel.isSupergroup) {
                return count;
            }
            if (mCurrentChannel.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                count += 2;
            }
            return ++count;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_big_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView secondaryTextView = (TextView)convertView.findViewById(R.id.secondaryTextView);
                if ((mCurrentChannel.isSupergroup && position == 4) || (position == 6 && !mCurrentChannel.isSupergroup)) {
                    iconImageView.setImageResource(R.drawable.ic_about);
                    primaryTextView.setText(mChannelFull != null ? mChannelFull.about : "");
                    secondaryTextView.setText(Utils.sContext.getString(R.string.profile_desc));
                } else if (!mCurrentChannel.isSupergroup && position == 4) {
                    iconImageView.setImageResource(R.drawable.ic_link_gray);
                    if (mCurrentChannel.username.length() != 0) {
                        primaryTextView.setText("/" + mCurrentChannel.username);
                    } else {
                        primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                    }
                    secondaryTextView.setText(Utils.sContext.getString(R.string.profile_link));
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_divider, parent, false);
                }
            } else if (type == 3) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView valueTextView = (TextView)convertView.findViewById(R.id.valueTextView);
                iconImageView.setVisibility(View.VISIBLE);
                if (mCurrentChannel.isSupergroup) {
                    if (position == 5) {
                        iconImageView.setImageResource(R.drawable.ic_add);
                        primaryTextView.setText(Utils.sContext.getString(R.string.action_add_member));
                        valueTextView.setVisibility(View.GONE);
                    } else if (position == 7) {
                        iconImageView.setImageResource(R.drawable.ic_supervisor_account_gray);
                        primaryTextView.setText(Utils.sContext.getString(R.string.show_members));
                        valueTextView.setVisibility(View.VISIBLE);
                        valueTextView.setText(mChannelFull != null ? String.valueOf(mChannelFull.participantsCount) : "");
                    }
                    if (mCurrentChannel.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR && position == 9) {
                        iconImageView.setImageResource(R.drawable.ic_star_rate_gray);
                        primaryTextView.setText(Utils.sContext.getString(R.string.show_admins));
                        valueTextView.setVisibility(View.VISIBLE);
                        if (mChannelFull != null) {
                            if (mChannelFull.adminsCount == mChannelFull.participantsCount) {
                                valueTextView.setText(Utils.sContext.getString(R.string.all_admins));
                            } else {
                                valueTextView.setText(mChannelFull.adminsCount);
                            }
                        }
                    }
                }
            } else if (type == 4) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.space_list_item, parent, false);
                }
            }
            return super.getView(position, convertView, parent);
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 4) {
                return 1;
            } else if (position == 5) {
                if (!mCurrentChannel.isSupergroup) {
                    return 2;
                } else {
                    return 3;
                }
            } else if (position == 6) {
                if (!mCurrentChannel.isSupergroup) {
                    return 1;
                } else {
                    return 4;
                }
            } else if (position == 7) {
                return 3;
            } else if (mCurrentChannel.role.getConstructor() == TdApi.ChatParticipantRoleAdmin.CONSTRUCTOR) {
                if (position == 8) {
                    return 2;
                } else if (position == 9) {
                    return 3;
                }
            }
            return super.getItemViewType(position);
        }

        @Override
        public int getViewTypeCount() {
            return 5;
        }
    }
}
