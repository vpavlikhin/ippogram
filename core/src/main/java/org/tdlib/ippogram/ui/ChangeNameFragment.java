package org.tdlib.ippogram.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Utils;

public class ChangeNameFragment extends MainFragment {

    private EditText mFirstNameField;
    private EditText mLastNameField;
    private ImageButton mDoneImageButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_change_name, container, false);

        mToolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(ChangeNameFragment.this);
            }
        });

        mHeaderTextView = (TextView) mToolbar.findViewById(R.id.headerTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
            mHeaderTextView.setText(Utils.sContext.getString(R.string.action_edit_name));
        }

        mFirstNameField = (EditText) mRootView.findViewById(R.id.firstNameField);
        mFirstNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mLastNameField.requestFocus();
                    mLastNameField.setSelection(mLastNameField.length());
                    return true;
                }
                return false;
            }
        });

        mLastNameField = (EditText) mRootView.findViewById(R.id.lastNameField);
        mLastNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });

        final TdApi.User me = ChatsCache.getUser(Utils.sCurrentUserId);
        if (me != null) {
            mFirstNameField.setText(me.firstName);
            mFirstNameField.setSelection(mFirstNameField.length());
            mLastNameField.setText(me.lastName);
        }

        mDoneImageButton = (ImageButton) mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFirstNameField.getText().length() != 0 && me != null) {
                    me.firstName = mFirstNameField.getText().toString();
                    me.lastName = mLastNameField.getText().toString();
                    Utils.sClient.send(new TdApi.ChangeName(mFirstNameField.getText().toString(), mLastNameField.getText().toString()), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {

                        }
                    });
                    ((MainActivity) getParentActivity()).destroyFragment(ChangeNameFragment.this);
                }
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mFirstNameField != null) {
            mFirstNameField.requestFocus();
            Utils.showKeyboard(mFirstNameField);
        }
    }
}
