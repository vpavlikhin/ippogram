package org.tdlib.ippogram.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Emoji;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.EmptyRecyclerView;
import org.tdlib.ippogram.ui.components.TextDrawable;

public class ChatsAdapter extends EmptyRecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    /*public interface OnItemClickListener {
        void onClick(View view, int position);
    }*/

    //private OnItemClickListener mListener;
    //private GenericRequestBuilder<TextDrawable, Drawable, Drawable, Drawable> glide;

    public ChatsAdapter(/*OnItemClickListener listener*/) {
        //mListener = listener;
        /*glide = Glide
                .with(Utils.sContext)
                .using(new PassthroughModelLoader<Drawable, TextDrawable>(), Drawable.class)
                .from(TextDrawable.class)
                .as(Drawable.class)
                .decoder(new DrawableResourceDecoder())
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE);*/
    }

    @Override
    public ChatsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (viewType != 2) {
            v = inflater.inflate(R.layout.chats_list_item, parent, false);
        } else {
            v = inflater.inflate(R.layout.loading_more, parent, false);
        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ChatsAdapter.ViewHolder holder, int position) {
        if (getItemViewType(position) == 2) {
            return;
        }

        TdApi.Chat chat = ChatsCache.sChatsList.get(position);
        //Log.d(Utils.TAG, "" + chat);
        TdApi.Message message = chat.topMessage;
        TdApi.User user = null;
        TdApi.Group group = null;
        TdApi.Channel channel = null;

        TdApi.ChatInfo chatInfo = chat.type;
        if (chatInfo.getConstructor() == TdApi.PrivateChatInfo.CONSTRUCTOR) {
            TdApi.PrivateChatInfo privateChatInfo = (TdApi.PrivateChatInfo)chatInfo;
            user = ChatsCache.getUser(privateChatInfo.user.id);
            if (user != null && user.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR) {
                holder.groupImageView.setVisibility(View.VISIBLE);
                holder.groupImageView.setImageResource(R.drawable.ic_bot);
            } else {
                holder.groupImageView.setVisibility(View.GONE);
            }
        } else if (chatInfo.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
            TdApi.GroupChatInfo groupChatInfo = (TdApi.GroupChatInfo)chatInfo;
            group = ChatsCache.getGroup(groupChatInfo.group.id);
            holder.groupImageView.setVisibility(View.VISIBLE);
            holder.groupImageView.setImageResource(R.drawable.ic_group);
        } else if (chatInfo.getConstructor() == TdApi.ChannelChatInfo.CONSTRUCTOR) {
            TdApi.ChannelChatInfo channelChatInfo = (TdApi.ChannelChatInfo)chatInfo;
            channel = ChatsCache.getChannel(channelChatInfo.channel.id);
            holder.groupImageView.setVisibility(View.VISIBLE);
            if (channel != null) {
                if (channel.isSupergroup) {
                    holder.groupImageView.setImageResource(R.drawable.ic_group);
                } else {
                    holder.groupImageView.setImageResource(R.drawable.ic_channel);
                }
            }
        }

        if (group != null || channel != null) {
            holder.userNameTextView.setText(chat.title);
        } else if (user != null) {
            if (user.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                holder.userNameTextView.setText(Utils.sContext.getString(R.string.deleted_account));
            } else {
                holder.userNameTextView.setText(chat.title);
            }
        }

        TextDrawable placeholder = Utils.getChatAvatar(chat, 56);
        holder.avatarImageView.setChatPhoto(chat.photo, placeholder);

        if (channel != null && channel.isVerified || user != null && user.isVerified) {
            holder.muteImageView.setVisibility(View.VISIBLE);
            holder.muteImageView.setImageResource(R.drawable.ic_verify);
        } else {
            if (!ChatsCache.isChatMuted(chat.id)) {
                holder.muteImageView.setVisibility(View.GONE);
            } else {
                holder.muteImageView.setVisibility(View.VISIBLE);
                holder.muteImageView.setImageResource(R.drawable.ic_mute);
            }
        }

        if (message == null || message.id == 0) {
            holder.messageTextView.setText("");
            if (message != null) {
                holder.timeTextView.setText(Utils.getFormatDate(message.date));
            } else {
                holder.timeTextView.setText("");
            }
            holder.badgeTextView.setVisibility(View.GONE);
            holder.clockImageView.setVisibility(View.GONE);
            holder.errorImageView.setVisibility(View.GONE);
        } else {
            user = ChatsCache.getUser(message.fromId);
            holder.timeTextView.setText(Utils.getFormatDate(message.date));
            TdApi.MessageContent messageContent = message.content;
            String fullName;
            CharSequence messageStr = null;
            if (messageContent.getConstructor() == TdApi.MessageText.CONSTRUCTOR || messageContent.getConstructor() == TdApi.MessageWebPage.CONSTRUCTOR) {
                if (messageContent.getConstructor() == TdApi.MessageText.CONSTRUCTOR) {
                    TdApi.MessageText messageText = (TdApi.MessageText) messageContent;
                    messageStr = messageText.text;
                } else {
                    TdApi.MessageWebPage messageWebPage = (TdApi.MessageWebPage) messageContent;
                    messageStr = messageWebPage.text;
                }
                holder.messageTextView.setTextColor(0xff8a8a8a);
                if (group != null && user != null) {
                    if (user.id == Utils.sCurrentUserId) {
                        fullName = Utils.sContext.getString(R.string.message_you);
                    } else {
                        if (user.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                            fullName = Utils.sContext.getString(R.string.deleted_caps);
                        } else {
                            fullName = Utils.formatName(user.firstName, user.lastName);
                        }
                    }
                    SpannableString text = new SpannableString(String.format("%s: %s", fullName, messageStr));
                    text.setSpan(new ForegroundColorSpan(0xff222222), 0, fullName.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageStr = text;
                } else if (user != null && user.id == Utils.sCurrentUserId) {
                    messageStr = Html.fromHtml(String.format("<font color='#222222'>%s:</font> %s", Utils.sContext.getString(R.string.message_you), messageStr));
                }
            } else if (messageContent.getConstructor() == TdApi.MessageDeleted.CONSTRUCTOR) {
                holder.messageTextView.setTextColor(0xff8a8a8a);
                messageStr = Utils.convertMessageToText(message);
            } else if (messageContent.getConstructor() == TdApi.MessageChatAddParticipants.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChatChangePhoto.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChatChangeTitle.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChatDeletePhoto.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChatJoinByLink.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageGroupChatCreate.CONSTRUCTOR ||
                    messageContent.getConstructor() == TdApi.MessageChannelChatCreate.CONSTRUCTOR) {
                holder.messageTextView.setTextColor(0xff6b9cc2);
                messageStr = Utils.convertMessageToText(message);
                if (messageContent.getConstructor() == TdApi.MessageChatDeleteParticipant.CONSTRUCTOR) {
                    TdApi.User deletedUser = ((TdApi.MessageChatDeleteParticipant)messageContent).user;
                    if (group != null && deletedUser.id == Utils.sCurrentUserId && group.role.getConstructor() != TdApi.ChatParticipantRoleLeft.CONSTRUCTOR) {
                        group.role = new TdApi.ChatParticipantRoleLeft();
                    }
                } else if (messageContent.getConstructor() == TdApi.MessageChatAddParticipants.CONSTRUCTOR) {
                    TdApi.User[] addedUsers = ((TdApi.MessageChatAddParticipants)messageContent).participants;
                    for (TdApi.User participant : addedUsers) {
                        if (group != null && participant.id == Utils.sCurrentUserId && group.role.getConstructor() == TdApi.ChatParticipantRoleLeft.CONSTRUCTOR) {
                            group.role = new TdApi.ChatParticipantRoleGeneral();
                            break;
                        }
                    }
                }
            } else if (messageContent.getConstructor() != TdApi.MessageUnsupported.CONSTRUCTOR) {
                holder.messageTextView.setTextColor(0xff6b9cc2);
                if (group != null && user != null) {
                    if (user.id == Utils.sCurrentUserId) {
                        fullName = Utils.sContext.getString(R.string.message_you);
                    } else {
                        fullName = Utils.formatName(user.firstName, user.lastName);
                    }
                    SpannableString text = new SpannableString(String.format("%s: %s", fullName, Utils.convertMessageToText(message)));
                    text.setSpan(new ForegroundColorSpan(0xff222222), 0, fullName.length() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    messageStr = text;
                } else if (user != null && user.id == Utils.sCurrentUserId) {
                    messageStr = Html.fromHtml(String.format("<font color='#222222'>%s:</font> %s", Utils.sContext.getString(R.string.message_you), Utils.convertMessageToText(message)));
                } else {
                    messageStr = Utils.convertMessageToText(message);
                }
            }
            holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView, false));

            if (chat.unreadCount != 0) {
                holder.badgeTextView.setVisibility(View.VISIBLE);
                holder.badgeTextView.setText(String.format("%d", chat.unreadCount));
            } else {
                holder.badgeTextView.setVisibility(View.GONE);
            }

            if (message.sendState.getConstructor() != TdApi.MessageIsIncoming.CONSTRUCTOR) {
                if (message.sendState.getConstructor() == TdApi.MessageIsFailedToSend.CONSTRUCTOR) {
                    holder.badgeTextView.setVisibility(View.GONE);
                    holder.clockImageView.setVisibility(View.GONE);
                    holder.errorImageView.setVisibility(View.VISIBLE);
                } else {
                    if (message.sendState.getConstructor() == TdApi.MessageIsBeingSent.CONSTRUCTOR) {
                        holder.clockImageView.setVisibility(View.VISIBLE);
                        holder.clockImageView.setImageResource(R.drawable.ic_clock);
                        holder.errorImageView.setVisibility(View.GONE);
                    } else if (message.sendState.getConstructor() == TdApi.MessageIsSuccessfullySent.CONSTRUCTOR){
                        if (message.id <= chat.lastReadOutboxMessageId) {
                            holder.clockImageView.setVisibility(View.GONE);
                        } else {
                            holder.clockImageView.setVisibility(View.VISIBLE);
                            holder.clockImageView.setImageResource(R.drawable.ic_blue_point);
                        }
                        holder.errorImageView.setVisibility(View.GONE);
                    }
                }
            } else {
                holder.clockImageView.setVisibility(View.GONE);
                holder.errorImageView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        int count = ChatsCache.sChatsList.size();
        if (count == 0 && ChatsCache.sLoadingChats) {
            return 0;
        }
        if (!ChatsCache.sChatsEndReached) {
            ++count;
        }
        return count;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == ChatsCache.sChatsList.size()) {
            return 2;
        }
        TdApi.Chat chat = ChatsCache.sChatsList.get(position);
        TdApi.Message message = chat.topMessage;
        if (message == null) {
            return 0;
        }
        TdApi.ChatInfo chatInfo = chat.type;
        if (chatInfo.getConstructor() == TdApi.GroupChatInfo.CONSTRUCTOR) {
            return 1;
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final BlockingImageView avatarImageView;
        public final TextView userNameTextView;
        public final TextView messageTextView;
        public final TextView timeTextView;
        public final TextView badgeTextView;
        public final ImageView errorImageView;
        public final ImageView clockImageView;
        public final ImageView muteImageView;
        public final ImageView groupImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (position == -1) {
                        position = getLayoutPosition();
                    }
                    mListener.onClick(v, position);
                }
            });*/
            avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
            userNameTextView = (TextView)itemView.findViewById(R.id.userNameTextView);
            messageTextView = (TextView)itemView.findViewById(R.id.messageTextView);
            timeTextView = (TextView)itemView.findViewById(R.id.timeTextView);
            badgeTextView = (TextView)itemView.findViewById(R.id.badgeTextView);
            errorImageView = (ImageView)itemView.findViewById(R.id.errorImageView);
            clockImageView = (ImageView)itemView.findViewById(R.id.clockImageView);
            muteImageView = (ImageView)itemView.findViewById(R.id.muteImageView);
            groupImageView = (ImageView)itemView.findViewById(R.id.groupImageView);

            if (userNameTextView != null) {
                userNameTextView.setTypeface(Utils.getTypeface("medium"));
            }
            if (messageTextView != null) {
                messageTextView.setTypeface(Utils.getTypeface("regular"));
            }
            if (timeTextView != null) {
                timeTextView.setTypeface(Utils.getTypeface("regular"));
            }
            if (badgeTextView != null) {
                badgeTextView.setTypeface(Utils.getTypeface("medium"));
            }
        }
    }
 }

/*class PassthroughModelLoader<Result, Source extends Result> implements ModelLoader<Source, Result> {
    @Override
    public DataFetcher<Result> getResourceFetcher(final Source model, int width, int height) {
        return new CastingDataFetcher<>(model); // upcasting is ok
    }


    private static class CastingDataFetcher<Source, Result> implements DataFetcher<Result> {
        private final Source model;
        public CastingDataFetcher(Source model) {
            this.model = model;
        }

        @SuppressWarnings("unchecked")
        @Override
        public Result loadData(Priority priority) throws Exception {
            return (Result) model;
        }

        @Override
        public void cleanup() {

        }
        @Override
        public String getId() {
            return "";

        }
        @Override
        public void cancel() {

        }
    }
}*/

/*class BitmapBitmapResourceDecoder implements ResourceDecoder<Bitmap, Bitmap> {
    private final BitmapPool pool;
    public BitmapBitmapResourceDecoder(Context context) {
        this(Glide.get(context).getBitmapPool());
    }
    public BitmapBitmapResourceDecoder(BitmapPool pool) {
        this.pool = pool;
    }

    @Override
    public Resource<Bitmap> decode(Bitmap source, int width, int height) throws IOException {
        return BitmapResource.obtain(source, pool);
    }

    @Override public String getId() { return "BitmapBitmapResourceDecoder"; }
}*/

/*class DrawableResourceDecoder implements ResourceDecoder<Drawable, Drawable> {

    @Override
    public Resource<Drawable> decode(Drawable source, int width, int height) throws IOException {
        return new DrawableResource<Drawable>(source) {
            @Override
            public int getSize() {
                return 1;
            }

            @Override
            public void recycle() {

            }
        };
    }

    @Override
    public String getId() {
        return "DrawableDrawableResourceDecoder";
    }
}*/
