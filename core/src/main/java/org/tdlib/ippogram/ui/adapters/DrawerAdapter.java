package org.tdlib.ippogram.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ui.R;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;

public class DrawerAdapter extends AbsDrawerAdapter<DrawerAdapter.ViewHolder> {

    private String[] mData;
    private int[] mResIconId;
    private OnItemClickListener mListener;

    public DrawerAdapter(String[] data, int[] resIconId, OnItemClickListener listener) {
        mData = data;
        mResIconId = resIconId;
        mListener = listener;
    }

    @Override
    protected int getMainItemCount() {
        return mData.length;
    }

    @Override
    protected int getMainItemType(int position) {
        return 0;
    }

    @Override
    protected ViewHolder onCreateMainViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.drawer_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    protected HeaderHolder onCreateHeaderViewHolder(LayoutInflater inflater, ViewGroup parent) {
        View v = inflater.inflate(R.layout.drawer_header, parent, false);
        return new ViewHeaderHolder(v);
    }

    @Override
    public void onBindMainViewHolder(ViewHolder holder, int position) {
        holder.iconImageView.setImageResource(mResIconId[position]);
        holder.itemTextView.setText(mData[position]);
    }

    @Override
    protected <HH extends HeaderHolder> void onBindHeaderViewHolder(HH holder) {
        TdApi.User me = ChatsCache.getUser(Utils.sCurrentUserId);
        if (me != null) {
            ((ViewHeaderHolder) holder).nameTextView.setText(Utils.formatName(me.firstName, me.lastName));
            ((ViewHeaderHolder) holder).phoneTextView.setText(Utils.formatPhone("+" + me.phoneNumber));
            ((ViewHeaderHolder) holder).avatarImageView.setProfilePhoto(me.profilePhoto, Utils.getUserAvatar(me, 64));
        }
    }

    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    protected static class ViewHeaderHolder extends HeaderHolder {

        public final BlockingImageView avatarImageView;
        public final TextView nameTextView;
        public final TextView phoneTextView;

        public ViewHeaderHolder(final View itemView) {
            super(itemView);
            avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
            nameTextView = (TextView)itemView.findViewById(R.id.userNameTextView);
            phoneTextView = (TextView)itemView.findViewById(R.id.phoneTextView);

            if (nameTextView != null) {
                nameTextView.setTypeface(Utils.getTypeface("medium"));
            }
            if (phoneTextView != null) {
                phoneTextView.setTypeface(Utils.getTypeface("regular"));
            }
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        public final ImageView iconImageView;
        public final TextView itemTextView;

        public ViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onClick(v, getLayoutPosition());
                }
            });
            itemTextView = (TextView)itemView.findViewById(R.id.itemTextView);
            iconImageView = (ImageView)itemView.findViewById(R.id.iconImageView);
            if (itemTextView != null) {
                itemTextView.setTypeface(Utils.getTypeface("medium"));
            }
        }
    }
}
