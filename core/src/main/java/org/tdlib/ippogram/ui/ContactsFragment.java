package org.tdlib.ippogram.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.adapters.ContactsAdapter;
import org.tdlib.ippogram.ui.components.PinnedHeaderListView;

import java.util.ArrayList;

public class ContactsFragment extends MainFragment implements UpdatesHandler.UpdatesForContactsDelegate {

    private PinnedHeaderListView mContactsListView;
    private ContactsAdapter mContactsAdapter;

    private TextView mSubHeaderTextView;

    private long mChatId;

    private boolean mCreateChat = false;
    private boolean mCreateGroup = false;
    private boolean mCreateChannel = false;

    private ArrayList<Integer> mCheckedUsers;
    private ArrayList<Integer> mIgnoreUsers;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            updateVisibleItems();
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);
        mChatId = getArguments().getLong("chat_id", 0);
        mCreateChat = getArguments().getBoolean("create_chat", false);
        mCreateGroup = getArguments().getBoolean("create_group", false);
        mCreateChannel = getArguments().getBoolean("create_channel", false);
        mIgnoreUsers = getArguments().getIntegerArrayList("ignore_users");
        if (mCreateGroup) {
            mCheckedUsers = new ArrayList<>();
        }
        Utils.runOnUIThread(runnable, 1000L);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_contacts, container, false);

        mToolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(ContactsFragment.this);
            }
        });

        mHeaderTextView = (TextView) mToolbar.findViewById(R.id.headerTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
            if (mCreateGroup) {
                mHeaderTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                mHeaderTextView.setText(Utils.sContext.getString(R.string.new_group));
            } else {
                mHeaderTextView.setText(Utils.sContext.getString(R.string.select_contact_title));
            }
        }

        if (mCreateGroup) {
            mSubHeaderTextView = (TextView) mToolbar.findViewById(R.id.subHeaderTextView);
            mSubHeaderTextView.setVisibility(View.VISIBLE);
            mSubHeaderTextView.setText(String.format(Utils.sContext.getString(R.string.select_members), mCheckedUsers.size()));
            ImageButton doneImageButton = (ImageButton) mToolbar.findViewById(R.id.doneImageButton);
            doneImageButton.setVisibility(View.VISIBLE);
            doneImageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCheckedUsers.size() == 0) {
                        return;
                    }
                    GroupCreateFragment fragment = new GroupCreateFragment();
                    Bundle args = new Bundle();
                    args.putIntegerArrayList("checked_users", mCheckedUsers);
                    fragment.setArguments(args);
                    ((MainActivity) getParentActivity()).createFragment(fragment, "create_group", true, false, true, false);
                }
            });
        }

        mContactsListView = (PinnedHeaderListView)mRootView.findViewById(R.id.contactsListView);
        mContactsListView.setVerticalScrollBarEnabled(false);
        View headerView = inflater.inflate(R.layout.letter_section, mContactsListView, false);
        ViewGroup.LayoutParams params = headerView.getLayoutParams();
        params.height = Utils.convertDpToPx(64);
        headerView.setLayoutParams(params);
        mContactsListView.setPinnedHeaderView(headerView);

        mContactsAdapter = new ContactsAdapter();
        mContactsAdapter.setPinnedHeaderBackgroundColor(ContextCompat.getColor(Utils.sContext, android.R.color.transparent));
        mContactsAdapter.setPinnedHeaderTextColor(ContextCompat.getColor(Utils.sContext, R.color.gray));
        if (mCreateGroup) {
            mContactsAdapter.setSelectedUsers(mCheckedUsers);
        } else {
            mContactsAdapter.setIgnoreUsers(mIgnoreUsers);
        }

        mContactsListView.setAdapter(mContactsAdapter);
        mContactsListView.setOnScrollListener(mContactsAdapter);
        mContactsListView.setEnableHeaderTransparencyChanges(true);
        mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mCreateChat) {
                    Utils.showProgressDialog(getParentActivity(), Utils.sContext.getString(R.string.loading));
                    int index = position - mContactsAdapter.getSectionForPosition(position);
                    final int userId = ContactsCache.getInstance().users.get(index);
                    Utils.sClient.send(new TdApi.CreatePrivateChat(userId), new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                                final TdApi.Chat chat = (TdApi.Chat) object;
                                if (!ChatsCache.sChats.containsKey(chat.id)) {
                                    ChatsCache.sChats.put(chat.id, chat);
                                }
                                Utils.runOnUIThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ChatFragment chatFragment = new ChatFragment();
                                        Bundle args = new Bundle();
                                        args.putLong("chat_id", chat.id);
                                        args.putInt("user_id", userId);
                                        chatFragment.setArguments(args);
                                        ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + chat.id, true, false, true, false);
                                        Utils.hideProgressDialog(getParentActivity());
                                    }
                                });
                            }
                        }
                    });
                } else if (mCreateGroup) {
                    int index = position - mContactsAdapter.getSectionForPosition(position);
                    TdApi.User user = ChatsCache.getUser(ContactsCache.getInstance().users.get(index));
                    if (user == null) {
                        return;
                    }
                    if (mCheckedUsers.contains(user.id)) {
                        mCheckedUsers.remove(Integer.valueOf(user.id));
                    } else {
                        mCheckedUsers.add(user.id);
                    }
                    mSubHeaderTextView.setText(String.format(Utils.sContext.getString(R.string.select_members), mCheckedUsers.size()));
                    if (mContactsListView != null) {
                        mContactsListView.invalidateViews();
                    }
                } else {
                    int index = position - mContactsAdapter.getSectionForPosition(position);
                    final TdApi.User user = ChatsCache.getUser(ContactsCache.getInstance().users.get(index));
                    if (user == null || getParentActivity() == null) {
                        return;
                    }
                    if (mIgnoreUsers.contains(user.id)) {
                        return;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                    builder.setMessage(String.format(Utils.sContext.getString(R.string.add_member_text), Utils.formatName(user.firstName, user.lastName)));
                    final EditText forwardField = new EditText(getParentActivity());
                    forwardField.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
                    forwardField.setText("50");
                    forwardField.setGravity(Gravity.CENTER);
                    forwardField.setInputType(InputType.TYPE_CLASS_NUMBER);
                    forwardField.setImeOptions(EditorInfo.IME_ACTION_DONE);
                    forwardField.setSelection(forwardField.getText().length());
                    builder.setView(forwardField);
                    builder.setPositiveButton(R.string.ok_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mChatId != 0) {
                                int forwardLimit = Integer.parseInt(forwardField.getText().toString());
                                ChatsCache.addUserToChat(mChatId, user.id, forwardLimit);
                            }
                            ((MainActivity) getParentActivity()).destroyFragment(ContactsFragment.this);
                        }
                    });
                    builder.setNegativeButton(R.string.cancel_button, null);
                    showAlertDialog(builder);
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)forwardField.getLayoutParams();
                    params.gravity = Gravity.CENTER_HORIZONTAL;
                    params.leftMargin = params.rightMargin = Utils.convertDpToPx(24);
                    forwardField.setLayoutParams(params);
                }
            }
        });
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mContactsAdapter != null) {
            mContactsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void updateInterface() {
        if (mContactsListView != null) {
            mContactsListView.invalidateViews();
        }
    }

    @Override
    public void updateContactsList() {
        if (mContactsAdapter != null) {
            mContactsAdapter.notifyDataSetChanged();
        }
    }

    private void updateVisibleItems() {
        if (mContactsListView != null) {
            int count = mContactsListView.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = mContactsListView.getChildAt(i);
                Object tag = child.getTag();
                if (tag instanceof ContactsAdapter.ViewHolder) {
                    ((ContactsAdapter.ViewHolder)tag).updateItem();
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.cancelRunOnUIThread(runnable);
        ChatsCache.removeDelegate(this);
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }
}
