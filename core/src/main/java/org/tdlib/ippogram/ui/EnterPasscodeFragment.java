package org.tdlib.ippogram.ui;

import android.content.res.Configuration;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.LockPatternView;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EnterPasscodeFragment extends MainFragment {

    public static final int PIN_MENU_ITEM = 0;
    public static final int PASSWORD_MENU_ITEM = 1;
    public static final int PATTERN_MENU_ITEM = 2;
    public static final int GESTURE_MENU_ITEM = 3;

    private ToolbarMenuLayout mSpinnerLayout;
    private TextView mSpinnerTextView;
    private ImageButton mDoneImageButton;

    private final File mStoreFile = new File(Environment.getExternalStorageDirectory(), "gestures");
    private static GestureLibrary sStore;
    private Gesture mGesture;

    private String mChosenPassword;

    private ImageView mLogoImageView;
    private TextView mInfoTextView;
    private EditText mPasswordEditText;
    private FrameLayout mKeyboardLayout;
    private LockPatternView mLockPatternView;
    private List<LockPatternView.Cell> mPattern;
    private GestureOverlayView mLockGestureView;

    private static int sType;
    private static boolean sUnlock;

    public static EnterPasscodeFragment newInstance(int type, boolean unlock) {
        sType = type;
        sUnlock = unlock;
        return new EnterPasscodeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (sType == PIN_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_pin, container, false);
        } else if (sType == PASSWORD_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_password, container, false);
        } else if (sType == PATTERN_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_pattern, container, false);
        } else {
            mRootView = inflater.inflate(R.layout.passcode_gesture, container, false);
        }

        createView(inflater);
        return mRootView;
    }

    private void populateViewForOrientation(LayoutInflater inflater, ViewGroup container) {
        container.removeAllViewsInLayout();
        if (sType == PIN_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_pin, container);
        } else if (sType == PASSWORD_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_password, container);
        } else if (sType == PATTERN_MENU_ITEM) {
            mRootView = inflater.inflate(R.layout.passcode_pattern, container);
        } else {
            mRootView = inflater.inflate(R.layout.passcode_gesture, container);
        }
        createView(inflater);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LayoutInflater inflater = LayoutInflater.from(getParentActivity());
        populateViewForOrientation(inflater, (ViewGroup) getView());
    }

    private void createView(LayoutInflater inflater) {
        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(EnterPasscodeFragment.this);
            }
        });
        if (sUnlock) {
            mToolbar.setVisibility(View.GONE);
        }

        mSpinnerLayout = (ToolbarMenuLayout)mToolbar.findViewById(R.id.spinnerLayout);
        mSpinnerLayout.setParentToolbar(mToolbar);
        mSpinnerLayout.setDropDown(true);
        mSpinnerLayout.addItem(PIN_MENU_ITEM, Utils.sContext.getString(R.string.passcode_pin));
        mSpinnerLayout.addItem(PASSWORD_MENU_ITEM, Utils.sContext.getString(R.string.passcode_password));
        mSpinnerLayout.addItem(PATTERN_MENU_ITEM, Utils.sContext.getString(R.string.passcode_pattern));
        mSpinnerLayout.addItem(GESTURE_MENU_ITEM, Utils.sContext.getString(R.string.passcode_gesture));
        mSpinnerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpinnerLayout.createMenu();
            }
        });
        mSpinnerLayout.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                EnterPasscodeFragment fragment = null;
                switch ((Integer) view.getTag()) {
                    case PIN_MENU_ITEM:
                        fragment = newInstance(PIN_MENU_ITEM, false);
                        break;
                    case PASSWORD_MENU_ITEM:
                        fragment = newInstance(PASSWORD_MENU_ITEM, false);
                        break;
                    case PATTERN_MENU_ITEM:
                        fragment = newInstance(PATTERN_MENU_ITEM, false);
                        break;
                    case GESTURE_MENU_ITEM:
                        fragment = newInstance(GESTURE_MENU_ITEM, false);
                        break;
                }
                ((MainActivity) getParentActivity()).createFragment(fragment, "enter_passcode", false, true, false, false);
            }
        });

        mSpinnerTextView = (TextView)mSpinnerLayout.findViewById(R.id.spinnerTextView);
        mSpinnerTextView.setTypeface(Utils.getTypeface("medium"));
        if (sType == PIN_MENU_ITEM) {
            mSpinnerTextView.setText(Utils.sContext.getString(R.string.passcode_pin));
        } else if (sType == PASSWORD_MENU_ITEM) {
            mSpinnerTextView.setText(Utils.sContext.getString(R.string.passcode_password));
        } else if (sType == PATTERN_MENU_ITEM) {
            mSpinnerTextView.setText(Utils.sContext.getString(R.string.passcode_pattern));
        } else {
            mSpinnerTextView.setText(Utils.sContext.getString(R.string.passcode_gesture));
        }

        mDoneImageButton = (ImageButton)mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sType == PIN_MENU_ITEM || sType == PASSWORD_MENU_ITEM) {
                    if (mPasswordEditText.getImeOptions() == EditorInfo.IME_ACTION_NEXT) {
                        nextAction();
                    } else if (mPasswordEditText.getImeOptions() == EditorInfo.IME_ACTION_DONE) {
                        doneAction();
                    }
                } else if (sType == PATTERN_MENU_ITEM) {
                    if (mChosenPassword == null || mChosenPassword.length() == 0) {
                        nextAction();
                    } else {
                        doneAction();
                    }
                } else if (sType == GESTURE_MENU_ITEM) {
                    if (mGesture == null) {
                        nextAction();
                    } else {
                        doneAction();
                    }
                }
            }
        });

        mLogoImageView = (ImageView)mRootView.findViewById(R.id.logoImageView);
        if (!sUnlock) {
            mLogoImageView.setVisibility(View.GONE);
        }

        mInfoTextView = (TextView)mRootView.findViewById(R.id.infoTextView);
        if (!sUnlock) {
            if (sType == PIN_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.choose_pin_text));
            } else if (sType == PASSWORD_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.choose_password_text));
            } else if (sType == PATTERN_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.choose_pattern_text));
            } else {
                mInfoTextView.setText(Utils.sContext.getString(R.string.draw_gesture_text));
            }
        } else {
            if (sType == PIN_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.enter_pin_text));
            } else if (sType == PASSWORD_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.enter_password_text));
            } else if (sType == PATTERN_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.enter_pattern_text));
            } else {
                mInfoTextView.setText(Utils.sContext.getString(R.string.enter_gesture_text));
            }
        }

        if (sType == PIN_MENU_ITEM || sType == PASSWORD_MENU_ITEM) {
            mPasswordEditText = (EditText) mRootView.findViewById(R.id.passwordEditText);
            if (!sUnlock) {
                mPasswordEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
            } else {
                mPasswordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            }

            mPasswordEditText.setFilters(new InputFilter[0]);
            mPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            mPasswordEditText.setText("");

            mPasswordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_NEXT) {
                        nextAction();
                        return true;
                    } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                        doneAction();
                        return true;
                    }
                    return false;
                }
            });

            if (sType == PIN_MENU_ITEM) {
                mPasswordEditText.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return true;
                    }
                });
                mPasswordEditText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (mPasswordEditText.length() == 4) {
                            if (sUnlock) {
                                doneAction();
                            } else {
                                if (mPasswordEditText.getImeOptions() == EditorInfo.IME_ACTION_NEXT) {
                                    nextAction();
                                } else if (mPasswordEditText.getImeOptions() == EditorInfo.IME_ACTION_DONE) {
                                    doneAction();
                                }
                            }
                        }
                    }
                });
                mKeyboardLayout = (FrameLayout) mRootView.findViewById(R.id.keyboardLayout);
                for (int i = 0; i < 10; i++) {
                    View keyView = inflater.inflate(R.layout.key, mKeyboardLayout, false);
                    mKeyboardLayout.addView(keyView);
                    keyView.setTag(i);
                    keyView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int tag = (Integer) v.getTag();
                            switch (tag) {
                                case 0:
                                    mPasswordEditText.append("0");
                                    break;
                                case 1:
                                    mPasswordEditText.append("1");
                                    break;
                                case 2:
                                    mPasswordEditText.append("2");
                                    break;
                                case 3:
                                    mPasswordEditText.append("3");
                                    break;
                                case 4:
                                    mPasswordEditText.append("4");
                                    break;
                                case 5:
                                    mPasswordEditText.append("5");
                                    break;
                                case 6:
                                    mPasswordEditText.append("6");
                                    break;
                                case 7:
                                    mPasswordEditText.append("7");
                                    break;
                                case 8:
                                    mPasswordEditText.append("8");
                                    break;
                                case 9:
                                    mPasswordEditText.append("9");
                                    break;
                            }
                        }
                    });

                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) keyView.getLayoutParams();
                    if (i == 1 || i == 4 || i == 7) {
                        params.gravity = Gravity.LEFT;
                        params.leftMargin = Utils.convertDpToPx(48);
                        if (i == 1) {
                            params.topMargin = Utils.convertDpToPx(24);
                        } else if (i == 4) {
                            params.topMargin = Utils.convertDpToPx(101);
                        } else {
                            params.topMargin = Utils.convertDpToPx(178);
                        }
                    } else if (i == 2 || i == 5 || i == 8 || i == 0) {
                        params.gravity = Gravity.CENTER_HORIZONTAL;
                        if (i == 2) {
                            params.topMargin = Utils.convertDpToPx(24);
                        } else if (i == 5) {
                            params.topMargin = Utils.convertDpToPx(101);
                        } else if (i == 8) {
                            params.topMargin = Utils.convertDpToPx(178);
                        } else {
                            params.topMargin = Utils.convertDpToPx(255);
                        }
                    } else {
                        params.gravity = Gravity.RIGHT;
                        params.rightMargin = Utils.convertDpToPx(48);
                        if (i == 3) {
                            params.topMargin = Utils.convertDpToPx(24);
                        } else if (i == 6) {
                            params.topMargin = Utils.convertDpToPx(101);
                        } else {
                            params.topMargin = Utils.convertDpToPx(178);
                        }
                    }
                    keyView.setLayoutParams(params);

                    TextView numberTextView = (TextView) keyView.findViewById(R.id.numberTextView);
                    numberTextView.setTypeface(Utils.getTypeface("regular"));
                    numberTextView.setText(String.format("%d", i));

                    TextView lettersTextView = (TextView) keyView.findViewById(R.id.lettersTextView);
                    switch (i) {
                        case 0:
                            lettersTextView.setText("+");
                            break;
                        case 2:
                            lettersTextView.setText("ABC");
                            break;
                        case 3:
                            lettersTextView.setText("DEF");
                            break;
                        case 4:
                            lettersTextView.setText("GHI");
                            break;
                        case 5:
                            lettersTextView.setText("JKL");
                            break;
                        case 6:
                            lettersTextView.setText("MNO");
                            break;
                        case 7:
                            lettersTextView.setText("PQRS");
                            break;
                        case 8:
                            lettersTextView.setText("TUV");
                            break;
                        case 9:
                            lettersTextView.setText("WXYZ");
                            break;
                    }
                }

                ImageView eraseImageView = new ImageView(getParentActivity());
                eraseImageView.setImageResource(R.drawable.ic_passcode_delete);
                eraseImageView.setScaleType(ImageView.ScaleType.CENTER);
                mKeyboardLayout.addView(eraseImageView);
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) eraseImageView.getLayoutParams();
                params.width = Utils.convertDpToPx(50);
                params.height = Utils.convertDpToPx(50);
                params.gravity = Gravity.RIGHT;
                params.rightMargin = Utils.convertDpToPx(48);
                params.topMargin = Utils.convertDpToPx(255);
                eraseImageView.setLayoutParams(params);
                eraseImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int length = mPasswordEditText.getText().length();
                        if (length > 0) {
                            mPasswordEditText.getText().delete(length - 1, length);
                        }
                    }
                });
                eraseImageView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mPasswordEditText.setText("");
                        return true;
                    }
                });
            }
        } else if (sType == PATTERN_MENU_ITEM) {
            mLockPatternView = (LockPatternView)mRootView.findViewById(R.id.lockPatternView);
            mLockPatternView.setOnPatternListener(new LockPatternView.OnPatternListener() {
                @Override
                public void onPatternStart() {

                }

                @Override
                public void onPatternCleared() {

                }

                @Override
                public void onPatternCellAdded(List<LockPatternView.Cell> pattern) {

                }

                @Override
                public void onPatternDetected(List<LockPatternView.Cell> pattern) {
                    mPattern = pattern;
                    if (sUnlock || mChosenPassword != null) {
                        doneAction();
                    }
                }
            });
        } else {
            if (sStore == null) {
                sStore = GestureLibraries.fromFile(mStoreFile);
            }
            sStore.load();
            mLockGestureView = (GestureOverlayView)mRootView.findViewById(R.id.lockGestureView);
            mLockGestureView.addOnGestureListener(new GestureOverlayView.OnGestureListener() {
                @Override
                public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {

                }

                @Override
                public void onGesture(GestureOverlayView overlay, MotionEvent event) {

                }

                @Override
                public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
                    if (sUnlock || mGesture != null) {
                        doneAction();
                    }
                }

                @Override
                public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {

                }
            });
        }
    }

    public void nextAction() {
        mSpinnerLayout.setClickable(false);
        mSpinnerTextView.setCompoundDrawables(null, null, null, null);
        if (sType == PIN_MENU_ITEM || sType == PASSWORD_MENU_ITEM) {
            if (mPasswordEditText.getText().length() == 0) {
                return;
            }
            if (sType == PIN_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.reenter_pin_text));
            } else if (sType == PASSWORD_MENU_ITEM) {
                mInfoTextView.setText(Utils.sContext.getString(R.string.reenter_password_text));
            }
            mChosenPassword = mPasswordEditText.getText().toString();
            mPasswordEditText.setText("");
            mPasswordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
            mPasswordEditText.setSingleLine();
            if (sType == PIN_MENU_ITEM) {
                mPasswordEditText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
            } else {
                mPasswordEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
            }
            mPasswordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        } else if (sType == PATTERN_MENU_ITEM) {
            mInfoTextView.setText(Utils.sContext.getString(R.string.reenter_pattern_text));
            if (mPattern != null) {
                mChosenPassword = Utils.patternToString(mPattern);
                mLockPatternView.clearPattern();
            }
        } else if (sType == GESTURE_MENU_ITEM) {
            mInfoTextView.setText(Utils.sContext.getString(R.string.redraw_gesture_text));
            mGesture = mLockGestureView.getGesture();
            sStore.addGesture("lock_gesture", mGesture);
            mLockGestureView.clear(false);
        }
    }

    public void doneAction() {
        if (sType == PASSWORD_MENU_ITEM || sType == PIN_MENU_ITEM) {
            if (!sUnlock) {
                if (mPasswordEditText.getText().toString().equals(mChosenPassword)) {
                    Utils.sPasscodeHash = Utils.MD5(mChosenPassword);
                    Utils.sLocked = false;
                    Utils.sPasscodeType = sType;
                    Utils.savePasscode(getParentActivity());
                    mPasswordEditText.clearFocus();
                    onBackPressed();
                    ((MainActivity) getParentActivity()).destroyFragment(this);
                } else {
                    Toast.makeText(getParentActivity(), R.string.passcodes_mismatch, Toast.LENGTH_SHORT).show();
                    mPasswordEditText.setText("");
                }
            } else {
                if (Utils.MD5(mPasswordEditText.getText().toString()).equals(Utils.sPasscodeHash)) {
                    mPasswordEditText.clearFocus();
                    if (mAddedToBackStack) {
                        onBackPressed();
                        Utils.savePasscode(getParentActivity());
                        ((MainActivity) getParentActivity()).destroyFragment(this);
                    } else {
                        ((MainActivity) getParentActivity()).createFragment(new PasscodeSettingsFragment(), "settings_passcode", true);
                    }
                } else {
                    Toast.makeText(getParentActivity(), R.string.incorrect_passcode, Toast.LENGTH_SHORT).show();
                    mPasswordEditText.setText("");
                }
            }
        } else if (sType == PATTERN_MENU_ITEM) {
            if (!sUnlock) {
                if (Utils.patternToString(mPattern).equals(mChosenPassword)) {
                    Utils.sPasscodeHash = Utils.MD5(mChosenPassword);
                    Utils.sLocked = false;
                    Utils.sPasscodeType = sType;
                    Utils.savePasscode(getParentActivity());
                    onBackPressed();
                    ((MainActivity) getParentActivity()).destroyFragment(this);
                } else {
                    Toast.makeText(getParentActivity(), R.string.passcodes_mismatch, Toast.LENGTH_SHORT).show();
                    mLockPatternView.clearPattern();
                }
            } else {
                if (Utils.MD5(Utils.patternToString(mPattern)).equals(Utils.sPasscodeHash)) {
                    if (mAddedToBackStack) {
                        onBackPressed();
                        Utils.savePasscode(getParentActivity());
                        ((MainActivity) getParentActivity()).destroyFragment(this);
                    } else {
                        ((MainActivity) getParentActivity()).createFragment(new PasscodeSettingsFragment(), "settings_passcode", true);
                    }
                } else {
                    Toast.makeText(getParentActivity(), R.string.incorrect_passcode, Toast.LENGTH_SHORT).show();
                    mLockPatternView.clearPattern();
                }
            }
        } else if (sType == GESTURE_MENU_ITEM) {
            mGesture = mLockGestureView.getGesture();
            ArrayList<Prediction> predictions = sStore.recognize(mGesture);
            for (Prediction prediction : predictions) {
                if (prediction.score > 10.0) {
                    if (!sUnlock) {
                        Utils.sPasscodeHash = Utils.MD5(String.valueOf(mGesture.getID()));
                        Utils.sLocked = false;
                        Utils.sPasscodeType = sType;
                        Utils.savePasscode(getParentActivity());
                        onBackPressed();
                        ((MainActivity) getParentActivity()).destroyFragment(this);
                    } else {
                        if (mAddedToBackStack) {
                            onBackPressed();
                            Utils.savePasscode(getParentActivity());
                            ((MainActivity) getParentActivity()).destroyFragment(this);
                        } else {
                            ((MainActivity) getParentActivity()).createFragment(new PasscodeSettingsFragment(), "settings_passcode", true);
                        }
                    }
                    mLockGestureView.clear(false);
                } else {
                    if (!sUnlock) {
                        Toast.makeText(getParentActivity(), R.string.passcodes_mismatch, Toast.LENGTH_SHORT).show();
                        mLockGestureView.clear(false);
                    } else {
                        Toast.makeText(getParentActivity(), R.string.incorrect_passcode, Toast.LENGTH_SHORT).show();
                        mLockGestureView.clear(false);
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (sType == PASSWORD_MENU_ITEM && mPasswordEditText != null) {
            mPasswordEditText.requestFocus();
            Utils.showKeyboard(mPasswordEditText);
        }
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }
}
