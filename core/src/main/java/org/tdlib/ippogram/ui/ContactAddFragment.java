package org.tdlib.ippogram.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;

public class ContactAddFragment extends MainFragment implements UpdatesHandler.UpdatesDelegate {

    private ImageButton mDoneImageButton;
    private BlockingImageView mAvatarImageView;
    private TextView mPrimaryTextView;
    private TextView mSecondaryTextView;
    private EditText mFirstNameField;
    private EditText mLastNameField;

    private int mUserId;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            updateStatus();
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        mUserId = getArguments().getInt("user_id");
        ChatsCache.addDelegate(this);

        Utils.runOnUIThread(runnable, 1000L);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_contact_add, container, false);

        mToolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(ContactAddFragment.this);
            }
        });

        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(Utils.sContext.getString(R.string.action_edit_name));

        mDoneImageButton = (ImageButton)mRootView.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFirstNameField.length() != 0) {
                    TdApi.User user = ChatsCache.getUser(mUserId);
                    if (user == null) {
                        return;
                    }
                    user.firstName = mFirstNameField.getText().toString();
                    user.lastName = mLastNameField.getText().toString();
                    ContactsCache.getInstance().addContact(user);
                    ((MainActivity) getParentActivity()).destroyFragment(ContactAddFragment.this);
                    ChatsCache.updateInterface();
                }
            }
        });

        mAvatarImageView = (BlockingImageView)mRootView.findViewById(R.id.avatarImageView);
        
        mPrimaryTextView = (TextView)mRootView.findViewById(R.id.primaryTextView);
        mPrimaryTextView.setTypeface(Utils.getTypeface("medium"));

        mSecondaryTextView = (TextView)mRootView.findViewById(R.id.secondaryTextView);
        mSecondaryTextView.setTypeface(Utils.getTypeface("regular"));

        mFirstNameField = (EditText)mRootView.findViewById(R.id.firstNameField);
        mFirstNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mLastNameField.requestFocus();
                    mLastNameField.setSelection(mLastNameField.length());
                    return true;
                }
                return false;
            }
        });

        mLastNameField = (EditText)mRootView.findViewById(R.id.lastNameField);
        mLastNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });

        TdApi.User user = ChatsCache.getUser(mUserId);
        if (user != null) {
            mFirstNameField.setText(user.firstName);
            mFirstNameField.setSelection(mFirstNameField.length());
            mLastNameField.setText(user.lastName);
        }
        return mRootView;
    }

    private void updateUserData() {
        if (mLastNameField == null) {
            return;
        }
        TdApi.User user = ChatsCache.getUser(mUserId);
        if (user == null) {
            return;
        }
        mAvatarImageView.setProfilePhoto(user.profilePhoto, Utils.getUserAvatar(user, 60));
        mPrimaryTextView.setText(Utils.formatPhone("+" + user.phoneNumber));
        updateStatus();
    }

    private void updateStatus() {
        if (mSecondaryTextView == null) {
            return;
        }
        TdApi.User user = ChatsCache.getUser(mUserId);
        String status = Utils.formatUserStatus(user);
        if (status.equals("online")) {
            mSecondaryTextView.setTextColor(0xff6b9cc2);
        } else {
            mSecondaryTextView.setTextColor(0xff8a8a8a);
        }
        mSecondaryTextView.setText(status);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUserData();
        mFirstNameField.requestFocus();
        Utils.showKeyboard(mFirstNameField);
    }

    @Override
    public void updateInterface() {
        updateUserData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(runnable);
    }
}
