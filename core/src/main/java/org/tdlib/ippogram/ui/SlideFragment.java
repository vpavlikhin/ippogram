package org.tdlib.ippogram.ui;

import android.content.res.Configuration;
import android.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.Utils;

import java.util.HashMap;

public class SlideFragment extends Fragment {

    private LoginActivity mParentActivity;

    protected View mRootView;
    protected Toolbar mToolbar;
    protected TextView mHeaderTextView;
    protected ImageButton mDoneImageButton;

    public LoginActivity getParentActivity() {
        return mParentActivity;
    }

    public void setParentActivity(LoginActivity parentActivity) {
        this.mParentActivity = parentActivity;
    }

    @Deprecated
    public String getTitle() {
        return "";
    }

    public void setParams(HashMap<String, Object> params) {

    }

    public void onBackPressed() {

    }

    public void onResult(TdApi.TLObject object) {

    }

    public void onDonePressed() {

    }

    public void onSmsReceived(String code) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mParentActivity == null || mToolbar == null) {
            return;
        }
        int actionBarSize = Utils.calculateActionBarSize(mParentActivity);
        mToolbar.setMinimumHeight(actionBarSize);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mToolbar.getLayoutParams();
        params.height = actionBarSize;
        mToolbar.setLayoutParams(params);
    }
}
