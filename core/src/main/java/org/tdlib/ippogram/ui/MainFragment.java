package org.tdlib.ippogram.ui;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.tdlib.ippogram.Utils;

public class MainFragment extends Fragment {

    private AppCompatActivity mParentActivity;
    private AlertDialog mAlertDialog;

    protected View mRootView;
    protected Toolbar mToolbar;
    protected TextView mHeaderTextView;
    protected boolean mAddedToBackStack;

    protected boolean mEnterAnimated = false;
    protected boolean mExitAnimated = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mParentActivity = (AppCompatActivity)getActivity();
    }

    public boolean isAddedToBackStack() {
        return mAddedToBackStack;
    }

    public void setAddedToBackStack(boolean addedToBackStack) {
        mAddedToBackStack = addedToBackStack;
    }

    public AppCompatActivity getParentActivity() {
        return mParentActivity;
    }

    public void onPreCreate() {

    }

    public void withAnimation(boolean enterAnimated, boolean exitAnimated) {
        mEnterAnimated = enterAnimated;
        mExitAnimated = exitAnimated;
    }

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        AnimatorSet slideWithAlpha = new AnimatorSet();
        ObjectAnimator alpha;
        ObjectAnimator slide;
        if (enter) {
            if (mEnterAnimated) {
                alpha = ObjectAnimator.ofFloat(mRootView, View.ALPHA, 0f, 0f, 0.2f, 1f);
                alpha.setDuration(350L);
                slide = ObjectAnimator.ofFloat(mRootView, View.X, Utils.sDisplaySize.x, 0f);
                slide.setDuration(350L);
            } else {
                return null;
            }
        } else {
            if (mExitAnimated) {
                alpha = ObjectAnimator.ofFloat(mRootView, View.ALPHA, 1f, 0.3f, 0f);
                alpha.setDuration(100L);
                slide = ObjectAnimator.ofFloat(mRootView, View.X, 0f, Utils.sDisplaySize.x);
                slide.setDuration(350L);
            } else {
                return null;
            }
        }
        alpha.setInterpolator(new AccelerateInterpolator());
        slide.setInterpolator(new AccelerateDecelerateInterpolator());
        slideWithAlpha.playTogether(alpha, slide);
        return slideWithAlpha;
    }

    @Nullable
    public AlertDialog showAlertDialog(AlertDialog.Builder builder) {
        try {
            if (mAlertDialog != null) {
                mAlertDialog.dismiss();
                mAlertDialog = null;
            }
        } catch (Exception e) {
            Log.e(Utils.TAG, "" + e);
        }
        try {
            mAlertDialog = builder.show();
            mAlertDialog.setCanceledOnTouchOutside(true);
            mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mAlertDialog = null;
                }
            });

            Typeface typeface = Utils.getTypeface("medium");

            Button positiveButton = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
            if (positiveButton != null) {
                positiveButton.setTypeface(typeface);
                positiveButton.setTextColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
            }

            Button negativeButton = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
            if (negativeButton != null) {
                negativeButton.setTypeface(typeface);
                negativeButton.setTextColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
            }

            return mAlertDialog;
        } catch (Exception e) {
            Log.e(Utils.TAG, "" + e);
        }
        return null;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mParentActivity == null || mToolbar == null) {
            return;
        }
        int actionBarSize = Utils.calculateActionBarSize(mParentActivity);
        mToolbar.setMinimumHeight(actionBarSize);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mToolbar.getLayoutParams();
        params.height = actionBarSize;
        mToolbar.setLayoutParams(params);
    }

    public boolean onBackPressed() {
        return true;
    }
}
