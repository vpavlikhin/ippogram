package org.tdlib.ippogram.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.Utils;

import java.util.HashMap;

public class SmsFragment extends SlideFragment {

    private TextView mCodeTextView;
    private EditText mCodeField;
    private TextView mErrorTextView;

    private boolean mWaitingSms = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_sms, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onBackPressed();
            }
        });
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(R.string.sms_title);

        mDoneImageButton = (ImageButton)mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onNextAction();
            }
        });

        Typeface typeface = Utils.getTypeface("regular");

        mCodeTextView = (TextView)mRootView.findViewById(R.id.codeTextView);
        mCodeTextView.setTypeface(typeface);

        mCodeField = (EditText) mRootView.findViewById(R.id.codeField);
        mCodeField.setTypeface(typeface);
        mCodeField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (getParentActivity() != null) {
                        mDoneImageButton.performClick();
                    }
                    return true;
                }
                return false;
            }
        });

        mCodeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mErrorTextView.getVisibility() == View.VISIBLE) {
                    setBackgroundAndKeepPadding(mCodeField, R.drawable.edit_text);
                    mErrorTextView.setVisibility(View.GONE);
                }
                mCodeField.setSelection(mCodeField.getText().length());
            }
        });

        mErrorTextView = (TextView) mRootView.findViewById(R.id.errorTextView);
        mErrorTextView.setTypeface(typeface);

        return mRootView;
    }

    private void setBackgroundAndKeepPadding(EditText editText, @DrawableRes int resId) {
        int top = editText.getPaddingTop();
        int left = editText.getPaddingLeft();
        int right = editText.getPaddingRight();
        int bottom = editText.getPaddingBottom();

        editText.setBackgroundResource(resId);
        editText.setPadding(left, top, right, bottom);
    }

    @Override
    public void setParams(HashMap<String, Object> params) {
        String smsCode = (String) params.get("smsCode");
        if (smsCode == null || smsCode.length() == 0) {
            mCodeField.setText("");
            mWaitingSms = true;
        } else {
            mCodeField.setText("" + smsCode);
            mWaitingSms = false;
        }
        String phone = (String) params.get("phone");

        String number = Utils.formatPhone(phone);
        mCodeTextView.setText(Html.fromHtml(String.format(Utils.sContext.getString(R.string.sms_text), number)));

        Utils.showKeyboard(mCodeField);
        mCodeField.requestFocus();
    }

    @Override
    public void onDonePressed() {
        mWaitingSms = false;
        getParentActivity().showProgress();
        TdApi.CheckAuthCode authSetCode = new TdApi.CheckAuthCode(mCodeField.getText().toString());
        getParentActivity().send(authSetCode);
    }

    @Override
    public void onBackPressed() {
        mWaitingSms = false;
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        Log.e(Utils.TAG, "" + object);
        getParentActivity().hideProgress();
        if (object.getConstructor() == TdApi.AuthStateOk.CONSTRUCTOR) {
            ChatsCache.reset();
            ContactsCache.getInstance().reset();
            getParentActivity().finishActivity();
        } else if (object.getConstructor() == TdApi.AuthStateWaitPassword.CONSTRUCTOR) {
            TdApi.AuthStateWaitPassword state = (TdApi.AuthStateWaitPassword)object;
            final HashMap<String, Object> params = new HashMap<>();
            params.put("password_hint", state.hint.length() != 0 ? state.hint : Utils.sContext.getString(R.string.password_title));
            params.put("has_email", state.hasRecovery);
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    getParentActivity().sendParams(params);
                    getParentActivity().slidePager(3, true);
                }
            });
        } else if (object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
            mWaitingSms = true;
            if (((TdApi.Error)object).text != null) {
                if (((TdApi.Error)object).text.contains("PHONE_CODE_EMPTY") || ((TdApi.Error)object).text.contains("PHONE_CODE_INVALID")) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            setBackgroundAndKeepPadding(mCodeField, R.drawable.edittext_error);
                            mErrorTextView.setVisibility(View.VISIBLE);
                        }
                    });
                } else if (((TdApi.Error)object).text.contains("PHONE_CODE_EXPIRED")) {
                    getParentActivity().showAlert(Utils.sContext.getString(R.string.code_expired_text));
                } else {
                    getParentActivity().showAlert(((TdApi.Error) object).text);
                }
            }
        }
    }

    @Override
    public void onSmsReceived(final String code) {
        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                if (mWaitingSms) {
                    if (mCodeField != null) {
                        mCodeField.setText("" + code);
                        onDonePressed();
                    }
                }
            }
        });
    }

    @Override
    public String getTitle() {
        return getResources().getString(R.string.sms_title);
    }
}
