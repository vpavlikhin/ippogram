package org.tdlib.ippogram.ui.adapters;

import android.widget.BaseAdapter;
import android.widget.SectionIndexer;

import org.tdlib.ippogram.ui.components.SectionedSectionIndexer;

/**
 * A base adapter that allows having multiple sections. Each section has its own items. Items don't have to be of the
 * same type
 */
public abstract class BaseSectionedAdapter extends BaseAdapter implements SectionIndexer {

    private SectionedSectionIndexer mSectionIndexer;

    public void setSectionIndexer(SectionedSectionIndexer sectionIndexer) {
        mSectionIndexer = sectionIndexer;
    }

    public SectionIndexer getSectionIndexer() {
        return mSectionIndexer;
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (mSectionIndexer == null) {
            return -1;
        }
        return mSectionIndexer.getPositionForSection(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        if (mSectionIndexer == null) {
            return -1;
        }
        return mSectionIndexer.getSectionForPosition(position);
    }

    @Override
    public Object[] getSections() {
        if (mSectionIndexer == null) {
            return new String[]{" "};
        }
        return mSectionIndexer.getSections();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return mSectionIndexer.getItemsCount();
    }

    @Override
    public Object getItem(int position) {
        return mSectionIndexer.getItem(position);
    }

    @Override
    public int getViewTypeCount() {
        return mSectionIndexer.getSections().length;
    }

    @Override
    public int getItemViewType(int position) {
        return mSectionIndexer.getSectionForPosition(position);
    }
}
