package org.tdlib.ippogram.ui.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import org.tdlib.ippogram.ui.components.PinnedHeaderListView;
import org.tdlib.ippogram.ui.components.SectionedSectionIndexer;

public abstract class BasePinnedHeaderListViewAdapter extends BaseAdapter implements SectionIndexer,
        AbsListView.OnScrollListener, PinnedHeaderListView.PinnedHeaderAdapter {

    private SectionedSectionIndexer mSectionIndexer;
    private boolean mHeaderViewVisible = true;

    public void setSectionIndexer(SectionedSectionIndexer sectionIndexer) {
        mSectionIndexer = sectionIndexer;
    }

    /** remember to call bindSectionHeader(v,position); before calling return */
    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

    public abstract CharSequence getSectionTitle(int sectionIndex);

    protected void bindSectionHeader(TextView headerView, int position) {
        headerView.setVisibility(View.GONE);
        int sectionIndex = getSectionForPosition(position);
        if (getPositionForSection(sectionIndex) == position) {
            CharSequence title = getSectionTitle(sectionIndex);
            headerView.setText(title);
            headerView.setVisibility(View.VISIBLE);
        }

        if (!mHeaderViewVisible) {
            headerView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getPinnedHeaderState(int position) {
        if (mSectionIndexer == null || getCount() == 0 || !mHeaderViewVisible) {
            return PINNED_HEADER_GONE;
        }
        if (position < 0) {
            return PINNED_HEADER_GONE;
        }
        // The header should get pushed up if the top item shown
        // is the last item in a section for a particular letter.
        int section = getSectionForPosition(position);
        int nextSectionPosition = getPositionForSection(section + 1);
        if (nextSectionPosition != -1 ) {
            if (position == nextSectionPosition - 1) {
                return PINNED_HEADER_GONE;
            }
            if (position == nextSectionPosition - 2) {
                return PINNED_HEADER_PUSHED_UP;
            }
        }
        return PINNED_HEADER_VISIBLE;
    }

    public void setHeaderViewVisible(boolean isHeaderViewVisible) {
        mHeaderViewVisible = isHeaderViewVisible;
    }

    public boolean isHeaderViewVisible() {
        return mHeaderViewVisible;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        ((PinnedHeaderListView) view).configureHeaderView(firstVisibleItem);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        if (mSectionIndexer == null) {
            return -1;
        }
        return mSectionIndexer.getPositionForSection(sectionIndex);
    }

    @Override
    public int getSectionForPosition(int position) {
        if (mSectionIndexer == null) {
            return -1;
        }
        return mSectionIndexer.getSectionForPosition(position);
    }

    public int getPositionInSection(int position) {
        if (mSectionIndexer == null) {
            return -1;
        }
        return mSectionIndexer.getPositionInSection(position);
    }

    @Override
    public Object[] getSections() {
        if (mSectionIndexer == null) {
            return new String[]{" "};
        }
        return mSectionIndexer.getSections();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
