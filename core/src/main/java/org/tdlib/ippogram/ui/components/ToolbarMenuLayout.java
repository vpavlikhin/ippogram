package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;

public class ToolbarMenuLayout extends FrameLayout {

    public interface OnItemClickListener {
        void onItemClick(View view);
    }

    private Toolbar mParentToolbar;
    //private RelativeLayout mParentContainer;
    private Context mContext;
    private LinearLayout mPopupLayout;
    private PopupWindow mPopupWindow;
    private Rect mRect;
    private ImageView mIconImageView;

    private boolean mDropDown = false;

    private OnItemClickListener mListener;

    //private boolean mOffset = Build.VERSION.SDK_INT >= 21;
    private boolean mOffset = false;

    public ToolbarMenuLayout(Context context) {
        super(context);
        mContext = context;
    }

    public ToolbarMenuLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public ToolbarMenuLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public void setParentToolbar(Toolbar toolbar) {
        mParentToolbar = toolbar;
    }

    public void setDropDown(boolean dropDown) {
        mDropDown = dropDown;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    private int getParentLeft() {
        if (getParent() instanceof LinearLayout) {
            return ((LinearLayout) getParent()).getLeft();
        }
        return 0;
    }

    public void createMenu() {
        if (mPopupLayout == null || mParentToolbar == null) {
            return;
        }
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
        if (mPopupWindow == null) {
            mPopupWindow= new PopupWindow(mPopupLayout, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setClippingEnabled(true);
            mPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
            mPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED);
            mPopupLayout.measure(FrameLayout.MeasureSpec.makeMeasureSpec(Utils.convertDpToPx(1000), FrameLayout.MeasureSpec.AT_MOST),
                    FrameLayout.MeasureSpec.makeMeasureSpec(Utils.convertDpToPx(1000), FrameLayout.MeasureSpec.AT_MOST));
            mPopupWindow.getContentView().setFocusableInTouchMode(true);
            mPopupWindow.getContentView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if ((keyCode == KeyEvent.KEYCODE_MENU || keyCode == KeyEvent.KEYCODE_BACK) && event.getRepeatCount() == 0 && event.getAction() == KeyEvent.ACTION_UP
                            && mPopupWindow != null && mPopupWindow.isShowing()) {
                        mPopupWindow.dismiss();
                        return true;
                    }
                    return false;
                }
            });
        }
        mPopupWindow.setFocusable(true);
        /*Log.d(Utils.TAG, "mParentToolbar.getMeasuredWidth(): " + Utils.convertPxToDp(mParentToolbar.getMeasuredWidth()));
        Log.d(Utils.TAG, "mPopupLayout.getMeasuredWidth(): " + Utils.convertPxToDp(mPopupLayout.getMeasuredWidth()));
        Log.d(Utils.TAG, "getLeft(): " + Utils.convertPxToDp(getLeft()));
        Log.d(Utils.TAG, "getParentContainer().getLeft(): " + Utils.convertPxToDp(getParentLeft()));
        Log.d(Utils.TAG, "-getMeasuredHeight(): " + Utils.convertPxToDp(-getMeasuredHeight()));*/
        if (mPopupLayout.getMeasuredWidth() == 0) {
            if (!mDropDown) {
            /*mPopupWindow.showAsDropDown(this, mParentToolbar.getMeasuredWidth() - mPopupLayout.getMeasuredWidth() -
                    getLeft() - getParentLeft() - getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));
            mPopupWindow.update(this, mParentToolbar.getMeasuredWidth() - mPopupLayout.getMeasuredWidth() -
                    getLeft() - getParentLeft() - getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);*/
                mPopupWindow.showAsDropDown(this, -mPopupLayout.getMeasuredWidth() + getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));
                mPopupWindow.update(this, -mPopupLayout.getMeasuredWidth() + getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);
            } else {
                mPopupWindow.showAsDropDown(this, -Utils.convertDpToPx(8), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));
                mPopupWindow.update(this, -Utils.convertDpToPx(8), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);
            }
        } else {
            if (!mDropDown) {
                mPopupWindow.showAsDropDown(this, -mPopupLayout.getMeasuredWidth() + getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));
            /*mPopupWindow.showAsDropDown(this, mParentToolbar.getMeasuredWidth() - mPopupLayout.getMeasuredWidth() -
                    getLeft() - getParentLeft() - getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));*/
            } else {
                mPopupWindow.showAsDropDown(this, -Utils.convertDpToPx(8), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0));
            }
        }
    }

    public TextView addItem(int id, String text) {
        if (mPopupLayout == null) {
            mRect = new Rect();
            mPopupLayout = new LinearLayout(getContext());
            mPopupLayout.setOrientation(LinearLayout.VERTICAL);
            mPopupLayout.setBackgroundResource(R.drawable.popup_fixed);
            mPopupLayout.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                        if (mPopupWindow != null && mPopupWindow.isShowing()) {
                            v.getHitRect(mRect);
                            if (!mRect.contains((int)event.getX(), (int)event.getY())) {
                                mPopupWindow.dismiss();
                            }
                        }
                    }
                    return false;
                }
            });
        }
        TextView itemTextView = new TextView(getContext());
        itemTextView.setTextColor(0xff222222);
        itemTextView.setTypeface(Utils.getTypeface("regular"));
        itemTextView.setBackgroundResource(R.drawable.list_selector);
        itemTextView.setGravity(Gravity.CENTER_VERTICAL);
        int padding = Utils.convertDpToPx(16);
        itemTextView.setPadding(padding, 0, padding, 0);
        itemTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        itemTextView.setMinWidth(Utils.convertDpToPx(196));
        itemTextView.setTag(id);
        itemTextView.setText(text);
        mPopupLayout.addView(itemTextView);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)itemTextView.getLayoutParams();
        params.width = LinearLayout.LayoutParams.MATCH_PARENT;
        params.height = Utils.convertDpToPx(48);
        itemTextView.setLayoutParams(params);
        itemTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(v);
                if (mPopupWindow != null && mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                }
            }
        });
        return itemTextView;
    }

    public void clearItems() {
        if (mPopupLayout != null) {
            mPopupLayout.removeAllViews();
        }
        /*for (int i = 0; i < mPopupLayout.getChildCount(); i++) {
            removeView(getChildAt(i));
        }*/
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            if (!mDropDown) {
                mPopupWindow.update(this, -mPopupLayout.getMeasuredWidth() + getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);
            /*mPopupWindow.update(this, mParentToolbar.getMeasuredWidth() - mPopupLayout.getMeasuredWidth() -
                    getLeft() - getParentLeft() - getMeasuredWidth(), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);*/
            } else {
                mPopupWindow.update(this, -Utils.convertDpToPx(8), -getMeasuredHeight() - (mOffset ? Utils.sStatusBarHeight : 0), -1, -1);
            }
        }
    }

    public void setImageResource(@DrawableRes int resId) {
        if (mIconImageView == null) {
            mIconImageView = new ImageView(mContext);
            mIconImageView.setScaleType(ImageView.ScaleType.CENTER);

            addView(mIconImageView);
            LayoutParams params = (LayoutParams) mIconImageView.getLayoutParams();
            params.width = LayoutParams.MATCH_PARENT;
            params.height = LayoutParams.MATCH_PARENT;
            mIconImageView.setLayoutParams(params);
        }
        mIconImageView.setImageResource(resId);
    }
}
