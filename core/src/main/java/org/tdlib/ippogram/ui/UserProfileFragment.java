package org.tdlib.ippogram.ui;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.TextDrawable;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.util.ArrayList;

@Deprecated
public class UserProfileFragment extends ProfileFragment implements UpdatesHandler.UserUpdatesDelegate,
        UpdatesHandler.UpdatesForContactsDelegate {

    private static final int MUTE_FOR_HOUR_MENU_ITEM = 0;
    private static final int MUTE_FOR_HOURS_MENU_ITEM = 1;
    private static final int MUTE_FOR_DAYS_MENU_ITEM = 2;
    private static final int MUTE_DISABLE_MENU_ITEM = 3;

    private static final int ADD_MENU_ITEM = 4;
    private static final int ADD_TO_GROUP_MENU_ITEM = 5;
    private static final int SHARE_MENU_ITEM = 6;
    private static final int BLOCK_MENU_ITEM = 7;
    private static final int EDIT_MENU_ITEM = 8;
    private static final int DELETE_MENU_ITEM = 9;

    private ListView mUserProfileListView;
    private UserProfileAdapter mUserProfileAdapter;

    private View mEmptyHeaderView;
    private LinearLayout mEmptyHeader;
    private LinearLayout mProfileHeaderLayout;
    private RelativeLayout mProfileHeaderContainer;
    private LinearLayout mInfoLayout;

    private ToolbarMenuLayout mMoreMenuButton;
    private ToolbarMenuLayout mNotifyMenuButton;

    private TextView mBlockItemTextView;

    private int mUserId;
    private long mChatId;
    private int mGroupId;
    //private boolean mBlocked = false;
    //private TdApi.BotInfo mBotInfo;
    private TdApi.UserFull mUserFull = null;

    private TextView mSubHeaderTextView;
    private BlockingImageView mAvatarImageView;
    private ImageButton mFloatingActionButton;

    private int mProfileHeaderHeight;
    private int mProfileMinHeaderTranslation;
    private int mProfileAvatarTranslation;
    private int mProfileInfoLayoutTranslation;
    private Configuration mConfig;

    private boolean mHideFab = true;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            updateUser();
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        mChatId = getArguments().getLong("chat_id", 0);
        mUserId = getArguments().getInt("user_id", 0);
        mGroupId = getArguments().getInt("group_chat_id", 0);
        //mBlocked = getArguments().getBoolean("blocked", false);
        mConfig = Utils.sContext.getResources().getConfiguration();
        ChatsCache.addDelegate(this);
        if (!ChatsCache.sUsersFull.containsKey(mUserId)) {
            ChatsCache.loadUserFull(mUserId);
        } else {
            mUserFull = ChatsCache.sUsersFull.get(mUserId);
        }
        Utils.runOnUIThread(runnable, 1000L);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_user_profile, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(UserProfileFragment.this);
            }
        });

        mNotifyMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.notifyMenuButton);
        mNotifyMenuButton.setParentToolbar(mToolbar);
        mNotifyMenuButton.setVisibility(View.VISIBLE);
        mNotifyMenuButton.addItem(MUTE_FOR_HOUR_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hour));
        mNotifyMenuButton.addItem(MUTE_FOR_HOURS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hours));
        mNotifyMenuButton.addItem(MUTE_FOR_DAYS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_days));
        mNotifyMenuButton.addItem(MUTE_DISABLE_MENU_ITEM, Utils.sContext.getString(R.string.notifications_disable));
        mNotifyMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean muted = ChatsCache.isChatMuted(mChatId);
                if (muted) {
                    TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                    if (chat != null) {
                        chat.notificationSettings.muteFor = 0;
                    }
                    Utils.cancelRunOnUIThread(ChatsCache.sChatsMuteRunnable.get(mChatId));
                    ChatsCache.sChatsMuteRunnable.remove(mChatId);
                    ChatsCache.updateNotifySettingsForChat(mChatId);
                    mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
                } else {
                    mNotifyMenuButton.createMenu();
                }
            }
        });
        mNotifyMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                int untilTime = 0;
                switch ((Integer) view.getTag()) {
                    case MUTE_FOR_HOUR_MENU_ITEM:
                        untilTime = 60 * 60;
                        break;
                    case MUTE_FOR_HOURS_MENU_ITEM:
                        untilTime = 60 * 60 * 8;
                        break;
                    case MUTE_FOR_DAYS_MENU_ITEM:
                        untilTime = 60 * 60 * 48;
                        break;
                    case MUTE_DISABLE_MENU_ITEM:
                        untilTime = Integer.MAX_VALUE;
                        break;
                }
                final TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                if (chat != null) {
                    chat.notificationSettings.muteFor = untilTime;
                }
                ChatsCache.sChatsMuteRunnable.put(mChatId, new Runnable() {
                    @Override
                    public void run() {
                        TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                        if (chat != null) {
                            chat.notificationSettings.muteFor = 0;
                        }
                        ChatsCache.sChatsMuteRunnable.remove(mChatId);
                        ChatsCache.updateNotifySettings();
                    }
                });
                ChatsCache.updateNotifySettingsForChat(mChatId);
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
            }
        });

        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more);
        createToolbarMenu();
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });
        mMoreMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                switch ((Integer) view.getTag()) {
                    case ADD_MENU_ITEM:
                        ContactAddFragment contactAddFragment = new ContactAddFragment();
                        Bundle addArgs = new Bundle();
                        addArgs.putInt("user_id", mUserId);
                        addArgs.putBoolean("add_contact", true);
                        contactAddFragment.setArguments(addArgs);
                        ((MainActivity)getParentActivity()).createFragment(contactAddFragment, "add_contact", true);
                        break;
                    case ADD_TO_GROUP_MENU_ITEM:
                        break;
                    case SHARE_MENU_ITEM:
                        ChatsFragment chatsFragment = new ChatsFragment();
                        Bundle args = new Bundle();
                        args.putInt("selected_user_id", mUserId);
                        args.putLong("chat_id", mChatId);
                        args.putInt("group_chat_id", mGroupId);
                        args.putBoolean("select_chat", true);
                        chatsFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(chatsFragment, "select_chat", true, false, true, false);
                        break;
                    case BLOCK_MENU_ITEM:
                        if (getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder blockBuilder = new AlertDialog.Builder(getParentActivity());
                        blockBuilder.setMessage((mUserFull != null && mUserFull.isBlocked) ? R.string.unblock_message : R.string.block_message);
                        blockBuilder.setPositiveButton((mUserFull != null && mUserFull.isBlocked) ? R.string.action_unblock : R.string.action_block, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (mUserFull != null && mUserFull.isBlocked) {
                                    ChatsCache.unblockUser(mUserId);
                                } else {
                                    ChatsCache.blockUser(mUserId);
                                }
                            }
                        });
                        blockBuilder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(blockBuilder);
                        break;
                    case EDIT_MENU_ITEM:
                        ContactAddFragment contactEditFragment = new ContactAddFragment();
                        Bundle editArgs = new Bundle();
                        editArgs.putInt("user_id", mUserId);
                        editArgs.putBoolean("add_contact", false);
                        contactEditFragment.setArguments(editArgs);
                        ((MainActivity)getParentActivity()).createFragment(contactEditFragment, "edit_contact", true);
                        break;
                    case DELETE_MENU_ITEM:
                        final TdApi.User user = ChatsCache.getUser(mUserId);
                        if (user == null || getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(getParentActivity());
                        deleteBuilder.setMessage(R.string.delete_contact_message);
                        deleteBuilder.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ArrayList<TdApi.User> deleteUsers = new ArrayList<>();
                                deleteUsers.add(user);
                                ContactsCache.getInstance().deleteContacts(deleteUsers);
                            }
                        });
                        deleteBuilder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(deleteBuilder);
                        break;
                }
            }
        });

        mUserProfileListView = (ListView)mRootView.findViewById(R.id.profileListView);
        mUserProfileListView.setVerticalScrollBarEnabled(false);

        mProfileHeaderLayout = (LinearLayout) mRootView.findViewById(R.id.profileHeaderLayout);
        mProfileHeaderContainer = (RelativeLayout)mProfileHeaderLayout.findViewById(R.id.profileHeaderContainer);
        mInfoLayout = (LinearLayout)mProfileHeaderLayout.findViewById(R.id.infoLayout);
        mAvatarImageView = (BlockingImageView)mProfileHeaderLayout.findViewById(R.id.avatarImageView);
        mHeaderTextView = (TextView)mProfileHeaderLayout.findViewById(R.id.headerTextView);
        mSubHeaderTextView = (TextView)mProfileHeaderLayout.findViewById(R.id.subHeaderTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }
        if (mSubHeaderTextView != null) {
            mSubHeaderTextView.setTypeface(Utils.getTypeface("regular"));
        }

        mFloatingActionButton = (ImageButton)mProfileHeaderLayout.findViewById(R.id.floatingActionButton);
        mFloatingActionButton.setImageResource(R.drawable.ic_message);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment("chat_" + mChatId);
                ((MainActivity) getParentActivity()).destroyFragment("group_profile_" + getArguments().getInt("group_chat_id", 0));

                if (mGroupId != 0) {
                    Utils.showProgressDialog(getParentActivity(), Utils.sContext.getString(R.string.loading));
                    Utils.sClient.send(new TdApi.CreatePrivateChat(mUserId), new Client.ResultHandler() {
                        @Override
                        public void onResult(final TdApi.TLObject object) {
                            if (object.getConstructor() == TdApi.Chat.CONSTRUCTOR) {
                                final TdApi.Chat chat = (TdApi.Chat) object;
                                if (!ChatsCache.sChats.containsKey(chat.id)) {
                                    ChatsCache.sChats.put(chat.id, chat);
                                }
                                Utils.runOnUIThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ChatFragment chatFragment = new ChatFragment();
                                        Bundle args = new Bundle();
                                        args.putLong("chat_id", chat.id);
                                        args.putInt("user_id", mUserId);
                                        chatFragment.setArguments(args);
                                        ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + chat.id, true, false, true, false);
                                        Utils.hideProgressDialog(getParentActivity());
                                    }
                                });
                            }
                        }
                    });
                } else {
                    ChatFragment chatFragment = new ChatFragment();
                    Bundle args = new Bundle();
                    args.putLong("chat_id", mChatId);
                    args.putInt("user_id", mUserId);
                    chatFragment.setArguments(args);
                    ((MainActivity) getParentActivity()).createFragment(chatFragment, "chat_" + mChatId, true, false, true, false);
                }
            }
        });

        mEmptyHeaderView = inflater.inflate(R.layout.empty_header, mUserProfileListView, false);
        mUserProfileListView.addHeaderView(mEmptyHeaderView, null, false);
        mEmptyHeader = (LinearLayout)mEmptyHeaderView.findViewById(R.id.emptyHeader);

        if (mConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (mConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }

        mUserProfileListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Integer scrollY = getScrollY(view);
                mProfileHeaderLayout.setTranslationY(Math.max(-scrollY, mProfileMinHeaderTranslation));

                final float offset = 1.0f - Math.max((float) (-mProfileMinHeaderTranslation - scrollY) / -mProfileMinHeaderTranslation, 0f);

                mToolbar.setTranslationY(-mProfileMinHeaderTranslation * offset);

                AnimatorSet animatorSet;
                if (offset >= 0.7f) {
                    if (mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_down_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = false;
                    }
                } else {
                    if (!mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_up_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = true;
                    }
                }

                //LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mAvatarImageView.getLayoutParams();
                //params.width = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //params.height = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //mAvatarImageView.setLayoutParams(params););
                mAvatarImageView.setScaleX(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setScaleY(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setTranslationX(Utils.convertDpToPx(37.5f) * offset);
                mAvatarImageView.setTranslationY(mProfileAvatarTranslation * offset);

                mInfoLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mInfoLayout.getLayoutParams();
                        params.height = (int) (Utils.convertDpToPx(61.0f) - Utils.convertDpToPx(13.0f) * offset);
                        params.rightMargin = (int) (Utils.convertDpToPx(16.0f) + Utils.convertDpToPx(96.0f) * offset);
                        mInfoLayout.setLayoutParams(params);

                        params = (LinearLayout.LayoutParams) mHeaderTextView.getLayoutParams();
                        params.bottomMargin = (int) (Utils.convertDpToPx(4) - Utils.convertDpToPx(4) * offset);
                        mHeaderTextView.setLayoutParams(params);
                    }
                });

                mInfoLayout.setTranslationX(Utils.convertDpToPx(14.0f) * offset);
                mInfoLayout.setTranslationY(mProfileInfoLayoutTranslation * offset);

                mHeaderTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20.0f - 2.0f * offset);
            }
        });
        mUserProfileListView.setAdapter(mUserProfileAdapter = new UserProfileAdapter());
        return mRootView;
    }

    /*public void setBotInfo(TdApi.BotInfo botInfo) {
        mBotInfo = botInfo;
    }*/

    private int getScrollY(AbsListView view) {
        View c = view.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mProfileHeaderHeight;
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mUserProfileAdapter != null) {
            mUserProfileAdapter.notifyDataSetChanged();
        }
        updateUser();
        updateNotifySettings();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(runnable);
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }

    @Override
    public void updateInterface() {
        updateUser();
        if (mUserProfileListView != null) {
            mUserProfileListView.invalidateViews();
        }
    }

    @Override
    public void updateNotifySettings() {
        if (ChatsCache.isChatMuted(mChatId)) {
            mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
        } else {
            mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
        }
    }

    @Override
    public void updateUserInfo(int userId, boolean blocked, TdApi.BotInfo botInfo) {
        if (mUserFull == null) {
            mUserFull = ChatsCache.sUsersFull.get(userId);
            createToolbarMenu();
            if (mUserProfileListView != null) {
                mUserProfileListView.invalidateViews();
            }
            return;
        }
        if (userId == mUserId) {
            mUserFull.isBlocked = blocked;
            createToolbarMenu();
            if (botInfo != null) {
                mUserFull.botInfo = botInfo;
            }
            if (mUserProfileListView != null) {
                mUserProfileListView.invalidateViews();
            }
        }
    }

    @Override
    public void updateContactsList() {
        createToolbarMenu();
    }

    public void updateUser() {
        final TdApi.User currentUser = ChatsCache.getUser(mUserId);
        TextDrawable textDrawable;
        if (currentUser != null) {
            textDrawable = Utils.getUserAvatar(currentUser, 61);
            if (mSubHeaderTextView != null) {
                mAvatarImageView.setProfilePhoto(currentUser.profilePhoto, textDrawable);
                mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentUser.profilePhoto.id != 0) {
                            PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                            Bundle args = new Bundle();
                            TdApi.File smallPhoto = ChatsCache.getFile(currentUser.profilePhoto.small.id);
                            TdApi.File bigPhoto = ChatsCache.getFile(currentUser.profilePhoto.big.id);
                            if (bigPhoto.path.length() != 0) {
                                args.putString("path", bigPhoto.path);
                            } else {
                                args.putString("path", smallPhoto.path);
                                args.putInt("big_photo_id", bigPhoto.id);
                            }
                            photoViewFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                        }
                    }
                });
                if (currentUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                } else {
                    mHeaderTextView.setText(Utils.formatName(currentUser.firstName, currentUser.lastName));
                }
                mSubHeaderTextView.setText(Utils.formatUserStatus(currentUser));
            }
        }
    }

    private void createToolbarMenu() {
        if (mMoreMenuButton == null) {
            return;
        }

        mMoreMenuButton.clearItems();
        TdApi.User currentUser = ContactsCache.getInstance().assignUsers.get(mUserId);
        if (currentUser == null) {
            currentUser = ChatsCache.getUser(mUserId);
            if (currentUser == null) {
                return;
            }
            if (currentUser.type.getConstructor() == TdApi.UserTypeBot.CONSTRUCTOR) {
                TdApi.UserTypeBot userBot = (TdApi.UserTypeBot)currentUser.type;
                if (userBot.canJoinGroupChats) {
                    mMoreMenuButton.addItem(ADD_TO_GROUP_MENU_ITEM, Utils.sContext.getString(R.string.action_add_to_group));
                }
                mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
            } else {
                if (currentUser.phoneNumber != null && currentUser.phoneNumber.length() != 0) {
                    mMoreMenuButton.addItem(ADD_MENU_ITEM, Utils.sContext.getString(R.string.action_add));
                    mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
                }
            }
            mBlockItemTextView = mMoreMenuButton.addItem(BLOCK_MENU_ITEM, (mUserFull != null && mUserFull.isBlocked) ? Utils.sContext.getString(R.string.action_unblock) : Utils.sContext.getString(R.string.action_block));
        } else {
            mMoreMenuButton.addItem(SHARE_MENU_ITEM, Utils.sContext.getString(R.string.action_share));
            mBlockItemTextView = mMoreMenuButton.addItem(BLOCK_MENU_ITEM, (mUserFull != null && mUserFull.isBlocked) ? Utils.sContext.getString(R.string.action_unblock) : Utils.sContext.getString(R.string.action_block));
            mMoreMenuButton.addItem(EDIT_MENU_ITEM, Utils.sContext.getString(R.string.action_edit));
            mMoreMenuButton.addItem(DELETE_MENU_ITEM, Utils.sContext.getString(R.string.action_delete));
        }
    }

    private class UserProfileAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return false;
        }

        @Override
        public int getCount() {
            return 6;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.empty_list_item, parent, false);
                }
            } else if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_big_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView secondaryTextView = (TextView)convertView.findViewById(R.id.secondaryTextView);
                TdApi.User user = ChatsCache.getUser(mUserId);
                if (user != null) {
                    if (position == 3) {
                        //convertView.setVisibility(View.GONE);
                        iconImageView.setImageResource(R.drawable.ic_user);
                        if (user.username == null || user.username.length() == 0) {
                            primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                        } else {
                            primaryTextView.setText("@" + user.username);
                        }
                        secondaryTextView.setText(Utils.sContext.getString(R.string.profile_username));
                    } else if (position == 5 && mUserFull != null) {
                        if (mUserFull.botInfo.getConstructor() == TdApi.BotInfoEmpty.CONSTRUCTOR) {
                            //convertView.setVisibility(View.VISIBLE);
                            iconImageView.setImageResource(R.drawable.ic_phone);
                            if (user.phoneNumber == null || user.phoneNumber.length() == 0 || user.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                                primaryTextView.setText(Utils.sContext.getString(R.string.unknown_phone));
                            } else {
                                primaryTextView.setText(Utils.formatPhone("+" + user.phoneNumber));
                            }
                            secondaryTextView.setText(Utils.sContext.getString(R.string.profile_phone));
                        } else if (mUserFull.botInfo.getConstructor() == TdApi.BotInfoGeneral.CONSTRUCTOR) {
                            iconImageView.setImageResource(R.drawable.ic_about);
                            TdApi.BotInfoGeneral botInfoGeneral = (TdApi.BotInfoGeneral)mUserFull.botInfo;
                            if (botInfoGeneral.shareText == null || botInfoGeneral.shareText.length() == 0) {
                                primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                            } else {
                                primaryTextView.setText(botInfoGeneral.shareText);
                            }
                            secondaryTextView.setText(Utils.sContext.getString(R.string.profile_about));
                        }
                    }
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_divider, parent, false);
                }
                //convertView.setVisibility(View.GONE);
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            switch (position) {
                case 0:
                case 1:
                case 2:
                    return 0;
                case 3:
                case 5:
                    return 1;
                case 4:
                    return 2;
                default:
                    return 0;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}
