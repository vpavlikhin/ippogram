package org.tdlib.ippogram.ui;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;
import org.tdlib.ippogram.ui.components.TextDrawable;

import java.io.File;

public class SettingsFragment extends MainFragment implements UpdatesHandler.UpdatesDelegate {

    private static final int EDIT_NAME_MENU_ITEM = 0;
    private static final int LOG_OUT_MENU_ITEM = 1;

    private ListView mSettingsListView;
    private SettingsAdapter mSettingsAdapter;

    private View mEmptyHeaderView;
    private LinearLayout mEmptyHeader;
    private LinearLayout mProfileHeaderLayout;
    private RelativeLayout mProfileHeaderContainer;
    private LinearLayout mInfoLayout;
    private ToolbarMenuLayout mMoreMenuButton;

    private TextView mSubHeaderTextView;
    private BlockingImageView mAvatarImageView;
    private ImageButton mFloatingActionButton;

    private int mProfileHeaderHeight;
    private int mProfileMinHeaderTranslation;
    private int mProfileAvatarTranslation;
    private int mProfileInfoLayoutTranslation;
    private Configuration mConfig;

    private boolean mHideFab = true;
    private String mProfilePhotoPath;

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_settings, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(SettingsFragment.this);
            }
        });

        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more);

        mMoreMenuButton.addItem(EDIT_NAME_MENU_ITEM, Utils.sContext.getString(R.string.action_edit_name));
        mMoreMenuButton.addItem(LOG_OUT_MENU_ITEM, Utils.sContext.getString(R.string.action_log_out));
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });
        mMoreMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                switch ((Integer) view.getTag()) {
                    case EDIT_NAME_MENU_ITEM:
                        ChangeNameFragment changeNameFragment = new ChangeNameFragment();
                        ((MainActivity) getParentActivity()).createFragment(changeNameFragment, "change_name", true);
                        break;
                    case LOG_OUT_MENU_ITEM:
                        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                        builder.setMessage(R.string.logout_message);
                        builder.setPositiveButton(Utils.sContext.getString(R.string.logout_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Utils.sClient.send(new TdApi.ResetAuth(false), new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {

                                    }
                                });
                                ((MainActivity) getParentActivity()).logout();
                            }
                        });
                        builder.setNegativeButton(Utils.sContext.getString(R.string.cancel_button), null);
                        showAlertDialog(builder);
                        break;
                }
            }
        });

        mSettingsListView = (ListView)mRootView.findViewById(R.id.settingsListView);
        mSettingsListView.setVerticalScrollBarEnabled(false);
        mSettingsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 9) {
                    if (Utils.sPasscodeHash.length() == 0) {
                        ((MainActivity) getParentActivity()).createFragment(new PasscodeSettingsFragment(), "settings_passcode", true);
                    } else {
                        EnterPasscodeFragment fragment = EnterPasscodeFragment.newInstance(Utils.sPasscodeType, true);
                        ((MainActivity) getParentActivity()).createFragment(fragment, "unlock_passcode", true, false, true, false);
                    }
                }
            }
        });

        mProfileHeaderLayout = (LinearLayout) mRootView.findViewById(R.id.profileHeaderLayout);
        mProfileHeaderContainer = (RelativeLayout)mProfileHeaderLayout.findViewById(R.id.profileHeaderContainer);
        mInfoLayout = (LinearLayout)mProfileHeaderLayout.findViewById(R.id.infoLayout);
        mAvatarImageView = (BlockingImageView)mProfileHeaderLayout.findViewById(R.id.avatarImageView);
        mHeaderTextView = (TextView)mProfileHeaderLayout.findViewById(R.id.headerTextView);
        mSubHeaderTextView = (TextView)mProfileHeaderLayout.findViewById(R.id.subHeaderTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }
        if (mSubHeaderTextView != null) {
            mSubHeaderTextView.setTypeface(Utils.getTypeface("regular"));
        }

        mFloatingActionButton = (ImageButton)mProfileHeaderLayout.findViewById(R.id.floatingActionButton);
        mFloatingActionButton.setImageResource(R.drawable.ic_camera);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                final TdApi.User user = ChatsCache.getUser(Utils.sCurrentUserId);
                CharSequence[] items;
                if (user != null && user.profilePhoto != null && user.profilePhoto.id != 0) {
                    items = new CharSequence[]{
                            Utils.sContext.getString(R.string.take_photo),
                            Utils.sContext.getString(R.string.choose_from_gallery),
                            Utils.sContext.getString(R.string.delete_photo)
                    };
                } else {
                    items = new CharSequence[]{
                            Utils.sContext.getString(R.string.take_photo),
                            Utils.sContext.getString(R.string.choose_from_gallery)
                    };
                }
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File profilePhoto = Utils.createImageFile();
                            if (profilePhoto != null) {
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profilePhoto));
                                mProfilePhotoPath = profilePhoto.getAbsolutePath();
                            }
                            startActivityForResult(cameraIntent, 0);
                        } else if (which == 1) {
                            Intent pickIntent = new Intent(Intent.ACTION_PICK);
                            pickIntent.setType("image/*");
                            startActivityForResult(pickIntent, 1);
                        } else {
                            Utils.sClient.send(new TdApi.DeleteProfilePhoto(user.profilePhoto.id), new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {

                                }
                            });
                        }
                    }
                });
                builder.show().setCanceledOnTouchOutside(true);
            }
        });

        mEmptyHeaderView = inflater.inflate(R.layout.empty_header, mSettingsListView, false);
        mSettingsListView.addHeaderView(mEmptyHeaderView, null, false);
        mEmptyHeader = (LinearLayout)mEmptyHeaderView.findViewById(R.id.emptyHeader);

        if (mConfig == null) {
            mConfig = Utils.sContext.getResources().getConfiguration();
        }

        if (mConfig != null) {
            if (mConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                mProfileHeaderHeight = Utils.convertDpToPx(144);
                mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

                FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams) mEmptyHeader.getLayoutParams();
                flParams.height = mProfileHeaderHeight;
                mEmptyHeader.setLayoutParams(flParams);

                LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
                llParams.height = mProfileHeaderHeight;
                mProfileHeaderContainer.setLayoutParams(llParams);

                mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
                mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
            } else if (mConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                mProfileHeaderHeight = Utils.convertDpToPx(136);
                mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

                FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams) mEmptyHeader.getLayoutParams();
                flParams.height = mProfileHeaderHeight;
                mEmptyHeader.setLayoutParams(flParams);

                LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
                llParams.height = mProfileHeaderHeight;
                mProfileHeaderContainer.setLayoutParams(llParams);

                mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
                mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
            }
        }

        mSettingsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Integer scrollY = getScrollY(view);
                mProfileHeaderLayout.setTranslationY(Math.max(-scrollY, mProfileMinHeaderTranslation));

                final float offset = 1.0f - Math.max((float) (-mProfileMinHeaderTranslation - scrollY) / -mProfileMinHeaderTranslation, 0f);

                mToolbar.setTranslationY(-mProfileMinHeaderTranslation * offset);

                AnimatorSet animatorSet;
                if (offset >= 0.7f) {
                    if (mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_down_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = false;
                    }
                } else {
                    if (!mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_up_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = true;
                    }
                }

                //LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mAvatarImageView.getLayoutParams();
                //params.width = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //params.height = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //mAvatarImageView.setLayoutParams(params););
                mAvatarImageView.setScaleX(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setScaleY(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setTranslationX(Utils.convertDpToPx(37.5f) * offset);
                mAvatarImageView.setTranslationY(mProfileAvatarTranslation * offset);

                mInfoLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mInfoLayout.getLayoutParams();
                        params.height = (int) (Utils.convertDpToPx(61.0f) - Utils.convertDpToPx(13.0f) * offset);
                        params.rightMargin = (int) (Utils.convertDpToPx(16.0f) + Utils.convertDpToPx(48.0f) * offset);
                        mInfoLayout.setLayoutParams(params);

                        params = (LinearLayout.LayoutParams) mHeaderTextView.getLayoutParams();
                        params.bottomMargin = (int) (Utils.convertDpToPx(4) - Utils.convertDpToPx(4) * offset);
                        mHeaderTextView.setLayoutParams(params);
                    }
                });

                mInfoLayout.setTranslationX(Utils.convertDpToPx(14.0f) * offset);
                mInfoLayout.setTranslationY(mProfileInfoLayoutTranslation * offset);

                mHeaderTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20.0f - 2.0f * offset);
            }
        });

        mSettingsListView.setAdapter(mSettingsAdapter = new SettingsAdapter());

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (mSettingsListView != null) {
                    mSettingsListView.invalidateViews();
                }
            }
        });
        return mRootView;
    }

    private int getScrollY(AbsListView view) {
        View c = view.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mProfileHeaderHeight;
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == 0) {
                cropPhoto(mProfilePhotoPath);
                mProfilePhotoPath = null;
            } else if (requestCode == 1) {
                Uri uri = data.getData();
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getParentActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                if (cursor == null) {
                    return;
                }
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mProfilePhotoPath = cursor.getString(columnIndex);
                cursor.close();
                cropPhoto(mProfilePhotoPath);
            } else if (requestCode == 2) {
                Utils.sClient.send(new TdApi.SetProfilePhoto(data.getData().getPath(), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                    }
                });
            }
        }
    }

    private void cropPhoto(String path) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            cropIntent.putExtra("scale", "true");
            cropIntent.putExtra("return-data", "false");
            File cropProfilePhoto = Utils.createImageFile();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropProfilePhoto));
            startActivityForResult(cropIntent, 2);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(Utils.sContext, "Your device doesn't support the crop action", Toast.LENGTH_LONG).show();
            Utils.sClient.send(new TdApi.SetProfilePhoto(path, new TdApi.PhotoCrop()), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mSettingsAdapter != null) {
            mSettingsAdapter.notifyDataSetChanged();
        }
        updateCurrentUser();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
    }

    @Override
    public void updateInterface() {
        updateCurrentUser();
        if (mSettingsListView != null) {
            mSettingsListView.invalidateViews();
        }
    }

    public void updateCurrentUser() {
        final TdApi.User currentUser = ChatsCache.getUser(Utils.sCurrentUserId);
        TextDrawable textDrawable;
        if (currentUser != null) {
            textDrawable = Utils.getUserAvatar(currentUser, 61);
            if (mSubHeaderTextView != null) {
                mAvatarImageView.setProfilePhoto(currentUser.profilePhoto, textDrawable);
                mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentUser.profilePhoto.id != 0) {
                            PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                            Bundle args = new Bundle();
                            TdApi.File smallPhoto = ChatsCache.getFile(currentUser.profilePhoto.small.id);
                            TdApi.File bigPhoto = ChatsCache.getFile(currentUser.profilePhoto.big.id);
                            if (bigPhoto.path.length() != 0) {
                                args.putString("path", bigPhoto.path);
                            } else {
                                args.putString("path", smallPhoto.path);
                                args.putInt("big_photo_id", bigPhoto.id);
                            }
                            photoViewFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                        }
                    }
                });
                mHeaderTextView.setText(Utils.formatName(currentUser.firstName, currentUser.lastName));
                mSubHeaderTextView.setText(Utils.sContext.getString(R.string.online_status));
            }
        }
    }

    private class SettingsAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return position == 8;
        }

        @Override
        public int getCount() {
            return 9;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.empty_list_item, parent, false);
                }
            } else if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_big_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView secondaryTextView = (TextView)convertView.findViewById(R.id.secondaryTextView);
                TdApi.User currentUser = ChatsCache.getUser(Utils.sCurrentUserId);
                if (currentUser != null) {
                    if (position == 4) {
                        iconImageView.setImageResource(R.drawable.ic_user);
                        if (currentUser.username == null || currentUser.username.length() == 0) {
                            primaryTextView.setText(Utils.sContext.getString(R.string.no_username));
                        } else {
                            primaryTextView.setText("@" + currentUser.username);
                        }
                        secondaryTextView.setText(Utils.sContext.getString(R.string.profile_username));
                    } else if (position == 6) {
                        iconImageView.setImageResource(R.drawable.ic_phone);
                        if (currentUser.phoneNumber == null || currentUser.phoneNumber.length() == 0) {
                            primaryTextView.setText(Utils.sContext.getString(R.string.unknown_phone));
                        } else {
                            primaryTextView.setText(Utils.formatPhone("+" + currentUser.phoneNumber));
                        }
                        secondaryTextView.setText(Utils.sContext.getString(R.string.profile_phone));
                    }
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_divider, parent, false);
                }
            } else if (type == 3) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.space_list_item, parent, false);
                }
            } else if (type == 4) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView valueTextView = (TextView)convertView.findViewById(R.id.valueTextView);
                iconImageView.setVisibility(View.VISIBLE);
                valueTextView.setVisibility(View.VISIBLE);
                iconImageView.setImageResource(R.drawable.ic_setlock);
                primaryTextView.setText(Utils.sContext.getString(R.string.settings_passcode));
                if (Utils.sPasscodeHash.length() > 0) {
                    valueTextView.setTextColor(0xff6b9cc2);
                    valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_enabled));
                } else {
                    valueTextView.setTextColor(0xff8a8a8a);
                    valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_disabled));
                }
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            switch (position) {
                case 0:
                case 1:
                case 2:
                case 3:
                    return 0;
                case 4:
                case 6:
                    return 1;
                case 5:
                    return 2;
                case 7:
                    return 3;
                case 8:
                    return 4;
                default:
                    return 0;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 5;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}
