package org.tdlib.ippogram.ui;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.TextDrawable;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@Deprecated
public class GroupProfileFragment extends ProfileFragment implements UpdatesHandler.GroupUpdatesDelegate {

    private static final int MUTE_FOR_HOUR_MENU_ITEM = 0;
    private static final int MUTE_FOR_HOURS_MENU_ITEM = 1;
    private static final int MUTE_FOR_DAYS_MENU_ITEM = 2;
    private static final int MUTE_DISABLE_MENU_ITEM = 3;

    private static final int ADD_MEMBER_MENU_ITEM = 4;
    private static final int EDIT_NAME_MENU_ITEM = 5;
    private static final int DELETE_MENU_ITEM = 6;

    private ListView mGroupProfileListView;
    private GroupProfileAdapter mGroupProfileAdapter;

    private View mEmptyHeaderView;
    private LinearLayout mEmptyHeader;
    private LinearLayout mProfileHeaderLayout;
    private RelativeLayout mProfileHeaderContainer;
    private LinearLayout mInfoLayout;

    private ToolbarMenuLayout mMoreMenuButton;
    private ToolbarMenuLayout mNotifyMenuButton;

    private int mGroupId;
    private long mChatId;

    private TextView mSubHeaderTextView;
    private BlockingImageView mAvatarImageView;
    private ImageButton mFloatingActionButton;

    private TdApi.GroupFull mGroupFull;
    private TdApi.ChatParticipant[] mChatParticipants = null;
    private ArrayList<TdApi.ChatParticipant> mSortedUsers = new ArrayList<>();
    private ArrayList<Integer> mIgnoreUsers = new ArrayList<>();
    private int mOnlineCount = -1;

    private int mProfileHeaderHeight;
    private int mProfileMinHeaderTranslation;
    private int mProfileAvatarTranslation;
    private int mProfileInfoLayoutTranslation;
    private Configuration mConfig;

    private boolean mHideFab = true;
    private String mProfilePhotoPath;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            Utils.cancelRunOnUIThread(runnable);
            updateVisibleItems();
            Utils.runOnUIThread(runnable, 1000L);
        }
    };

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);

        mGroupId = getArguments().getInt("group_chat_id", 0);
        mChatId = getArguments().getLong("chat_id", 0);
        updateOnlineCount();
        mConfig = Utils.sContext.getResources().getConfiguration();

        Utils.runOnUIThread(runnable, 1000L);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_group_profile, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                ((MainActivity) getParentActivity()).destroyFragment(GroupProfileFragment.this);
            }
        });

        mNotifyMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.notifyMenuButton);
        mNotifyMenuButton.setParentToolbar(mToolbar);
        mNotifyMenuButton.setVisibility(View.VISIBLE);
        mNotifyMenuButton.addItem(MUTE_FOR_HOUR_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hour));
        mNotifyMenuButton.addItem(MUTE_FOR_HOURS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_hours));
        mNotifyMenuButton.addItem(MUTE_FOR_DAYS_MENU_ITEM, Utils.sContext.getString(R.string.notifications_days));
        mNotifyMenuButton.addItem(MUTE_DISABLE_MENU_ITEM, Utils.sContext.getString(R.string.notifications_disable));
        mNotifyMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean muted = ChatsCache.isChatMuted(mChatId);
                if (muted) {
                    TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                    if (chat != null) {
                        chat.notificationSettings.muteFor = 0;
                    }
                    Utils.cancelRunOnUIThread(ChatsCache.sChatsMuteRunnable.get(mChatId));
                    ChatsCache.sChatsMuteRunnable.remove(mChatId);
                    ChatsCache.updateNotifySettingsForChat(mChatId);
                    mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
                } else {
                    mNotifyMenuButton.createMenu();
                }
            }
        });
        mNotifyMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                int untilTime = 0;
                switch ((Integer) view.getTag()) {
                    case MUTE_FOR_HOUR_MENU_ITEM:
                        untilTime = 60 * 60;
                        break;
                    case MUTE_FOR_HOURS_MENU_ITEM:
                        untilTime = 60 * 60 * 8;
                        break;
                    case MUTE_FOR_DAYS_MENU_ITEM:
                        untilTime = 60 * 60 * 48;
                        break;
                    case MUTE_DISABLE_MENU_ITEM:
                        untilTime = Integer.MAX_VALUE;
                        break;
                }
                final TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                if (chat != null) {
                    chat.notificationSettings.muteFor = untilTime;
                }
                ChatsCache.sChatsMuteRunnable.put(mChatId, new Runnable() {
                    @Override
                    public void run() {
                        TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                        if (chat != null) {
                            chat.notificationSettings.muteFor = 0;
                        }
                        ChatsCache.sChatsMuteRunnable.remove(mChatId);
                        ChatsCache.updateNotifySettings();
                    }
                });
                ChatsCache.updateNotifySettingsForChat(mChatId);
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
            }
        });

        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more);

        mMoreMenuButton.addItem(ADD_MEMBER_MENU_ITEM, Utils.sContext.getString(R.string.action_add_member));
        mMoreMenuButton.addItem(EDIT_NAME_MENU_ITEM, Utils.sContext.getString(R.string.action_edit_name));
        mMoreMenuButton.addItem(DELETE_MENU_ITEM, Utils.sContext.getString(R.string.action_delete_and_leave));
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });
        mMoreMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                switch ((Integer) view.getTag()) {
                    case ADD_MEMBER_MENU_ITEM:
                        selectContacts();
                        break;
                    case EDIT_NAME_MENU_ITEM:
                        GroupProfileChangeNameFragment fragment = new GroupProfileChangeNameFragment();
                        Bundle args = new Bundle();
                        args.putInt("group_chat_id", mGroupId);
                        args.putLong("chat_id", mChatId);
                        fragment.setArguments(args);
                        ((MainActivity)getParentActivity()).createFragment(fragment, "change_group_title", true);
                        break;
                    case DELETE_MENU_ITEM:
                        if (getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(getParentActivity());
                        deleteBuilder.setMessage(R.string.delete_and_leave_message);
                        deleteBuilder.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ((MainActivity) getParentActivity()).destroyFragment("chat_" + mChatId);
                                ChatsCache.deleteUserFromChat(mChatId, Utils.sCurrentUserId, true);
                                mExitAnimated = true;
                                ((MainActivity) getParentActivity()).destroyFragment(GroupProfileFragment.this);
                            }
                        });
                        deleteBuilder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(deleteBuilder);
                        break;
                }
            }
        });

        mGroupProfileListView = (ListView)mRootView.findViewById(R.id.profileListView);
        mGroupProfileListView.setVerticalScrollBarEnabled(false);

        mProfileHeaderLayout = (LinearLayout) mRootView.findViewById(R.id.profileHeaderLayout);
        mProfileHeaderContainer = (RelativeLayout)mProfileHeaderLayout.findViewById(R.id.profileHeaderContainer);
        mInfoLayout = (LinearLayout)mProfileHeaderLayout.findViewById(R.id.infoLayout);
        mAvatarImageView = (BlockingImageView) mProfileHeaderLayout.findViewById(R.id.avatarImageView);
        mHeaderTextView = (TextView) mProfileHeaderLayout.findViewById(R.id.headerTextView);
        mSubHeaderTextView = (TextView) mProfileHeaderLayout.findViewById(R.id.subHeaderTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }
        if (mSubHeaderTextView != null) {
            mSubHeaderTextView.setTypeface(Utils.getTypeface("regular"));
        }

        mFloatingActionButton = (ImageButton)mProfileHeaderLayout.findViewById(R.id.floatingActionButton);
        mFloatingActionButton.setImageResource(R.drawable.ic_camera);
        mFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                final TdApi.Chat chat = ChatsCache.getChat(mChatId);
                CharSequence[] items;
                if (chat != null && chat.photo != null && chat.photo.small.id != 0) {
                    items = new CharSequence[]{
                            Utils.sContext.getString(R.string.take_photo),
                            Utils.sContext.getString(R.string.choose_from_gallery),
                            Utils.sContext.getString(R.string.delete_photo)
                    };
                } else {
                    items = new CharSequence[]{
                            Utils.sContext.getString(R.string.take_photo),
                            Utils.sContext.getString(R.string.choose_from_gallery)
                    };
                }
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File profilePhoto = Utils.createImageFile();
                            if (profilePhoto != null) {
                                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profilePhoto));
                                mProfilePhotoPath = profilePhoto.getAbsolutePath();
                            }
                            startActivityForResult(cameraIntent, 0);
                        } else if (which == 1) {
                            Intent pickIntent = new Intent(Intent.ACTION_PICK);
                            pickIntent.setType("image/*");
                            startActivityForResult(pickIntent, 1);
                        } else {
                            Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileId(0), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                                @Override
                                public void onResult(TdApi.TLObject object) {

                                }
                            });
                        }
                    }
                });
                builder.show().setCanceledOnTouchOutside(true);
            }
        });

        mEmptyHeaderView = inflater.inflate(R.layout.empty_header, mGroupProfileListView, false);
        mGroupProfileListView.addHeaderView(mEmptyHeaderView, null, false);
        mEmptyHeader = (LinearLayout)mEmptyHeaderView.findViewById(R.id.emptyHeader);

        if (mConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (mConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams llParams = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            llParams.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(llParams);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }

        mGroupProfileListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Integer scrollY = getScrollY(view);
                mProfileHeaderLayout.setTranslationY(Math.max(-scrollY, mProfileMinHeaderTranslation));

                final float offset = 1.0f - Math.max((float) (-mProfileMinHeaderTranslation - scrollY) / -mProfileMinHeaderTranslation, 0f);

                mToolbar.setTranslationY(-mProfileMinHeaderTranslation * offset);

                AnimatorSet animatorSet;
                if (offset >= 0.7f) {
                    if (mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_down_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = false;
                    }
                } else {
                    if (!mHideFab) {
                        animatorSet = (AnimatorSet) AnimatorInflater.loadAnimator(Utils.sContext, R.animator.scale_up_fab);
                        animatorSet.setTarget(mFloatingActionButton);
                        animatorSet.start();
                        mHideFab = true;
                    }
                }

                //LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mAvatarImageView.getLayoutParams();
                //params.width = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //params.height = (int)(Utils.convertDpToPx(61) - Utils.convertDpToPx(21) * offset);
                //mAvatarImageView.setLayoutParams(params));
                mAvatarImageView.setScaleX(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setScaleY(1.0f - offset * Utils.convertDpToPx(21.0f) / Utils.convertDpToPx(61.0f));
                mAvatarImageView.setTranslationX(Utils.convertDpToPx(37.5f) * offset);
                mAvatarImageView.setTranslationY(mProfileAvatarTranslation * offset);

                mInfoLayout.post(new Runnable() {
                    @Override
                    public void run() {
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mInfoLayout.getLayoutParams();
                        params.height = (int) (Utils.convertDpToPx(61.0f) - Utils.convertDpToPx(13.0f) * offset);
                        params.rightMargin = (int) (Utils.convertDpToPx(16.0f) + Utils.convertDpToPx(96.0f) * offset);
                        mInfoLayout.setLayoutParams(params);

                        params = (LinearLayout.LayoutParams) mHeaderTextView.getLayoutParams();
                        params.bottomMargin = (int) (Utils.convertDpToPx(4) - Utils.convertDpToPx(4) * offset);
                        mHeaderTextView.setLayoutParams(params);
                    }
                });

                mInfoLayout.setTranslationX(Utils.convertDpToPx(14.0f) * offset);
                mInfoLayout.setTranslationY(mProfileInfoLayoutTranslation * offset);

                mHeaderTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20.0f - 2.0f * offset);
            }
        });
        mGroupProfileListView.setAdapter(mGroupProfileAdapter = new GroupProfileAdapter());
        mGroupProfileListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 4) {
                    selectContacts();
                } else {
                    int count = 0;
                    if (mChatParticipants != null) {
                        count += mChatParticipants.length;
                    }
                    if (position > 6 && position < count + 7) {
                        int userId = mSortedUsers.get(position - 7).user.id;
                        if (userId == Utils.sCurrentUserId) {
                            return;
                        }
                        UserProfileFragment userProfileFragment = new UserProfileFragment();
                        Bundle args = new Bundle();
                        args.putLong("chat_id", mChatId);
                        args.putInt("user_id", userId);
                        args.putInt("group_chat_id", mGroupId);
                        userProfileFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + userId, true, false, true, false);
                    }
                }
            }
        });
        mGroupProfileListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                int count = 0;
                if (mChatParticipants != null) {
                    count += mChatParticipants.length;
                }
                if (position > 6 && position < count + 7) {
                    final TdApi.ChatParticipant part = mSortedUsers.get(position - 7);
                    if (part.user.id == Utils.sCurrentUserId) {
                        return false;
                    }
                    if (mGroupFull.adminId != Utils.sCurrentUserId && part.inviterId != Utils.sCurrentUserId) {
                        return false;
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                    CharSequence[] item = new CharSequence[]{Utils.sContext.getString(R.string.remove_from_group)};
                    builder.setItems(item, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                ChatsCache.deleteUserFromChat(mChatId, part.user.id, false);
                            }
                        }
                    });
                    showAlertDialog(builder);
                    return true;
                }
                return false;
            }
        });
        return mRootView;
    }

    private int getScrollY(AbsListView view) {
        View c = view.getChildAt(0);
        if (c == null) {
            return 0;
        }
        int firstVisiblePosition = view.getFirstVisiblePosition();
        int top = c.getTop();

        int headerHeight = 0;
        if (firstVisiblePosition >= 1) {
            headerHeight = mProfileHeaderHeight;
        }

        return -top + firstVisiblePosition * c.getHeight() + headerHeight;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mProfileHeaderHeight = Utils.convertDpToPx(144);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(56);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            params.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(params);

            mProfileAvatarTranslation = Utils.convertDpToPx(24.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(18.0f);
        } else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mProfileHeaderHeight = Utils.convertDpToPx(136);
            mProfileMinHeaderTranslation = -mProfileHeaderHeight + Utils.convertDpToPx(48);

            FrameLayout.LayoutParams flParams = (FrameLayout.LayoutParams)mEmptyHeader.getLayoutParams();
            flParams.height = mProfileHeaderHeight;
            mEmptyHeader.setLayoutParams(flParams);

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mProfileHeaderContainer.getLayoutParams();
            params.height = mProfileHeaderHeight;
            mProfileHeaderContainer.setLayoutParams(params);

            mProfileAvatarTranslation = Utils.convertDpToPx(28.5f);
            mProfileInfoLayoutTranslation = Utils.convertDpToPx(22.0f);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == 0) {
                cropPhoto(mProfilePhotoPath);
                mProfilePhotoPath = null;
            } else if (requestCode == 1) {
                Uri uri = data.getData();
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getParentActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                if (cursor == null) {
                    return;
                }
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mProfilePhotoPath = cursor.getString(columnIndex);
                cursor.close();
                cropPhoto(mProfilePhotoPath);
            } else if (requestCode == 2) {
                Log.d(Utils.TAG, "groupProfile");
                Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileLocal(data.getData().getPath()), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        Log.d(Utils.TAG, "" + object);
                    }
                });
            }
        }
    }

    private void cropPhoto(String path) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            cropIntent.putExtra("scale", "true");
            cropIntent.putExtra("return-data", "false");
            File cropProfilePhoto = Utils.createImageFile();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropProfilePhoto));
            startActivityForResult(cropIntent, 2);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(Utils.sContext, "Your device doesn't support the crop action", Toast.LENGTH_LONG).show();
            Utils.sClient.send(new TdApi.ChangeChatPhoto(mChatId, new TdApi.InputFileLocal(path), new TdApi.PhotoCrop()), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        }
    }

    private void selectContacts() {
        if (mIgnoreUsers.size() != 0) {
            mIgnoreUsers.clear();
        }
        for (TdApi.ChatParticipant part : mChatParticipants) {
            if (part.user.myLink.getConstructor() == TdApi.LinkStateContact.CONSTRUCTOR) {
                if (ContactsCache.getInstance().assignUsers.get(part.user.id) != null) {
                    mIgnoreUsers.add(part.user.id);
                }
            }
        }
        ContactsFragment contactsFragment = new ContactsFragment();
        Bundle args = new Bundle();
        args.putLong("chat_id", mChatId);
        args.putBoolean("show_only_users", true);
        args.putBoolean("create_group", false);
        args.putIntegerArrayList("ignore_users", mIgnoreUsers);
        contactsFragment.setArguments(args);
        ((MainActivity) getParentActivity()).createFragment(contactsFragment, "contacts", true);
    }

    private void updateOnlineCount() {
        if (mChatParticipants == null) {
            return;
        }
        mOnlineCount = 0;
        mSortedUsers.clear();
        for (TdApi.ChatParticipant chatParticipant : mChatParticipants) {
            TdApi.User user = ChatsCache.getUser(chatParticipant.user.id);
            if (user == null) {
                return;
            }
            if (Utils.formatUserStatus(user).equals("online")) {
                ++mOnlineCount;
            }
            mSortedUsers.add(chatParticipant);
        }

        Collections.sort(mSortedUsers, new Comparator<TdApi.ChatParticipant>() {
            @Override
            public int compare(TdApi.ChatParticipant lhs, TdApi.ChatParticipant rhs) {
                TdApi.UserStatus status1 = lhs.user.status;
                TdApi.UserStatus status2 = rhs.user.status;
                int statusInt1 = 0;
                int statusInt2 = 0;
                if (status1 != null) {
                    if (lhs.user.id != Utils.sCurrentUserId) {
                        if (status1.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
                            statusInt1 = ((TdApi.UserStatusOnline) status1).expires;
                        } else if (status1.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR) {
                            statusInt1 = ((TdApi.UserStatusOffline) status1).wasOnline;
                        }
                    } else {
                        statusInt1 = Utils.getCurrentTime() + 50000;
                    }
                }

                if (status2 != null) {
                    if (rhs.user.id != Utils.sCurrentUserId) {
                        if (status2.getConstructor() == TdApi.UserStatusOnline.CONSTRUCTOR) {
                            statusInt2 = ((TdApi.UserStatusOnline) status2).expires;
                        } else if (status2.getConstructor() == TdApi.UserStatusOffline.CONSTRUCTOR) {
                            statusInt2 = ((TdApi.UserStatusOffline) status2).wasOnline;
                        }
                    } else {
                        statusInt2 = Utils.getCurrentTime() + 50000;
                    }
                }
                if ((statusInt1 > 0 && statusInt2 > 0) || (statusInt1 < 0 && statusInt2 < 0)) {
                    if (statusInt1 < statusInt2) {
                        return 1;
                    } else if (statusInt1 == statusInt2) {
                        return 0;
                    } else {
                        return -1;
                    }
                } else if ((statusInt1 < 0 && statusInt2 > 0) || (statusInt1 == 0 && statusInt2 != 0)) {
                    return 1;
                } else if ((statusInt1 > 0 && statusInt2 < 0) || (statusInt1 != 0 && statusInt2 == 0)) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        if (mGroupProfileListView != null) {
            mGroupProfileListView.invalidateViews();
        }
    }

    public void setChatParticipants(TdApi.ChatParticipant[] participants) {
        mChatParticipants = participants;
    }

    public void setGroupFull(TdApi.GroupFull groupFull) {
        mGroupFull = groupFull;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }
        if (mGroupProfileAdapter != null) {
            mGroupProfileAdapter.notifyDataSetChanged();
        }
        updateGroupChat();
        updateNotifySettings();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //((MainActivity) getParentActivity()).destroyFragment(this);
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(runnable);
    }

    @Override
    public boolean onBackPressed() {
        mExitAnimated = true;
        return super.onBackPressed();
    }

    @Override
    public void updateInterface() {
        updateOnlineCount();
        updateGroupChat();
        updateVisibleItems();
    }

    @Override
    public void updateNotifySettings() {
        if (mGroupProfileListView != null) {
            if (ChatsCache.isChatMuted(mChatId)) {
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_off);
            } else {
                mNotifyMenuButton.setImageResource(R.drawable.ic_notifications_on);
            }
        }
    }

    @Override
    public void updateChatParts(int groupId, TdApi.GroupFull groupFull) {
        if (groupId == mGroupId) {
            mGroupFull = groupFull;
            mChatParticipants = groupFull.participants;
            updateOnlineCount();
            if (mGroupProfileAdapter != null) {
                mGroupProfileAdapter.notifyDataSetChanged();
            }
        }
    }

    private void updateVisibleItems() {
        if (mGroupProfileListView != null) {
            int count = mGroupProfileListView.getChildCount();
            for (int i = 0; i < count; i++) {
                View child = mGroupProfileListView.getChildAt(i);
                Object tag = child.getTag();
                if (tag instanceof GroupProfileAdapter.ViewHolder) {
                    ((GroupProfileAdapter.ViewHolder)tag).updateItem();
                }
            }
        }
    }

    public void updateGroupChat() {
        final TdApi.Group currentGroup = ChatsCache.getGroup(mGroupId);
        final TdApi.Chat currentChat = ChatsCache.getChat(mChatId);
        TextDrawable textDrawable;
        String countStatus;
        if (currentGroup != null && mSubHeaderTextView != null) {
            mHeaderTextView.setText(currentChat.title);

            textDrawable = Utils.getChatAvatar(currentChat, 61);
            mAvatarImageView.setChatPhoto(currentChat.photo, textDrawable);
            mAvatarImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentChat.photo.small.id != 0) {
                        PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                        Bundle args = new Bundle();
                        TdApi.File smallPhoto = ChatsCache.getFile(currentChat.photo.small.id);
                        TdApi.File bigPhoto = ChatsCache.getFile(currentChat.photo.big.id);
                        if (bigPhoto.path.length() != 0) {
                            args.putString("path", bigPhoto.path);
                        } else {
                            args.putString("path", smallPhoto.path);
                            args.putInt("big_photo_id", bigPhoto.id);
                        }
                        photoViewFragment.setArguments(args);
                        ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                    }
                }
            });

            if (currentGroup.participantsCount > 1) {
                countStatus = String.format("%d %s", currentGroup.participantsCount, Utils.sContext.getString(R.string.group_members));
            } else if (currentGroup.participantsCount == 1) {
                countStatus = String.format("%d %s", currentGroup.participantsCount, Utils.sContext.getString(R.string.group_member));
            } else {
                countStatus = Utils.sContext.getString(R.string.group_no_members);
            }

            if (mOnlineCount > 0) {
                mSubHeaderTextView.setText(String.format("%s, %d %s", countStatus, mOnlineCount, Utils.sContext.getString(R.string.online_status)));
            } else {
                mSubHeaderTextView.setText(countStatus);
            }
        }
    }

    private class GroupProfileAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return position == 3 || (position > 5 && position < getCount() - 1);
        }

        @Override
        public int getCount() {
            int count = 6;
            if (mChatParticipants != null) {
                count += mChatParticipants.length;
            }
            return ++count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.empty_list_item, parent, false);
                }
            } else if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                }
                ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                iconImageView.setVisibility(View.VISIBLE);
                iconImageView.setImageResource(R.drawable.ic_add);
                primaryTextView.setText(Utils.sContext.getString(R.string.action_add_member));
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.space_list_item, parent, false);
                }
            } else if (type == 3) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.group_list_item, parent, false);
                }
                ViewHolder viewHolder = (ViewHolder)convertView.getTag();
                if (viewHolder == null) {
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }

                if (position == 6) {
                    viewHolder.iconImageView.setVisibility(View.VISIBLE);
                    viewHolder.iconImageView.setImageResource(R.drawable.ic_groupusers);
                } else {
                    viewHolder.iconImageView.setVisibility(View.INVISIBLE);
                }

                viewHolder.user = ChatsCache.getUser(mSortedUsers.get(position - 6).user.id);
                viewHolder.updateItem();
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0 || position == 1 || position == 2 || position == 5 || position == getCount() - 1) {
                return 0;
            } else if (position == 3) {
                return 1;
            } else if (position == 4) {
                return 2;
            }
            return 3;
        }

        @Override
        public int getViewTypeCount() {
            return 4;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        private class ViewHolder {
            public final ImageView iconImageView;
            public final BlockingImageView avatarImageView;
            public final TextView primaryTextView;
            public final TextView secondaryTextView;
            public TdApi.User user;

            public ViewHolder(View itemView) {
                iconImageView = (ImageView)itemView.findViewById(R.id.iconImageView);
                avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
                primaryTextView = (TextView)itemView.findViewById(R.id.primaryTextView);
                secondaryTextView = (TextView)itemView.findViewById(R.id.secondaryTextView);
            }

            public void updateItem() {
                avatarImageView.setProfilePhoto(user.profilePhoto, Utils.getUserAvatar(user, 48));
                if (user.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    primaryTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                } else {
                    primaryTextView.setText(Utils.formatName(user.firstName, user.lastName));
                }
                String userStatus = Utils.formatUserStatus(user);
                switch (userStatus) {
                    case "online":
                        secondaryTextView.setTextColor(0xff6b9cc2);
                        break;
                    case "bot":
                        secondaryTextView.setTextColor(0xff8a8a8a);
                        TdApi.UserTypeBot userBot = (TdApi.UserTypeBot)user.type;
                        if (userBot.canReadAllGroupChatMessages) {
                            userStatus = Utils.sContext.getString(R.string.access_status);
                        } else {
                            userStatus = Utils.sContext.getString(R.string.no_access_status);
                        }
                        break;
                    default:
                        secondaryTextView.setTextColor(0xff8a8a8a);
                }
                secondaryTextView.setText(userStatus);
            }
        }
    }
}
