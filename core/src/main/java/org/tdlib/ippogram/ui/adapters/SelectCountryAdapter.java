package org.tdlib.ippogram.ui.adapters;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;
import static org.tdlib.ippogram.ui.components.SectionedSectionIndexer.SimpleSection;
import org.tdlib.ippogram.ui.components.StringArrayAlphabetIndexer;

import java.util.ArrayList;
import java.util.List;

public class SelectCountryAdapter extends IndexedPinnedHeaderListViewAdapter {

    private ArrayList<Country> mCountries;

    public SelectCountryAdapter(ArrayList<Country> countries) {
        setData(countries);
    }

    @Override
    public CharSequence getSectionTitle(int sectionIndex) {
        return ((StringArrayAlphabetIndexer.AlphaBetSection)getSections()[sectionIndex]).getName();
    }

    public void setData(ArrayList<Country> countries) {
        mCountries = countries;
        String[] generatedCountryNames = generateCountryNames(mCountries);
        setSectionIndexer(new StringArrayAlphabetIndexer(generatedCountryNames, true));
    }

    private String[] generateCountryNames(List<Country> countries) {
        ArrayList<String> countryNames = new ArrayList<>();
        if (countries != null) {
            for (Country countryEntity : countries) {
                countryNames.add(countryEntity.name);
            }
        }
        return countryNames.toArray(new String[countryNames.size()]);
    }

    @Override
    public boolean isEnabled(int position) {
        SimpleSection countrySection = (SimpleSection)getSections()[getSectionForPosition(position)];
        return getPositionInSection(position) < countrySection.getItemsCount();
    }

    @Override
    public int getCount() {
        return mCountries.size() + getSections().length - 1;
    }

    @Override
    public Country getItem(int position) {
        return mCountries.get(position - getSectionForPosition(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        if (type == 0) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.pinned_header_listview_item, parent, false);
            }
            CountryViewHolder holder = (CountryViewHolder) convertView.getTag();
            if (holder == null) {
                holder = new CountryViewHolder(convertView);
                convertView.setTag(holder);
            }
            Country country = getItem(position);
            holder.countryNameTextView.setText(country.name);
            holder.phoneCodeTextView.setText("+" + String.valueOf(country.phoneCode));
            bindSectionHeader(holder.letterSectionTextView, position);
        } else if (type == 1) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider, parent, false);
            }
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        SimpleSection countrySection = (SimpleSection)getSections()[getSectionForPosition(position)];
        return getPositionInSection(position) < countrySection.getItemsCount() ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public static class Country implements Parcelable {

        public static final Creator<Country> CREATOR = new Creator<Country>() {
            @Override
            public Country createFromParcel(Parcel source) {
                return new Country(source);
            }

            @Override
            public Country[] newArray(int size) {
                return new Country[size];
            }
        };
        public int phoneCode;
        public String code;
        public String name;

        public Country() {
            phoneCode = 0;
            code = "";
            name = "";
        }

        private Country(Parcel parcel) {
            phoneCode = parcel.readInt();
            code = parcel.readString();
            name = parcel.readString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(phoneCode);
            dest.writeString(code);
            dest.writeString(name);
        }
    }

    public class CountryViewHolder extends IndexedPinnedHeaderListViewAdapter.ViewHolder {
        public TextView countryNameTextView;
        public TextView phoneCodeTextView;

        public CountryViewHolder(View itemView) {
            super(itemView);
            countryNameTextView = (TextView) itemView.findViewById(R.id.userNameTextView);
            phoneCodeTextView = (TextView) itemView.findViewById(R.id.phoneCodeTextView);
        }
    }
}