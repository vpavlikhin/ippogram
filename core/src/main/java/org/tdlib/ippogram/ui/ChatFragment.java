package org.tdlib.ippogram.ui;

import android.animation.Animator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.LeadingMarginSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapperResource;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Emoji;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.bottomsheet.BottomSheet;
import org.tdlib.ippogram.ui.adapters.ImagePickerAdapter;
import org.tdlib.ippogram.ui.components.AlignedTextView;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.EmojiView;
import org.tdlib.ippogram.ui.components.EmptyRecyclerView;
import org.tdlib.ippogram.ui.components.ProgressView;
import org.tdlib.ippogram.ui.components.SizeChangingRelativeLayout;
import org.tdlib.ippogram.ui.components.TextDrawable;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class ChatFragment extends MainFragment implements SizeChangingRelativeLayout.SizeChangingRelativeLayoutDelegate,
        UpdatesHandler.UpdatesForChatDelegate, UpdatesHandler.GroupUpdatesDelegate, ToolbarMenuLayout.OnItemClickListener,
        UpdatesHandler.UserUpdatesDelegate, UpdatesHandler.ChannelUpdatesDelegate, UpdatesHandler.DownloadUpdatesDelegate {

    private static final int DELETE_CHAT_MENU_ITEM = 0;
    private static final int CLEAR_HISTORY_MENU_ITEM = 1;
    private static final int LEAVE_GROUP_MENU_ITEM = 2;
    private static final int MUTE_MENU_ITEM = 3;
    private static final int UNBLOCK_MENU_ITEM = 4;

    private SizeChangingRelativeLayout mSizeChangingRelativeLayout;

    private ToolbarMenuLayout mMoreMenuButton;
    private TextView mMuteItemTextView;

    private TextView mSubHeaderTextView;
    private ImageView mMuteImageView;
    private LinearLayout mProfileButton;

    private BlockingImageView mAvatarImageView;
    private EmptyRecyclerView mChatListView;
    private View mLoadingMessagesProgressBar;
    private TextView mListEmptyTextView;
    private ChatAdapter mChatAdapter;
    private LinearLayoutManager mLayoutManager;
    private RelativeLayout mBottomPanel;
    private EmojiView mEmojiView;
    private ImageView mEmojiImageView;
    private PopupWindow mEmojiPopup;
    private EditText mMessageEditText;
    private ImageButton mSendMessageButton;

    private FrameLayout mBottomActionPanel;
    private TextView mBottomTextView;

    private View mContentView;
    private int mKeyboardHeight;
    private boolean mKeyboardVisible;

    private TdApi.Chat mCurrentChat = null;
    private TdApi.User mCurrentUser = null;
    private TdApi.Group mCurrentGroup = null;
    private TdApi.Channel mCurrentChannel = null;
    //private TdApi.BotInfo mBotInfo = null;
    private TdApi.UserFull mUserFull = null;

    private long mChatId;
    private boolean mChatEndReached = false;
    private boolean mChatLoading = false;
    private int mMaxMessageId = Integer.MAX_VALUE;
    private boolean mScrollToTop = false;
    private boolean mIsFirst = true;
    private boolean mPaused = true;
    private boolean mReadWhenResume = false;

    private int mSelectedUserId;

    private TdApi.GroupFull mGroupFull;
    private TdApi.ChannelFull mChannelFull;
    private TdApi.ChatParticipant [] mChatParticipants = null;
    private int mOnlineCount = -1;

    private AudioTrack mAudioTrack = null;
    private long mSamples = 0;

    private ArrayList<String> mSelectedImages = new ArrayList<>();

    //private boolean mBlocked = false;

    private HashMap<Integer, TdApi.Message> mAssignMessages = new HashMap<>();
    private ArrayList<TdApi.Message> mMessages = new ArrayList<>();
    private HashMap<String, ArrayList<TdApi.Message>> mMessagesByDays = new HashMap<>();

    private HashMap<Integer, ArrayList<ProgressView>> mStatesProgress = new HashMap<>();
    private HashMap<Integer, ArrayList<TextView>> mStringStatesProgress = new HashMap<>();

    private Runnable mUpdateSubHeaderRunnable = new Runnable() {
        @Override
        public void run() {
            updateSubHeader();
        }
    };

    @Override
    public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
        if (mSelectedUserId != 0) {
            Animator animator = super.onCreateAnimator(transit, enter, nextAnim);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    ChatsCache.sendMessage(mCurrentChat.id, mSelectedUserId);
                    mSelectedUserId = 0;
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            return animator;
        }
        return super.onCreateAnimator(transit, enter, nextAnim);
    }

    @Override
    public void onPreCreate() {
        ChatsCache.addDelegate(this);

        mChatId = getArguments().getLong("chat_id", 0);
        int userId = getArguments().getInt("user_id", 0);
        int groupId = getArguments().getInt("group_id", 0);
        int channelId = getArguments().getInt("channel_id", 0);
        mSelectedUserId = getArguments().getInt("selected_user_id", 0);
        if (mChatId != 0) {
            mCurrentChat = ChatsCache.sChats.get(mChatId);
        }
        if (userId != 0) {
            mCurrentUser = ChatsCache.getUser(userId);
            if (!ChatsCache.sUsersFull.containsKey(userId)) {
                ChatsCache.loadUserFull(userId);
            } else {
                mUserFull = ChatsCache.sUsersFull.get(userId);
                //mBlocked = userFull.isBlocked;
                //mBotInfo = userFull.botInfo;
                //ChatsCache.updateUserInfo(userId, userFull.isBlocked, userFull.botInfo);
            }
        } else if (groupId != 0) {
            mCurrentGroup = ChatsCache.getGroup(groupId);
            ChatsCache.loadGroupFull(groupId);
        } else if (channelId != 0) {
            mCurrentChannel = ChatsCache.getChannel(channelId);
            ChatsCache.loadChannelFull(channelId);
        }
        mChatLoading = true;
        if (mCurrentChat != null && mCurrentChat.topMessage.id != 0) {
            loadMessages(mChatId, mCurrentChat.topMessage.id + 1, 0, 30);
        } else {
            loadMessages(mChatId, mMaxMessageId, 0, 30);
        }

        Utils.sClient.send(new TdApi.OpenChat(mChatId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });

        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getParentActivity()).destroyFragment("contacts");
                ((MainActivity) getParentActivity()).destroyFragment("select_contacts");
                ((MainActivity) getParentActivity()).destroyFragment("select_chat");
                ((MainActivity) getParentActivity()).destroyFragment("create_group");
                if (mCurrentGroup != null) {
                    ((MainActivity) getParentActivity()).destroyFragment("group_profile_" + mCurrentGroup.id);
                } else if (mCurrentUser != null) {
                    ((MainActivity) getParentActivity()).destroyFragment("user_profile_" + mCurrentUser.id);
                } else if (mCurrentChannel != null) {
                    ((MainActivity) getParentActivity()).destroyFragment("channel_profile_" + mCurrentChannel.id);
                }
            }
        }, 600L);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_chat, parent, false);

        mSizeChangingRelativeLayout = (SizeChangingRelativeLayout)mRootView.findViewById(R.id.chatLayout);
        mSizeChangingRelativeLayout.delegate = this;
        mContentView = mSizeChangingRelativeLayout;

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessages.isEmpty() && !ChatsCache.sChatsList.contains(mCurrentChat)) {
                    ChatsCache.sChats.remove(mChatId);
                }
                mExitAnimated = true;
                ((MainActivity) getParentActivity()).destroyFragment(ChatFragment.this);
            }
        });

        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more);

        if (mCurrentUser != null) {
            mMoreMenuButton.addItem(DELETE_CHAT_MENU_ITEM, Utils.sContext.getString(R.string.action_delete_chat));
        } else if (mCurrentGroup != null) {
            mMoreMenuButton.addItem(CLEAR_HISTORY_MENU_ITEM, Utils.sContext.getString(R.string.action_clear_history));
            mMoreMenuButton.addItem(LEAVE_GROUP_MENU_ITEM, Utils.sContext.getString(R.string.action_leave_group));
        }
        mMuteItemTextView = mMoreMenuButton.addItem(MUTE_MENU_ITEM, null);

        mMoreMenuButton.setOnItemClickListener(this);
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });

        mAvatarImageView = (BlockingImageView)mRootView.findViewById(R.id.avatarImageView);

        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }

        mSubHeaderTextView = (TextView)mToolbar.findViewById(R.id.subHeaderTextView);
        if (mSubHeaderTextView != null) {
            mSubHeaderTextView.setTypeface(Utils.getTypeface("regular"));
        }

        mMuteImageView = (ImageView)mToolbar.findViewById(R.id.muteImageView);
        mProfileButton = (LinearLayout)mToolbar.findViewById(R.id.profileButton);
        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentUser != null) {
                    ProfileFragment userProfileFragment = new ProfileFragment();
                    //userProfileFragment.setBotInfo(mBotInfo);
                    Bundle args = new Bundle();
                    args.putInt("user_id", mCurrentUser.id);
                    args.putLong("chat_id", mCurrentChat.id);
                    //args.putBoolean("blocked", mBlocked);
                    userProfileFragment.setArguments(args);
                    ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + mCurrentUser.id, true, false, true, false);
                } else if (mCurrentGroup != null) {
                    if (mCurrentGroup.participantsCount == 0) {
                        return;
                    }
                    ProfileFragment groupProfileFragment = new ProfileFragment();
                    if (mChatParticipants != null) {
                        groupProfileFragment.setChatParticipants(mChatParticipants);
                    }
                    if (mGroupFull != null) {
                        groupProfileFragment.setGroupFull(mGroupFull);
                    }
                    Bundle args = new Bundle();
                    args.putInt("group_chat_id", mCurrentGroup.id);
                    args.putLong("chat_id", mCurrentChat.id);
                    groupProfileFragment.setArguments(args);
                    ((MainActivity) getParentActivity()).createFragment(groupProfileFragment, "group_profile_" + mCurrentGroup.id, true, false, true, false);
                } else if (mCurrentChannel != null) {
                    if (mChannelFull == null || mChannelFull.participantsCount == 0) {
                        return;
                    }
                    ProfileFragment channelProfileFragment = new ProfileFragment();
                    if (mChatParticipants != null) {
                        channelProfileFragment.setChatParticipants(mChatParticipants);
                    }
                    if (mChannelFull != null) {
                        channelProfileFragment.setChannelFull(mChannelFull);
                    }
                    Bundle args = new Bundle();
                    args.putInt("channel_id", mCurrentChannel.id);
                    args.putLong("chat_id", mCurrentChat.id);
                    channelProfileFragment.setArguments(args);
                    ((MainActivity) getParentActivity()).createFragment(channelProfileFragment, "channel_profile_" + mCurrentChannel.id, true, false, true, false);
                }
            }
        });
        if (mCurrentGroup != null && mCurrentGroup.participantsCount == 0 || mChannelFull != null && mChannelFull.participantsCount == 0) {
            mProfileButton.setClickable(false);
        }
        mChatListView = (EmptyRecyclerView)mRootView.findViewById(R.id.chatListView);
        mChatListView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getParentActivity());
        mLayoutManager.setStackFromEnd(true);
        mChatListView.setLayoutManager(mLayoutManager);
        //RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        //((DefaultItemAnimator)itemAnimator).setSupportsChangeAnimations(false);
        //mChatListView.setItemAnimator(itemAnimator);
        mChatAdapter = new ChatAdapter(getParentActivity());
        mChatListView.setAdapter(mChatAdapter);
        mChatListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                int visibleItemCount = mLayoutManager.getChildCount();
                if (visibleItemCount > 0) {
                    if (firstVisibleItem <= 10) {
                        if (!mChatEndReached && !mChatLoading) {
                            if (mMessagesByDays.size() != 0) {
                                loadMessages(mChatId, mMaxMessageId, 0, 20);
                            } else {
                                loadMessages(mChatId, Integer.MAX_VALUE, 0, 20);
                            }
                            mChatLoading = true;
                        }
                    }
                }
            }
        });

        mListEmptyTextView = (TextView)mRootView.findViewById(R.id.listEmptyTextView);
        mLoadingMessagesProgressBar = mRootView.findViewById(R.id.loadingMessagesProgressBar);
        if (mChatLoading && mMessages.isEmpty()) {
            mLoadingMessagesProgressBar.setVisibility(View.VISIBLE);
            mChatListView.setEmptyView(null);
        } else {
            mLoadingMessagesProgressBar.setVisibility(View.GONE);
            mChatListView.setEmptyView(mListEmptyTextView);
        }

        mBottomPanel = (RelativeLayout)mRootView.findViewById(R.id.bottomPanel);
        if (mCurrentChannel != null && mCurrentChannel.isBroadcast) {
            mBottomPanel.setVisibility(View.GONE);
        }

        mEmojiImageView = (ImageView)mRootView.findViewById(R.id.smilesImageView);
        mEmojiImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEmojiPopup == null) {
                    showEmojiPopup(true);
                } else {
                    showEmojiPopup(!mEmojiPopup.isShowing());
                }
            }
        });

        mMessageEditText = (EditText)mRootView.findViewById(R.id.messageEditText);
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String message = s.toString().trim();
                message = message.replaceAll("\n\n+", "\n\n");
                message = message.replaceAll(" +", " ");
                if (message.length() == 0) {
                    mSendMessageButton.setImageResource(R.drawable.ic_attach);
                } else {
                    mSendMessageButton.setImageResource(R.drawable.ic_send);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int i = 0;
                ImageSpan[] arrayOfImageSpan = s.getSpans(0, s.length(), ImageSpan.class);
                int j = arrayOfImageSpan.length;
                while (true) {
                    if (i >= j) {
                        Emoji.replaceEmoji(s, mMessageEditText);
                        return;
                    }
                    s.removeSpan(arrayOfImageSpan[i]);
                    i++;
                }
            }
        });

        mMessageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK && !mKeyboardVisible && mEmojiPopup != null && mEmojiPopup.isShowing()) {
                    if (keyEvent.getAction() == 1) {
                        showEmojiPopup(false);
                    }
                    return true;
                }
                return false;
            }
        });

        mSendMessageButton = (ImageButton)mRootView.findViewById(R.id.sendMessageButton);
        mSendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mMessageEditText.getText().toString().trim();
                message = message.replaceAll("\n\n+", "\n\n");
                message = message.replaceAll(" +", " ");
                if (message.length() != 0) {
                    ChatsCache.sendMessage(mChatId, message);
                    mMessageEditText.setText("");
                    mChatListView.post(new Runnable() {
                        @Override
                        public void run() {
                            mLayoutManager.scrollToPositionWithOffset(mMessages.size() - 1, -10000 - mChatListView.getPaddingTop());
                        }
                    });
                } else {
                    BottomSheet.Builder pickBuilder = new BottomSheet.Builder(getParentActivity());
                    pickBuilder.pick();
                    final ImagePickerAdapter adapter = new ImagePickerAdapter(getParentActivity());
                    pickBuilder.pickerAdapter(adapter);
                    pickBuilder.sheet(0, R.drawable.ic_attach_photo, R.string.take_photo);
                    pickBuilder.sheet(1, R.drawable.ic_attach_gallery, R.string.choose_from_gallery);
                    pickBuilder.listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                if (mSelectedImages.size() != 0) {
                                    mSelectedImages.clear();
                                }
                                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                File photo = Utils.createImageFile();
                                if (photo != null) {
                                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                                    mSelectedImages.add(photo.getAbsolutePath());
                                }
                                startActivityForResult(cameraIntent, 0);
                            } else if (which == 1) {
                                if (mSelectedImages.size() == 0) {
                                    Intent pickIntent = new Intent(Intent.ACTION_PICK);
                                    pickIntent.setType("image/*");
                                    startActivityForResult(pickIntent, 1);
                                } else {
                                    ChatsCache.sendMessage(mChatId, mSelectedImages);
                                }
                            }
                        }
                    });
                    final BottomSheet pickSheet = pickBuilder.build();
                    pickSheet.setCanceledOnTouchOutside(true);
                    pickSheet.setCanceledOnSwipeDown(false);
                    pickSheet.show();
                    pickSheet.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                            mSelectedImages.clear();
                        }
                    });
                    adapter.setOnPickListener(new ImagePickerAdapter.OnPickListener() {
                        @Override
                        public void onPick(View view, int position) {
                            String path = adapter.getPath(position);
                            if (mSelectedImages.contains(path)) {
                                mSelectedImages.remove(path);
                            } else {
                                mSelectedImages.add(path);
                            }

                            int size = mSelectedImages.size();
                            MenuItem item = pickSheet.getMenu().getItem(1);
                            if (size == 0) {
                                item.setTitle(Utils.sContext.getString(R.string.choose_from_gallery));
                            } else if (size == 1) {
                                item.setTitle(Html.fromHtml(Utils.sContext.getString(R.string.send_photo)));
                            } else {
                                item.setTitle(Html.fromHtml(String.format(Utils.sContext.getString(R.string.send_photos), mSelectedImages.size())));
                            }
                            pickSheet.invalidate();
                            adapter.setSelectedImages(mSelectedImages);
                            adapter.notifyItemChanged(position);
                        }
                    });
                }
            }
        });

        mBottomActionPanel = (FrameLayout)mRootView.findViewById(R.id.bottomActionPanel);
        mBottomTextView = (TextView)mBottomActionPanel.findViewById(R.id.bottomTextView);
//        if (mCurrentChannel != null && mCurrentChannel.role.getConstructor() == TdApi.ChatParticipantRoleGeneral.CONSTRUCTOR) {
//            mBottomActionPanel.setBackgroundColor(0xff73c979);
//            mBottomTextView.setTextColor(0xffffffff);
//            mBottomTextView.setText(Utils.sContext.getString(R.string.action_follow));
//            mBottomActionPanel.setVisibility(View.VISIBLE);
//        }
        mBottomActionPanel.setTag(DELETE_CHAT_MENU_ITEM);
        mBottomActionPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(v);
            }
        });
        return mRootView;
    }

    @Override
    public void onItemClick(View view) {
        final int id = (Integer)view.getTag();
        switch (id) {
            case DELETE_CHAT_MENU_ITEM:
            case CLEAR_HISTORY_MENU_ITEM:
            case LEAVE_GROUP_MENU_ITEM:
            case UNBLOCK_MENU_ITEM:
                if (getParentActivity() == null) {
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                if (id != UNBLOCK_MENU_ITEM) {
                    TextView titleTextView = new TextView(getParentActivity());
                    titleTextView.setTypeface(Utils.getTypeface("medium"));
                    titleTextView.setTextSize(18);
                    titleTextView.setTextColor(0xde000000);
                    titleTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.START);
                    titleTextView.setSingleLine(true);
                    titleTextView.setEllipsize(TextUtils.TruncateAt.END);
                    if (id == DELETE_CHAT_MENU_ITEM) {
                        titleTextView.setText(Utils.sContext.getString(R.string.delete_chat_title));
                    } else if (id == CLEAR_HISTORY_MENU_ITEM) {
                        titleTextView.setText(Utils.sContext.getString(R.string.clear_history_title));
                    } else {
                        titleTextView.setText(Utils.sContext.getString(R.string.leave_group_title));
                    }
                    int padding = Utils.convertDpToPx(24);
                    titleTextView.setPadding(padding, Utils.convertDpToPx(18), padding, 0);
                    builder.setCustomTitle(titleTextView);
                }
                if (id == LEAVE_GROUP_MENU_ITEM) {
                    builder.setMessage(R.string.leave_group_message);
                } else if (id == UNBLOCK_MENU_ITEM) {
                    builder.setMessage(R.string.unblock_message);
                } else {
                    builder.setMessage(R.string.delete_chat_message);
                }
                int buttonTitle;
                if (id == DELETE_CHAT_MENU_ITEM) {
                    buttonTitle = R.string.delete_button;
                } else if (id == CLEAR_HISTORY_MENU_ITEM) {
                    buttonTitle = R.string.clear_button;
                } else if (id == LEAVE_GROUP_MENU_ITEM) {
                    buttonTitle = R.string.leave_button;
                } else {
                    buttonTitle = R.string.action_unblock;
                }
                builder.setPositiveButton(buttonTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (id == LEAVE_GROUP_MENU_ITEM) {
                            ChatsCache.deleteUserFromChat(mChatId, Utils.sCurrentUserId, false);
                        } else if (id == UNBLOCK_MENU_ITEM) {
                            ChatsCache.unblockUser(mCurrentUser.id);
                        } else if (id == CLEAR_HISTORY_MENU_ITEM){
                            removeAllMessages();
                            ChatsCache.deleteChat(mChatId, true);
                        } else {
                            ChatsCache.deleteChat(mChatId, false);
                        }
                        if (id == DELETE_CHAT_MENU_ITEM || id == LEAVE_GROUP_MENU_ITEM) {
                            ((MainActivity) getParentActivity()).destroyFragment(ChatFragment.this);
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel_button, null);
                showAlertDialog(builder);
                break;
            case MUTE_MENU_ITEM:
                boolean muted = ChatsCache.isChatMuted(mChatId);
                if (muted) {
                    TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                    if (chat != null) {
                        chat.notificationSettings.muteFor = 0;
                    }
                    Utils.cancelRunOnUIThread(ChatsCache.sChatsMuteRunnable.get(mChatId));
                    ChatsCache.sChatsMuteRunnable.remove(mChatId);
                    ChatsCache.updateNotifySettingsForChat(mChatId);
                } else {
                    BottomSheet.Builder muteBuilder = new BottomSheet.Builder(getParentActivity());
                    muteBuilder.title(Utils.sContext.getString(R.string.notifications_title));
                    CharSequence [] items = new CharSequence[] {
                            Utils.sContext.getText(R.string.notifications_hour),
                            Utils.sContext.getText(R.string.notifications_hours),
                            Utils.sContext.getText(R.string.notifications_days),
                            Utils.sContext.getText(R.string.notifications_disable)
                    };
                    for (int i = 0; i < items.length; i++) {
                        muteBuilder.sheet(i, items[i]);
                    }
                    muteBuilder.listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int untilTime;
                            switch (which) {
                                case 0:
                                    untilTime = 60 * 60;
                                    break;
                                case 1:
                                    untilTime = 60 * 60 * 8;
                                    break;
                                case 2:
                                    untilTime = 60 * 60 * 48;
                                    break;
                                case 3:
                                    untilTime = Integer.MAX_VALUE;
                                    break;
                                default:
                                    untilTime = 0;
                            }
                            final TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                            if (chat != null) {
                                chat.notificationSettings.muteFor = untilTime;
                            }
                            ChatsCache.sChatsMuteRunnable.put(mChatId, new Runnable() {
                                @Override
                                public void run() {
                                    TdApi.Chat chat = ChatsCache.sChats.get(mChatId);
                                    if (chat != null) {
                                        chat.notificationSettings.muteFor = 0;
                                    }
                                    ChatsCache.sChatsMuteRunnable.remove(mChatId);
                                    ChatsCache.updateNotifySettings();
                                }
                            });
                            ChatsCache.updateNotifySettingsForChat(mChatId);
                        }
                    });
                    BottomSheet menuSheet = muteBuilder.build();
                    menuSheet.setCanceledOnTouchOutside(true);
                    menuSheet.setCanceledOnSwipeDown(false);
                    menuSheet.show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == AppCompatActivity.RESULT_OK) {
            if (requestCode == 0) {
                ChatsCache.sendMessage(mChatId, mSelectedImages);
                //cropPhoto(mProfilePhotoPath);
            } else if (requestCode == 1) {
                Uri uri = data.getData();
                String [] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getParentActivity().getContentResolver().query(uri, filePathColumn, null, null, null);
                if (cursor == null) {
                    return;
                }
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                mSelectedImages.add(cursor.getString(columnIndex));
                cursor.close();
                ChatsCache.sendMessage(mChatId, mSelectedImages);
                //cropPhoto(mProfilePhotoPath);
            }/* else if (requestCode == 2) {
                Utils.sClient.send(new TdApi.SetProfilePhoto(data.getData().getPath(), new TdApi.ProfilePhotoCrop()), new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                    }
                });
            }*/
        } else if (resultCode == AppCompatActivity.RESULT_CANCELED) {
            mSelectedImages.clear();
        }
    }

    private void cropPhoto(String path) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            cropIntent.setDataAndType(Uri.fromFile(new File(path)), "image/*");
            cropIntent.putExtra("crop", "true");
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            cropIntent.putExtra("scale", "true");
            cropIntent.putExtra("return-data", "false");
            File cropProfilePhoto = Utils.createImageFile();
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(cropProfilePhoto));
            startActivityForResult(cropIntent, 2);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(Utils.sContext, "Your device doesn't support the crop action", Toast.LENGTH_LONG).show();
            Utils.sClient.send(new TdApi.SetProfilePhoto(path, new TdApi.PhotoCrop()), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        }
    }

    private void createEmojiPopup() {
        mEmojiView = new EmojiView(getParentActivity());
        mEmojiView.setListener(new EmojiView.Listener() {
            @Override
            public void onBackspace() {
                mMessageEditText.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DEL));
            }

            @Override
            public void onEmojiSelected(String paramString) {
                int i = mMessageEditText.getSelectionEnd();
                CharSequence charSequence = Emoji.replaceEmoji(paramString, mMessageEditText);
                mMessageEditText.setText(mMessageEditText.getText().insert(i, charSequence));
                int j = i + charSequence.length();
                mMessageEditText.setSelection(j, j);
            }
        });
        mEmojiPopup = new PopupWindow(mEmojiView);
    }

    private void showEmojiPopup(boolean show) {
        if (show) {
            if (mEmojiPopup == null) {
                if (getParentActivity() == null) {
                    return;
                }
                createEmojiPopup();
            }
            if (mKeyboardHeight <= 0) {
                mKeyboardHeight = getParentActivity().getSharedPreferences("emoji", 0).getInt("kbd_height" + mContentView.getWidth() + "_" + mContentView.getHeight(), Utils.convertDpToPx(200.0f));
            }
            mEmojiPopup.setHeight(View.MeasureSpec.makeMeasureSpec(mKeyboardHeight, View.MeasureSpec.EXACTLY));
            mEmojiPopup.setWidth(View.MeasureSpec.makeMeasureSpec(mContentView.getWidth(), View.MeasureSpec.EXACTLY));
            mEmojiPopup.showAtLocation(getParentActivity().getWindow().getDecorView(), Gravity.BOTTOM | Gravity.LEFT, 0, 0);
            if (!mKeyboardVisible) {
                mContentView.setPadding(0, 0, 0, mKeyboardHeight);
                mEmojiImageView.setImageResource(R.drawable.ic_arrow_down);
                return;
            }
            mEmojiImageView.setImageResource(R.drawable.ic_msg_panel_kb);
            return;
        }
        mEmojiImageView.setImageResource(R.drawable.ic_smiles);
        mEmojiPopup.dismiss();
        mContentView.post(new Runnable() {
            @Override
            public void run() {
                mContentView.setPadding(0, 0, 0, 0);
            }
        });
    }

    public void hideEmojiPopup() {
        if (mEmojiPopup != null && mEmojiPopup.isShowing()) {
            showEmojiPopup(false);
        }
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        Rect localRect = new Rect();
        getParentActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(localRect);
        int i = localRect.top;
        int j = getResources().getDisplayMetrics().heightPixels - i;
        int m = j - h;
        if (m > 0) {
            mKeyboardHeight = m;
        }
        onKeyboardStateChanged(m > 0);
    }

    private void onKeyboardStateChanged(boolean visible) {
        mKeyboardVisible = visible;

        if (mKeyboardHeight > 0) {
            getParentActivity().getSharedPreferences("emoji", 0).edit().putInt("kbd_height" + mContentView.getWidth() + "_" + mContentView.getHeight(), mKeyboardHeight).commit();
        }
        if (visible && mContentView.getPaddingBottom() > 0) {
            showEmojiPopup(false);
        }
        if (!visible && mEmojiPopup != null && mEmojiPopup.isShowing()) {
            showEmojiPopup(false);
        }
    }

    private void updateOnlineCount() {
        if (mChatParticipants == null) {
            return;
        }
        /*if (mCurrentChannel == null) {
            return;
        }*/

        mOnlineCount = 0;
        for (TdApi.ChatParticipant chatParticipant : mChatParticipants) {
            TdApi.User user = ChatsCache.getUser(chatParticipant.user.id);
            if (Utils.formatUserStatus(user).equals("online")) {
                ++mOnlineCount;
            }
        }

        updateToolbar();
    }

    private void loadMessages(final long chatId, int fromId, int offset, final int limit) {
        Utils.sClient.send(new TdApi.GetChatHistory(chatId, fromId, offset, limit), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {
                if (object.getConstructor() == TdApi.Messages.CONSTRUCTOR) {
                    final TdApi.Messages messages = (TdApi.Messages) object;
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            int newMessagesCount = 0;
                            for (TdApi.Message message : messages.messages) {
                                if (message.id > 0) {
                                    mMaxMessageId = Math.min(message.id, mMaxMessageId);
                                }
                                mAssignMessages.put(message.id, message);
                                ArrayList<TdApi.Message> messagesByDay = mMessagesByDays.get(Utils.formatMessageDate(message.date));
                                if (messagesByDay == null) {
                                    messagesByDay = new ArrayList<>();
                                    mMessagesByDays.put(Utils.formatMessageDate(message.date), messagesByDay);

                                    TdApi.Message dateMessage = new TdApi.Message();
                                    dateMessage.content = new TdApi.MessageText(Utils.getFormatDateChat(message.date), null);
                                    dateMessage.id = 0;
                                    mMessages.add(dateMessage);
                                    ++newMessagesCount;
                                }
                                ++newMessagesCount;
                                messagesByDay.add(message);
                                mMessages.add(mMessages.size() - 1, message);
                            }

                            if (messages.messages.length == 0) {
                                mChatEndReached = true;
                            }

                            mChatLoading = false;
                            if (mChatListView != null) {
                                if (mIsFirst || mScrollToTop) {
                                    mChatAdapter.notifyDataSetChanged();
                                    mChatListView.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            mLayoutManager.scrollToPositionWithOffset(mMessages.size() - 1, -10000 - mChatListView.getPaddingTop());
                                        }
                                    });

                                } else {
                                    int lastVisibleItem = mLayoutManager.findLastVisibleItemPosition();
                                    View lastVisibleView = mChatListView.getChildAt(mChatListView.getChildCount() - 1);
                                    int top = (lastVisibleView == null ? 0 : lastVisibleView.getTop()) - mChatListView.getPaddingTop();
                                    mChatAdapter.notifyDataSetChanged();
                                    mLayoutManager.scrollToPositionWithOffset(lastVisibleItem + newMessagesCount - (mChatEndReached ? 1 : 0), top);
                                }

                                if (mPaused) {
                                    mScrollToTop = true;
                                }

                                if (mIsFirst) {
                                    if (mChatListView.getEmptyView() == null) {
                                        mChatListView.setEmptyView(mListEmptyTextView);
                                    }
                                }
                            } else {
                                mScrollToTop = true;
                            }

                            if (mIsFirst && mMessages.size() > 0) {
//                                TdApi.Chat chat = ChatsCache.sChats.get(chatId);
//                                if (chat != null) {
//                                    chat.unreadCount = 0;
//                                    ChatsCache.updateChatsList();
//                                }
                                ChatsCache.viewMessages(mChatId, mMessages);
                                mIsFirst = false;
                            }

                            if (mLoadingMessagesProgressBar != null) {
                                mLoadingMessagesProgressBar.setVisibility(View.GONE);
                            }
                        }
                    }, 800L);
                }
            }
        });
    }

    @Override
    public void updateInterface() {
        updateAvatar();
        updateToolbar();
        updateOnlineCount();
    }

    @Override
    public void updateMessageId(long chatId, int oldId, int newId) {
        if (chatId == mChatId) {
            TdApi.Message message = mAssignMessages.get(oldId);
            if (message != null) {
                mAssignMessages.remove(oldId);
                mAssignMessages.put(newId, message);
                message.id = newId;
                message.sendState = new TdApi.MessageIsSuccessfullySent();
                updateVisibleItems();
            }
        }
    }

    @Override
    public void updateMessageViews(long chatId, int messageId, int views) {
        if (chatId == mChatId) {
            TdApi.Message message = mAssignMessages.get(messageId);
            if (message != null) {
                message.views = views;
                updateVisibleItems();
            }
        }
    }

    @Override
    public void updateMessageDate(long chatId, int messageId, int newDate) {
        if (chatId == mChatId) {
            TdApi.Message message = mAssignMessages.get(messageId);
            if (message != null) {
                message.date = newDate;
                if (newDate == 0) {
                    message.sendState = new TdApi.MessageIsFailedToSend();
                }
                updateVisibleItems();
            }
        }
    }

    @Override
    public void updateMessageContent(long chatId, int messageId, TdApi.MessageContent newContent) {
        if (chatId == mChatId) {
            TdApi.Message message = mAssignMessages.get(messageId);
            if (message != null) {
                if (newContent.getConstructor() == TdApi.MessageWebPage.CONSTRUCTOR) {
                    message.content = newContent;
                }
                updateVisibleItems();
            }
        }
    }

    @Override
    public void updateNotifySettings() {
        updateToolbar();
    }

    @Override
    public void updateListsWithEmoji() {
        if (mChatListView != null) {
            updateVisibleItems();
            //mChatListView.invalidateViews();
        }
        if (mEmojiView != null) {
            mEmojiView.invalidateViews();
        }
    }

    @Override
    public void updateChatParts(int groupId, TdApi.GroupFull groupFull) {
        if (mCurrentGroup != null && groupId == mCurrentGroup.id) {
            mGroupFull = groupFull;
            mChatParticipants = groupFull.participants;
            updateOnlineCount();
        }
    }

    @Override
    public void updateUserInfo(int userId, boolean blocked, TdApi.BotInfo botInfo) {
        if (mUserFull == null) {
            mUserFull = ChatsCache.sUsersFull.get(userId);
            return;
        }
        if (mCurrentUser != null && userId == mCurrentUser.id) {
            mUserFull.isBlocked = blocked;
            //mBlocked = blocked;
            if (botInfo != null) {
                mUserFull.botInfo = botInfo;
                //mBotInfo = botInfo;
            }
            updateToolbar();
            updateSubHeader();
        }
    }

    @Override
    public void updateChannelInfo(final int channelId, TdApi.ChannelFull channelFull) {
        if (channelFull != null && channelId == channelFull.channel.id) {
            mChannelFull = channelFull;
            if (mChannelFull.canGetParticipants) {
                final ArrayList<TdApi.ChatParticipant> channelParticipants = new ArrayList<>();
                TdApi.GetChannelParticipants getChannelParticipants = new TdApi.GetChannelParticipants();
                getChannelParticipants.channelId = channelId;
                getChannelParticipants.filter = new TdApi.ChannelParticipantsRecent();
                getChannelParticipants.offset = 0;
                getChannelParticipants.limit = 200;
                Utils.sClient.send(getChannelParticipants, new Client.ResultHandler() {
                    @Override
                    public void onResult(TdApi.TLObject object) {
                        if (object.getConstructor() == TdApi.ChatParticipants.CONSTRUCTOR) {
                            TdApi.ChatParticipants chatParticipants = (TdApi.ChatParticipants) object;
                            Collections.addAll(channelParticipants, chatParticipants.participants);
                            Utils.runOnUIThread(new Runnable() {
                                @Override
                                public void run() {
                                    mChatParticipants = new TdApi.ChatParticipant[channelParticipants.size()];
                                    channelParticipants.toArray(mChatParticipants);
                                    updateOnlineCount();
                                }
                            });
                        }
                    }
                });
            } else {
                updateSubHeader();
            }
        }
    }

    @Override
    public void updateReadMessages() {
        updateVisibleItems();
    }

    @Override
    public void removeDeletedMessages(ArrayList<Integer> deletedMessages) {
        boolean updated = false;
        for (Integer messageId : deletedMessages) {
            TdApi.Message message = mAssignMessages.get(messageId);
            if (message != null) {
                int index = mMessages.indexOf(message);
                if (index != -1) {
                    mMessages.remove(index);
                    mAssignMessages.remove(messageId);
                    ArrayList<TdApi.Message> messagesByDay = mMessagesByDays.get(Utils.formatMessageDate(message.date));
                    messagesByDay.remove(message);
                    if (messagesByDay.isEmpty()) {
                        mMessagesByDays.remove(Utils.formatMessageDate(message.date));
                        mMessages.remove(index);
                    }
                    updated = true;
                }
            }
        }
        if (mMessages.isEmpty() && !mChatEndReached && !mChatLoading) {
            mLoadingMessagesProgressBar.setVisibility(View.VISIBLE);
            mChatListView.setEmptyView(null);
            mMaxMessageId = Integer.MAX_VALUE;
            loadMessages(mChatId, mMaxMessageId, 0, 30);
            mChatLoading = true;
        }
        if (updated && mChatAdapter != null) {
            mChatAdapter.notifyDataSetChanged();
        }
    }

    private void removeAllMessages() {
        mMessages.clear();
        mMessagesByDays.clear();
        mAssignMessages.clear();
        mChatListView.setEmptyView(mListEmptyTextView);
        mMaxMessageId = Integer.MAX_VALUE;
        mChatAdapter.notifyDataSetChanged();
    }

    @Override
    public void addNewMessages(long chatId, ArrayList<TdApi.Message> messages) {
        if (chatId == mChatId) {
            boolean markAsRead = false;
            int oldCount = mMessages.size();
            for (TdApi.Message message : messages) {
                if (mAssignMessages.containsKey(message.id)) {
                    continue;
                }
                if (message.id > 0) {
                    mMaxMessageId = Math.min(message.id, mMaxMessageId);
                }
                mAssignMessages.put(message.id, message);
                ArrayList<TdApi.Message> messagesByDay = mMessagesByDays.get(Utils.formatMessageDate(message.date));
                if (messagesByDay == null) {
                    messagesByDay = new ArrayList<>();
                    mMessagesByDays.put(Utils.formatMessageDate(message.date), messagesByDay);

                    TdApi.Message dateMessage = new TdApi.Message();
                    dateMessage.content = new TdApi.MessageText(Utils.getFormatDateChat(message.date), null);
                    dateMessage.id = 0;
                    mMessages.add(0, dateMessage);
                }
                if (message.fromId != Utils.sCurrentUserId) {
                    markAsRead = true;
                }
                messagesByDay.add(0, message);
                mMessages.add(0, message);
            }

            if (markAsRead) {
                if (mPaused) {
                    mReadWhenResume = true;
                } else {
                    //ChatsCache.markChatAsRead(chatId, mMessages.get(0).id + 1, 0, 1);
                    ChatsCache.viewMessages(chatId, messages);
                }
            }
            if (mLoadingMessagesProgressBar != null) {
                mLoadingMessagesProgressBar.setVisibility(View.GONE);
            }

            if (mChatAdapter != null) {
                mChatAdapter.notifyDataSetChanged();
                if (mChatListView != null) {
                    if (mLayoutManager.findLastVisibleItemPosition() >= oldCount - 5) {
                        mChatListView.post(new Runnable() {
                            @Override
                            public void run() {
                                mLayoutManager.scrollToPositionWithOffset(mMessages.size() - 1, -10000 - mChatListView.getPaddingTop());
                            }
                        });
                    }
                } else {
                    mScrollToTop = true;
                }
            } else {
                mScrollToTop = true;
            }

            if (mPaused) {
                mScrollToTop = true;
            }
        }
    }

    @Override
    public void updateDownloadProgress(int fileId, int ready, int size) {
        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(fileId);
        if (state == null) {
            return;
        }
        state.put(size, ready);

        ArrayList<ProgressView> progressViews = mStatesProgress.get(fileId);
        if (progressViews == null) {
            return;
        }
        for (ProgressView view : progressViews) {
            view.setProgress((float)ready / (float)size);
        }
        ArrayList<TextView> textViews = mStringStatesProgress.get(fileId);
        if (textViews == null) {
            return;
        }
        for (TextView view : textViews) {
            view.setText(String.format(Utils.sContext.getString(R.string.downloading_file), Utils.formatFileSize(ready), Utils.formatFileSize(size)));
        }
    }

    @Override
    public void updateDownloadedFile(int fileId) {
        if (mStatesProgress.containsKey(fileId)) {
            mStatesProgress.remove(fileId);
        }
        /*if (mChatAdapter != null) {
            mChatAdapter.notifyDataSetChanged();
        }*/
        updateVisibleItems();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mIsFirst && mChatAdapter != null) {
            mChatAdapter.notifyDataSetChanged();
        }
        ChatsCache.sOpenChatId = mChatId;
        if (mScrollToTop) {
            if (mLayoutManager != null) {
                mLayoutManager.scrollToPosition(mMessages.size() + 1);
            }
            mScrollToTop = false;
        }
        if (mEmojiView != null) {
            mEmojiView.loadRecents();
        }
        mPaused = false;
        if (mReadWhenResume && !mMessages.isEmpty()) {
            mReadWhenResume = false;
            //ChatsCache.markChatAsRead(mChatId, mMessages.get(0).id + 1, 0, 1);
            ChatsCache.viewMessages(mChatId, mMessages);
        }

        updateToolbar();
        updateAvatar();
    }

    @Override
    public void onPause() {
        super.onPause();
        ChatsCache.sOpenChatId = 0;
        mPaused = true;
        hideEmojiPopup();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mKeyboardHeight = -1;
        if (mEmojiPopup != null && mEmojiPopup.isShowing()) {
            showEmojiPopup(false);
        }
    }

    @Override
    public boolean onBackPressed() {
        if (mEmojiPopup != null && mEmojiPopup.isShowing()) {
            hideEmojiPopup();
            return false;
        }
        if (mMessages.isEmpty() && !ChatsCache.sChatsList.contains(mCurrentChat)) {
            ChatsCache.sChats.remove(mChatId);
        }
        mExitAnimated = true;
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Utils.sClient.send(new TdApi.CloseChat(mChatId), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
        ChatsCache.removeDelegate(this);
        Utils.cancelRunOnUIThread(mUpdateSubHeaderRunnable);
        if (mAudioTrack != null) {
            mAudioTrack.pause();
            mAudioTrack.release();
            mAudioTrack = null;
            ChatsCache.sPlayingFileId = 0;
        }
    }

    public void updateAvatar() {
        if (mCurrentChat == null) {
            return;
        }
        mCurrentChat = ChatsCache.getChat(mCurrentChat.id);
        TextDrawable textDrawable = Utils.getChatAvatar(mCurrentChat, 40);
        if (mAvatarImageView != null) {
            mAvatarImageView.setChatPhoto(mCurrentChat.photo, textDrawable);
        }
        /*if (mCurrentUser != null) {
            mCurrentUser = ChatsCache.getUser(mCurrentUser.id);
            textDrawable = Utils.getUserAvatar(mCurrentUser, 40);
            if (mAvatarImageView != null) {
                mAvatarImageView.setProfilePhoto(mCurrentUser.profilePhoto, textDrawable);
            }
        } else if (mCurrentGroup != null) {
            mCurrentGroup = ChatsCache.getGroup(mCurrentGroup.id);
            textDrawable = Utils.getChatAvatar(mCurrentChat, 40);
            if (mAvatarImageView != null) {
                mAvatarImageView.setChatPhoto(mCurrentChat.photo, textDrawable);
            }
        } else if (mCurrentChannel != null) {
            mCurrentChannel = ChatsCache.getChannel(mCurrentChannel.id);
            textDrawable = Utils.getChatAvatar(mCurrentChat, 40);
            if (mAvatarImageView != null) {
                mAvatarImageView.setChatPhoto(mCurrentChat.photo, textDrawable);
            }
        }*/
    }

    private void updateToolbar() {
        if (mToolbar == null) {
            return;
        }

        if (mCurrentChat != null) {
            if (!ChatsCache.isChatMuted(mCurrentChat.id)) {
                mMuteImageView.setVisibility(View.GONE);
                mMuteItemTextView.setText(Utils.sContext.getString(R.string.action_mute));
                if (mUserFull != null) {
                    if (mUserFull.isBlocked) {
                        mMuteItemTextView.setVisibility(View.GONE);
                    } else {
                        mMuteItemTextView.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                mMuteImageView.setVisibility(View.VISIBLE);
                mMuteItemTextView.setText(Utils.sContext.getString(R.string.action_unmute));
                if (mUserFull != null) {
                    if (mUserFull.isBlocked) {
                        mMuteItemTextView.setVisibility(View.GONE);
                    } else {
                        mMuteItemTextView.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

        if (mCurrentGroup != null) {
            mCurrentGroup = ChatsCache.getGroup(mCurrentGroup.id);
            if (mCurrentGroup != null) {
                mHeaderTextView.setText(mCurrentChat.title);
            }
        } else if (mCurrentUser != null) {
            mCurrentUser = ChatsCache.getUser(mCurrentUser.id);
            if (mCurrentUser != null) {
                if (mCurrentUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    mHeaderTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                } else {
                    mHeaderTextView.setText(Utils.formatName(mCurrentUser.firstName, mCurrentUser.lastName));
                }
            }
        } else if (mCurrentChannel != null) {
            mCurrentChannel = ChatsCache.getChannel(mCurrentChannel.id);
            if (mCurrentChannel != null) {
                mHeaderTextView.setText(mCurrentChat.title);
            }
        }

        updateSubHeader();
    }

    public void updateSubHeader() {
        Utils.cancelRunOnUIThread(mUpdateSubHeaderRunnable);
        if (mCurrentGroup != null) {
            if (mCurrentGroup.role.getConstructor() == TdApi.ChatParticipantRoleLeft.CONSTRUCTOR && mBottomTextView != null) {
                TdApi.Message lastMessage = mCurrentChat.topMessage;
                if (lastMessage.fromId == Utils.sCurrentUserId) {
                    mSubHeaderTextView.setText(Utils.sContext.getString(R.string.you_left_status));
                } else {
                    mSubHeaderTextView.setText(Utils.sContext.getString(R.string.you_removed_status));
                }
                mMoreMenuButton.setVisibility(View.GONE);
                mBottomActionPanel.setVisibility(View.VISIBLE);
                mBottomTextView.setText(Utils.sContext.getString(R.string.delete_group_button));
            } else {
                String actionUsers = ChatsCache.sActionStrings.get(mChatId);
                if (actionUsers == null || actionUsers.length() == 0) {
                    String countStatus = Utils.formatMembersCount(mCurrentGroup.participantsCount);
                    if (mBottomActionPanel != null) {
                        if (mOnlineCount > 0) {
                            mSubHeaderTextView.setText(String.format("%s, %d %s", countStatus, mOnlineCount, Utils.sContext.getString(R.string.online_status)));
                        } else {
                            mSubHeaderTextView.setText(countStatus);
                        }
                        mBottomActionPanel.setVisibility(View.GONE);
                    }
                } else {
                    if (mBottomActionPanel != null) {
                        mSubHeaderTextView.setText(actionUsers);
                        mBottomActionPanel.setVisibility(View.GONE);
                    }
                }
                Utils.runOnUIThread(mUpdateSubHeaderRunnable, 5000L);
            }
        } else if (mCurrentChannel != null && mChannelFull != null) {
            String countStatus = Utils.formatMembersCount(mChannelFull.participantsCount);
            String actionUsers = ChatsCache.sActionStrings.get(mChatId);
            if (mBottomActionPanel != null) {
                if (actionUsers == null || actionUsers.length() == 0) {
                    if (mOnlineCount > 0) {
                        mSubHeaderTextView.setText(String.format("%s, %d %s", countStatus, mOnlineCount, Utils.sContext.getString(R.string.online_status)));
                    } else {
                        mSubHeaderTextView.setText(countStatus);
                    }
                } else {
                    mSubHeaderTextView.setText(actionUsers);
                }
            }
            Utils.runOnUIThread(mUpdateSubHeaderRunnable, 5000L);
        } else if (mCurrentUser != null) {
            String newStatus = Utils.formatUserStatus(mCurrentUser);
            if (mCurrentUser.id == 333000 || mCurrentUser.id == 777000) {
                newStatus = Utils.sContext.getString(R.string.service_notifications_status);
            }
            if (mBottomTextView != null) {
                String actionUsers = ChatsCache.sActionStrings.get(mChatId);
                if (actionUsers == null || actionUsers.length() == 0) {
                    mSubHeaderTextView.setText(newStatus);
                } else {
                    mSubHeaderTextView.setText(actionUsers);
                }
                if (mCurrentUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                    mBottomActionPanel.setVisibility(View.VISIBLE);
                    mBottomTextView.setText(Utils.sContext.getString(R.string.delete_chat_button));
                    mMoreMenuButton.setVisibility(View.GONE);
                    return;
                }
                if (mUserFull != null && mUserFull.isBlocked) {
                    mBottomActionPanel.setTag(UNBLOCK_MENU_ITEM);
                    mBottomActionPanel.setVisibility(View.VISIBLE);
                    mBottomTextView.setText(Utils.sContext.getString(R.string.action_unblock));
                } else {
                    mBottomActionPanel.setVisibility(View.GONE);
                }
            }
            Utils.runOnUIThread(mUpdateSubHeaderRunnable, 5000L);
        }
    }

    public void updateVisibleItems() {
        if (mChatListView == null || mChatAdapter == null) {
            return;
        }
        int count = mChatListView.getChildCount();
        //int firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
        for (int i = 0; i < count; i++) {
            //mChatAdapter.notifyItemChanged(firstVisibleItem + i);
            View view = mChatListView.getChildAt(i);
            RecyclerView.ViewHolder viewHolder = mChatListView.getChildViewHolder(view);
            if (viewHolder != null) {
                mChatAdapter.bindViewHolder((ChatAdapter.ViewHolder) viewHolder, viewHolder.getLayoutPosition());
            }
        }
    }

    private class UserClickableSpan extends ClickableSpan {

        private int mUserId;

        public UserClickableSpan(int userId) {
            mUserId = userId;
        }

        @Override
        public void onClick(View widget) {
            ProfileFragment userProfileFragment = new ProfileFragment();
            Bundle args = new Bundle();
            args.putInt("user_id", mUserId);
            args.putLong("chat_id", mCurrentChat.id);
            args.putInt("group_chat_id", 1);
            userProfileFragment.setArguments(args);
            ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + mUserId, true, false, true, false);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setFakeBoldText(true);
            ds.setColor(0xff569ace);
        }
    }

    private class LeftLeadingMarginSpan2 implements LeadingMarginSpan.LeadingMarginSpan2 {
        private int margin;
        private int lines;

        LeftLeadingMarginSpan2(int lines, int margin) {
            this.margin = margin;
            this.lines = lines;
        }

        @Override
        public int getLeadingMargin(boolean first) {
            if (first) {
                return margin;
            }
            return 0;
        }

        @Override
        public void drawLeadingMargin(Canvas c, Paint p, int x, int dir, int top, int baseline, int bottom, CharSequence text, int start, int end, boolean first, Layout layout) {

        }

        @Override
        public int getLeadingMarginLineCount() {
            return lines;
        }
    }

    private static class WebPDecoder implements ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> {

        private static final String ID = "org.tdlib.ippogram.ui.ChatFragment$WebPDecoder";
        private static final String TAG = WebPDecoder.class.getSimpleName();

        private final Downsampler mDownsampler;
        private BitmapPool mBitmapPool;
        private DecodeFormat mDecodeFormat;
        private String mId;

        @SuppressWarnings("unused")
        public WebPDecoder(Context context) {
            this(Glide.get(context).getBitmapPool());
        }

        public WebPDecoder(BitmapPool bitmapPool) {
            this(bitmapPool, DecodeFormat.DEFAULT);
        }

        public WebPDecoder(Context context, DecodeFormat decodeFormat) {
            this(Glide.get(context).getBitmapPool(), decodeFormat);
        }

        public WebPDecoder(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            this(Downsampler.AT_LEAST, bitmapPool, decodeFormat);
        }

        public WebPDecoder(Downsampler downsampler, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            mDownsampler = downsampler;
            mBitmapPool = bitmapPool;
            mDecodeFormat = decodeFormat;
        }

        @Override
        public Resource<GifBitmapWrapper> decode(ImageVideoWrapper source, int width, int height) throws IOException {
            InputStream inputStream = source.getStream();
            if (!inputStream.markSupported()) {
                inputStream = new BufferedInputStream(inputStream);
            }
            Bitmap bitmap = Utils.decodeSticker(Utils.streamToByteArray(inputStream));
            BitmapResource bitmapResource = BitmapResource.obtain(bitmap, mBitmapPool);
            return new GifBitmapWrapperResource(new GifBitmapWrapper(bitmapResource, null));
        }

        @Override
        public String getId() {
            if (mId == null) {
                mId = ID + mDownsampler.getId() + mDecodeFormat.name();
            }
            return mId;
        }
    }

    private class ChatAdapter extends EmptyRecyclerView.Adapter<ChatAdapter.ViewHolder> {

        private LayoutInflater mInflater;

        /*private OnItemClickListener mListener;

        interface OnItemClickListener {
            void onClick(View view, int position);
        }*/

        public ChatAdapter(/*OnItemClickListener listener*/Context context) {
            mInflater = LayoutInflater.from(context);
            //mListener = listener;
        }

        @Override
        public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 21) {
                return new ViewHolder(mInflater.inflate(R.layout.loading_more, parent, false));
            } else if (viewType == 2 || viewType == 3 || viewType == 4 || viewType == 5 || viewType == 6 || viewType == 10 || viewType == 16 || viewType == 20 || viewType == 22) {
                return new ViewHolder(mInflater.inflate(R.layout.message_action, parent, false));
            } else {
                View itemRootView = mInflater.inflate(R.layout.chat_list_item, parent, false);
                LinearLayout contentContainer = (LinearLayout) itemRootView.findViewById(R.id.contentContainer);
                if (viewType == 0 || viewType == 18) {
                    View webPageMessage = mInflater.inflate(R.layout.web_page_content_message, parent, false);
                    contentContainer.addView(webPageMessage);
                    /*View messageText = mInflater.inflate(R.layout.text_content_message, parent, false);
                    contentContainer.addView(messageText);*/
                } else if (viewType == 1) {
                    View audioMessage = mInflater.inflate(R.layout.audio_content_message, parent, false);
                    contentContainer.addView(audioMessage);
                } else if (viewType == 7) {
                    View contactMessage = mInflater.inflate(R.layout.contact_content_message, parent, false);
                    contentContainer.addView(contactMessage);
                } else if (viewType == 8) {
                    View documentMessage = mInflater.inflate(R.layout.document_content_message, parent, false);
                    contentContainer.addView(documentMessage);
                } else if (viewType == 9) {
                    View locationMessage = mInflater.inflate(R.layout.location_content_message, parent, false);
                    contentContainer.addView(locationMessage);
                } else if (viewType == 11) {
                    View photoMessage = mInflater.inflate(R.layout.photo_content_message, parent, false);
                    contentContainer.addView(photoMessage);
                } else if (viewType == 12) {
                    View stickerMessage = mInflater.inflate(R.layout.sticker_content_message, parent, false);
                    contentContainer.addView(stickerMessage);
                } else if (viewType == 13) {
                    View videoMessage = mInflater.inflate(R.layout.video_content_message, parent, false);
                    contentContainer.addView(videoMessage);
                } else if (viewType == 17) {
                    View venueMessage = mInflater.inflate(R.layout.venue_content_message, parent, false);
                    contentContainer.addView(venueMessage);
                } /*else if (viewType == 18) {
                    View webPageMessage = mInflater.inflate(R.layout.web_page_content_message, parent, false);
                    contentContainer.addView(webPageMessage);
                }*/ else if (viewType == 19) {
                    View voiceMessage = mInflater.inflate(R.layout.voice_content_message, parent, false);
                    contentContainer.addView(voiceMessage);
                }
                return new ViewHolder(itemRootView);
            }
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            int off = 1;
            if (!mChatEndReached && mMessages.size() != 0) {
                off = 0;
                if (position == 0) {
                    return;
                }
            }
            final TdApi.Message message = mMessages.get(mMessages.size() - position - off);
            TdApi.Chat chat = ChatsCache.sChats.get(message.chatId);
            final TdApi.User fromUser = holder.user = ChatsCache.getUser(message.fromId);
            TdApi.MessageContent messageContent = message.content;

            CharSequence messageStr;

            String fromUserFullName = "";
            UserClickableSpan fromUserClickableSpan = null;
            if (fromUser != null) {
                fromUserFullName = Utils.formatName(fromUser.firstName, fromUser.lastName);
                fromUserClickableSpan = new UserClickableSpan(fromUser.id);
            }

            int type = getItemViewType(position);
            if (type == 2) {
                TdApi.MessageChatAddParticipants addedParticipants = (TdApi.MessageChatAddParticipants) messageContent;
                TdApi.User[] addedUsers = addedParticipants.participants;
                if (addedUsers.length == 1) {
                    TdApi.User addedUser = addedUsers[0];

                    UserClickableSpan addedUserClickableSpan = new UserClickableSpan(addedUser.id);
                    String addedUserFullName = Utils.formatName(addedUser.firstName, addedUser.lastName);
                    if (fromUser.id == Utils.sCurrentUserId && fromUser.id == addedUser.id) {
                        messageStr = String.format(Utils.sContext.getString(R.string.message_chat_return_part), Utils.sContext.getString(R.string.message_you));
                    } else if (fromUser.id == Utils.sCurrentUserId) {
                        messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_add_part), Utils.sContext.getString(R.string.message_you), addedUserFullName));
                        ((SpannableString) messageStr).setSpan(addedUserClickableSpan, messageStr.length() - addedUserFullName.length(), messageStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (fromUser.id == addedUser.id) {
                        messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_return_part), addedUserFullName));
                        ((SpannableString) messageStr).setSpan(addedUserClickableSpan, 0, addedUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (addedUser.id == Utils.sCurrentUserId) {
                        messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_add_part), fromUserFullName, Utils.sContext.getString(R.string.message_you).toLowerCase()));
                        ((SpannableString) messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else {
                        messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_add_part), fromUserFullName, addedUserFullName));
                        ((SpannableString) messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ((SpannableString) messageStr).setSpan(addedUserClickableSpan, messageStr.length() - addedUserFullName.length(), messageStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                    holder.avatarImageView.setVisibility(View.GONE);
                    holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
                }
            } else if (type == 3) {
                TdApi.MessageChatChangePhoto changePhoto = (TdApi.MessageChatChangePhoto) messageContent;
                TdApi.Photo photo = changePhoto.photo;
                if (message.isPost) {
                    messageStr = Utils.sContext.getString(R.string.message_channel_change_photo);
                } else if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = String.format(Utils.sContext.getString(R.string.message_chat_change_photo), Utils.sContext.getString(R.string.message_you));
                } else {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_change_photo), fromUserFullName));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }

                TdApi.PhotoSize profilePhoto = Utils.getRightPhotoSize(Utils.generateThumbs(photo), Utils.getPhotoSize());
                //holder.photoImageView.setStickerThumb(thumbPhoto, sticker);

                if (!ChatsCache.sFiles.containsKey(profilePhoto.photo.id)) {
                    ChatsCache.sFiles.put(profilePhoto.photo.id, profilePhoto.photo);
                }

                holder.file = ChatsCache.getFile(profilePhoto.photo.id);
                if (holder.file == null) {
                    return;
                }

                if (holder.file.path == null || holder.file.path.length() == 0) {
                    Utils.sClient.send(new TdApi.DownloadFile(holder.file.id), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {

                        }
                    });
                } else {
                    holder.avatarImageView.setImage(holder.file.path, true, false, false, true);
                    holder.avatarImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                            Bundle args = new Bundle();
                            args.putLong("chat_id", mChatId);
                            args.putString("path", holder.file.path);
                            args.putInt("message_id", message.id);
                            photoViewFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                        }
                    });
                    //Glide.with(getParentActivity()).load(holder.file.path).decoder(new WebPDecoder(getParentActivity(), DecodeFormat.PREFER_ARGB_8888)).override(holder.photoImageView.getPhotoWidth(), holder.photoImageView.getPhotoHeight()).into(holder.photoImageView);
                }

                holder.avatarImageView.setVisibility(View.VISIBLE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 4) {
                TdApi.MessageChatChangeTitle changedTitle = (TdApi.MessageChatChangeTitle) messageContent;
                if (message.isPost) {
                    messageStr = String.format(Utils.sContext.getString(R.string.message_channel_change_name), "<b>" + changedTitle.title + "</b>");
                } else if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = Html.fromHtml(String.format(Utils.sContext.getString(R.string.message_chat_change_title),
                            Utils.sContext.getString(R.string.message_you), "<b>" + changedTitle.title + "</b>"));
                } else {
                    messageStr = new SpannableString(Html.fromHtml(String.format(Utils.sContext.getString(R.string.message_chat_change_title), fromUserFullName, "<b>" + changedTitle.title + "</b>")));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 5) {
                TdApi.MessageChatDeleteParticipant deleteParticipant = (TdApi.MessageChatDeleteParticipant) messageContent;
                TdApi.User deletedUser = deleteParticipant.user;

                UserClickableSpan deletedUserClickableSpan = new UserClickableSpan(deletedUser.id);
                String deletedUserFullName = Utils.formatName(deletedUser.firstName, deletedUser.lastName);
                if (fromUser.id == Utils.sCurrentUserId && fromUser.id == deletedUser.id) {
                    messageStr = String.format(Utils.sContext.getString(R.string.message_chat_left_part), Utils.sContext.getString(R.string.message_you));
                } else if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_del_part), Utils.sContext.getString(R.string.message_you), deletedUserFullName));
                    ((SpannableString)messageStr).setSpan(deletedUserClickableSpan, messageStr.length() - deletedUserFullName.length(), messageStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else if (fromUser.id == deletedUser.id) {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_left_part), deletedUserFullName));
                    ((SpannableString)messageStr).setSpan(deletedUserClickableSpan, 0, deletedUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else if (deletedUser.id == Utils.sCurrentUserId) {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_del_part), fromUserFullName, Utils.sContext.getString(R.string.message_you).toLowerCase()));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } else {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_del_part), fromUserFullName, deletedUserFullName));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    ((SpannableString)messageStr).setSpan(deletedUserClickableSpan, messageStr.length() - deletedUserFullName.length(), messageStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 6) {
                if (message.isPost) {
                    messageStr = Utils.sContext.getString(R.string.message_channel_delete_photo);
                } else if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = String.format(Utils.sContext.getString(R.string.message_chat_delete_photo), Utils.sContext.getString(R.string.message_you));
                } else {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_delete_photo), fromUserFullName));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 10) {
                TdApi.MessageGroupChatCreate groupChatCreate = (TdApi.MessageGroupChatCreate) messageContent;
                if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = Html.fromHtml(String.format(Utils.sContext.getString(R.string.message_group_chat_create), Utils.sContext.getString(R.string.message_you), "<b>" + groupChatCreate.title + "</b>"));
                } else {
                    messageStr = new SpannableString(Html.fromHtml(String.format(Utils.sContext.getString(R.string.message_group_chat_create), fromUserFullName, "<b>" + groupChatCreate.title + "</b>")));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 16) {
                if (fromUser.id == Utils.sCurrentUserId) {
                    messageStr = String.format(Utils.sContext.getString(R.string.message_chat_join_by_link), Utils.sContext.getString(R.string.message_you));
                } else {
                    messageStr = new SpannableString(String.format(Utils.sContext.getString(R.string.message_chat_join_by_link), fromUserFullName));
                    ((SpannableString)messageStr).setSpan(fromUserClickableSpan, 0, fromUserFullName.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView));
            } else if (type == 20) {
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setText(Utils.sContext.getString(R.string.message_channel_chat_create));
            } else if (type == 22) {
                holder.avatarImageView.setVisibility(View.GONE);
                holder.messageTextView.setTypeface(Utils.getTypeface("bold"));
                holder.messageTextView.setText(((TdApi.MessageText) message.content).text);
            } else if (type == 0 || type == 1 || type == 7 || type == 8 || type == 9 || type == 11 || type == 12 || type == 13 || type == 17 || type == 18 || type == 19) {
                if (holder.timeTextView != null) {
                    holder.timeTextView.setText(Utils.sDay.format(message.date * 1000L));
                }

                if (holder.avatarImageView != null) {
                    if (message.isPost) {
                        TextDrawable placeholder = Utils.getChatAvatar(chat, 64);
                        holder.avatarImageView.setChatPhoto(chat.photo, placeholder);
                    } else if (fromUser != null) {
                        TextDrawable placeholder = Utils.getUserAvatar(fromUser, 64);
                        holder.avatarImageView.setProfilePhoto(fromUser.profilePhoto, placeholder);
                    }
                }

                if (holder.userNameTextView != null) {
                    if (message.isPost) {
                        holder.userNameTextView.setText(chat.title);
                    } else if (fromUser != null) {
                        if (fromUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                            holder.userNameTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                        } else {
                            holder.userNameTextView.setText(Utils.formatName(fromUser.firstName, fromUser.lastName));
                        }
                    }
                }

                if (holder.viewsTextView != null) {
                    if (message.isPost) {
                        holder.viewsTextView.setText(Utils.formatViewsCount(message.views));
                        holder.viewsTextView.setVisibility(View.VISIBLE);
                    } else {
                        holder.viewsTextView.setVisibility(View.GONE);
                    }
                }

                if (message.forwardInfo.getConstructor() == TdApi.MessageForwardedFromUser.CONSTRUCTOR) {
                    TdApi.MessageForwardedFromUser forwardedMessage = (TdApi.MessageForwardedFromUser)message.forwardInfo;
                    TdApi.User forwardFromUser = holder.forwardUser = ChatsCache.getUser(forwardedMessage.userId);
                    if (holder.forwardTimeTextView != null) {
                        holder.forwardTimeTextView.setText(Utils.getFormatDate(forwardedMessage.date));
                        holder.forwardTimeTextView.setVisibility(View.VISIBLE);
                    }

                    if (holder.forwardAvatarImageView != null && forwardFromUser != null) {
                        TextDrawable placeholder = Utils.getUserAvatar(forwardFromUser, 64);
                        holder.forwardAvatarImageView.setProfilePhoto(forwardFromUser.profilePhoto, placeholder);
                        holder.forwardAvatarImageView.setVisibility(View.VISIBLE);
                    }

                    if (holder.forwardUserNameTextView != null && forwardFromUser != null) {
                        if (forwardFromUser.type.getConstructor() == TdApi.UserTypeDeleted.CONSTRUCTOR) {
                            holder.forwardUserNameTextView.setText(Utils.sContext.getString(R.string.deleted_account));
                        } else {
                            holder.forwardUserNameTextView.setText(Utils.formatName(forwardFromUser.firstName, forwardFromUser.lastName));
                        }
                        holder.forwardUserNameTextView.setText(Utils.formatName(forwardFromUser.firstName, forwardFromUser.lastName));
                        holder.forwardUserNameTextView.setVisibility(View.VISIBLE);
                    }

                    holder.forwardSymbolImageView.setVisibility(View.VISIBLE);
                } else if (holder.forwardTimeTextView != null) {
                    holder.forwardTimeTextView.setVisibility(View.GONE);
                    holder.forwardAvatarImageView.setVisibility(View.GONE);
                    holder.forwardUserNameTextView.setVisibility(View.GONE);
                    holder.forwardSymbolImageView.setVisibility(View.GONE);
                }

                if (holder.signTextView != null) {
                    if (fromUser != null && message.isPost) {
                        holder.signTextView.setText(Utils.formatName(fromUser.firstName, fromUser.lastName));
                        holder.signTextView.setVisibility(View.VISIBLE);
                    } else {
                        holder.signTextView.setVisibility(View.GONE);
                    }
                }

                if (type == 0 || type == 18) {
                    TdApi.MessageEntity[] entities;
                    if (type == 0) {
                        TdApi.MessageText messageText = (TdApi.MessageText) messageContent;
                        messageStr = messageText.text;
                        entities = messageText.entities;
                        holder.siteTextView.setVisibility(View.GONE);
                        holder.lineImageView.setVisibility(View.GONE);
                        holder.thumbImageView.setVisibility(View.GONE);
                        holder.photoImageView.setVisibility(View.GONE);
                        holder.durationTextView.setVisibility(View.GONE);
                        holder.actionImageButton.setVisibility(View.GONE);
                        holder.downloadProgressView.setVisibility(View.GONE);
                        holder.siteContentLayout.setVisibility(View.GONE);
                    } else {
                        TdApi.MessageWebPage messageWebPage = (TdApi.MessageWebPage)messageContent;
                        messageStr = messageWebPage.text;
                        entities = messageWebPage.entities;
                        TdApi.WebPage webPage = messageWebPage.webPage;

                        holder.siteTextView.setVisibility(View.VISIBLE);
                        holder.lineImageView.setVisibility(View.VISIBLE);
                        holder.siteContentLayout.setVisibility(View.VISIBLE);

                        SpannableStringBuilder ss = Utils.formatWebPageText(webPage.siteName, webPage.title.length() != 0 ? webPage.title : webPage.author, webPage.description);

                        if (webPage.photo.id != 0) {
                            if (webPage.type.equals("article") || webPage.type.equals("app") || webPage.type.equals("document") || webPage.type.equals("profile")) {
                                LeftLeadingMarginSpan2 marginSpan2 = new LeftLeadingMarginSpan2(3, Utils.convertDpToPx(64));
                                ss.setSpan(marginSpan2, 0, ss.length(), 0);
                            }
                            TdApi.PhotoSize optimalPhoto = Utils.getRightPhotoSize(Utils.generateThumbs(webPage.photo), Utils.getPhotoSize());
                            TdApi.PhotoSize thumbPhoto = Utils.getRightPhotoSize(Utils.generateThumbs(webPage.photo), 80);
                            holder.photoImageView.setPhotoThumb(thumbPhoto, optimalPhoto);

                            if (!ChatsCache.sFiles.containsKey(optimalPhoto.photo.id)) {
                                ChatsCache.sFiles.put(optimalPhoto.photo.id, optimalPhoto.photo);
                            }

                            holder.file = ChatsCache.getFile(optimalPhoto.photo.id);
                            if (holder.file == null) {
                                return;
                            }

                            if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                                if (mStatesProgress.containsKey(holder.file.id)) {
                                    mStatesProgress.remove(holder.file.id);
                                }
                            } else {
                                HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                                for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                                    ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                                    if (progressViews == null) {
                                        progressViews = new ArrayList<>();
                                        mStatesProgress.put(holder.file.id, progressViews);
                                    }
                                    progressViews.add(holder.downloadProgressView);

                                    updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                                }
                            }
                            if (holder.file.path == null || holder.file.path.length() == 0) {
                                if (webPage.type.equals("article") || webPage.type.equals("app") || webPage.type.equals("document") || webPage.type.equals("profile")) {
                                    holder.thumbImageView.setVisibility(View.VISIBLE);
                                    holder.photoImageView.setVisibility(View.GONE);
                                    holder.actionImageButton.setVisibility(View.GONE);
                                    holder.downloadProgressView.setVisibility(View.GONE);
                                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                                        holder.actionImageButton.performClick();
                                    }
                                } else {
                                    holder.thumbImageView.setVisibility(View.GONE);
                                    holder.actionImageButton.setVisibility(View.VISIBLE);
                                    holder.photoImageView.setVisibility(View.VISIBLE);
                                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                                        holder.downloadProgressView.setVisibility(View.GONE);
                                        if (ChatsCache.sFiles.get(thumbPhoto.photo.id).path.length() != 0) {
                                            holder.actionImageButton.performClick();
                                        }
                                    } else {
                                        holder.downloadProgressView.setVisibility(View.VISIBLE);
                                        holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.white));
                                        holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                                    }
                                }
                            } else {
                                holder.actionImageButton.setVisibility(View.GONE);
                                holder.downloadProgressView.setVisibility(View.GONE);
                                if (webPage.type.equals("article") || webPage.type.equals("app") || webPage.type.equals("document") || webPage.type.equals("profile")) {
                                    holder.photoImageView.setVisibility(View.GONE);
                                    holder.thumbImageView.setVisibility(View.VISIBLE);
                                    holder.thumbImageView.setImage(holder.file.path, true, false, false, false);
                                    //Glide.with(getParentActivity()).load(holder.file.path).into(holder.thumbImageView);
                                } else {
                                    //Glide.clear(holder.thumbImageView);
                                    holder.photoImageView.setVisibility(View.VISIBLE);
                                    holder.thumbImageView.setVisibility(View.GONE);
                                    holder.photoImageView.setImage(holder.file.path, false, false, false, false);
                                    //Glide.with(getParentActivity()).load(holder.file.path).override(holder.photoImageView.getPhotoWidth(), holder.photoImageView.getPhotoHeight()).dontAnimate().into(holder.photoImageView);
                                }
                            }
                        } else {
                            holder.thumbImageView.setVisibility(View.GONE);
                            holder.photoImageView.setVisibility(View.GONE);
                            holder.actionImageButton.setVisibility(View.GONE);
                            holder.downloadProgressView.setVisibility(View.GONE);
                        }
                        holder.siteTextView.setText(ss);
                        if (webPage.type.equals("video")) {
                            holder.durationTextView.setVisibility(View.VISIBLE);
                            String duration = String.format("%d:%02d", webPage.duration / 60, webPage.duration % 60);
                            holder.durationTextView.setText(duration);
                        } else {
                            holder.durationTextView.setVisibility(View.GONE);
                            int i = 0;
                        }
                        holder.intent.setData(Uri.parse(webPage.url));
                        //Log.e(Utils.TAG, "" + messageWebPage.webPage);
                    }
                    holder.messageTextView.setText(Emoji.replaceEmoji(messageStr, holder.messageTextView, true));
                    Utils.formatText(holder.messageTextView.getText(), entities);
                    //Utils.removeUrlUnderline(holder.messageTextView);
                } else if (type == 1) {
                    TdApi.MessageAudio messageAudio = (TdApi.MessageAudio)messageContent;
                    TdApi.Audio audio = messageAudio.audio;
                    if (!ChatsCache.sFiles.containsKey(audio.audio.id)) {
                        ChatsCache.sFiles.put(audio.audio.id, audio.audio);
                    }
                    holder.titleTextView.setText(audio.title);

                    holder.file = ChatsCache.getFile(audio.audio.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                        if (mStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                        if (mStringStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                        holder.performerTextView.setVisibility(View.VISIBLE);
                        holder.downloadStateTextView.setVisibility(View.GONE);
                        holder.performerTextView.setText(audio.performer);
                    } else {
                        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                        for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                            ArrayList<TextView> textViews = mStringStatesProgress.get(holder.file.id);
                            if (textViews == null) {
                                textViews = new ArrayList<>();
                                mStringStatesProgress.put(holder.file.id, textViews);
                            }
                            holder.performerTextView.setVisibility(View.GONE);
                            holder.downloadStateTextView.setVisibility(View.VISIBLE);
                            textViews.add(holder.downloadStateTextView);

                            ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                            if (progressViews == null) {
                                progressViews = new ArrayList<>();
                                mStatesProgress.put(holder.file.id, progressViews);
                            }
                            progressViews.add(holder.downloadProgressView);

                            updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                        }
                    }
                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
                        holder.actionImageButton.setBackgroundResource(R.drawable.light_blue_circle);
                        if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                            holder.downloadProgressView.setVisibility(View.GONE);
                            holder.actionImageButton.setImageResource(R.drawable.ic_download_blue);
                        } else {
                            holder.downloadProgressView.setVisibility(View.VISIBLE);
                            holder.actionImageButton.setImageResource(R.drawable.ic_pause_blue);
                        }
                    } else {
                        holder.downloadProgressView.setVisibility(View.GONE);
                        holder.actionImageButton.setBackgroundResource(R.drawable.blue_circle);
                        holder.actionImageButton.setImageResource(R.drawable.ic_play);
                        holder.intent.setDataAndType(Uri.fromFile(new File(holder.file.path)), audio.mimeType);
                        /*if (ChatsCache.sMediaPlayer != null) {
                            if (ChatsCache.sPlayingFileId == holder.file.id) {
                                if (ChatsCache.sMediaPlayer.isPlaying()) {
                                    holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                                } else {
                                    holder.actionImageButton.setImageResource(R.drawable.ic_play);
                                }
                            } else {
                                holder.actionImageButton.setImageResource(R.drawable.ic_play);
                            }
                        } else {
                            holder.actionImageButton.setImageResource(R.drawable.ic_play);
                        }*/

                        //holder.actionImageButton.setImageResource(R.drawable.ic_play);
                        /*holder.actionImageButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    if (ChatsCache.sPlayingFileId != holder.file.id) {
                                        ChatsCache.sPlayingFileId = holder.file.id;
                                        if (ChatsCache.sMediaPlayer != null) {
                                            ChatsCache.sMediaPlayer.release();
                                            ChatsCache.sMediaPlayer = null;
                                        }
                                        ChatsCache.sMediaPlayer = new MediaPlayer();
                                        ChatsCache.sMediaPlayer.setDataSource(holder.file.path);
                                        ChatsCache.sMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                        ChatsCache.sMediaPlayer.prepareAsync();
                                        ChatsCache.sMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                            @Override
                                            public void onPrepared(MediaPlayer mp) {
                                                holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                                                ChatsCache.sMediaPlayer.start();
                                            }
                                        });
                                        ChatsCache.sMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mp) {
                                                holder.actionImageButton.setImageResource(R.drawable.ic_play);
                                            }
                                        });
                                    } else {
                                        if (ChatsCache.sMediaPlayer != null) {
                                            if (ChatsCache.sMediaPlayer.isPlaying()) {
                                                ChatsCache.sMediaPlayer.pause();
                                            } else {
                                                ChatsCache.sMediaPlayer.start();
                                            }
                                        }
                                    }
                                    updateVisibleItems();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });*/
                    }
                } else if (type == 7) {
                    final TdApi.MessageContact messageContact = (TdApi.MessageContact)messageContent;
                    if (messageContact.userId != 0) {
                        TdApi.User user = ChatsCache.getUser(messageContact.userId);
                        if (user != null) {
                            TextDrawable placeholder = Utils.getUserAvatar(user, 64);
                            holder.contactAvatarImageView.setProfilePhoto(user.profilePhoto, placeholder);
                            if (user.phoneNumber == null || user.phoneNumber.length() == 0) {
                                user.phoneNumber = messageContact.phoneNumber;
                            }
                        }
                    }
                    holder.contactNameTextView.setText(Utils.formatName(messageContact.firstName, messageContact.lastName));
                    holder.contactPhoneTextView.setText(Utils.formatPhone("+" + messageContact.phoneNumber));
                    holder.contactLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProfileFragment userProfileFragment = new ProfileFragment();
                            Bundle args = new Bundle();
                            args.putInt("user_id", messageContact.userId);
                            args.putLong("chat_id", mCurrentChat.id);
                            args.putInt("group_chat_id", 1);
                            userProfileFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + messageContact.userId, true, false, true, false);
                        }
                    });
                } else if (type == 8) {
                    TdApi.MessageDocument messageDocument = (TdApi.MessageDocument)messageContent;
                    TdApi.Document document = messageDocument.document;
                    //Log.e(Utils.TAG, "" + document);
                    if (!ChatsCache.sFiles.containsKey(document.document.id)) {
                        ChatsCache.sFiles.put(document.document.id, document.document);
                    }
                    holder.titleTextView.setText(document.fileName);
                    holder.file = ChatsCache.getFile(document.document.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                        if (mStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                        if (mStringStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                        holder.sizeTextView.setVisibility(View.VISIBLE);
                        holder.downloadStateTextView.setVisibility(View.GONE);
                        holder.sizeTextView.setText(Utils.formatFileSize(holder.file.size));
                    } else {
                        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                        for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                            ArrayList<TextView> textViews = mStringStatesProgress.get(holder.file.id);
                            if (textViews == null) {
                                textViews = new ArrayList<>();
                                mStringStatesProgress.put(holder.file.id, textViews);
                            }
                            holder.sizeTextView.setVisibility(View.GONE);
                            holder.downloadStateTextView.setVisibility(View.VISIBLE);
                            textViews.add(holder.downloadStateTextView);

                            ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                            if (progressViews == null) {
                                progressViews = new ArrayList<>();
                                mStatesProgress.put(holder.file.id, progressViews);
                            }
                            progressViews.add(holder.downloadProgressView);

                            updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                        }
                    }
                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        holder.actionImageButton.setVisibility(View.VISIBLE);
                        if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                            holder.downloadProgressView.setVisibility(View.GONE);
                            if (!document.mimeType.startsWith("image/")) {
                                holder.thumbImageView.setVisibility(View.GONE);
                                holder.actionImageButton.setBackgroundResource(R.drawable.light_blue_circle);
                                holder.actionImageButton.setImageResource(R.drawable.ic_download_blue);
                            } else {
                                holder.thumbImageView.setVisibility(View.VISIBLE);
                                holder.thumbImageView.setDocumentThumb(document.thumb);
                                holder.actionImageButton.setBackgroundResource(R.drawable.black_circle);
                                holder.actionImageButton.setImageResource(R.drawable.ic_download);
                            }
                        } else {
                            holder.downloadProgressView.setVisibility(View.VISIBLE);
                            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)holder.downloadProgressView.getLayoutParams();
                            if (!document.mimeType.startsWith("image/")) {
                                holder.thumbImageView.setVisibility(View.GONE);
                                holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
                                params.width = params.height = Utils.convertDpToPx(38);
                                holder.actionImageButton.setBackgroundResource(R.drawable.light_blue_circle);
                                holder.actionImageButton.setImageResource(R.drawable.ic_pause_blue);
                            } else {
                                holder.thumbImageView.setVisibility(View.VISIBLE);
                                holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.white));
                                params.width = params.height = Utils.convertDpToPx(48);
                                holder.thumbImageView.setDocumentThumb(document.thumb);
                                holder.actionImageButton.setBackgroundResource(R.drawable.black_circle);
                                holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                            }
                            holder.downloadProgressView.setLayoutParams(params);
                        }
                    } else {
                        holder.downloadProgressView.setVisibility(View.GONE);
                        if (!document.mimeType.startsWith("image/")) {
                            holder.thumbImageView.setVisibility(View.GONE);
                            holder.actionImageButton.setVisibility(View.VISIBLE);
                            holder.actionImageButton.setBackgroundResource(R.drawable.light_blue_circle);
                            holder.actionImageButton.setImageResource(R.drawable.ic_file);
                        } else {
                            holder.thumbImageView.setVisibility(View.VISIBLE);
                            holder.thumbImageView.setImage(holder.file.path, true, false, false, false);
                            //Glide.with(getParentActivity()).load(holder.file.path).into(holder.thumbImageView);
                            holder.actionImageButton.setVisibility(View.GONE);
                        }
                        holder.intent.setDataAndType(Uri.fromFile(new File(holder.file.path)), document.mimeType);
                    }
                } else if (type == 9) {
                    TdApi.MessageLocation messageLocation = (TdApi.MessageLocation)messageContent;
                    TdApi.Location location = messageLocation.location;
                    int scale = (int)Math.min(2, Math.ceil(Utils.sDensity));
                    String urlLocation = String.format("https://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=13&size=160x113&scale=%3$s&markers=color:red|%1$s,%2$s", location.latitude, location.longitude, scale);
                    holder.locationImageView.setImage(urlLocation, true, false, false, false);
                    //Glide.with(getParentActivity()).load(urlLocation).crossFade().into(holder.locationImageView);
                    holder.intent.setData(Uri.parse(String.format("geo:%s,%s", location.latitude, location.longitude)));
                } else if (type == 11) {
                    TdApi.MessagePhoto messagePhoto = (TdApi.MessagePhoto)messageContent;
                    TdApi.Photo photo = messagePhoto.photo;
                    //Log.d(Utils.TAG, "" + photo);

                    TdApi.PhotoSize optimalPhoto = Utils.getRightPhotoSize(Utils.generateThumbs(photo), Utils.getPhotoSize());
                    TdApi.PhotoSize thumbPhoto = Utils.getRightPhotoSize(Utils.generateThumbs(photo), 80);
                    holder.photoImageView.setPhotoThumb(thumbPhoto, optimalPhoto);

                    if (!ChatsCache.sFiles.containsKey(optimalPhoto.photo.id)) {
                        ChatsCache.sFiles.put(optimalPhoto.photo.id, optimalPhoto.photo);
                    }

                    holder.file = ChatsCache.getFile(optimalPhoto.photo.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                        if (mStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                    } else {
                        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                        for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                            ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                            if (progressViews == null) {
                                progressViews = new ArrayList<>();
                                mStatesProgress.put(holder.file.id, progressViews);
                            }
                            progressViews.add(holder.downloadProgressView);

                            updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                        }
                    }
                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        holder.actionImageButton.setVisibility(View.VISIBLE);
                        if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                            holder.downloadProgressView.setVisibility(View.GONE);
                            if (ChatsCache.sFiles.get(thumbPhoto.photo.id).path.length() != 0) {
                                holder.actionImageButton.performClick();
                            }
                        } else {
                            holder.downloadProgressView.setVisibility(View.VISIBLE);
                            holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.white));
                            holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                        }
                    } else {
                        holder.downloadProgressView.setVisibility(View.GONE);
                        holder.actionImageButton.setVisibility(View.GONE);
                        holder.photoImageView.setImage(holder.file.path, false, false, false, false);
                        holder.photoImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PhotoViewFragment photoViewFragment = new PhotoViewFragment();
                                Bundle args = new Bundle();
                                args.putLong("chat_id", mChatId);
                                args.putString("path", holder.file.path);
                                args.putInt("message_id", message.id);
                                photoViewFragment.setArguments(args);
                                ((MainActivity) getParentActivity()).createFragment(photoViewFragment, null, true, false, true, true);
                            }
                        });
                        //Glide.with(getParentActivity()).load(holder.file.path).override(holder.photoImageView.getPhotoWidth(), holder.photoImageView.getPhotoHeight()).dontAnimate().into(holder.photoImageView);
                    }
                } else if (type == 12) {
                    TdApi.MessageSticker messageSticker = (TdApi.MessageSticker)messageContent;
                    TdApi.Sticker sticker = messageSticker.sticker;

                    TdApi.PhotoSize thumbPhoto = sticker.thumb;
                    holder.photoImageView.setStickerThumb(thumbPhoto, sticker);

                    if (!ChatsCache.sFiles.containsKey(sticker.sticker.id)) {
                        ChatsCache.sFiles.put(sticker.sticker.id, sticker.sticker);
                    }

                    holder.file = ChatsCache.getFile(sticker.sticker.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        Utils.sClient.send(new TdApi.DownloadFile(holder.file.id), new Client.ResultHandler() {
                            @Override
                            public void onResult(TdApi.TLObject object) {

                            }
                        });
                    } else {
                        holder.photoImageView.setImage(holder.file.path, false, false, true, false);
                        //Glide.with(getParentActivity()).load(holder.file.path).decoder(new WebPDecoder(getParentActivity(), DecodeFormat.PREFER_ARGB_8888)).override(holder.photoImageView.getPhotoWidth(), holder.photoImageView.getPhotoHeight()).into(holder.photoImageView);
                    }
                } else if (type == 13) {
                    TdApi.MessageVideo messageVideo = (TdApi.MessageVideo)messageContent;
                    TdApi.Video video = messageVideo.video;
                    String duration = String.format("%d:%02d, %s", video.duration / 60, video.duration % 60, Utils.formatFileSize(video.video.size));
                    holder.durationTextView.setText(duration);
                    holder.photoImageView.setVideoThumb(video.thumb, video);

                    if (!ChatsCache.sFiles.containsKey(video.video.id)) {
                        ChatsCache.sFiles.put(video.video.id, video.video);
                    }

                    holder.file = ChatsCache.getFile(video.video.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                        if (mStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                    } else {
                        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                        for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                            ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                            if (progressViews == null) {
                                progressViews = new ArrayList<>();
                                mStatesProgress.put(holder.file.id, progressViews);
                            }
                            progressViews.add(holder.downloadProgressView);

                            updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                        }
                    }

                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                            holder.downloadProgressView.setVisibility(View.GONE);
                            holder.actionImageButton.setImageResource(R.drawable.ic_download);
                        } else {
                            holder.downloadProgressView.setVisibility(View.VISIBLE);
                            holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.white));
                            holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                        }
                    } else {
                        holder.downloadProgressView.setVisibility(View.GONE);
                        holder.actionImageButton.setImageResource(R.drawable.ic_play);
                        holder.intent.setDataAndType(Uri.fromFile(new File(holder.file.path)), "video/mp4");
                    }
                } else if (type == 17) {
                    TdApi.MessageVenue messageVenue = (TdApi.MessageVenue)messageContent;
                    TdApi.Location location = messageVenue.location;
                    int scale = (int)Math.min(2, Math.ceil(Utils.sDensity));
                    String urlLocation = String.format("https://maps.googleapis.com/maps/api/staticmap?center=%1$s,%2$s&zoom=13&size=100x100&scale=%3$s&markers=color:red|%1$s,%2$s", location.latitude, location.longitude, scale);
                    //Glide.with(getParentActivity()).load(urlLocation).crossFade().into(holder.locationImageView);
                    holder.locationImageView.setImage(urlLocation, true, false, false, false);
                    holder.titleTextView.setText(messageVenue.title);
                    holder.addressTextView.setText(messageVenue.address);
                    holder.intent.setData(Uri.parse(String.format("geo:%s,%s", location.latitude, location.longitude)));
                } else if (type == 19) {
                    TdApi.MessageVoice messageVoice = (TdApi.MessageVoice) messageContent;
                    TdApi.Voice voice = messageVoice.voice;

                    final String duration = String.format("%d:%02d", voice.duration / 60, voice.duration % 60);
                    holder.durationTextView.setText(duration);

                    if (!ChatsCache.sFiles.containsKey(voice.voice.id)) {
                        ChatsCache.sFiles.put(voice.voice.id, voice.voice);
                    }

                    holder.file = ChatsCache.getFile(voice.voice.id);
                    if (holder.file == null) {
                        return;
                    }

                    if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                        if (mStatesProgress.containsKey(holder.file.id)) {
                            mStatesProgress.remove(holder.file.id);
                        }
                    } else {
                        HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(holder.file.id);
                        for (HashMap.Entry<Integer, Integer> entry : state.entrySet()) {
                            ArrayList<ProgressView> progressViews = mStatesProgress.get(holder.file.id);
                            if (progressViews == null) {
                                progressViews = new ArrayList<>();
                                mStatesProgress.put(holder.file.id, progressViews);
                            }
                            progressViews.add(holder.downloadProgressView);

                            updateDownloadProgress(holder.file.id, entry.getValue(), entry.getKey());
                        }
                    }

                    Drawable thumb = ContextCompat.getDrawable(Utils.sContext, R.drawable.ic_thumb);
                    if (holder.file.path == null || holder.file.path.length() == 0) {
                        holder.actionImageButton.setBackgroundResource(R.drawable.light_blue_circle);
                        thumb.mutate().setAlpha(0);
                        holder.durationSeekBar.setThumb(thumb);
                        if (!ChatsCache.sDownloadFileStates.containsKey(holder.file.id)) {
                            holder.downloadProgressView.setVisibility(View.GONE);
                            holder.actionImageButton.setImageResource(R.drawable.ic_download_blue);
                        } else {
                            holder.downloadProgressView.setVisibility(View.VISIBLE);
                            holder.downloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
                            holder.actionImageButton.setImageResource(R.drawable.ic_pause_blue);
                        }
                    } else {
                        holder.actionImageButton.setBackgroundResource(R.drawable.blue_circle);
                        holder.downloadProgressView.setVisibility(View.GONE);
                        if (mAudioTrack == null || holder.file.id != ChatsCache.sPlayingFileId) {
                            holder.actionImageButton.setImageResource(R.drawable.ic_play);
                            holder.durationSeekBar.setMax(0);
                            holder.durationSeekBar.setProgress(0);
                            holder.durationTextView.setText(duration);
                        } else {
                            holder.durationSeekBar.setMax((int) mSamples);
                            holder.durationSeekBar.setProgress(mAudioTrack.getPlaybackHeadPosition());
                            int currentDuration = mAudioTrack.getPlaybackHeadPosition() / mAudioTrack.getPlaybackRate();
                            holder.durationTextView.setText(String.format("%d:%02d", currentDuration / 60, currentDuration % 60));
                            mAudioTrack.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
                                @Override
                                public void onMarkerReached(AudioTrack track) {
                                    track.pause();
                                    track.release();
                                    mAudioTrack = null;
                                    ChatsCache.sPlayingFileId = 0;
                                    updateVisibleItems();
                                }

                                @Override
                                public void onPeriodicNotification(final AudioTrack track) {
                                    try {
                                        int currentDuration = track.getPlaybackHeadPosition() / mAudioTrack.getPlaybackRate();
                                        holder.durationTextView.setText(String.format("%d:%02d", currentDuration / 60, currentDuration % 60));
                                        holder.durationSeekBar.setProgress(track.getPlaybackHeadPosition());
                                    } catch (IllegalStateException e) {
                                        holder.durationSeekBar.setProgress(0);
                                    }
                                }
                            });
                            int playState = mAudioTrack.getPlayState();
                            if (playState == AudioTrack.PLAYSTATE_PLAYING) {
                                holder.actionImageButton.setImageResource(R.drawable.ic_pause);
                            } else {
                                holder.actionImageButton.setImageResource(R.drawable.ic_play);
                            }
                        }
                        thumb.mutate().setAlpha(255);
                        holder.durationSeekBar.setThumb(thumb);
                        holder.intent.setDataAndType(Uri.fromFile(new File(holder.file.path)), voice.mimeType);
                    }
                } else {
                    holder.siteTextView.setVisibility(View.GONE);
                    holder.lineImageView.setVisibility(View.GONE);
                    holder.thumbImageView.setVisibility(View.GONE);
                    holder.photoImageView.setVisibility(View.GONE);
                    holder.durationTextView.setVisibility(View.GONE);
                    holder.actionImageButton.setVisibility(View.GONE);
                    holder.downloadProgressView.setVisibility(View.GONE);
                    holder.siteContentLayout.setVisibility(View.GONE);
                    holder.messageTextView.setTextColor(0xff6b9cc2);
                    holder.messageTextView.setText(Utils.convertMessageToText(message));
                }

                if (message.sendState.getConstructor() != TdApi.MessageIsIncoming.CONSTRUCTOR) {
                    if (holder.clockImageView != null) {
                        if (message.sendState.getConstructor() == TdApi.MessageIsBeingSent.CONSTRUCTOR) {
                            holder.clockImageView.setBackgroundResource(R.drawable.ic_clock);
                            holder.clockImageView.setVisibility(View.VISIBLE);
                        } else if (message.sendState.getConstructor() == TdApi.MessageIsSuccessfullySent.CONSTRUCTOR) {
                            if (message.id <= chat.lastReadOutboxMessageId) {
                                holder.clockImageView.setVisibility(View.GONE);
                            } else {
                                holder.clockImageView.setVisibility(View.VISIBLE);
                                holder.clockImageView.setBackgroundResource(R.drawable.ic_blue_point);
                            }
                        }
                    }
                } else {
                    if (message.isPost) {
                        holder.clockImageView.setBackgroundResource(R.drawable.ic_post_views);
                        holder.clockImageView.setVisibility(View.VISIBLE);
                    } else {
                        holder.clockImageView.setVisibility(View.GONE);
                    }
                }
            }
        }

        @Override
        public int getItemCount() {
            int count = mMessages.size();
            if (count != 0 && !mChatEndReached) {
                return ++count;
            }
            return count;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            int off = 1;
            if (!mChatEndReached && mMessages.size() != 0) {
                off = 0;
                if (position == 0) {
                    return 21;
                }
            }
            TdApi.Message message = mMessages.get(mMessages.size() - position - off);
            if (message.id == 0) {
                return 22;
            }

            TdApi.MessageContent messageContent = message.content;
            switch (messageContent.getConstructor()) {
                case TdApi.MessageText.CONSTRUCTOR:
                    return 0;
                case TdApi.MessageAudio.CONSTRUCTOR:
                    return 1;
                case TdApi.MessageChatAddParticipants.CONSTRUCTOR:
                    return 2;
                case TdApi.MessageChatChangePhoto.CONSTRUCTOR:
                    return 3;
                case TdApi.MessageChatChangeTitle.CONSTRUCTOR:
                    return 4;
                case TdApi.MessageChatDeleteParticipant.CONSTRUCTOR:
                    return 5;
                case TdApi.MessageChatDeletePhoto.CONSTRUCTOR:
                    return 6;
                case TdApi.MessageContact.CONSTRUCTOR:
                    return 7;
                case TdApi.MessageDocument.CONSTRUCTOR:
                    return 8;
                case TdApi.MessageLocation.CONSTRUCTOR:
                    return 9;
                case TdApi.MessageGroupChatCreate.CONSTRUCTOR:
                    return 10;
                case TdApi.MessagePhoto.CONSTRUCTOR:
                    return 11;
                case TdApi.MessageSticker.CONSTRUCTOR:
                    return 12;
                case TdApi.MessageVideo.CONSTRUCTOR:
                    return 13;
                case TdApi.MessageDeleted.CONSTRUCTOR:
                    return 14;
                case TdApi.MessageUnsupported.CONSTRUCTOR:
                    return 15;
                case TdApi.MessageChatJoinByLink.CONSTRUCTOR:
                    return 16;
                case TdApi.MessageVenue.CONSTRUCTOR:
                    return 17;
                case TdApi.MessageWebPage.CONSTRUCTOR:
                    return 18;
                case TdApi.MessageVoice.CONSTRUCTOR:
                    return 19;
                case TdApi.MessageChannelChatCreate.CONSTRUCTOR:
                    return 20;
                default:
                    return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public final BlockingImageView avatarImageView;
            public final TextView userNameTextView;
            public final AlignedTextView messageTextView;
            public final TextView timeTextView;
            public final ImageView clockImageView;
            public final TextView viewsTextView;
            public final ImageView forwardSymbolImageView;
            public final BlockingImageView forwardAvatarImageView;
            public final TextView forwardUserNameTextView;
            public final TextView forwardTimeTextView;
            public final TextView signTextView;
            public final TextView newMessagesTextView;
            public final TextView durationTextView;
            public final SeekBar durationSeekBar;
            public final ImageButton actionImageButton;
            public final ProgressView downloadProgressView;
            public final RelativeLayout contactLayout;
            public final BlockingImageView contactAvatarImageView;
            public final TextView contactNameTextView;
            public final TextView contactPhoneTextView;
            public final BlockingImageView locationImageView;
            public final TextView titleTextView;
            public final TextView performerTextView;
            public final TextView downloadStateTextView;
            public final TextView sizeTextView;
            public final TextView addressTextView;
            public final BlockingImageView thumbImageView;
            public final BlockingImageView photoImageView;
            public final TextView siteTextView;
            public final ImageView lineImageView;
            public final LinearLayout siteContentLayout;

            public TdApi.File file = null;
            public TdApi.User user = null;
            public TdApi.User forwardUser = null;
            public Intent intent = new Intent(Intent.ACTION_VIEW);

            public ViewHolder(View itemView) {
                super(itemView);
                /*itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //mListener.onClick(v, getAdapterPosition());
                    }
                });*/
                avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
                userNameTextView = (TextView) itemView.findViewById(R.id.userNameTextView);
                messageTextView = (AlignedTextView) itemView.findViewById(R.id.messageTextView);
                timeTextView = (TextView) itemView.findViewById(R.id.timeTextView);
                clockImageView = (ImageView) itemView.findViewById(R.id.clockImageView);
                viewsTextView = (TextView) itemView.findViewById(R.id.viewsTextView);
                forwardSymbolImageView = (ImageView)itemView.findViewById(R.id.forwardSymbolImageView);
                forwardAvatarImageView = (BlockingImageView)itemView.findViewById(R.id.forwardAvatarImageView);
                forwardUserNameTextView = (TextView) itemView.findViewById(R.id.forwardUserNameTextView);
                forwardTimeTextView = (TextView) itemView.findViewById(R.id.forwardTimeTextView);
                signTextView = (TextView) itemView.findViewById(R.id.signTextView);
                newMessagesTextView = (TextView) itemView.findViewById(R.id.newMessagesTextView);
                durationTextView = (TextView) itemView.findViewById(R.id.durationTextView);
                durationSeekBar = (SeekBar)itemView.findViewById(R.id.durationSeekBar);
                actionImageButton = (ImageButton)itemView.findViewById(R.id.actionImageButton);
                downloadProgressView = (ProgressView)itemView.findViewById(R.id.downloadProgressView);
                contactLayout = (RelativeLayout)itemView.findViewById(R.id.contactLayout);
                contactAvatarImageView = (BlockingImageView)itemView.findViewById(R.id.contactAvatarImageView);
                contactNameTextView = (TextView) itemView.findViewById(R.id.contactNameTextView);
                contactPhoneTextView = (TextView) itemView.findViewById(R.id.contactPhoneTextView);
                locationImageView = (BlockingImageView)itemView.findViewById(R.id.locationImageView);
                titleTextView = (TextView)itemView.findViewById(R.id.titleTextView);
                performerTextView = (TextView)itemView.findViewById(R.id.performerTextView);
                sizeTextView = (TextView)itemView.findViewById(R.id.sizeTextView);
                addressTextView = (TextView)itemView.findViewById(R.id.addressTextView);
                downloadStateTextView = (TextView)itemView.findViewById(R.id.downloadStateTextView);
                thumbImageView = (BlockingImageView)itemView.findViewById(R.id.thumbImageView);
                photoImageView = (BlockingImageView)itemView.findViewById(R.id.photoImageView);
                siteTextView = (TextView)itemView.findViewById(R.id.siteTextView);
                lineImageView = (ImageView)itemView.findViewById(R.id.lineImageView);
                siteContentLayout = (LinearLayout)itemView.findViewById(R.id.siteContentLayout);

                if (userNameTextView != null) {
                    userNameTextView.setTypeface(Utils.getTypeface("bold"));
                    userNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProfileFragment userProfileFragment = new ProfileFragment();
                            Bundle args = new Bundle();
                            args.putInt("user_id", user.id);
                            args.putLong("chat_id", mCurrentChat.id);
                            args.putInt("group_chat_id", 1);
                            userProfileFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + user.id, true, false, true, false);
                        }
                    });
                }
                if (messageTextView != null) {
                    messageTextView.setTypeface(Utils.getTypeface("regular"));
                    messageTextView.setMovementMethod(LinkMovementMethod.getInstance());
                }
                if (timeTextView != null) {
                    timeTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (viewsTextView != null) {
                    viewsTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (signTextView != null) {
                    signTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (forwardUserNameTextView != null) {
                    forwardUserNameTextView.setTypeface(Utils.getTypeface("bold"));
                    forwardUserNameTextView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProfileFragment userProfileFragment = new ProfileFragment();
                            Bundle args = new Bundle();
                            args.putInt("user_id", forwardUser.id);
                            args.putLong("chat_id", mCurrentChat.id);
                            args.putInt("group_chat_id", 1);
                            userProfileFragment.setArguments(args);
                            ((MainActivity) getParentActivity()).createFragment(userProfileFragment, "user_profile_" + forwardUser.id, true, false, true, false);
                        }
                    });
                }
                if (forwardTimeTextView != null) {
                    forwardTimeTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (newMessagesTextView != null) {
                    newMessagesTextView.setTypeface(Utils.getTypeface("medium"));
                }
                if (durationTextView != null) {
                    durationTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (contactNameTextView != null) {
                    contactNameTextView.setTypeface(Utils.getTypeface("bold"));
                }
                if (contactPhoneTextView != null) {
                    contactPhoneTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (titleTextView != null) {
                    titleTextView.setTypeface(Utils.getTypeface("medium"));
                }
                if (performerTextView != null) {
                    performerTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (sizeTextView != null) {
                    sizeTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (addressTextView != null) {
                    addressTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (downloadStateTextView != null) {
                    downloadStateTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (siteTextView != null) {
                    siteTextView.setTypeface(Utils.getTypeface("regular"));
                }
                if (actionImageButton != null) {
                    actionImageButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (file.path == null || file.path.length() == 0) {
                                TdApi.TLFunction function;
                                if (!ChatsCache.sDownloadFileStates.containsKey(file.id)) {
                                    HashMap<Integer, Integer> state = ChatsCache.sDownloadFileStates.get(file.id);
                                    if (state == null) {
                                        state = new HashMap<>();
                                        ChatsCache.sDownloadFileStates.put(file.id, state);
                                    }
                                    state.put(file.size, 0);
                                    function = new TdApi.DownloadFile(file.id);
                                } else {
                                    if (ChatsCache.sDownloadFileStates.containsKey(file.id)) {
                                        ChatsCache.sDownloadFileStates.remove(file.id);
                                    }
                                    function = new TdApi.CancelDownloadFile(file.id);
                                }
                                updateVisibleItems();
                                Utils.sClient.send(function, new Client.ResultHandler() {
                                    @Override
                                    public void onResult(TdApi.TLObject object) {

                                    }
                                });
                            } else {
                                if (intent.getType().equals("audio/ogg")) {
                                    if (ChatsCache.sPlayingFileId != 0 && ChatsCache.sPlayingFileId != file.id) {
                                        mAudioTrack.pause();
                                        mAudioTrack.release();
                                        mAudioTrack = null;
                                        ChatsCache.sPlayingFileId = 0;
                                        updateVisibleItems();
                                    }
                                    if (mAudioTrack == null) {
                                        ChatsCache.sPlayingFileId = file.id;
                                        mSamples = Utils.openOggFile(file.path);
                                        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 48000, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, (int) (mSamples * 2), AudioTrack.MODE_STATIC);
                                        mAudioTrack.setNotificationMarkerPosition((int) mSamples);
                                        mAudioTrack.setPositionNotificationPeriod(4000);
                                        actionImageButton.setImageResource(R.drawable.ic_pause);
                                        Utils.sMainThread.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                short[] buffer = new short[(int) mSamples];
                                                Utils.readOggFile(buffer);
                                                mAudioTrack.write(buffer, 0, buffer.length);
                                                mAudioTrack.play();
                                                Utils.runOnUIThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        updateVisibleItems();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                    int playState = mAudioTrack.getPlayState();
                                    if (playState == AudioTrack.PLAYSTATE_PLAYING) {
                                        mAudioTrack.pause();
                                        actionImageButton.setImageResource(R.drawable.ic_play);
                                    } else if (playState == AudioTrack.PLAYSTATE_PAUSED) {
                                        mAudioTrack.play();
                                        actionImageButton.setImageResource(R.drawable.ic_pause);
                                    }
                                    /*final Playback playback = new Playback();
                                    if (playback.isPlaying) {
                                        playback.stopPlaying();
                                    } else {
                                        Utils.sMainThread.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                playback.playFile(new File(file.path));
                                            }
                                        });
                                    }*/
                                } else if (intent.resolveActivity(getParentActivity().getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                }

                if (durationSeekBar != null) {
                    durationSeekBar.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return true;
                        }
                    });
                }

                if (thumbImageView != null) {
                    thumbImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (file != null && file.path.length() != 0) {
                                if (intent.resolveActivity(getParentActivity().getPackageManager()) != null) {
                                    startActivity(intent);
                                }
                            }
                        }
                    });
                }

                if (locationImageView != null) {
                    locationImageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (intent.resolveActivity(getParentActivity().getPackageManager()) != null) {
                                startActivity(intent);
                            }
                        }
                    });
                }

                if (downloadProgressView != null) {
                    downloadProgressView.setSpinSpeed(0.333f);
                    downloadProgressView.setRimColor(0x00000000);
                }

                if (photoImageView != null) {
                    photoImageView.setIgnoreBlock(true);
                }
            }
        }
    }
}
