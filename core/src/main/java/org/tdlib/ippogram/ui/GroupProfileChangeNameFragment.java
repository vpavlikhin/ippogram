package org.tdlib.ippogram.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Utils;

public class GroupProfileChangeNameFragment extends MainFragment {

    private EditText mTitleField;
    private ImageButton mDoneImageButton;

    private long mChatId;
    private int mGroupId;

    @Override
    public void onPreCreate() {
        mChatId = getArguments().getLong("chat_id", 0);
        mGroupId = getArguments().getInt("group_chat_id", 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_group_profile_change_name, container, false);

        TdApi.Chat chat = ChatsCache.getChat(mGroupId);

        mToolbar = (Toolbar) mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(GroupProfileChangeNameFragment.this);
            }
        });

        mHeaderTextView = (TextView) mToolbar.findViewById(R.id.headerTextView);
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
            mHeaderTextView.setText(Utils.sContext.getString(R.string.action_edit_name));
        }

        mTitleField = (EditText) mRootView.findViewById(R.id.titleField);
        mTitleField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });
        if (chat != null) {
            mTitleField.setText(chat.title);
            mTitleField.setSelection(mTitleField.length());
        }
        mDoneImageButton = (ImageButton) mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTitleField.getText().length() != 0) {
                    Utils.sClient.send(new TdApi.ChangeChatTitle(mChatId, mTitleField.getText().toString()), new Client.ResultHandler() {
                        @Override
                        public void onResult(TdApi.TLObject object) {

                        }
                    });
                    ((MainActivity) getParentActivity()).destroyFragment(GroupProfileChangeNameFragment.this);
                }
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getParentActivity() == null) {
            return;
        }

        mTitleField.requestFocus();
        Utils.showKeyboard(mTitleField);
    }
}
