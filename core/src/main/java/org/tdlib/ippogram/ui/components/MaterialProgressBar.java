package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;

public class MaterialProgressBar extends ProgressBar {

    private Context mContext;
    private MaterialProgressDrawable mProgressDrawable;

    public MaterialProgressBar(Context context) {
        super(context);
        init(context);
    }

    public MaterialProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MaterialProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mProgressDrawable = new MaterialProgressDrawable(context, this);
        mProgressDrawable.setColorSchemeColors(ContextCompat.getColor(context, R.color.blue));
        mProgressDrawable.setAlpha(255);

        setProgress(20);
        setIndeterminateDrawable(mProgressDrawable);
        setProgressDrawable(mProgressDrawable);
    }

    public void setProgressColor(@ColorRes int resId) {
        mProgressDrawable.setColorSchemeColors(ContextCompat.getColor(mContext, resId));
    }
}
