package org.tdlib.ippogram.ui.components;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

public class TypefaceSpan extends MetricAffectingSpan {

    private Typeface mTypeface;
    private int mFontSize;

    public TypefaceSpan(Typeface typeface) {
        mTypeface = typeface;
    }

    public TypefaceSpan(Typeface typeface, int fontSize) {
        mTypeface = typeface;
        mFontSize = fontSize;
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        if (mTypeface != null) {
            tp.setTypeface(mTypeface);
        }
        if (mFontSize != 0) {
            tp.setTextSize(mFontSize);
        }
        tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    @Override
    public void updateMeasureState(TextPaint p) {
        if (mTypeface != null) {
            p.setTypeface(mTypeface);
        }
        if (mFontSize != 0) {
            p.setTextSize(mFontSize);
        }
        p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}
