package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;

public class EmptyRecyclerView extends RecyclerView {

    private View mEmptyView;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        boolean onItemClick(View v, int position);
    }

    public EmptyRecyclerView(Context context) {
        super(context);
        addOnItemTouchListener(new RecyclerItemClickListener(context));
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        addOnItemTouchListener(new RecyclerItemClickListener(context));
    }

    public EmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        addOnItemTouchListener(new RecyclerItemClickListener(context));
    }

    @Override
    public void setAdapter(Adapter adapter) {
        final Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(mObserver);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(mObserver);
        }

        checkIfEmpty();
    }

    void checkIfEmpty() {
        if (mEmptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible = getAdapter().getItemCount() == 0;
            mEmptyView.setVisibility(emptyViewVisible ? VISIBLE : GONE);
            setVisibility(emptyViewVisible ? GONE : VISIBLE);
        }
    }

    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;
        checkIfEmpty();
    }

    public View getEmptyView() {
        return mEmptyView;
    }

    public void invalidateViews() {
        for (int i = 0; i < getChildCount(); i++) {

            //getChildAt(i).invalidate();
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

    private AdapterDataObserver mObserver = new AdapterDataObserver() {

        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    private class RecyclerItemClickListener implements OnItemTouchListener {

        private GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context) {
            mGestureDetector = new RecyclerItemClickGestureDetector(context, new RecyclerItemClickGestureListener(EmptyRecyclerView.this));
        }

        private boolean isAttachedToWindow(RecyclerView rv) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return rv.isAttachedToWindow();
            }
            return rv.getHandler() != null;
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            if (!isAttachedToWindow(rv) || rv.getAdapter() == null) {
                return false;
            }
            mGestureDetector.onTouchEvent(e);
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

    private class RecyclerItemClickGestureDetector extends GestureDetector {

        private RecyclerItemClickGestureListener mGestureListener;

        public RecyclerItemClickGestureDetector(Context context, RecyclerItemClickGestureListener gestureListener) {
            super(context, gestureListener);
            mGestureListener = gestureListener;
        }

        @Override
        public boolean onTouchEvent(@NonNull MotionEvent ev) {
            boolean handled = super.onTouchEvent(ev);

            int action = ev.getAction() & MotionEvent.ACTION_MASK;

            if (action == MotionEvent.ACTION_UP) {
                mGestureListener.dispatchSingleTapUpIfNeeded(ev);
            }
            return handled;
        }
    }

    private class RecyclerItemClickGestureListener extends GestureDetector.SimpleOnGestureListener {

        private RecyclerView mRecyclerView;
        private View mTargetChild;

        public RecyclerItemClickGestureListener(RecyclerView recyclerView){
            mRecyclerView = recyclerView;
        }

        public void dispatchSingleTapUpIfNeeded(MotionEvent event) {
            if (mTargetChild != null) {
                onSingleTapUp(event);
            }
        }

        @Override
        public boolean onDown(MotionEvent e) {
            mTargetChild = mRecyclerView.findChildViewUnder((int)e.getX(), (int)e.getY());
            return mTargetChild != null;
        }

        @Override
        public void onShowPress(MotionEvent e) {
            if (mTargetChild != null) {
                mTargetChild.setPressed(true);
            }
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            if (mTargetChild != null) {
                mTargetChild.setPressed(false);
                mTargetChild = null;
                return true;
            }
            return false;
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            boolean handled = false;
            if (mTargetChild != null) {
                mTargetChild.setPressed(false);

                if (mListener != null) {
                    mTargetChild.playSoundEffect(SoundEffectConstants.CLICK);
                    handled = mListener.onItemClick(mTargetChild, mRecyclerView.getChildLayoutPosition(mTargetChild));
                }
                mTargetChild = null;
            }
            return handled;
        }
    }
}
