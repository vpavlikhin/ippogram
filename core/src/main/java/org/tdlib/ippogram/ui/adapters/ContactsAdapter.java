package org.tdlib.ippogram.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.R;
import org.tdlib.ippogram.ui.components.BlockingImageView;
import org.tdlib.ippogram.ui.components.SectionedSectionIndexer;
import org.tdlib.ippogram.ui.components.StringArrayAlphabetIndexer;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends IndexedPinnedHeaderListViewAdapter {

    private ArrayList<Integer> mCheckedUsers;
    private ArrayList<Integer> mIgnoreUsers;

    @Override
    public CharSequence getSectionTitle(int sectionIndex) {
        return ((StringArrayAlphabetIndexer.AlphaBetSection)getSections()[sectionIndex]).getName();
    }

    public void setSelectedUsers(ArrayList<Integer> checkedUsers) {
        mCheckedUsers = checkedUsers;
    }

    public void setIgnoreUsers(ArrayList<Integer> ignoreUsers) {
        mIgnoreUsers = ignoreUsers;
    }

    private String[] generateContactsNames(List<Integer> users) {
        ArrayList<String> userNames = new ArrayList<>();
        for (Integer userId : users) {
            TdApi.User user = ChatsCache.getUser(userId);
            if (user == null) {
                continue;
            }
            userNames.add(Utils.formatName(user.firstName, user.lastName));
        }
        return userNames.toArray(new String[userNames.size()]);
    }

    @Override
    public void notifyDataSetChanged() {
        if (ContactsCache.getInstance().users.size() == 0) {
            return;
        }
        String[] generatedContactsNames = generateContactsNames(ContactsCache.getInstance().users);
        setSectionIndexer(new StringArrayAlphabetIndexer(generatedContactsNames, true));
        super.notifyDataSetChanged();
    }

    @Override
    public boolean isEnabled(int position) {
        SectionedSectionIndexer.SimpleSection countrySection = (SectionedSectionIndexer.SimpleSection)getSections()[getSectionForPosition(position)];
        return getPositionInSection(position) < countrySection.getItemsCount();
    }

    @Override
    public int getCount() {
        int count = ContactsCache.getInstance().users.size();
        if (count == 0) {
            return 0;
        }
        count += getSections().length - 1;
        return count;
    }

    @Override
    public TdApi.User getItem(int position) {
        return null;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);
        if (type == 0) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_item, parent, false);
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            if (holder == null) {
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            int index = position - getSectionForPosition(position);
            holder.user = ChatsCache.getUser(ContactsCache.getInstance().users.get(index));
            holder.updateItem();
            bindSectionHeader(holder.letterSectionTextView, position);
        } else if (type == 1) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.divider, parent, false);
            }
        } else if (type == 2) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.settings_list_item, parent, false);
            }
            ImageView iconImageView = (ImageView)convertView.findViewById(R.id.iconImageView);
            TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
            iconImageView.setVisibility(View.VISIBLE);
            iconImageView.setImageResource(R.drawable.ic_groupusers);
            primaryTextView.setText(Utils.sContext.getString(R.string.new_group));
        } else if (type == 3) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.passcode_divider, parent, false);
            }
        } else if (type == 4) {
            if (convertView == null) {
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.subheader_listview, parent, false);
            }
            TextView subHeaderTextView = (TextView) convertView.findViewById(R.id.subHeaderTextView);
            subHeaderTextView.setTypeface(Utils.getTypeface("medium"));
            subHeaderTextView.setText(Utils.sContext.getString(R.string.subheader_contacts));
        }
        return convertView;
    }

    @Override
    public int getItemViewType(int position) {
        int id = getSectionForPosition(position);
        SectionedSectionIndexer.SimpleSection countrySection = (SectionedSectionIndexer.SimpleSection)getSections()[id];
        return getPositionInSection(position) < countrySection.getItemsCount() ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    public class ViewHolder extends IndexedPinnedHeaderListViewAdapter.ViewHolder {
        public BlockingImageView avatarImageView;
        public ImageView checkImageView;
        public TextView primaryTextView;
        public TextView secondaryTextView;
        public TdApi.User user;

        public ViewHolder(View itemView) {
            super(itemView);
            avatarImageView = (BlockingImageView)itemView.findViewById(R.id.avatarImageView);
            checkImageView = (ImageView)itemView.findViewById(R.id.checkImageView);
            primaryTextView = (TextView)itemView.findViewById(R.id.primaryTextView);
            secondaryTextView = (TextView)itemView.findViewById(R.id.secondaryTextView);
        }

        public void updateItem() {
            avatarImageView.setProfilePhoto(user.profilePhoto, Utils.getUserAvatar(user, 48));
            if (mCheckedUsers != null) {
                if (mCheckedUsers.contains(user.id)) {
                    checkImageView.setVisibility(View.VISIBLE);
                } else {
                    checkImageView.setVisibility(View.GONE);
                }
            }
            if (mIgnoreUsers != null) {
                if (mIgnoreUsers.contains(user.id)) {
                    avatarImageView.setAlpha(0.5f);
                    primaryTextView.setAlpha(0.5f);
                    secondaryTextView.setAlpha(0.5f);
                } else {
                    avatarImageView.setAlpha(1.0f);
                    primaryTextView.setAlpha(1.0f);
                    secondaryTextView.setAlpha(1.0f);
                }
            }
            primaryTextView.setText(Utils.formatName(user.firstName, user.lastName));
            String userStatus = Utils.formatUserStatus(user);
            switch (userStatus) {
                case "online":
                    secondaryTextView.setTextColor(0xff6b9cc2);
                    break;
                default:
                    secondaryTextView.setTextColor(0xff8a8a8a);
            }
            secondaryTextView.setText(userStatus);
        }
    }
}
