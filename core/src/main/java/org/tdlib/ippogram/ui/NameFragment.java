package org.tdlib.ippogram.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.Utils;

import java.util.HashMap;

public class NameFragment extends SlideFragment {

    private EditText mFirstNameField;
    private EditText mLastNameField;

    private HashMap<String, Object> mParams;

    private boolean mWaitingSms = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_name, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onBackPressed();
            }
        });
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(R.string.name_title);

        mDoneImageButton = (ImageButton)mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onNextAction();
            }
        });

        Typeface typeface = Utils.getTypeface("regular");

        mFirstNameField = (EditText)mRootView.findViewById(R.id.firstNameField);
        mFirstNameField.setTypeface(typeface);
        mFirstNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mLastNameField.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mLastNameField = (EditText)mRootView.findViewById(R.id.lastNameField);
        mLastNameField.setTypeface(typeface);
        mLastNameField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });

        return mRootView;
    }

    @Override
    public void setParams(HashMap<String, Object> params) {
        mLastNameField.setText("");
        mLastNameField.setText("");
        mWaitingSms = true;
        mParams = params;
    }

    @Override
    public void onDonePressed() {
        mWaitingSms = false;
        getParentActivity().showProgress();
        TdApi.SetAuthName authSetName = new TdApi.SetAuthName(mFirstNameField.getText().toString(), mLastNameField.getText().toString());
        getParentActivity().send(authSetName);
    }

    @Override
    public void onBackPressed() {
        mWaitingSms = false;
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        if (object.getConstructor() == TdApi.AuthStateWaitCode.CONSTRUCTOR) {
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    getParentActivity().sendParams(mParams);
                    getParentActivity().slidePager(2, true);
                }
            });
        } else if (object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
            mWaitingSms = true;
            if (((TdApi.Error)object).text != null) {
                if (((TdApi.Error)object).text.contains("FIRSTNAME_INVALID")) {
                    getParentActivity().showAlert(Utils.sContext.getString(R.string.invalid_first_name_text));
                } else if (((TdApi.Error)object).text.contains("LASTNAME_INVALID")) {
                    getParentActivity().showAlert(Utils.sContext.getString(R.string.invalid_last_name_text));
                } else{
                    getParentActivity().showAlert(((TdApi.Error) object).text);
                }
            }
        }
        getParentActivity().hideProgress();
    }

    @Override
    public void onSmsReceived(final String code) {
        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                if (mWaitingSms) {
                    if (mParams != null) {
                        mParams.put("smsCode", code);
                    }
                }
            }
        });
    }

    @Override
    public String getTitle() {
        return getResources().getString(R.string.name_title);
    }
}
