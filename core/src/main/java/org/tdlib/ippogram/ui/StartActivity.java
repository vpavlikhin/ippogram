package org.tdlib.ippogram.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.Utils;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            return;
        }
        getWindow().setBackgroundDrawableResource(R.drawable.transparent);
        ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getSize(Utils.sDisplaySize);
        int resId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resId > 0) {
            Utils.sStatusBarHeight = getResources().getDimensionPixelSize(resId);
        }

        SharedPreferences passcodePrefs = getSharedPreferences("passcode", Context.MODE_PRIVATE);
        Utils.sPasscodeHash = passcodePrefs.getString("passcode_hash", "");
        Utils.sLocked = passcodePrefs.getBoolean("locked", false);
        Utils.sAutoLock = passcodePrefs.getInt("auto_lock", 60 * 60);
        Utils.sPasscodeType = passcodePrefs.getInt("type", 0);

        Utils.sImportHash = getSharedPreferences("contacts", Context.MODE_PRIVATE).getString("import_hash", "");

        Utils.sClient.send(new TdApi.GetAuthState(), new Client.ResultHandler() {
            @Override
            public void onResult(final TdApi.TLObject object) {
                if (object.getConstructor() != TdApi.AuthStateOk.CONSTRUCTOR) {
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(StartActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                } else {
                    ContactsCache.getInstance().readContacts();
                    Utils.runOnUIThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(StartActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
//                Utils.runOnUIThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        if (object.getConstructor() != TdApi.AuthStateOk.CONSTRUCTOR) {
//                            Intent intent = new Intent(StartActivity.this, LoginActivity.class);
//                            startActivity(intent);
//                            finish();
//                        } else {
//                            ContactsCache.getInstance().readContacts();
//                            Intent intent = new Intent(StartActivity.this, MainActivity.class);
//                            startActivity(intent);
//                            finish();
//                        }
//                    }
//                });
            }
        });
    }
}
