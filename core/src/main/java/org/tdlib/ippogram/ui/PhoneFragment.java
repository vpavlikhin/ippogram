package org.tdlib.ippogram.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.Utils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static org.tdlib.ippogram.ui.adapters.SelectCountryAdapter.Country;

public class PhoneFragment extends SlideFragment {

    private TextView mCountryTextView;
    private EditText mPhoneCodeField;
    private EditText mPhoneField;
    private ArrayList<Country> mCountries;

    private HashMap<String, String> mCountryNames = new HashMap<>();
    private HashMap<String, Integer> mPhoneCodes = new HashMap<>();
    private HashMap<Integer, String> mCountryCodes = new HashMap<>();
    private HashMap<String, Object> mParams = new HashMap<>();

    private boolean mSelfChange = false;
    private boolean mStopFormatting;
    private AsYouTypeFormatter mFormatter;

    private String mCountryCode;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_phone, container, false);

        mCountries = getCountries();
        for (Country country : mCountries) {
            mCountryNames.put(country.code, country.name);
            mPhoneCodes.put(country.name, country.phoneCode);
            mCountryCodes.put(country.phoneCode, country.code);
        }

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(R.string.phone_title);

        mDoneImageButton = (ImageButton)mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onNextAction();
            }
        });

        mCountryTextView = (TextView)mRootView.findViewById(R.id.countryTextView);
        mCountryTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getParentActivity().getCurrentFocus() != null) {
                    Utils.hideKeyboard(getParentActivity().getCurrentFocus());
                }
                MainFragment selectCountryFragment = new SelectCountryFragment();
                selectCountryFragment.withAnimation(true, true);
                Bundle args = new Bundle();
                args.putParcelableArrayList("countries", mCountries);
                selectCountryFragment.setArguments(args);
                FragmentManager fm = PhoneFragment.this.getFragmentManager();
                fm.putFragment(args, "phone_fragment", PhoneFragment.this);
                FragmentTransaction ft = fm.beginTransaction();
                ft.addToBackStack(null);
                ft.replace(R.id.rlContainer, selectCountryFragment).commit();
            }
        });
        mPhoneCodeField = (EditText)mRootView.findViewById(R.id.phoneCodeField);
        mPhoneCodeField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = mPhoneCodeField.getText().toString();
                if (text.contains("+")) {
                    text = text.replace("+", "");
                    mPhoneCodeField.setText(text);
                }

                if (text == null || text.length() == 0) {
                    mCountryTextView.setText("Choose a country");
                } else {
                    String country = mCountryNames.get(mCountryCodes.get(Integer.valueOf(text)));
                    if (country != null) {
                        mCountryTextView.setText(country);
                    } else {
                        mCountryTextView.setText("Choose a country");
                    }
                }
                mPhoneCodeField.setSelection(mPhoneCodeField.getText().length());
            }
        });

        mPhoneCodeField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mPhoneField.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mPhoneField = (EditText)mRootView.findViewById(R.id.phoneField);

        mCountryCode = "RU";
        try {
            TelephonyManager telephonyManager = (TelephonyManager)Utils.sContext.getSystemService(Context.TELEPHONY_SERVICE);
            if (telephonyManager != null) {
                mCountryCode = telephonyManager.getSimCountryIso().toUpperCase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mCountryCode == null || mCountryCode.length() == 0) {
            try {
                Locale current = Utils.sContext.getResources().getConfiguration().locale;
                mCountryCode = current.getCountry().toUpperCase();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (mCountryCode == null || mCountryCode.length() == 0) {
            mCountryCode = "RU";
        }

        String countryName = mCountryNames.get(mCountryCode);
        if (countryName == null) {
            countryName = "Russian Federation";
        }

        int phoneCode = mPhoneCodes.get(countryName);
        if (phoneCode == 0) {
            phoneCode = 7;
        }

        mCountryTextView.setText(countryName);
        mPhoneCodeField.setText(String.valueOf(phoneCode));

        mFormatter = PhoneNumberUtil.getInstance().getAsYouTypeFormatter(mCountryCode);

        mPhoneField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (mSelfChange || mStopFormatting) {
                    return;
                }

                if (count > 0 && hasSeparator(s, start, count)) {
                    stopFormatting();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mSelfChange || mStopFormatting) {
                    return;
                }

                if (count > 0 && hasSeparator(s, start, count)) {
                    stopFormatting();
                }
            }

            @Override
            public synchronized void afterTextChanged(Editable s) {
                if (mStopFormatting) {
                    mStopFormatting = !(s.length() == 0);
                    return;
                }
                if (mSelfChange) {
                    return;
                }

                String code = mPhoneCodeField.getText().toString();
                String phone = "+" + code + s.toString();
                String formatted = reformat(phone, Selection.getSelectionEnd(phone));
                if (formatted != null) {
                    int index = formatted.indexOf(" ");
                    mSelfChange = true;
                    if (index != -1) {
                        String resultCode = Utils.stripExceptNumbers(formatted.substring(0, index));
                        if (code.equals(resultCode)) {
                            formatted = formatted.substring(index).trim();
                            s.replace(0, s.length(), formatted, 0, formatted.length());
                        } else {
                            formatted = reformat(s.toString(), Selection.getSelectionEnd(s)).trim();
                            s.replace(0, s.length(), formatted, 0, formatted.length());
                        }
                    } else {
                        formatted = reformat(s, Selection.getSelectionEnd(s));
                        if (formatted == null) {
                            mSelfChange = false;
                            return;
                        }
                        s.replace(0, s.length(), formatted, 0, formatted.length());
                    }
                    if (formatted.equals(s.toString())) {
                        Selection.setSelection(s, s.length());
                    }
                    mSelfChange = false;
                }
            }

            private String reformat(CharSequence s, int cursor) {
                int curIndex = cursor - 1;
                String formatted = null;
                mFormatter.clear();
                char lastNonSeparator = 0;
                boolean hasCursor = false;
                int len = s.length();
                for (int i = 0; i < len; i++) {
                    char c = s.charAt(i);
                    if (PhoneNumberUtils.isNonSeparator(c)) {
                        if (lastNonSeparator != 0) {
                            formatted = getFormattedNumber(lastNonSeparator, hasCursor);
                            hasCursor = false;
                        }
                        lastNonSeparator = c;
                    }
                    if (i == curIndex) {
                        hasCursor = true;
                    }
                }
                if (lastNonSeparator != 0) {
                    formatted = getFormattedNumber(lastNonSeparator, hasCursor);
                }
                return formatted;
            }

            private String getFormattedNumber(char lastNonSeparator, boolean hasCursor) {
                return hasCursor ? mFormatter.inputDigitAndRememberPosition(lastNonSeparator) : mFormatter.inputDigit(lastNonSeparator);
            }

            private void stopFormatting() {
                mStopFormatting = true;
                mFormatter.clear();
            }

            private boolean hasSeparator(final CharSequence s, final int start, final int count) {
                for (int i = start; i < start + count; i++) {
                    char c = s.charAt(i);
                    if (!PhoneNumberUtils.isNonSeparator(c)) {
                        return true;
                    }
                }
                return false;
            }
        });

        Typeface regularTypeface = Utils.getTypeface("regular");
        mCountryTextView.setTypeface(regularTypeface);
        mPhoneCodeField.setTypeface(regularTypeface);
        mPhoneField.setTypeface(regularTypeface);

        Utils.showKeyboard(mPhoneField);
        mPhoneField.requestFocus();
        mPhoneField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    mDoneImageButton.performClick();
                    return true;
                }
                return false;
            }
        });

        return mRootView;
    }

    public void setSelection(int position) {
        Country country = mCountries.get(position);
        if (country != null) {
            mCountryTextView.setText(country.name);
            mPhoneCodeField.setText(String.valueOf(country.phoneCode));
            mCountryCode = country.code;
            mFormatter = PhoneNumberUtil.getInstance().getAsYouTypeFormatter(mCountryCode);
        }
        mPhoneField.requestFocus();
    }

    private ArrayList<Country> getCountries() {
        ArrayList<Country> countries = new ArrayList<>();
        try {
            XmlPullParser xpp = getResources().getXml(R.xml.countries);
            Country country = null;
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        if (xpp.getAttributeCount() != 0) {
                            country = new Country();
                            country.code = xpp.getAttributeValue(0).toUpperCase();
                            country.phoneCode = Integer.valueOf(xpp.getAttributeValue(1));
                            country.name = xpp.getAttributeValue(2);
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if (country != null) {
                            countries.add(country);
                            country = null;
                        }
                        break;
                }
                xpp.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }
        return countries;
    }

    @Override
    public void onDonePressed() {
        String phone = Utils.stripExceptNumbers("" + mPhoneCodeField.getText() + mPhoneField.getText());
        mParams.put("phone", "+" + mPhoneCodeField.getText() + mPhoneField.getText());
        TdApi.SetAuthPhoneNumber authSetPhoneNumber = new TdApi.SetAuthPhoneNumber(phone);
        getParentActivity().showProgress();
        getParentActivity().send(authSetPhoneNumber);
    }

    @Override
    public void onResult(TdApi.TLObject object) {
        if (object.getConstructor() == TdApi.AuthStateWaitCode.CONSTRUCTOR) {
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    getParentActivity().sendParams(mParams);
                    getParentActivity().slidePager(2, true);
                }
            });
        } else if (object.getConstructor() == TdApi.AuthStateWaitName.CONSTRUCTOR) {
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    getParentActivity().sendParams(mParams);
                    getParentActivity().slidePager(1, true);
                }
            });
        } else if (object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
            if (((TdApi.Error)object).text != null) {
                if (((TdApi.Error)object).text.contains("PHONE_NUMBER_INVALID")) {
                    getParentActivity().showAlert(Utils.sContext.getString(R.string.invalid_phone_number_text));
                } else {
                    getParentActivity().showAlert(((TdApi.Error) object).text);
                }
            }
        }
        getParentActivity().hideProgress();
    }

    @Override
    public String getTitle() {
        return getResources().getString(R.string.phone_title);
    }

}
