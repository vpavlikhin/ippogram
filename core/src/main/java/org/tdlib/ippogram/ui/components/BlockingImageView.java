package org.tdlib.ippogram.ui.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.model.ImageVideoWrapper;
import com.bumptech.glide.load.resource.bitmap.BitmapResource;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.Downsampler;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapper;
import com.bumptech.glide.load.resource.gifbitmap.GifBitmapWrapperResource;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.Utils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class BlockingImageView extends ImageView {

    private boolean mBlockLayout = false;
    private boolean mIgnoreBlock = false;

    private int mPhotoWidth = getMeasuredWidth();
    private int mPhotoHeight = getMeasuredHeight();

    private RequestManager mRequestManager;

    public BlockingImageView(Context context) {
        super(context);
        mRequestManager = Glide.with(context);
    }

    public BlockingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mRequestManager = Glide.with(context);
    }

    public BlockingImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mRequestManager = Glide.with(context);
    }

    public int getPhotoWidth() {
        return mPhotoWidth;
    }

    public int getPhotoHeight() {
        return mPhotoHeight;
    }

    @Override
    public void requestLayout() {
        if (!mBlockLayout) {
            super.requestLayout();
        }
    }

    @Override
    public void setImageResource(int resId) {
        if (!mIgnoreBlock) {
            mBlockLayout = true;
        }
        super.setImageResource(resId);
        if (!mIgnoreBlock) {
            mBlockLayout = false;
        }
    }

    @Override
    public void setImageURI(Uri uri) {
        if (!mIgnoreBlock) {
            mBlockLayout = true;
        }
        super.setImageURI(uri);
        if (!mIgnoreBlock) {
            mBlockLayout = false;
        }
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (!mIgnoreBlock) {
            mBlockLayout = true;
        }
        super.setImageDrawable(drawable);
        if (!mIgnoreBlock) {
            mBlockLayout = false;
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        if (!mIgnoreBlock) {
            mBlockLayout = true;
        }
        super.setImageBitmap(bm);
        if (!mIgnoreBlock) {
            mBlockLayout = false;
        }
    }

    public void setIgnoreBlock(boolean ignoreBlock) {
        mIgnoreBlock = ignoreBlock;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (mIgnoreBlock) {
            setMeasuredDimension(mPhotoWidth, mPhotoHeight);
        }
    }

    public void setPhotoThumb(TdApi.PhotoSize thumbPhoto, TdApi.PhotoSize optimalPhoto) {
        mPhotoWidth = (int) (Math.min(Utils.sDisplaySize.x, Utils.sDisplaySize.y) * 0.7f);
        mPhotoHeight = mPhotoWidth + Utils.convertDpToPx(100);
        if (mPhotoWidth > Utils.getPhotoSize()) {
            mPhotoWidth = Utils.getPhotoSize();
        }
        if (mPhotoHeight > Utils.getPhotoSize()) {
            mPhotoHeight = Utils.getPhotoSize();
        }

        if (optimalPhoto != null) {
            if (optimalPhoto.type.equals("i")) {
                BitmapFactory.Options opts = new BitmapFactory.Options();
                opts.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(optimalPhoto.photo.path, opts);
                optimalPhoto.width = opts.outWidth;
                optimalPhoto.height = opts.outHeight;
            }
            float scale = (float) optimalPhoto.width / (float) mPhotoWidth;
            int w = (int) (optimalPhoto.width / scale);
            int h = (int) (optimalPhoto.height / scale);
            if (w == 0) {
                w = Utils.convertDpToPx(100);
            }
            if (h == 0) {
                h = Utils.convertDpToPx(100);
            }
            if (h > mPhotoHeight) {
                float scale2 = h;
                h = mPhotoHeight;
                scale2 /= h;
                w = (int) (w / scale2);
            } else if (h < Utils.convertDpToPx(120)) {
                h = Utils.convertDpToPx(120);
                float hScale = (float) optimalPhoto.height / h;
                //if (optimalPhoto.width / hScale < mPhotoWidth) {
                    w = (int) (optimalPhoto.width / hScale);
                //}
            }
            mPhotoWidth = w;
            mPhotoHeight = h;
            //Log.d(Utils.TAG, "" + mPhotoWidth + " " + mPhotoHeight);
        }
        requestLayout();

        //downloading thumb
        //Glide.clear(this);
        if (thumbPhoto.photo == null || thumbPhoto.photo.id == 0) {
            return;
        }
        if (!ChatsCache.sFiles.containsKey(thumbPhoto.photo.id)) {
            ChatsCache.sFiles.put(thumbPhoto.photo.id, thumbPhoto.photo);
        }
        TdApi.File thumbFile = ChatsCache.getFile(thumbPhoto.photo.id);
        if (thumbFile.path == null || thumbFile.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(thumbFile.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            if (optimalPhoto.photo.path.length() == 0) {
                setImage(thumbFile.path, false, true, false, false);
                //Glide.with(getContext()).load(thumbFile.path).override(mPhotoWidth, mPhotoHeight).transform(new BlurTransformation(getContext(), 5)).into(this);
            }
        }
    }

    public void setVideoThumb(TdApi.PhotoSize thumbPhoto, TdApi.Video video) {
        mPhotoWidth = (int) (Math.min(Utils.sDisplaySize.x, Utils.sDisplaySize.y) * 0.7f);
        mPhotoHeight = mPhotoWidth + Utils.convertDpToPx(100);
        if (mPhotoWidth > Utils.getPhotoSize()) {
            mPhotoWidth = Utils.getPhotoSize();
        }
        if (mPhotoHeight > Utils.getPhotoSize()) {
            mPhotoHeight = Utils.getPhotoSize();
        }

        if (video != null) {
            float scale = (float) video.width / (float) mPhotoWidth;
            int w = (int) (video.width / scale);
            int h = (int) (video.height / scale);
            if (w == 0) {
                w = Utils.convertDpToPx(100);
            }
            if (h == 0) {
                h = Utils.convertDpToPx(100);
            }
            if (h > mPhotoHeight) {
                float scale2 = h;
                h = mPhotoHeight;
                scale2 /= h;
                w = (int) (w / scale2);
            } else if (h < Utils.convertDpToPx(120)) {
                h = Utils.convertDpToPx(120);
                float hScale = (float) video.height / h;
                //if (optimalPhoto.width / hScale < mPhotoWidth) {
                w = (int) (video.width / hScale);
                //}
            }
            mPhotoWidth = w;
            mPhotoHeight = h;
        }
        requestLayout();

        //downloading thumb
        //Glide.clear(this);
        if (thumbPhoto.photo == null || thumbPhoto.photo.id == 0) {
            return;
        }
        if (!ChatsCache.sFiles.containsKey(thumbPhoto.photo.id)) {
            ChatsCache.sFiles.put(thumbPhoto.photo.id, thumbPhoto.photo);
        }
        TdApi.File thumbFile = ChatsCache.getFile(thumbPhoto.photo.id);
        if (thumbFile.path == null || thumbFile.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(thumbFile.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            setImage(thumbFile.path, false, true, false, false);
            //Glide.with(getContext()).load(thumbFile.path).override(mPhotoWidth, mPhotoHeight).transform(new BlurTransformation(getContext(), 5)).into(this);
        }
    }

    public void setStickerThumb(TdApi.PhotoSize thumbPhoto, TdApi.Sticker sticker) {
        mPhotoWidth = sticker.width;
        mPhotoHeight = sticker.height;
        float maxWidth = Utils.sDisplaySize.y * 0.5f;
        float maxHeight = Utils.sDisplaySize.y * 0.4f;
        if (mPhotoWidth == 0) {
            mPhotoHeight = (int) maxHeight;
            mPhotoWidth = mPhotoHeight + Utils.convertDpToPx(100);
        }
        if (mPhotoHeight > maxHeight) {
            mPhotoWidth *= maxHeight / mPhotoHeight;
            mPhotoHeight = (int)maxHeight;
        }
        if (mPhotoWidth > maxWidth) {
            mPhotoHeight *= maxWidth / mPhotoWidth;
            mPhotoWidth = (int) maxWidth;
        }
        requestLayout();

        //Glide.clear(this);
        if (thumbPhoto.photo == null || thumbPhoto.photo.id == 0) {
            return;
        }
        if (!ChatsCache.sFiles.containsKey(thumbPhoto.photo.id)) {
            ChatsCache.sFiles.put(thumbPhoto.photo.id, thumbPhoto.photo);
        }
        TdApi.File thumbFile = ChatsCache.getFile(thumbPhoto.photo.id);
        if (thumbFile.path == null || thumbFile.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(thumbFile.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            if (sticker.sticker.path.length() == 0) {
                setImage(thumbFile.path, false, false, false, false);
                //Glide.with(getContext()).load(thumbFile.path).override(mPhotoWidth, mPhotoHeight).into(this);
            }
        }
    }

    public void setDocumentThumb(TdApi.PhotoSize thumb) {
        //Glide.clear(this);
        if (thumb.photo == null || thumb.photo.id == 0) {
            return;
        }
        if (!ChatsCache.sFiles.containsKey(thumb.photo.id)) {
            ChatsCache.sFiles.put(thumb.photo.id, thumb.photo);
        }
        TdApi.File thumbFile = ChatsCache.getFile(thumb.photo.id);
        if (thumbFile.path == null || thumbFile.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(thumbFile.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            setImage(thumbFile.path, true, true, false, false);
            //Glide.with(getContext()).load(thumbFile.path).transform(new BlurTransformation(getContext(), 5)).into(this);
        }
    }

    public void setProfilePhoto(final TdApi.ProfilePhoto photo, Drawable placeholder) {
        Glide.clear(this);
        if (photo == null || photo.id == 0) {
            setImageDrawable(placeholder);
            return;
        }

        if (!ChatsCache.sFiles.containsKey(photo.small.id)) {
            ChatsCache.sFiles.put(photo.small.id, photo.small);
        }

        if (!ChatsCache.sFiles.containsKey(photo.big.id)) {
            ChatsCache.sFiles.put(photo.big.id, photo.big);
        }

        final TdApi.File smallPhoto = ChatsCache.getFile(photo.small.id);
        if (smallPhoto.path == null || smallPhoto.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(smallPhoto.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            setImage(smallPhoto.path, true, false, false, true);
            //Glide.with(getContext()).load(smallPhoto.path).centerCrop().transform(new CircleTransformation(getContext())).into(this);
        }
    }

    public void setChatPhoto(final TdApi.ChatPhoto photo, Drawable placeholder) {
        Glide.clear(this);
        if (photo == null || photo.small.id == 0) {
            setImageDrawable(placeholder);
            return;
        }

        if (!ChatsCache.sFiles.containsKey(photo.small.id)) {
            ChatsCache.sFiles.put(photo.small.id, photo.small);
        }

        if (!ChatsCache.sFiles.containsKey(photo.big.id)) {
            ChatsCache.sFiles.put(photo.big.id, photo.big);
        }

        final TdApi.File smallPhoto = ChatsCache.getFile(photo.small.id);
        if (smallPhoto.path == null || smallPhoto.path.length() == 0) {
            Utils.sClient.send(new TdApi.DownloadFile(smallPhoto.id), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        } else {
            setImage(smallPhoto.path, true, false, false, true);
            //Glide.with(getContext()).load(smallPhoto.path).centerCrop().transform(new CircleTransformation(getContext())).into(this);
        }
    }

    public void setImage(String path, boolean fixedSize, boolean doBlur, boolean isSticker, boolean isProfile) {
        if (mRequestManager == null) {
            return;
        }

        DrawableRequestBuilder<String> drawableRequestBuilder = mRequestManager.load(path);
        if (isSticker) {
            drawableRequestBuilder = drawableRequestBuilder.decoder(new WebPDecoder(getContext(), DecodeFormat.PREFER_ARGB_8888));
        } /*else {
            drawableRequestBuilder = drawableRequestBuilder
                    .transcoder(new GifBitmapWrapperDrawableTranscoder(new GlideBitmapDrawableTranscoder(getContext())))
                    .decoder(new GifBitmapDecoder(getContext(), DecodeFormat.PREFER_ARGB_8888));
        }*/
        if (!fixedSize) {
            drawableRequestBuilder = drawableRequestBuilder.override(mPhotoWidth, mPhotoHeight);
        }
        if (isProfile) {
            drawableRequestBuilder = drawableRequestBuilder.transform(new CircleTransformation(getContext()));
        }
        if (doBlur) {
            drawableRequestBuilder = drawableRequestBuilder.transform(new BlurTransformation(getContext(), 5));
        }
        drawableRequestBuilder.into(this);
    }

    private static class WebPDecoder implements ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> {

        private static final String ID = "org.tdlib.ippogram.ui.components.BlockingImageView$WebPDecoder";
        private static final String TAG = WebPDecoder.class.getSimpleName();

        private final Downsampler mDownsampler;
        private BitmapPool mBitmapPool;
        private DecodeFormat mDecodeFormat;
        private String mId;

        @SuppressWarnings("unused")
        public WebPDecoder(Context context) {
            this(Glide.get(context).getBitmapPool());
        }

        public WebPDecoder(BitmapPool bitmapPool) {
            this(bitmapPool, DecodeFormat.DEFAULT);
        }

        public WebPDecoder(Context context, DecodeFormat decodeFormat) {
            this(Glide.get(context).getBitmapPool(), decodeFormat);
        }

        public WebPDecoder(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            this(Downsampler.AT_LEAST, bitmapPool, decodeFormat);
        }

        public WebPDecoder(Downsampler downsampler, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            mDownsampler = downsampler;
            mBitmapPool = bitmapPool;
            mDecodeFormat = decodeFormat;
        }

        @Override
        public Resource<GifBitmapWrapper> decode(ImageVideoWrapper source, int width, int height) throws IOException {
            InputStream inputStream = source.getStream();
            if (!inputStream.markSupported()) {
                inputStream = new BufferedInputStream(inputStream);
            }
            Bitmap bitmap = Utils.decodeSticker(Utils.streamToByteArray(inputStream));
            BitmapResource bitmapResource = BitmapResource.obtain(bitmap, mBitmapPool);
            return new GifBitmapWrapperResource(new GifBitmapWrapper(bitmapResource, null));
        }

        @Override
        public String getId() {
            if (mId == null) {
                mId = ID + mDownsampler.getId() + mDecodeFormat.name();
            }
            return mId;
        }
    }

    private static class GifBitmapDecoder implements ResourceDecoder<ImageVideoWrapper, GifBitmapWrapper> {

        private static final String ID = "org.tdlib.ippogram.ui.components.BlockingImageView$BitmapDecoder";
        private static final String TAG = WebPDecoder.class.getSimpleName();

        private final Downsampler mDownsampler;
        private BitmapPool mBitmapPool;
        private DecodeFormat mDecodeFormat;
        private String mId;

        @SuppressWarnings("unused")
        public GifBitmapDecoder(Context context) {
            this(Glide.get(context).getBitmapPool());
        }

        public GifBitmapDecoder(BitmapPool bitmapPool) {
            this(bitmapPool, DecodeFormat.DEFAULT);
        }

        public GifBitmapDecoder(Context context, DecodeFormat decodeFormat) {
            this(Glide.get(context).getBitmapPool(), decodeFormat);
        }

        public GifBitmapDecoder(BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            this(Downsampler.AT_LEAST, bitmapPool, decodeFormat);
        }

        public GifBitmapDecoder(Downsampler downsampler, BitmapPool bitmapPool, DecodeFormat decodeFormat) {
            mDownsampler = downsampler;
            mBitmapPool = bitmapPool;
            mDecodeFormat = decodeFormat;
        }

        @Override
        public Resource<GifBitmapWrapper> decode(ImageVideoWrapper source, int width, int height) throws IOException {
            InputStream inputStream = source.getStream();
            if (!inputStream.markSupported()) {
                inputStream = new BufferedInputStream(inputStream);
            }
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            BitmapResource bitmapResource = BitmapResource.obtain(bitmap, mBitmapPool);
            return new GifBitmapWrapperResource(new GifBitmapWrapper(bitmapResource, null));
        }

        @Override
        public String getId() {
            if (mId == null) {
                mId = ID + mDownsampler.getId() + mDecodeFormat.name();
            }
            return mId;
        }
    }

    private static class CircleTransformation extends BitmapTransformation {

        public CircleTransformation(Context context) {
            super(context);
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            if (toTransform == null) {
                return null;
            }

            int size = Math.min(toTransform.getWidth(), toTransform.getHeight());
            int x = (toTransform.getWidth() - size) / 2;
            int y = (toTransform.getHeight() - size) / 2;

            // TODO this could be acquired from the pool too
            Bitmap squared = Bitmap.createBitmap(toTransform, x, y, size, size);
            Bitmap result = pool.get(size, size, Bitmap.Config.ARGB_8888);
            if (result == null) {
                result = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(new BitmapShader(squared, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            return result;
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }

    private static class BlurTransformation extends BitmapTransformation {

        private static int MAX_RADIUS = 25;
        private static int DEFAULT_DOWN_SAMPLING = 1;

        private int mRadius;
        private int mSampling;

        public BlurTransformation(Context context) {
            this(context, MAX_RADIUS, DEFAULT_DOWN_SAMPLING);
        }

        public BlurTransformation(Context context, int radius) {
            this(context, radius, DEFAULT_DOWN_SAMPLING);
        }

        public BlurTransformation(Context context, int radius, int sampling) {
            super(context);
            mRadius = radius;
            mSampling = sampling;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            int width = toTransform.getWidth();
            int height = toTransform.getHeight();
            int scaledWidth = width / mSampling;
            int scaledHeight = height / mSampling;

            Bitmap bitmap = pool.get(scaledWidth, scaledHeight, Bitmap.Config.ARGB_8888);
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(scaledWidth, scaledHeight, Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            canvas.scale(1 / (float) mSampling, 1 / (float) mSampling);
            Paint paint = new Paint();
            paint.setFlags(Paint.FILTER_BITMAP_FLAG);
            canvas.drawBitmap(toTransform, 0, 0, paint);

            return doBlur(bitmap, mRadius, true);
        }

        public Bitmap doBlur(Bitmap sentBitmap, int radius, boolean canReuseInBitmap) {
            // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

            Bitmap bitmap;
            if (canReuseInBitmap) {
                bitmap = sentBitmap;
            } else {
                bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
            }

            if (radius < 1) {
                return null;
            }

            int w = bitmap.getWidth();
            int h = bitmap.getHeight();

            int[] pix = new int[w * h];
            bitmap.getPixels(pix, 0, w, 0, 0, w, h);

            int wm = w - 1;
            int hm = h - 1;
            int wh = w * h;
            int div = radius + radius + 1;

            int r[] = new int[wh];
            int g[] = new int[wh];
            int b[] = new int[wh];
            int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
            int vmin[] = new int[Math.max(w, h)];

            int divsum = (div + 1) >> 1;
            divsum *= divsum;
            int dv[] = new int[256 * divsum];
            for (i = 0; i < 256 * divsum; i++) {
                dv[i] = (i / divsum);
            }

            yw = yi = 0;

            int[][] stack = new int[div][3];
            int stackpointer;
            int stackstart;
            int[] sir;
            int rbs;
            int r1 = radius + 1;
            int routsum, goutsum, boutsum;
            int rinsum, ginsum, binsum;

            for (y = 0; y < h; y++) {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                for (i = -radius; i <= radius; i++) {
                    p = pix[yi + Math.min(wm, Math.max(i, 0))];
                    sir = stack[i + radius];
                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);
                    rbs = r1 - Math.abs(i);
                    rsum += sir[0] * rbs;
                    gsum += sir[1] * rbs;
                    bsum += sir[2] * rbs;
                    if (i > 0) {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    } else {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }
                }
                stackpointer = radius;

                for (x = 0; x < w; x++) {

                    r[yi] = dv[rsum];
                    g[yi] = dv[gsum];
                    b[yi] = dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (y == 0) {
                        vmin[x] = Math.min(x + radius + 1, wm);
                    }
                    p = pix[yw + vmin[x]];

                    sir[0] = (p & 0xff0000) >> 16;
                    sir[1] = (p & 0x00ff00) >> 8;
                    sir[2] = (p & 0x0000ff);

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[(stackpointer) % div];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi++;
                }
                yw += w;
            }
            for (x = 0; x < w; x++) {
                rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
                yp = -radius * w;
                for (i = -radius; i <= radius; i++) {
                    yi = Math.max(0, yp) + x;

                    sir = stack[i + radius];

                    sir[0] = r[yi];
                    sir[1] = g[yi];
                    sir[2] = b[yi];

                    rbs = r1 - Math.abs(i);

                    rsum += r[yi] * rbs;
                    gsum += g[yi] * rbs;
                    bsum += b[yi] * rbs;

                    if (i > 0) {
                        rinsum += sir[0];
                        ginsum += sir[1];
                        binsum += sir[2];
                    } else {
                        routsum += sir[0];
                        goutsum += sir[1];
                        boutsum += sir[2];
                    }

                    if (i < hm) {
                        yp += w;
                    }
                }
                yi = x;
                stackpointer = radius;
                for (y = 0; y < h; y++) {
                    // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                    pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                    rsum -= routsum;
                    gsum -= goutsum;
                    bsum -= boutsum;

                    stackstart = stackpointer - radius + div;
                    sir = stack[stackstart % div];

                    routsum -= sir[0];
                    goutsum -= sir[1];
                    boutsum -= sir[2];

                    if (x == 0) {
                        vmin[y] = Math.min(y + r1, hm) * w;
                    }
                    p = x + vmin[y];

                    sir[0] = r[p];
                    sir[1] = g[p];
                    sir[2] = b[p];

                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];

                    rsum += rinsum;
                    gsum += ginsum;
                    bsum += binsum;

                    stackpointer = (stackpointer + 1) % div;
                    sir = stack[stackpointer];

                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];

                    rinsum -= sir[0];
                    ginsum -= sir[1];
                    binsum -= sir[2];

                    yi += w;
                }
            }

            bitmap.setPixels(pix, 0, w, 0, 0, w, h);

            return bitmap;
        }

        @Override
        public String getId() {
            return getClass().getName();
        }
    }
}
