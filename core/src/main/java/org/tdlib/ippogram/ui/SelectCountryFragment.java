package org.tdlib.ippogram.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import static org.tdlib.ippogram.ui.adapters.SelectCountryAdapter.Country;
import org.tdlib.ippogram.ui.adapters.SelectCountryAdapter;
import org.tdlib.ippogram.ui.components.PinnedHeaderListView;
import org.tdlib.ippogram.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SelectCountryFragment extends MainFragment {

    private PinnedHeaderListView mCountryListView;
    private SelectCountryAdapter mCountryAdapter;
    private ArrayList<Country> mCountries;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_select_country, container, false);

        final Bundle args = getArguments();
        if (args != null) {
            mCountries = args.getParcelableArrayList("countries");
        }
        Collections.sort(mCountries, new Comparator<Country>() {
            @Override
            public int compare(Country lhs, Country rhs) {
                char lhsFirstLetter = TextUtils.isEmpty(lhs.name) ? ' ' : lhs.name.charAt(0);
                char rhsFirstLetter = TextUtils.isEmpty(rhs.name) ? ' ' : rhs.name.charAt(0);
                int firstLetterComparison = Character.toUpperCase(lhsFirstLetter) - Character.toUpperCase(rhsFirstLetter);
                if (firstLetterComparison == 0) {
                    return lhs.name.compareTo(rhs.name);
                }
                return firstLetterComparison;
            }
        });

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onBackPressed();
            }
        });
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(R.string.select_country_title);

        mCountryListView = (PinnedHeaderListView)mRootView.findViewById(R.id.countryListView);
        mCountryListView.setPinnedHeaderView(inflater.inflate(R.layout.letter_section, mCountryListView, false));

        mCountryAdapter = new SelectCountryAdapter(mCountries);
        mCountryAdapter.setPinnedHeaderBackgroundColor(ContextCompat.getColor(Utils.sContext, android.R.color.transparent));
        mCountryAdapter.setPinnedHeaderTextColor(ContextCompat.getColor(Utils.sContext, R.color.gray));

        mCountryListView.setAdapter(mCountryAdapter);
        mCountryListView.setOnScrollListener(mCountryAdapter);
        mCountryListView.setEnableHeaderTransparencyChanges(true);
        mCountryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment phoneFragment = SelectCountryFragment.this.getFragmentManager().getFragment(args, "phone_fragment");
                ((PhoneFragment) phoneFragment).setSelection(position - mCountryAdapter.getSectionForPosition(position));
                getParentActivity().onBackPressed();
            }
        });

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCountryAdapter != null) {
            mCountryAdapter.notifyDataSetChanged();
        }
    }
}


