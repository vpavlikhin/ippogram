package org.tdlib.ippogram.ui;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.ContactsCache;
import org.tdlib.ippogram.Utils;

import java.util.HashMap;

public class PasswordFragment extends SlideFragment {

    private TextView mPasswordTextView;
    private EditText mPasswordField;
    private TextView mForgotPasswordTextView;

    private boolean mRecovery = false;

    public static PasswordFragment newInstance(boolean recovery) {
        Bundle args = new Bundle();
        args.putBoolean("recovery_password", recovery);
        PasswordFragment fragment = new PasswordFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_password, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onBackPressed();
            }
        });
        mHeaderTextView = (TextView)mToolbar.findViewById(R.id.headerTextView);
        mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        mHeaderTextView.setText(R.string.password_title);

        mDoneImageButton = (ImageButton)mToolbar.findViewById(R.id.doneImageButton);
        mDoneImageButton.setVisibility(View.VISIBLE);
        mDoneImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getParentActivity().onNextAction();
            }
        });

        Typeface typeface = Utils.getTypeface("regular");

        mPasswordTextView = (TextView)mRootView.findViewById(R.id.passwordTextView);
        mPasswordTextView.setTypeface(typeface);

        mPasswordField = (EditText) mRootView.findViewById(R.id.passwordField);

        mPasswordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    if (getParentActivity() != null) {
                        mDoneImageButton.performClick();
                    }
                    return true;
                }
                return false;
            }
        });

        mForgotPasswordTextView = (TextView) mRootView.findViewById(R.id.forgotPasswordTextView);
        mForgotPasswordTextView.setTypeface(typeface);

        return mRootView;
    }

    @Override
    public void setParams(HashMap<String, Object> params) {
        mRecovery = getArguments().getBoolean("recovery_password");

        String hint = (String)params.get("password_hint");
        final boolean hasRecoveryEmail = (Boolean)params.get("has_email");

        mPasswordTextView.setText(mRecovery ? R.string.password_recovery_text : R.string.password_text);

        if (mRecovery) {
            mPasswordField.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
        } else {
            mPasswordField.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        mPasswordField.setTypeface(Utils.getTypeface("regular"));
        mPasswordField.setHint(mRecovery ? Utils.sContext.getString(R.string.code_field_hint) : hint);
        mPasswordField.setText("");
        mPasswordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mRecovery && mPasswordField.length() == 6) {
                    mDoneImageButton.performClick();
                }
                mPasswordField.setSelection(mPasswordField.getText().length());
            }
        });

        if (mRecovery) {
            mForgotPasswordTextView.setVisibility(View.GONE);
        } else {
            mForgotPasswordTextView.setVisibility(View.VISIBLE);
            mForgotPasswordTextView.setText(R.string.forgot_password);
            mForgotPasswordTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (hasRecoveryEmail) {
                        getParentActivity().showProgress();
                        TdApi.RequestAuthPasswordRecovery requestAuthPasswordRecovery = new TdApi.RequestAuthPasswordRecovery();
                        getParentActivity().send(requestAuthPasswordRecovery);
                    } else {
                        Utils.hideKeyboard(mPasswordField);
                        getParentActivity().showAlert(Utils.sContext.getString(R.string.no_email_recovery_text));
                    }
                }
            });
        }

        Utils.showKeyboard(mPasswordField);
        mPasswordField.requestFocus();
    }

    @Override
    public void onDonePressed() {
        getParentActivity().showProgress();
        if (mRecovery) {
            TdApi.RecoverAuthPassword recoverAuthPassword = new TdApi.RecoverAuthPassword(mPasswordField.getText().toString());
            getParentActivity().send(recoverAuthPassword);
        } else {
            TdApi.CheckAuthPassword checkAuthPassword = new TdApi.CheckAuthPassword(mPasswordField.getText().toString());
            getParentActivity().send(checkAuthPassword);
        }
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onResult(TdApi.TLObject object) {
        getParentActivity().hideProgress();
        if (object.getConstructor() == TdApi.AuthStateOk.CONSTRUCTOR) {
            ChatsCache.reset();
            ContactsCache.getInstance().reset();
            getParentActivity().finishActivity();
        } else if (object.getConstructor() == TdApi.AuthStateWaitPassword.CONSTRUCTOR) {
            final TdApi.AuthStateWaitPassword state = (TdApi.AuthStateWaitPassword)object;
            final HashMap<String, Object> params = new HashMap<>();
            params.put("password_hint", state.hint.length() != 0 ? state.hint : Utils.sContext.getString(R.string.password_title));
            params.put("has_email", state.hasRecovery);
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                    builder.setMessage(String.format(Utils.sContext.getString(R.string.has_email_recovery_text), state.emailUnconfirmedPattern));
                    builder.setPositiveButton(Utils.sContext.getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getParentActivity().sendParams(params);
                            getParentActivity().slidePager(4, true);
                        }
                    });
                    AlertDialog alertDialog = builder.show();
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setCancelable(false);

                    Button okButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    okButton.setTypeface(Utils.getTypeface("medium"));
                    okButton.setTextColor(ContextCompat.getColor(Utils.sContext, R.color.blue));
                }
            });
        } else if (object.getConstructor() == TdApi.Error.CONSTRUCTOR) {
            Utils.runOnUIThread(new Runnable() {
                @Override
                public void run() {
                    mPasswordField.setText("");
                }
            });
            if (((TdApi.Error)object).text != null) {
               if (((TdApi.Error)object).text.contains("PASSWORD_HASH_INVALID")) {
                   getParentActivity().showAlert(Utils.sContext.getString(R.string.invalid_password_text));
               } else if (((TdApi.Error)object).text.contains("CODE_INVALID")) {
                   getParentActivity().showAlert(Utils.sContext.getString(R.string.invalid_code_text));
               } else {
                   getParentActivity().showAlert(((TdApi.Error) object).text);
               }
            }
        }
    }

    @Override
    public String getTitle() {
        return getResources().getString(R.string.password_title);
    }
}
