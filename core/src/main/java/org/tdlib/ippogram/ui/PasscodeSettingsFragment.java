package org.tdlib.ippogram.ui;

import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.tdlib.ippogram.bottomsheet.BottomSheet;

import org.tdlib.ippogram.Utils;

public class PasscodeSettingsFragment extends MainFragment {

    private ListView mSettingsPasscodeListView;
    private SettingsPasscodeAdapter mSettingsPasscodeAdapter;

    private boolean mExistHash;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_passcode_settings, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_ab_back);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(PasscodeSettingsFragment.this);
            }
        });

        mHeaderTextView = (TextView)mRootView.findViewById(R.id.headerTextView);
        mHeaderTextView.setText(Utils.sContext.getString(R.string.settings_passcode));
        if (mHeaderTextView != null) {
            mHeaderTextView.setTypeface(Utils.getTypeface("medium"));
        }

        mSettingsPasscodeListView = (ListView)mRootView.findViewById(R.id.settingsPasscodeListView);
        mSettingsPasscodeListView.setVerticalScrollBarEnabled(false);
        mSettingsPasscodeListView.setAdapter(mSettingsPasscodeAdapter = new SettingsPasscodeAdapter());
        mSettingsPasscodeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    if (!mExistHash) {
                        EnterPasscodeFragment fragment = EnterPasscodeFragment.newInstance(Utils.sPasscodeType, false);
                        ((MainActivity) getParentActivity()).createFragment(fragment, "choose_passcode", true, true, true, false);
                    } else {
                        Utils.sPasscodeHash = "";
                        Utils.sPasscodeType = 0;
                        mExistHash = false;
                        Utils.savePasscode(getParentActivity());
                        if (mSettingsPasscodeListView != null) {
                            mSettingsPasscodeListView.invalidateViews();
                        }
                    }
                } else if (position == 2) {
                    EnterPasscodeFragment fragment = EnterPasscodeFragment.newInstance(Utils.sPasscodeType, false);
                    ((MainActivity) getParentActivity()).createFragment(fragment, "choose_passcode", true, true, true, false);
                } else if (position == 4) {
                    if (getParentActivity() == null) {
                        return;
                    }
                    BottomSheet.Builder builder = new BottomSheet.Builder(getParentActivity());
                    builder.title(R.string.auto_lock_passcode);
                    CharSequence [] items = new CharSequence[] {
                            Utils.sContext.getText(R.string.auto_lock_in_minute),
                            Utils.sContext.getText(R.string.auto_lock_in_minutes),
                            Utils.sContext.getText(R.string.auto_lock_in_hour),
                            Utils.sContext.getText(R.string.auto_lock_in_hours),
                            Utils.sContext.getText(R.string.auto_lock_disabled)
                    };
                    for (int i = 0; i < items.length; i++) {
                        builder.sheet(i, items[i]);
                    }
                    builder.listener(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    Utils.sAutoLock = 60;
                                    break;
                                case 1:
                                    Utils.sAutoLock = 60 * 5;
                                    break;
                                case 2:
                                    Utils.sAutoLock = 60 * 60;
                                    break;
                                case 3:
                                    Utils.sAutoLock = 60 * 60 * 5;
                                    break;
                                case 4:
                                    Utils.sAutoLock = 0;
                                    break;
                                default:
                                    Utils.sAutoLock = 60 * 60;
                            }
                            mSettingsPasscodeListView.invalidateViews();
                            Utils.savePasscode(getParentActivity());
                        }
                    });
                    BottomSheet menuSheet = builder.build();
                    menuSheet.setCanceledOnTouchOutside(true);
                    menuSheet.setCanceledOnSwipeDown(false);
                    menuSheet.show();
                }
            }
        });
        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Utils.runOnUIThread(new Runnable() {
            @Override
            public void run() {
                ((MainActivity) getParentActivity()).destroyFragment("unlock_passcode");
            }
        }, 600L);

        getFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                mExistHash = Utils.sPasscodeHash.length() > 0;
                if (mSettingsPasscodeListView != null) {
                    mSettingsPasscodeListView.invalidateViews();
                }
            }
        });
        mSettingsPasscodeAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onBackPressed() {
        return super.onBackPressed();
    }

    private class SettingsPasscodeAdapter extends BaseAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }

        @Override
        public boolean isEnabled(int position) {
            return position == 0 || (mExistHash && position == 2) || position == 4;
        }

        @Override
        public int getCount() {
            if (mExistHash) {
                return 6;
            }
            return 4;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            int type = getItemViewType(position);
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            if (type == 0) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.settings_list_item, parent, false);
                }
                TextView primaryTextView = (TextView)convertView.findViewById(R.id.primaryTextView);
                TextView valueTextView = (TextView)convertView.findViewById(R.id.valueTextView);
                SwitchCompat stateSwitch = (SwitchCompat)convertView.findViewById(R.id.stateSwitch);
                if (position == 0) {
                    primaryTextView.setText(Utils.sContext.getString(R.string.settings_passcode));
                    valueTextView.setVisibility(View.GONE);
                    stateSwitch.setVisibility(View.VISIBLE);
                    if (mExistHash) {
                        stateSwitch.setChecked(true);
                    } else {
                        stateSwitch.setChecked(false);
                    }
                } else if (position == 2) {
                    primaryTextView.setTextColor(!mExistHash ? 0xffa6a6a6 : 0xff222222);
                    primaryTextView.setText(Utils.sContext.getString(R.string.change_passcode));
                    valueTextView.setVisibility(View.GONE);
                    stateSwitch.setVisibility(View.GONE);
                } else if (position == 4) {
                    primaryTextView.setText(Utils.sContext.getString(R.string.auto_lock_passcode));
                    valueTextView.setVisibility(View.VISIBLE);
                    switch (Utils.sAutoLock) {
                        case 0:
                            valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_disabled));
                            break;
                        case 60:
                            valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_in_minute));
                            break;
                        case 60 * 5:
                            valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_in_minutes));
                            break;
                        case 60 * 60:
                            valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_in_hour));
                            break;
                        case 60 * 60 * 5:
                            valueTextView.setText(Utils.sContext.getString(R.string.auto_lock_in_hours));
                            break;
                    }
                    stateSwitch.setVisibility(View.GONE);
                }
            } else if (type == 1) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.passcode_divider, parent, false);
                }
            } else if (type == 2) {
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.space_list_item, parent, false);
                }
                View topShadowView = convertView.findViewById(R.id.topShadowView);
                TextView infoTextView = (TextView)convertView.findViewById(R.id.infoTextView);
                if (position == 3) {
                    infoTextView.setVisibility(View.VISIBLE);
                    infoTextView.setText(Utils.sContext.getString(R.string.change_passcode_info));
                    if (mExistHash) {
                        topShadowView.setVisibility(View.VISIBLE);
                    } else {
                        topShadowView.setVisibility(View.GONE);
                    }
                } else if (position == 5) {
                    infoTextView.setVisibility(View.VISIBLE);
                    infoTextView.setText(Utils.sContext.getString(R.string.auto_lock_passcode_info));
                    topShadowView.setVisibility(View.GONE);
                }
            }
            return convertView;
        }

        @Override
        public int getItemViewType(int position) {
            switch (position) {
                case 0:
                case 2:
                case 4:
                    return 0;
                case 1:
                    return 1;
                case 3:
                case 5:
                    return 2;
                default:
                    return 0;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }
}
