package org.tdlib.ippogram.ui;

import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.drinkless.td.libcore.telegram.Client;
import org.drinkless.td.libcore.telegram.TdApi;
import org.tdlib.ippogram.ChatsCache;
import org.tdlib.ippogram.UpdatesHandler;
import org.tdlib.ippogram.Utils;
import org.tdlib.ippogram.ui.components.PinchImageView;
import org.tdlib.ippogram.ui.components.ProgressView;
import org.tdlib.ippogram.ui.components.ToolbarMenuLayout;

import java.io.File;
import java.util.ArrayList;

public class PhotoViewFragment extends MainFragment implements UpdatesHandler.DownloadUpdatesDelegate {

    private static final int SAVE_TO_GALLERY_MENU_ITEM = 0;
    private static final int DELETE_MENU_ITEM = 1;

    private ToolbarMenuLayout mMoreMenuButton;
    private PinchImageView mPinchImageView;
    private View mBackgroundView;
    private ProgressView mDownloadProgressView;

    private Uri mPhotoUri;
    private long mChatId;
    private int messageId;

    private int mBigPhotoId;

    @Override
    public void onPreCreate() {
        String path = getArguments().getString("path", "");
        mPhotoUri = Uri.fromFile(new File(path));
        mChatId = getArguments().getLong("chat_id", 0);
        messageId = getArguments().getInt("message_id", 0);
        mBigPhotoId = getArguments().getInt("big_photo_id", 0);
        ChatsCache.addDelegate(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_photo_view, container, false);

        mToolbar = (Toolbar)mRootView.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getParentActivity()).destroyFragment(PhotoViewFragment.this);
            }
        });

        mMoreMenuButton = (ToolbarMenuLayout)mToolbar.findViewById(R.id.moreMenuButton);
        mMoreMenuButton.setParentToolbar(mToolbar);
        mMoreMenuButton.setImageResource(R.drawable.ic_more_photo);

        mMoreMenuButton.addItem(SAVE_TO_GALLERY_MENU_ITEM, Utils.sContext.getString(R.string.action_save_to_gallery));
        if (messageId != 0) {
            mMoreMenuButton.addItem(DELETE_MENU_ITEM, Utils.sContext.getString(R.string.action_delete));
        }
        mMoreMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreMenuButton.createMenu();
            }
        });
        mMoreMenuButton.setOnItemClickListener(new ToolbarMenuLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view) {
                switch ((Integer) view.getTag()) {
                    case SAVE_TO_GALLERY_MENU_ITEM:
                        Utils.addImageToGallery(mPhotoUri.getPath());
                        break;
                    case DELETE_MENU_ITEM:
                        if (getParentActivity() == null) {
                            return;
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(getParentActivity());
                        builder.setMessage(R.string.delete_photo_message);
                        builder.setPositiveButton(R.string.delete_button, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deletePhoto();
                            }
                        });
                        builder.setNegativeButton(R.string.cancel_button, null);
                        showAlertDialog(builder);
                        break;
                }
            }
        });

        mBackgroundView = mRootView.findViewById(R.id.backgroundView);
        mDownloadProgressView = (ProgressView)mRootView.findViewById(R.id.downloadProgressView);
        mDownloadProgressView.setSpinSpeed(0.333f);
        mDownloadProgressView.setRimColor(0x00000000);
        mDownloadProgressView.setBarColor(ContextCompat.getColor(Utils.sContext, R.color.white));
        /*mDownloadProgressView.setCallback(new ProgressView.ProgressCallback() {
            @Override
            public void onProgressUpdate(float progress) {
                Log.d(Utils.TAG, "" + progress);
            }
        });*/

        mPinchImageView = (PinchImageView)mRootView.findViewById(R.id.pinchImageView);
        mPinchImageView.setImageURI(mPhotoUri);
        if (mBigPhotoId != 0) {
            mBackgroundView.setVisibility(View.VISIBLE);
            mDownloadProgressView.setVisibility(View.VISIBLE);
            Utils.sClient.send(new TdApi.DownloadFile(mBigPhotoId), new Client.ResultHandler() {
                @Override
                public void onResult(TdApi.TLObject object) {

                }
            });
        }
        return mRootView;
    }

    private void deletePhoto() {
        ArrayList<Integer> messageIds = new ArrayList<>();
        messageIds.add(messageId);
        Utils.sClient.send(new TdApi.DeleteMessages(mChatId, new int[]{messageId}), new Client.ResultHandler() {
            @Override
            public void onResult(TdApi.TLObject object) {

            }
        });
        ChatsCache.deleteMessages(mChatId, messageIds);
        ((MainActivity) getParentActivity()).destroyFragment(PhotoViewFragment.this);

        ChatsCache.updateInterface();
    }

    @Override
    public void onResume() {
        super.onResume();
        //updateInterface();
    }

    @Override
    public void updateInterface() {

    }

    @Override
    public void updateDownloadProgress(int fileId, int ready, int size) {
        if (fileId == mBigPhotoId) {
            mDownloadProgressView.setProgress((float) ready / (float) size);
        }
    }

    @Override
    public void updateDownloadedFile(int fileId) {
        if (fileId == mBigPhotoId) {
            mBackgroundView.setVisibility(View.GONE);
            mDownloadProgressView.setVisibility(View.GONE);
            TdApi.File file = ChatsCache.getFile(fileId);
            if (file == null) {
                return;
            }
            mPhotoUri = Uri.fromFile(new File(file.path));
            mPinchImageView.setImageURI(mPhotoUri);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ChatsCache.removeDelegate(this);
    }
}
